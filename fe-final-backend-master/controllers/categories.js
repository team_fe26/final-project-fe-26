const Category = require("../models/Category");
const queryCreator = require("../commonHelpers/queryCreator");
const _ = require("lodash");

exports.addCategory = (req, res, next) => {
  Category .findOne({ name: req.body.name }).then(category => {
    if (category ) {
      return res
        .status(400)
        .json({ message: `Category with name "${category .name}" already exists` });
    } else {
      const initialQuery = _.cloneDeep(req.body);
      const newCategory  = new Category(queryCreator(initialQuery));

      newCategory
        .save()
        .then(category => res.json(category))
        .catch(err =>
          res.status(400).json({
            message: `Error happened on server: "${err}" `
          })
        );
    }
  });
};

exports.updateCategory = (req, res, next) => {
  Category.findOne({ _id: req.params.id })
    .then(category => {
      if (!category) {
        return res
          .status(400)
          .json({ message: `Category with _id "${req.params.id}" is not found.` });
      } else {
        const initialQuery = _.cloneDeep(req.body);
        const updatedCategory = queryCreator(initialQuery);

        Category.findOneAndUpdate(
          { _id: req.params.id },
          { $set: updatedCategory },
          { new: true }
        )
          .then(category => res.json(category))
          .catch(err =>
            res.status(400).json({
              message: `Error happened on server: "${err}" `
            })
          );
      }
    })
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};

exports.deleteCategory = (req, res, next) => {
  Category.findOne({ _id: req.params.id }).then(async category => {
    if (!category) {
      return res
        .status(400)
        .json({ message: `Category with _id "${req.params.id}" is not found.` });
    } else {
      const categoryToDelete = await Category.findOne({ _id: req.params.id });

      Category.deleteOne({ _id: req.params.id })
        .then(deletedCount =>
          res.status(200).json({
            message: `Category witn name "${categoryToDelete.name}" is successfully deletes from DB `
          })
        )
        .catch(err =>
          res.status(400).json({
            message: `Error happened on server: "${err}" `
          })
        );
    }
  });
};

exports.getCategories = (req, res, next) => {
  Category.find()
    .then(categorys => res.json(categorys))
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};
