const express = require("express");
const router = express.Router();
const passport = require("passport"); // multer for parsing multipart form data (files)

//Import controllers
const {
  addCategory,
  updateCategory,
  deleteCategory,
  getCategories
} = require("../controllers/categories");

// @route   POST /colors
// @desc    Create new color
// @access  Private
router.post(
  "/",
  passport.authenticate("jwt-admin", { session: false }),
  addCategory
);

// @route   PUT /colors
// @desc    Update existing color
// @access  Private
router.put(
  "/:id",
  passport.authenticate("jwt-admin", { session: false }),
  updateCategory
);

// @route   DELETE /colors/:id
// @desc    DELETE existing color
// @access  Private
router.delete(
  "/:id",
  passport.authenticate("jwt-admin", { session: false }),
  deleteCategory
);

// @route   GET /colors
// @desc    GET existing colors
// @access  Public
router.get("/", getCategories);

module.exports = router;
