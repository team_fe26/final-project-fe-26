const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CommentSchema = new Schema(
  {
    customer: {
      type: Schema.Types.ObjectId,
      ref: "customers",
      required: true,
    },
    product: {
      type: Schema.Types.ObjectId,
      ref: "products",
      required: true,
    },
    content: {
      type: String,
      required: true,
    }, 
    rating: {
      type: Number,
      required: true,
      min: 0,
      max: 5,
    },
    date:{
      type: String,
    }
  },
  { strict: false }
);

module.exports = Comment = mongoose.model("comments", CommentSchema);
