const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProductSchema = new Schema(
  {
    itemNo: {
      type: String,
      required: true
    },
    enabled: {
      type: Boolean,
      required: true,
      default: true
    },
    name: {
      type: String,
      required: true
    },
    currentPrice: {
      type: Number,
      required: true
    },
    previousPrice: {
      type: Number
    },
    categories: {
      type: String,
      required: true
    },
    imageUrls: [
      {
        type: String,
        required: true
      }
    ],
    quantity: {
      type: Number,
      required: true,
      default: 0
    },
    size: {
      type: String
    },
    // productUrl: {
    //   type: String
    // },
    // brand: {
    //   type: String
    // },
    // manufacturer: {
    //   type: String
    // },
    // manufacturerCountry: {
    //   type: String
    // },
    shortDescription: {
type: String
    },
    longDescription: {
      type: String
          },
    seller: {
      type: String
    },
    carePlant: {
type: String
    },
    waterPlant:{
      type: String
    },
    feedPlant: {
      type: String
    },
    // date: {
    //   type: Date,
    //   default: Date.now
    // }
  },
  { strict: false }
);

ProductSchema.index({ "$**": "text" });

module.exports = Product = mongoose.model("products", ProductSchema);
