const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SliderSchema = new Schema(
  {
    title: String,
    name: String,
    imageUrl: {
      type: String,
      required: true
    },
    description: String,
    product: {
      type: Schema.Types.ObjectId,
      ref: "products"
    },
    date: {
      type: Date,
      default: Date.now
    }
  },
  { strict: false }
);

module.exports = Slider = mongoose.model("slides", SliderSchema, "slides");
