const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcryptjs");

const AddressSchema = new Schema({
  country: { type: String, required: true },
  street: { type: String, required: true },
  town: { type: String, required: true },
  zip: { type: String, required: true },
  appartment: { type: String },
  state: { type: String },
  phoneNumber: { type: String, required: true },
  email: { type: String, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  main: {type: Boolean, required: true, default: false}
});

const CustomerSchema = new Schema(
  {
    customerNo: { type: String, required: true },
    username: { type: String, required: true },
    googleId: { type: String },
    facebookId: { type: String },
    email: { type: String, required: true },
    new: {type: Boolean},
    couponCode: {type: String},
    password: { type: String },
    isConfirmed: { type: Boolean, default: false, required: true },
    firstName: { type: String },
    lastName: { type: String },
    phone: { type: String },
    avatarUrl: { type: String },
    enabled: { type: Boolean, default: true },
    date: { type: Date, default: Date.now },
    addresses: [AddressSchema],
  },
  { strict: false }
);

CustomerSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

const Customer = mongoose.model("customers", CustomerSchema);

module.exports = Customer;
