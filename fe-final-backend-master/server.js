const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const cors = require("cors");
const path = require("path");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");
const crypto = require("crypto");
const session = require("express-session");
const MongoStore = require("connect-mongo");
const jwt = require("jsonwebtoken");
const multer = require("multer");
const fs = require("fs");
const cron = require("node-cron");
const fetch = require("node-fetch");
const paypal = require("paypal-rest-sdk");
require("dotenv").config();
const emailValidator = require("email-validator");
const dns = require("dns");
const { initializeApp } = require("firebase/app");
const {
  getStorage,
  ref,
  uploadBytes,
  getDownloadURL,
} = require("firebase/storage");
const globalConfigs = require("./routes/globalConfigs");
const customers = require("./routes/customers");
const catalog = require("./routes/catalog");
const products = require("./routes/products");
const categories = require("./routes/categories");
const sizes = require("./routes/sizes");
const filters = require("./routes/filters");
const subscribers = require("./routes/subscribers");
const cart = require("./routes/cart");
const orders = require("./routes/orders");
const links = require("./routes/links");
const pages = require("./routes/pages");
const slides = require("./routes/slides");
const wishlist = require("./routes/wishlist");
const comments = require("./routes/comments");
const shippingMethods = require("./routes/shippingMethods");
const paymentMethods = require("./routes/paymentMethods");
const partners = require("./routes/partners");
const app = express();
const allowedOrigins = [
  "https://greenshop-fe-26.netlify.app",
  "http://localhost:5173",
];
app.use(
  cors({
    origin: function (origin, callback) {
      if (!origin || allowedOrigins.indexOf(origin) !== -1) {
        callback(null, true);
      } else {
        callback(new Error("Not allowed by CORS"));
      }
    },
    credentials: true,
  })
);
app.use(passport.initialize());
mongoose
  .connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("MongoDB Connected"))
  .catch((err) => console.error("MongoDB Connection Error:", err));

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
require("./services/passport")(passport);

// Use Routes
app.use("/api/configs", globalConfigs);
app.use("/api/customers", customers);
app.use("/api/catalog", catalog);
app.use("/api/products", products);
app.use("/api/categories", categories);
app.use("/api/sizes", sizes);
app.use("/api/filters", filters);
app.use("/api/subscribers", subscribers);
app.use("/api/cart", cart);
app.use("/api/orders", orders);
app.use("/api/links", links);
app.use("/api/pages", pages);
app.use("/api/slides", slides);
app.use("/api/wishlist", wishlist);
app.use("/api/comments", comments);
app.use("/api/shipping-methods", shippingMethods);
app.use("/api/payment-methods", paymentMethods);
app.use("/api/partners", partners);

const Customer = require("./models/Customer");
const Cart = require("./models/Cart");
const Wishlist = require("./models/Wishlist");
const Category = require("./models/Category")
const Size = require ("./models/Size")
const Product = require("./models/Product");
const Comment = require("./models/Comment");
const { zip } = require("lodash");
const firebaseConfig = {
  apiKey: process.env.FIREBASE_APIKEY,
  authDomain: process.env.FIREBASE_AUTHDOMAIN,
  projectId: process.env.FIREBASE_PROJECTID,
  storageBucket: process.env.FIREBASE_STORAGEBUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDERID,
  appId: process.env.FIREBASE_APPID,
  measurementId: process.env.FIREBASE_MEASUREMENTID,
};

const firebaseApp = initializeApp(firebaseConfig);
const storage = getStorage(firebaseApp);

const upload = multer({ storage: multer.memoryStorage() });


const isValidEmail = (email) => {
  if (!emailValidator.validate(email)) {
    return Promise.resolve(false);
  }
  const domain = email.split("@")[1];
  return new Promise((resolve, reject) => {
    dns.resolveMx(domain, (err, addresses) => {
      if (err || addresses.length === 0) {
        resolve(false);
      } else {
        resolve(true);
      }
    });
  });
};

function generateToken(customerNo) {
  return jwt.sign({ customerNo }, process.env.CODE_TOKEN, {});
}
const verifyToken = (req, res, next) => {
  const token = req.headers.authorization.split(" ")[1];

  if (!token) {
    return res.status(401).json({ error: "Not authorized" });
  }

  jwt.verify(token, process.env.CODE_TOKEN, (err, decoded) => {
    if (err) {
      return res.status(401).json({ error: "Invalid token" });
    }
    req.customerId = decoded.customerNo;
    next();
  });
};

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "greenshopfe26@gmail.com",
    pass: process.env.GMAIL_PASSWORD,
  },
});

function generateCouponCode(length = 6) {
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let coupon = "";
  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    coupon += characters[randomIndex];
  }
  return coupon;
}
async function sendEmail(to, subject, text) {
  if (!emailValidator.validate(to)) {
    throw new Error("Invalid email address");
  }

  try {
    const info = await transporter.sendMail({
      from: "greenshopfe26@gmail.com",
      to,
      subject,
      text,
    });
    console.log("Email sent:", info.response);
  } catch (error) {
    console.error("Error sending email:", error);
    throw error;
  }
}

paypal.configure({
  mode: "sandbox",
  client_id: process.env.PAYPAL_CLIENT_ID,
  client_secret: process.env.PAYPAL_CLIENT_SECRET,
});

app.use(express.json());
function generateToken(customerNo) {
  return jwt.sign({ customerNo }, process.env.CODE_TOKEN, {});
}

app.use("/api/uploads", express.static(path.join(__dirname, "uploads")));
passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL:
        "https://greenshop-server.onrender.com/api/auth/google/callback",
      passReqToCallback: true,
    },
    async (req, accessToken, refreshToken, profile, done) => {
      try {
        let customer = await Customer.findOne({
          email: profile.emails[0].value,
        });

        if (customer && customer.isConfirmed) {
          delete customer.new;
          if (!customer.googleId) {
            customer.googleId = profile.id;
            await customer.save();
          }
          const token = generateToken(customer._id);
          return done(null, { token, customer });
        } else {
          const randomNumber =
            Math.floor(Math.random() * (9999999999 - 1000000000 + 1)) +
            1000000000;
          customer = new Customer({
            firstName: profile.name.givenName || "",
            customerNo: randomNumber,
            isConfirmed: true,
            new: true,
            lastName: profile.name.familyName || "",
            username:
              (
                profile.name.givenName + profile.name.familyName
              ).toLowerCase() || "",
            email: profile.emails[0].value,
            googleId: profile.id,
          });
          const savedCustomer = await customer.save();
          await new Wishlist({ customerId: savedCustomer._id }).save();
          await new Cart({ customerId: savedCustomer._id }).save();
          const token = generateToken(savedCustomer._id);
          const couponCode = generateCouponCode();
          customer.couponCode = couponCode;
          try {
            await sendEmail(
              customer.email,
              "Congratulations!!!",
              `You got a coupon for 20% off any amount of goods, just use this code: ${couponCode}`
            );
          } catch (emailError) {
            return res.status(400).json({ error: "Invalid email address" });
          }
          return done(null, { token, customer: savedCustomer, wishlist, cart });
        }
      } catch (error) {
        console.error("Error in GoogleStrategy:", error);
        return done(error, null);
      }
    }
  )
);

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  Customer.findById(id, (err, user) => {
    done(err, user);
  });
});
app.use((req, res, next) => {
  console.log(`Received request from origin: ${req.get("origin")}`);
  next();
});

app.get(
  "/api/auth/google",
  passport.authenticate("google", { scope: ["profile", "email"] })
);

app.get(
  "/api/auth/google/callback",
  passport.authenticate("google", {
    session: false,
    failureRedirect: "/",
  }),
  (req, res) => {
    if (req.user && req.user.token) {
      res.redirect(
        `https://greenshop-fe-26.netlify.app/token-handler?token=${req.user.token}`
      );
    } else {
      res.redirect("/");
    }
  }
);
app.post("/api/register", async (req, res) => {
  const { username, email, password } = req.body;
  try {
    const existingUser = await Customer.findOne({ username });
    if (existingUser && existingUser.isConfirmed) {
      return res.status(400).json({ error: "Username already taken*" });
    }

    const hashedPassword = await bcrypt.hash(password, 10);
    const randomNumber =
      Math.floor(Math.random() * (9999999999 - 1000000000 + 1)) + 1000000000;

    let customer;

    const existingEmail = await Customer.findOne({ email });
    if (existingEmail) {
      if (existingEmail.isConfirmed) {
        return res.status(400).json({ error: "Email already taken*" });
      } else {
        existingEmail.username = username;
        existingEmail.password = hashedPassword;
        customer = existingEmail;
      }
    } else {
      customer = new Customer({
        username,
        password: hashedPassword,
        email,
        customerNo: randomNumber,
        isConfirmed: false,
      });
    }

    const savedCustomer = await customer.save();

    const confirmToken = generateToken(savedCustomer._id);
    const confirmationUrl = `https://greenshop-fe-26.netlify.app/token-handler-registration?token=${confirmToken}`;

    try {
      await sendEmail(
        email,
        "Please confirm your email",
        `Click on the following link to confirm your registration: ${confirmationUrl}. If this is not you, just ignore this message.`
      );
    } catch (emailError) {
      return res.status(400).json({ error: "Invalid email address" });
    }

    res.status(200).json({
      message:
        "Registration successful. Please check your email to confirm your registration.",
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Error during registration. Try again" });
  }
});
app.get("/api/confirm-email", verifyToken, async (req, res) => {
  try {
    const customerId = req.customerId;
    const customer = await Customer.findById(customerId);

    if (!customer) {
      return res.status(400).json({ error: "Invalid token or user not found" });
    }

    if (customer.isConfirmed === true) {
      return res.status(400).json({ message: "Email already confirmed" });
    }
    customer.isConfirmed = true;
    const couponCode = generateCouponCode();
    customer.couponCode = couponCode;
    try {
      await sendEmail(
        customer.email,
        "Congratulations!!!",
        `You got a coupon for 20% off any amount of goods, just use this code: ${couponCode}`
      );
    } catch (emailError) {
      return res.status(400).json({ error: "Invalid email address" });
    }
    await customer.save();
    await new Wishlist({ customerId: customer._id }).save();
    await new Cart({ customerId: customer._id }).save();
    return res
      .status(200)
      .json({ message: "Successfully registration", cart, wishlist, customer });
  } catch (error) {
    console.error("Error confirming email:", error);
    res.status(500).json({ error: "Error confirming email. Try again" });
  }
});
app.post("/api/login", async (req, res) => {
  const { email, password } = req.body;
  try {
    const customer = await Customer.findOne({ email: email });
    if (!customer) {
      return res.status(400).json({ error: "Email not found" });
    }
    if (customer.isConfirmed === false) {
      return res.status(400).json({ error: "Customer not confirmed" });
    }

    const isPasswordValid = await bcrypt.compare(password, customer.password);
    if (!isPasswordValid) {
      return res.status(400).json({ error: "Invalid password" });
    }
    let cart = await Cart.findOne({ customerId: customer._id });
    let wishlist = await Wishlist.findOne({ customerId: customer._id });
    const token = generateToken(customer._id);
    res.status(200).json({
      token,
      userId: customer._id,
      wishlist,
      cart,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Error during login. Try again" });
  }
});
app.post("/api/logout", verifyToken, (req, res) => {
  try {
    res.status(200).json({ message: "User logged out successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Error during logout" });
  }
});

app.post("/api/delete-account", verifyToken, async (req, res) => {
  try {
    const customerId = req.customerId;
    await Customer.findByIdAndDelete(customerId);
    await Wishlist.deleteMany({ customerId: customerId });
    await Cart.deleteMany({ customerId: customerId });

    res.status(200).json({ message: "Account deleted successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Error deleting account" });
  }
});
app.post("/api/forgot-password", async (req, res) => {
  const { email } = req.body;
  try {
    const customer = await Customer.findOne({ email: email });
    if (!customer) {
      return res
        .status(400)
        .json({ error: "Sorry, but you are not registered with this email" });
    }
    function generateSixDigitCode() {
      const min = 100000;
      const max = 999999;
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    const code = generateSixDigitCode();
    await sendEmail(email, "Reset Password", ` Your reset code is: ${code}`);
    res.status(200).json({ code });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Error during login. Try again" });
  }
});
app.post("/api/reset-password", async (req, res) => {
  const { email, password } = req.body;
  try {
    const customer = await Customer.findOne({ email });
    if (!customer) {
      return res
        .status(400)
        .json({ error: "Sorry, but you are not registered with this email" });
    }
    let isPasswordNotValid = false;
    if (customer.password) {
      isPasswordNotValid = await bcrypt.compare(password, customer.password);
    }
    if (isPasswordNotValid) {
      return res
        .status(400)
        .json({ error: "Sorry, but you are already using this password" });
    }

    const hashedPassword = await bcrypt.hash(password, 10);
    customer.password = hashedPassword;
    await customer.save();

    res.status(200).json({ message: "Password was successfully changed" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Error during reset password. Try again" });
  }
});
app.post("/api/product", async (req, res) => {
  const { _id } = req.body;
  try {
    const product = await Product.findOne({ _id });
    if (!product) {
      return res.status(404).json({ error: "Product not found" });
    }
    res.status(200).json(product);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Error found product" });
  }
});
app.get("/api/products-all", async (req, res) => {
  try {
    const products = await Product.find();
    if (products.length === 0) {
      return res.status(404).json({ error: "Products not found" });
    }
    res.status(200).json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Error found products" });
  }
});
app.get("/api/products-random", async (req, res) => {
  try {
    const randomProducts = await Product.aggregate([
      { $sample: { size: 12 } }
    ]);

    if (randomProducts.length === 0) {
      return res.status(404).json({ error: "No random products found" });
    }

    res.status(200).json(randomProducts);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Error fetching random products" });
  }
});

app.post("/api/product-reject", async (req, res) => {
  const { _id } = req.body;

  if (!_id) {
    return res.status(400).json({ error: "Product ID is required" });
  }

  try {
    const product = await Product.findById(_id);
    if (!product) {
      return res.status(404).json({ error: "Product not found" });
    }
    if (product.quantity > 0) {
      product.quantity -= 1;
      await product.save();

      return res
        .status(200)
        .json({ message: "Product quantity updated", product });
    } else {
      return res.status(400).json({ error: "Insufficient product quantity" });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Error updating product quantity" });
  }
});

app.post("/api/product-recover", async (req, res) => {
  const { _id } = req.body;

  if (!_id) {
    return res.status(400).json({ error: "Product ID is required" });
  }

  try {
    const product = await Product.findById(_id);
    if (!product) {
      return res.status(404).json({ error: "Product not found" });
    }

    product.quantity += 1;
    await product.save();

    return res
      .status(200)
      .json({ message: "Product quantity updated", product });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Error updating product quantity" });
  }
});
app.post("/api/product-recover-one", async (req, res) => {
  const { _id, quantityCart } = req.body;

  if (!_id) {
    return res.status(400).json({ error: "Product ID is required" });
  }

  try {
    const product = await Product.findById(_id);
    if (!product) {
      return res.status(404).json({ error: "Product not found" });
    }

    product.quantity += quantityCart;
    await product.save();

    return res
      .status(200)
      .json({ message: "Product quantity updated", product });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Error updating product quantity" });
  }
});
app.post("/api/product-reject-one", async (req, res) => {
  const { _id } = req.body;

  if (!_id) {
    return res.status(400).json({ error: "Product ID is required" });
  }

  try {
    const product = await Product.findById(_id);
    if (!product) {
      return res.status(404).json({ error: "Product not found" });
    }
    if (product.quantity >= 1) {
      product.quantity -= 1;
      await product.save();

      return res
        .status(200)
        .json({ message: "Product quantity updated", product });
    } else {
      return res.status(400).json({ error: "Insufficient product quantity" });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Error updating product quantity" });
  }
});
app.post("/api/customer/address/add", verifyToken, async (req, res) => {
  try {
    const customerId = req.customerId;
    const {
      firstName,
      lastName,
      phoneNumber,
      email,
      country,
      state,
      street,
      zip,
      appartment,
      town,
      main,
    } = req.body;
    const customer = await Customer.findById(customerId);

    if (!customer) {
      return res.status(404).json({ error: "Customer not found" });
    }
    const addressExists = customer.addresses.some(
      (address) =>
        address.firstName === firstName &&
        address.lastName === lastName &&
        address.phoneNumber === phoneNumber &&
        address.email === email &&
        address.country === country &&
        address.state === state &&
        address.street === street &&
        address.zip === zip &&
        address.appartment === appartment &&
        address.town === town
    );

    if (email && !(await isValidEmail(email))) {
      return res
        .status(400)
        .json({ error: "Invalid or non-existent email address" });
    }

    if (addressExists) {
      return res
        .status(400)
        .json({ error: "Identical address already exists!" });
    }

    if (main) {
      customer.addresses.forEach((address) => {
        address.main = false;
      });
    }

    if (customer.addresses.length >= 3) {
      return res
        .status(400)
        .json({ error: "A customer can have a maximum of 3 addresses." });
    }
    const newAddress = {
      firstName,
      lastName,
      email,
      phoneNumber,
      street,
      town,
      country,
      zip,
      main,
    };

    if (appartment) {
      newAddress.appartment = appartment;
    }

    if (state) {
      newAddress.state = state;
    }
    customer.addresses.push(newAddress);
    await customer.save();
    res.status(200).json({ message: "Address of client was added" });
  } catch (error) {
    console.error("Error adding address:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

app.post("/api/customer/address/main", verifyToken, async (req, res) => {
  try {
    const customerId = req.customerId;
    const { zip, main } = req.body;
    const customer = await Customer.findById(customerId);
    if (!customer) {
      return res.status(404).json({ error: "Customer not found" });
    }
    if (main) {
      customer.addresses.forEach((address) => {
        if (address.zip.toString() === zip) {
          address.main = true;
        } else {
          address.main = false;
        }
      });
    }
    await customer.save();
    res.status(200).json({ message: "Address of client was update", customer });
  } catch (error) {
    console.error("Error adding address:", error);
    res.status(500).json({ error: "Interval rerver error" });
  }
});
app.post("/api/customer/address/remove", verifyToken, async (req, res) => {
  try {
    const customerId = req.customerId;
    const { address } = req.body;

    const customer = await Customer.findById(customerId);
    if (!customer) {
      return res.status(404).json({ error: "Customer not found" });
    }

    const addressIndex = customer.addresses.findIndex((ADDR) => {
      return (
        ADDR.street === address.street &&
        ADDR.town === address.town &&
        (!address.state || ADDR.state === address.state) &&
        (!address.appartment || ADDR.appartment === address.appartment) &&
        ADDR.zip === address.zip &&
        ADDR.country === address.country &&
        ADDR.firstName === address.firstName &&
        ADDR.lastName === address.lastName &&
        ADDR.phoneNumber === address.phoneNumber &&
        ADDR.email === address.email
      );
    });

    if (addressIndex === -1) {
      return res.status(404).json({ message: "Address not found" });
    }

    customer.addresses.splice(addressIndex, 1);
    await customer.save();

    res.status(200).json({ message: "Address of client was removed" });
  } catch (error) {
    console.error("Error removing address:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
app.post(
  "/api/customer/settings",
  upload.single("file"),
  verifyToken,
  async (req, res) => {
    try {
      const customerId = req.customerId;
      const { firstName, lastName, username, email, phoneNumber } = req.body;
      const file = req.file;

      const customer = await Customer.findById(customerId);
      if (!customer) {
        return res.status(404).json({ error: "Customer not found" });
      }
      if (phoneNumber) customer.phone = phoneNumber;
      if (firstName) customer.firstName = firstName;
      if (lastName) customer.lastName = lastName;
      if (username) customer.username = username;
      if (email) {
        const isValid = await isValidEmail(email);
        if (isValid) {
          customer.email = email;
        } else {
          return res
            .status(400)
            .json({ error: "Invalid or non-existent email address" });
        }
      }
      if (file) {
        const fileName = `${Date.now()}-${file.originalname}`;
        const storageRef = ref(storage, `uploads/${fileName}`);
        try {
          await uploadBytes(storageRef, file.buffer);
          console.log(`File uploaded successfully: ${fileName}`);
        } catch (uploadError) {
          console.error("Error uploading file:", uploadError);
          return res.status(500).json({ error: "Error uploading file" });
        }
        let fileUrl;
        try {
          fileUrl = await getDownloadURL(storageRef);
        } catch (urlError) {
          console.error("Error getting download URL:", urlError);
          return res.status(500).json({ error: "Error getting file URL" });
        }
        if (customer.avatarUrl) {
          const oldFileRef = ref(storage, customer.avatarUrl);

          try {
            await deleteObject(oldFileRef);
            console.log("Old file deleted successfully");
          } catch (deleteError) {
            console.error("Error deleting old file:", deleteError);
          }
        }

        customer.avatarUrl = fileUrl;
      }

      await customer.save();
      res.status(200).json({
        message: "User data updated successfully",
        avatar: customer.avatarUrl,
      });
    } catch (error) {
      console.error("Error updating data:", error);
      res.status(500).json({ error: "Internal server error" });
    }
  }
);

app.post("/api/customer/change-password", verifyToken, async (req, res) => {
  try {
    const customerId = req.customerId;
    const { currentPassword, newPassword } = req.body;
    const customer = await Customer.findById(customerId);
    if (!customer) {
      return res.status(404).json({ error: "Customer not found" });
    }

    if (customer.password) {
      const isPasswordValid = await bcrypt.compare(
        currentPassword,
        customer.password
      );
      if (!isPasswordValid) {
        console.log("Invalid current password");
        return res.status(400).json({ error: "Invalid current password" });
      }
    }

    const hashedPassword = await bcrypt.hash(newPassword, 10);
    customer.password = hashedPassword;
    await customer.save();
    res.status(200).json({ message: "Password updated successfully" });
  } catch (error) {
    console.error("Error updating password:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
app.get('/api/comments-user', verifyToken, async (req, res) => {
  const { page = 1, limit = 10 } = req.query;
  const token = req.headers.authorization.split(' ')[1];

  try {
    const customerId = req.customerId;
    
    const customer = await Customer.findById(customerId);
    if (!customer) {
      return res.status(404).json({ error: "Customer not found" });
    }
    const comments = await Comment.find({ customer: customerId })
      .skip((page - 1) * limit)
      .limit(parseInt(limit));

    const totalComments = await Comment.countDocuments({ customer: customerId });
    
    const totalPages = Math.ceil(totalComments / limit);

    res.json({
      message: "Comments retrieved successfully",
      comments,
      totalPages
    });
  } catch (error) {
    console.error("Error fetching comments:", error);
    res.status(500).json({ error: "Failed to retrieve comments" });
  }
});

app.post("/api/comment/add", verifyToken, async (req, res) => {
  try {
    const customerId = req.customerId;
    const { productId, content, rating } = req.body;
    const customer = await Customer.findById(customerId);
    if (!customer) {
      return res.status(404).json({ error: "Didn't find customer" });
    }
    let now = new Date();
    let year = now.getFullYear();
    let month = String(now.getMonth() + 1).padStart(2, "0");
    let day = String(now.getDate()).padStart(2, "0");
    let hours = String(now.getHours()).padStart(2, "0");
    let minutes = String(now.getMinutes()).padStart(2, "0");
    let formattedDate = `${year}-${month}-${day} ${hours}:${minutes}`;
    const newComment = new Comment({
      customer: customerId,
      product: productId,
      content: content,
      rating: rating,
      date: formattedDate,
    });
    await newComment.save();
    res.status(200).json({
      newComment,
      customer: { avatar: customer.avatarUrl, username: customer.username },
    });
  } catch (error) {
    console.error("Error adding comment:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

app.post("/api/comment/delete", verifyToken, async (req, res) => {
  try {
    const customerId = req.customerId;
    const { _id } = req.body;
    const customer = await Customer.findById(customerId);
    if (!customer) {
      return res.status(404).json({ error: "Didn't find customer" });
    }
    await Comment.findByIdAndDelete({ _id: _id });
    res.status(200).json({ message: "Comment deleted successfully" });
  } catch (error) {
    console.error("Error deleted comment:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
app.post("/api/user", verifyToken, async (req, res) => {
  try {
    const customerId = req.customerId;
    const customer = await Customer.findById(customerId);

    if (!customer) {
      return res.status(404).json({ error: "Customer not found" });
    }

    res.status(200).json({ message: "User data", customer });
  } catch (error) {
    console.error("Error finding user:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
app.post("/api/create-payment", (req, res) => {
  const create_payment_json = {
    intent: "sale",
    payer: {
      payment_method: "paypal",
    },
    redirect_urls: {
      return_url: "https://greenshop-fe-26.netlify.app/checkout",
      cancel_url: "https://greenshop-fe-26.netlify.app/checkout",
    },
    transactions: [
      {
        item_list: {
          items: [
            {
              name: "item",
              sku: "item",
              price: "10.00",
              currency: "USD",
              quantity: 1,
            },
          ],
        },
        amount: {
          currency: "USD",
          total: "10.00",
        },
        description: "This is the payment description.",
      },
    ],
  };

  paypal.payment.create(create_payment_json, (error, payment) => {
    if (error) {
      console.error(error);
      res.status(500).send("Error creating payment");
    } else {
      res.json(payment);
    }
  });
});
app.post("/api/wishlist/update", verifyToken, async (req, res) => {
  try {
    const { wishlist } = req.body;
    const customerId = req.customerId;
    const customer = await Customer.findById(customerId);

    if (!customer) {
      return res.status(404).json({ error: "Customer not found" });
    }

    let wishlistRecord = await Wishlist.findOne({ customerId: customer._id });
    wishlistRecord.products = wishlist;
    await wishlistRecord.save();
    res.status(200).json({ message: "Wishlist was successfully updated" });
  } catch (error) {
    console.error("Error updating wishlist:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
app.post("/api/cart/update", verifyToken, async (req, res) => {
  try {
    const { cart } = req.body;
    const customerId = req.customerId;
    const customer = await Customer.findById(customerId);

    if (!customer) {
      return res.status(404).json({ error: "Customer not found" });
    }

    let cartRecord = await Cart.findOne({ customerId: customer._id });

    cartRecord.products = cart;
    await cartRecord.save();
    res.status(200).json({ message: "Cart was successfully updated" });
  } catch (error) {
    console.error("Error updating cart:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
app.get("/api/wishlist/cart/google", verifyToken, async (req, res) => {
  try {
    const customerId = req.customerId;
    const customer = await Customer.findById(customerId);

    if (!customer) {
      return res.status(404).json({ error: "Customer not found" });
    }
    let cart = await Cart.findOne({ customerId: customer._id });
    let wishlist = await Wishlist.findOne({ customerId: customer._id });
    res
      .status(200)
      .json({ message: "User data", cart, wishlist, new: customer.new });
  } catch (error) {
    console.error("Error finding user:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
app.post("/api/cart/qty", verifyToken, async (req, res) => {
  try {
    const customerId = req.customerId;
    const { id, delta } = req.body;
    const customer = await Customer.findById(customerId);
    if (!customer) {
      return res.status(404).json({ error: "Customer not found" });
    }
    let cart = await Cart.findOne({ customerId: customer._id });
    if (!cart) {
      return res.status(404).json({ error: "Cart not found" });
    }
    const productIndex = cart.products.findIndex((p) => p.product.equals(id));
    if (productIndex === -1) {
      return res.status(404).json({ error: "Product not found in cart" });
    }
    cart.products[productIndex].quantityCart += delta;
    if (cart.products[productIndex].quantityCart < 1) {
      cart.products[productIndex].quantityCart = 1;
    }
    await cart.save();
    res.json(cart);
  } catch (error) {
    res
      .status(500)
      .json({ error: "An error occurred while updating the cart" });
  }
});
app.post("/api/check/email", async (req, res) => {
  try {
    const { email } = req.body;
    if (!email) {
      return res.status(400).json({ error: "Email is required" });
    }
    const isValid = await isValidEmail(email);
    if (!isValid) {
      return res.status(400).json({ valid: false });
    }
    res.status(200).json({ valid: true });
  } catch (error) {
    console.error("Error finding email:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
app.post("/api/verify/coupon", async (req, res) => {
  try {
    const { coupon } = req.body;
    const customer = await Customer.findOne({ couponCode: coupon });
    if (!customer) {
      return res.status(400).json({ valid: false });
    }
    res.status(200).json({ valid: true, discount: 20 });
  } catch (error) {
    console.error("Error finding coupon:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
app.post("/api/send/order", async (req, res) => {
  try {
    const { products, address, total, coupon } = req.body;
console.log(coupon)
    if (coupon) {
      const customer = await Customer.findOne({ couponCode: coupon });

      if (customer) {
        customer.couponCode = null;
        await customer.save();
      } else {
        return res.status(400).json({ error: "Invalid or expired coupon" });
      }
    }
    const productList = products
      .map((product) => `${product.name} = ${product.quantityCart}`)
      .join(", ");

    await sendEmail(
      address.email,
      "Purchase successful",
      `
      Your products: ${productList},
      Shipping address: ${address.country}, ${address.state}, ${address.town}, ${address.street}, ${address.apartment}, ${address.zip},
      Total: ${total.toFixed(2)}
      `
    );
    res.status(200).json({ message: "Order processed successfully" });
  } catch (error) {
    console.error("Error sending order:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
const PORT = process.env.PORT || 3000;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
