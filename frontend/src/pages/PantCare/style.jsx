import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  Wrapper: {
    padding: 0,
    "@media (max-width: 1200px)": {
      marginTop: "20px",
    },
  },
  BreadcrumbsWrapper: {
    position: "relative",
    padding: 0,
    "@media (max-width: 1200px)": {
      padding: "0 25px 0 25px",
    },
  },
  IconContainer: {
    display: "flex",
    width: "100%",
    alignItems: "center",
    "@media (min-width: 768px)": {
      display: "none",
    },
  },
  PageTitle: {
    width: "85%",
    textAlign: "center",
  },
  LinkCurentPage: {
    fontWeight: 700,
  },
  BackButton: {
    width: "35px",
    height: "35px",
    background: "#f0f0f0",
  },
  Link: {
    textDecoration: "none",
    color: "inherit",
    "&:hover": {
      textDecoration: "underline",
    },
  },
  WrapperAccordion: {
    marginTop: "25px",
    padding: 0,
    "@media (max-width: 1200px)": {
      padding: "0 25px 0 25px",
    },
  },
  AccordionTitle: {
    color: "#46a358",
    fontWeight: 700,
  },
  CircleIcon: {
    color: "#46a358",
    width: "55px",
    height: "15px",
  },
});
