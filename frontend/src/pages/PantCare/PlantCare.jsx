
import { 
    Container, 
    Accordion, 
    AccordionSummary, 
    AccordionDetails, 
    Typography, 
    Box, 
    IconButton, 
    Breadcrumbs,
    List, 
    ListItem, 
    ListItemText,
    ListItemIcon, 
} from '@mui/material';
import CircleIcon from '@mui/icons-material/Circle';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Styles } from "./style";
import { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { ArrowBack } from "@mui/icons-material";
const PlantCare = () => {
    const classes = Styles();
    const navigate = useNavigate();
    const [isMobile, setIsMobile] = useState(window.innerWidth < 768);
    useEffect(() => {
        const handleResize = () => {
            setIsMobile(window.innerWidth < 768);
        };

        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    }, []);



    const careTips = [
        {
            title: 'Proper Lighting',
            details: `Choose the right spot. Different plants require different levels of light. Determine whether your plant prefers full sun, partial shade, or low light, and place it accordingly.`,
            points: [
                'Use sheer curtains to diffuse intense sunlight.',
                'Rotate your plants regularly to ensure even light exposure.',
                'Consider using grow lights during darker months to supplement natural light.'
            ]
        },
        {
            title: 'Watering',
            details: `Establish a consistent watering schedule. Some plants need daily watering, while others may only require water once a week.`,
            points: [
                'Use room-temperature, filtered, or rainwater for best results.',
                'Water thoroughly, allowing excess water to drain out to prevent salt buildup.',
                'Ensure pots have drainage holes to avoid waterlogged soil.'
            ]
        },
        {
            title: 'Humidity',
            details: `Many tropical plants thrive in high humidity environments. To increase humidity, use a humidifier, place plants on a tray filled with water and pebbles, or mist the leaves regularly.`,
            points: [
                'Group plants together to create a microclimate with higher humidity.',
                'Monitor humidity levels, especially during winter when indoor air can be dry.',
                'Avoid placing plants near heat sources that can dry out the air.'
            ]
        },
        {
            title: 'Feeding and Fertilizing',
            details: `Regularly feed your plants with appropriate fertilizers during the growing season.`,
            points: [
                'Use a balanced, water-soluble fertilizer for most houseplants.',
                'Consider organic fertilizers like compost or worm castings to enhance soil health.',
                'Follow the recommended dosage to avoid fertilizer burn.'
            ]
        },
        {
            title: 'Repotting',
            details: `Repot your plant into a larger container if it has outgrown its current pot or if the soil has become compacted and depleted of nutrients.`,
            points: [
                'Choose a pot that is 1-2 inches larger in diameter than the current one.',
                'Use a fresh, well-draining potting mix appropriate for your plant type.',
                'After repotting, water thoroughly and place the plant in a suitable location.'
            ]
        },
        {
            title: 'Pest Control',
            details: `Regularly inspect your plants for common pests such as spider mites, aphids, and mealybugs.`,
            points: [
                'Use natural pest control methods like neem oil or insecticidal soap.',
                'Isolate affected plants from others until the problem is resolved.',
                'Maintain good air circulation and avoid overwatering to prevent pest issues.'
            ]
        },
        {
            title: 'Pruning',
            details: `Trim off dead, damaged, or yellowing leaves to improve the appearance of your plant and prevent disease spread.`,
            points: [
                'Use clean, sharp pruning shears to avoid damaging the plant.',
                'Prune flowering plants to encourage new blooms.',
                'For bonsai, regular pruning is essential to maintain their desired shape.'
            ]
        },
        {
            title: 'Winter Care',
            details: `During the winter months, most plants enter a period of dormancy, requiring less water and no fertilization.`,
            points: [
                'Reduce watering frequency and ensure the soil dries out slightly between waterings.',
                'Keep plants away from cold drafts, radiators, and heaters.',
                'Consider using grow lights to provide necessary light levels during shorter days.'
            ]
        }
    ];


    return (
        <Container className={classes.Wrapper}>
            <Container className={classes.BreadcrumbsWrapper}>
                {isMobile ? (
                    <Box className={classes.IconContainer}>
                        <IconButton
                            onClick={() => {
                                navigate(-1);
                            }}
                            className={classes.BackButton}
                        >
                            <ArrowBack style={{ color: "#000" }} />
                        </IconButton>
                        <Typography  className={classes.PageTitle} color="textPrimary">Plant Care</Typography>

                    </Box>) : (
                    <Breadcrumbs aria-label="breadcrumb" style={{ margin: "115px auto 0 auto", maxWidth: "1200px" }}>
                        <Link className={classes.Link} to="/">
                            Home
                        </Link>
                        <Typography className={classes.LinkCurentPage} color="textPrimary">Plant Care</Typography>
                    </Breadcrumbs>
                )}
            </Container>
            <Container className={classes.WrapperAccordion}>
                {careTips.map((tip, index) => (
                    <Accordion key={index}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.AccordionTitle} variant="h6">{tip.title}</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography variant="body1" gutterBottom>
                                {tip.details}
                            </Typography>
                            <List>
                                {tip.points.map((point, idx) => (
                                    <ListItem key={idx}>
                                         <ListItemIcon>
                                            <CircleIcon className={classes.CircleIcon} />
                                        </ListItemIcon>
                                        <ListItemText primary={point} />
                                    </ListItem>
                                ))}
                            </List>
                        </AccordionDetails>
                    </Accordion>
                ))}
            </Container>
        </Container>
    );
};

export default PlantCare;
