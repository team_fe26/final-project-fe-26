import { styled } from '@mui/material/styles';
import { Paper, IconButton, Button, Typography, TextField, Box, Divider } from '@mui/material';
import { createUseStyles } from "react-jss";

export const ContainerWrapper = styled('div')({
  margin: "115px 24px 0 24px"

});

export const HeaderContainer = styled('div')({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  padding: '10px 0',
});

export const CartTotalsTitle = styled(Typography)({
  fontWeight: 'bold',
});

export const CouponApplyTitle = styled(Typography)({
  paddingTop: '5px',
});

export const StyledDivider = styled(Divider)({
  width: '100%',
  margin: '5px 0',
  color: '#46A35880'
});

export const PaperStyled = styled(Paper)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  padding: theme.spacing(1),
  maxWidth: '783px',
  backgroundColor: 'transparent',
  border: 'none',
  boxShadow: 'none',
  overflowY: 'auto',
  maxHeight: 'calc(100vh - 200px)',
  [theme.breakpoints.down('custom768')]: {
    maxHeight: 'calc(100vh - 150px)',
  },
}));

export const ItemContent = styled('div')(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-start',
  flexGrow: 1,
  [theme.breakpoints.down('custom768')]: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    boxShadow: '0 4px 8px rgba(0, 0, 0, 0.2)',
    borderRadius: '14px',
    height: '100px',
    marginBottom: '15px',


  },
  [theme.breakpoints.down('custom400')]: {


  },
}));

export const ItemImage = styled('img')(({ theme }) => ({
  width: '70px',
  height: '70px',
  minWidth: '70px',
  minHeight: '70px',
  objectFit: "cover",
  marginRight: theme.spacing(2),
  [theme.breakpoints.down('custom768')]: {
    width: '100px',
    height: '100px',
    minWidth: '100px',
    minHeight: '100px',
    marginRight: '15px',
    borderRadius: '14px 0 0 14px',
  },
}));

export const ItemDetails = styled('div')(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-start',
  flexGrow: 1,
  [theme.breakpoints.down('custom768')]: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  [theme.breakpoints.down('custom400')]: {
    width: '25%',
  },
}));

export const NameTypography = styled(Typography)(({ theme }) => ({
  fontWeight: '500',
  fontSize: '16px',
  textTransform: 'lowercase',
  '&::first-letter': {
    textTransform: 'uppercase',
  },
  marginBottom: theme.spacing(0.5),
  [theme.breakpoints.down('custom768')]: {
    marginBottom: '5px',
    color: '#3D3D3D',
    fontWeight: '600',
    maxWidth: '185px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
}));

export const PriceTypography = styled(Typography)(({ theme }) => ({
  fontWeight: '500',
  color: '#727272',
  [theme.breakpoints.down('custom768')]: {
    marginBottom: '5px',
    fontWeight: 700,
    color: '#46A358',
    fontSize: '18px',
  },
}));

export const ItemActions = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  [theme.breakpoints.down('custom768')]: {
    marginLeft: 'auto',
  },
}));

export const QuantityButton = styled(IconButton)(({ theme }) => ({
  margin: '0 8px',
  border: '1px solid #FFFFFF',
  borderRadius: '15px',
  width: '22px',
  height: '25px',
  backgroundColor: '#46A358',
  color: '#FFFFFF',
  '& svg': {
    width: '16px',
    height: '16px',
  },

  [theme.breakpoints.down('custom768')]: {
    backgroundColor: '#F6F6F6',
    color: '#3D3D3D',
  },
}));

export const DeleteButton = styled(IconButton)(({ theme }) => ({
  color: '#A5A5A5',
  [theme.breakpoints.down('custom768')]: {
    color: '#46A358',
  },
}));

export const FooterContainer = styled('div')(({ theme }) => ({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  marginTop: theme.spacing(3),
  marginBottom: theme.spacing(3),
  width: '100%',
}));

export const ApplyCouponContainer = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  marginTop: '10px',
  width: '100%',
  borderRadius: "6px",
  [theme.breakpoints.down('custom768')]: {
    borderRadius: "40px",
    flexDirection: 'row',
    gap: 0,
    marginTop: '-15px',
    border: "none"
  },
}));

export const ButtonApplySisabled = styled(Button)(({ theme }) => ({
  backgroundColor: '#E0E0E0',
  color: '#A0A0A0',
  cursor: "not-allowed",
}));

export const ApplyButton = styled(Button)(({ theme }) => ({
  borderRadius: '0 3px 3px 0',
  width: '130px',
  color: 'white',
  backgroundColor: '#46A358',
  height: '40px',
  fontWeight: 700,
  textTransform: 'none',
  fontSize: '16px',
  padding: '0',
  boxShadow: "none",
  [theme.breakpoints.down('custom768')]: {
    width: '130px',
    height: '50px',
    borderRadius: '40px',
    background: 'linear-gradient(110deg, #46A358 0%, #46A358CC 100%)',
    marginTop: 0,
  },
  "&:hover": {
    backgroundColor: "#3A8C47",
  },
}));

export const SummaryContainer = styled('div')(({ theme }) => ({
  padding: '5px 0',
  width: '100%',
  [theme.breakpoints.down('custom768')]: {
    maxHeight: '205px'
  },
}));

export const SummaryRow = styled('div')(({ theme }) => ({
  display: 'flex',
  justifyContent: 'space-between',
  margin: theme.spacing(1, 0),
}));

export const TotalTypography = styled(Typography)(({ theme }) => ({
  fontWeight: 700,
  color: '#46A358',
  [theme.breakpoints.down('custom768')]: {
    color: '#3D3D3D',
  },
}));

export const PriceTotalTypography = styled(Typography)({
  fontWeight: 'bold',
  color: '#46A358',
});

export const CheckoutButton = styled(Button)(({ theme, total }) => ({
  borderRadius: '3px',
  color: 'white',
  backgroundColor: total > 0 ? '#46A358' : "rgba(165, 165, 165, 1)",
  cursor: total > 0 ? 'pointer' : 'not-allowed',
  height: '50px',
  fontWeight: 700,
  marginTop: theme.spacing(2),
  textTransform: 'none',
  fontSize: '16px',
  [theme.breakpoints.down('custom768')]: {
    borderRadius: '40px',
    height: '60px',
    width: '95%',
    background: 'linear-gradient(90deg, #46A358 100%, #46A358CC 80%)',
    marginTop: '0',

  },
  "&:hover": {
    backgroundColor: total > 0 ? "#3A8C47" : "rgba(165, 165, 165, 1)",
  },
}));

export const ViewShippingButton = styled(Button)({
  alignSelf: 'flex-end',
  justifySelf: 'flex-end',
  fontSize: '12px',
});

export const CouponTextField = styled(TextField)(({ theme }) => ({
  width: '100%',
  height: '40px',
  borderRadius: '3px 0 0 3px',
  '& .MuiOutlinedInput-root': {
    height: '100%',
    borderRadius: 'inherit',
    fontSize: '14px',


    '& fieldset': {
      border: "none",
    },
    '&:hover fieldset': {
      border: "none",
    },
    '&.Mui-focused fieldset': {
      border: "none",
    },
  },
  [theme.breakpoints.down('custom768')]: {
    height: '50px',
    borderRadius: '40px 0 0 40px',
    '& .MuiOutlinedInput-root': {
      height: '50px',
      borderRadius: '40px 0 0 40px',
      marginRight: '-20px',
      boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)',

      '& fieldset': {
        borderColor: 'transparent',
        borderWidth: '2px',
      },
      '&:hover fieldset': {
        border: "1px solid #46A358",
      },
      '&.Mui-focused fieldset': {
        border: "1px solid #46A358",
      },
    },
  },
}));

export const FixedFooter = styled('div')(({ theme }) => ({
  position: 'fixed',
  bottom: 0,
  left: 0,
  right: 0,
  maxWidth: '768px',
  margin: '0 auto',
  backgroundColor: '#fff',
  padding: '10px 0',
  boxShadow: '0 -4px 8px rgba(0, 0, 0, 0.1)',
  zIndex: 10,
  borderRadius: '25px 25px 0 0',
  [theme.breakpoints.down('custom768')]: {
    padding: '5px 10px',
  },
}));

export const InnerFooterContainer = styled('div')(({ theme }) => ({
  maxWidth: '768px',
  margin: '0 auto',
  padding: '0 10px',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  [theme.breakpoints.down('custom768')]: {
    padding: '0',
    maxHeight: '320px'
  },
}));

export const DesktopContainer = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  maxWidth: '1200px',
  margin: '0 auto',
});

export const DesktopCartTotals = styled('div')({
  marginTop: '20px',
  paddingLeft: '86px'
});

export const DesktopProductHeader = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  padding: '10px 0',
  alignItems: 'center',
});

export const ProductDetailsContainer = styled('div')({
  display: 'flex',
  alignItems: 'center',
  width: '100%',
  cursor: 'pointer',
  backgroundColor: '#FBFBFB',
  padding: '10px',
  marginBottom: '10px',
  justifyContent: 'center',
});

export const ProductColumn = styled('div')({
  display: 'flex',
  alignItems: 'center',
  '&:first-of-type': {
    justifyContent: 'flex-start',
  },
  flex: '1 1 auto',
  justifyContent: 'center'
});

export const ProductColumnHeader = styled(Typography)({
  marginBottom: '16px',
  textAlign: 'center',
  width: '100%',
  marginLeft: '39px',
  fontWeight: '500px',
  color: '#3D3D3D'
});

export const ContinueShoppingButton = styled(Button)({
  alignSelf: 'center',
  justifySelf: 'center',
  fontSize: '12px',
  marginTop: '10px',
  textTransform: 'none',
});

export const ViewShippingButtonDesk = styled(Button)({
  maxWidth: '140px',
  fontSize: '12px',
  textTransform: 'none',
  '&::first-letter': {
    textTransform: 'uppercase',
  },
});

export const BackButtonContainer = styled(Box)({
  display: 'flex',
  alignItems: 'center',
  margin: '20px 0',
});

export const CustomIconButton = styled(IconButton)({
  backgroundColor: '#f0f0f0',
});

export const CartTitle = styled(Typography)({
  flex: 1,
  textAlign: 'center'
});


export const Styles = createUseStyles({
  Link: {
    textDecoration: "none",
    color: "inherit",
    "&:hover": {
      textDecoration: "underline",
    },
  },
  BackButton: {
    width: "35px",
    height: "35px",
    background: "#f0f0f0",
  },
})