import React from 'react';
import { Snackbar, SnackbarContent, IconButton } from '@mui/material';
import { Close } from '@mui/icons-material';
import { useSelector } from 'react-redux';
const PopUpMessage = ({ open, onClose }) => {
const {loggedIn} = useSelector((state)=> state.authorization)
  const messages = [
    "🌿So Glad You’re Here🌿\nWe offer 20% discount for signing up—your wallet deserves a green hug",
    "🌱Fresh Finds for Home🌱\nEvery plant is a new beginning—let’s grow something beautiful",
    "🍃Find Plant Match🍃\nWhether you’re a novice or a green thumb, we have something special just for you",
    "🌳Your Eco Journey🌳\nDiscover new arrivals and get 20% off your first order when you sign up"
  ];
  const filteredMessages = loggedIn ? messages.slice(1,3) : messages  

  const currentMessage = React.useMemo(() => {
    return filteredMessages[Math.floor(Math.random() * filteredMessages.length)];
  }, []);

  return (
    <Snackbar
      open={open}
      autoHideDuration={5000}
      onClose={onClose}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
    >
      <SnackbarContent
        style={{
          color: '#46A358',
          borderRadius: '12px',
          boxShadow: '0 4px 8px rgba(0, 0, 0, 0.2)',
          display: 'flex',
          alignItems: 'center',
          padding: '16px',
          maxWidth: '150px',
          backgroundColor: 'white',
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          textAlign: 'center',
          position: 'relative',
        }}
        message={
          <span style={{ display: 'block', fontSize: '18px' }}>
            <strong>{currentMessage.split('\n')[0]}</strong><br />
            {currentMessage.split('\n')[1]}
          </span>
        }
        action={
          <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={onClose}
            style={{
              position: 'absolute',
              top: '8px',
              right: '8px',
            }}
          >
            <Close fontSize="small" />
          </IconButton>
        }
      />
    </Snackbar>
  );
};
export default PopUpMessage;