import { useEffect } from "react";
import { useState } from "react";
import {
  Box,
  Container,
  Grid,
  Typography,
  IconButton,
  Divider,
  useMediaQuery,
  Breadcrumbs,

} from "@mui/material";
import { Styles } from "./styles";
import { Add, Remove, DeleteOutline, ArrowBack } from "@mui/icons-material";
import { ThemeProvider, createTheme, styled } from "@mui/material/styles";
import Pagination from "@mui/material/Pagination";
import {
  ContainerWrapper,
  PaperStyled,
  ItemContent,
  ItemImage,
  NameTypography,
  ItemDetails,
  CartTotalsTitle,
  CouponApplyTitle,
  StyledDivider,
  PriceTypography,
  ItemActions,
  QuantityButton,
  DeleteButton,
  ApplyCouponContainer,
  FooterContainer,
  ApplyButton,
  SummaryContainer,
  SummaryRow,
  TotalTypography,
  PriceTotalTypography,
  CheckoutButton,
  CouponTextField,
  FixedFooter,
  InnerFooterContainer,
  DesktopContainer,
  DesktopCartTotals,
  DesktopProductHeader,
  ProductDetailsContainer,
  ProductColumn,
  ProductColumnHeader,
  ContinueShoppingButton,
  BackButtonContainer,
  CartTitle,
} from "./styles";
import { useDispatch, useSelector } from "react-redux";
import {
  removeItemFromCart,
  updateItemQuantity,
  replaceCartItems,
} from "../../redux/cart.slice/cart.slice";
import {
  setPaymentItems,
  setSubtotal,
  setTotal,
  setPaymentId,
} from "../../redux/payment.slice/payment.slice";
import { applyCoupon} from '../../redux/coupon.slice/coupon.slice';
import { Link, useNavigate } from "react-router-dom";
import { setProductId } from "../../redux/product.slice/product.slice";
import ProductSlider from "../../components/SliderReletedProduct/SliderReletedProsuct"
const theme = createTheme({
  palette: {
    primary: {
      main: "#46A358",
    },
    secondary: {
      main: "#ff5722",
    },
  },
  typography: {
    h5: {
      fontWeight: 700,
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: 1536,
      custom768: 768,
      custom400: 464,
    },
  },
});

const Cart = () => {
  const classes = Styles();
  const dispatch = useDispatch();
  const { discount} = useSelector((state) => state.coupon);
  const navigate = useNavigate();
  const { cartId, cartItems } = useSelector((state) => state.cart);
  const [coupon, setCoupon] = useState(null) 
  const isDesktop = useMediaQuery("(min-width:768px)");
  const handleQuantityChange = async (itemId, delta) => {
    const item = cartId.find((item) => item.product === itemId);

    if (item.quantityCart === 1 && delta === -1) {
      return;
    }
    if (item.quantity === 1) {
      return;
    }
    try {
      const response = await fetch(
        `https://greenshop-server.onrender.com/api/product-${delta > 0 ? "recover" : "reject"
        }`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ _id: itemId }),
        }
      );
      if (!response.ok) {
        const data = await response.json();
        console.error("Error", data.error);
        return;
      }
      const data = await response.json();
      console.log(data.message);
      dispatch(
        updateItemQuantity({
          productId: itemId,
          quantityCart: item.quantityCart + delta,
        })
      );
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const handleDeleteItem = async (id, quantityCart) => {
    dispatch(removeItemFromCart(id));
    try {
      const response = await fetch(
        "https://greenshop-server.onrender.com/api/product-recover-one",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ _id: id, quantityCart }),
        }
      );

      if (!response.ok) {
        const data = await response.json();
        console.error("Error:", data.error);
        return;
      }

      const data = await response.json();
      console.log(data.message);
    } catch (error) {
      console.error("Error:", error);
    }
  };
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 8;
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = cartItems.slice(indexOfFirstItem, indexOfLastItem);
  const handlePageChange = (event, value) => {
    setCurrentPage(value);
  };
  const totalPages = Math.ceil(cartItems.length / itemsPerPage);
  const CustomPagination = styled(Pagination)(({ theme }) => ({
    display: "flex",
    justifyContent: "flex-end",
    marginTop: theme.spacing(2),
    "& .MuiPaginationItem-root": {
      borderRadius: "8px",
    },
  }));
  const subtotal = cartItems.reduce(
    (acc, item) => acc + item.currentPrice * item.quantityCart,
    0
  );
  const shipping = 16;
  const total = subtotal > 0 ? subtotal * (1 - discount / 100) + shipping : 0;
  const proceedToCheckout = () => {
    if (total > 0) {
      dispatch(setPaymentItems(cartItems));
      dispatch(setPaymentId(cartId));
      dispatch(setSubtotal(subtotal));
      dispatch(setTotal(total));
      navigate("/checkout");
    } else {
      alert(`You must add at least one product to your cart!`);
      navigate("/");
    }
  };
  useEffect(() => {
    if (cartId && cartId.length >= 0) {
      const fetchProducts = async () => {
        try {
          const quantityMap = cartId.reduce((map, item) => {
            map[item.product] = item.quantityCart;
            return map;
          }, {});
          const productFetchPromises = cartId.map(async (item) => {
            const response = await fetch(
              "https://greenshop-server.onrender.com/api/product",
              {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({ _id: item.product }),
              }
            );

            if (!response.ok) {
              const data = await response.json();
              console.error("Failed to fetch product data", data.error);
              return null;
            }

            const data = await response.json();
            return { ...data, quantityCart: quantityMap[item.product] };
          });

          const products = await Promise.all(productFetchPromises);
          const validProducts = products.filter((product) => product !== null);
          dispatch(replaceCartItems(validProducts));
        } catch (error) {
          console.error("Fetch error:", error);
        }
      };

      fetchProducts();
    }
  }, [cartId, dispatch]);

  const handleApplyCoupon = async () => {
    try {
      const response = await fetch('https://greenshop-server.onrender.com/api/verify/coupon', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ coupon }),
      });

      const data = await response.json();
      
      if (data.valid) {
        dispatch(applyCoupon({ code: coupon, discount: data.discount }));
      } else {
        alert("Invalid coupon code");
      }
    } catch (error) {
      console.error("Error applying coupon:", error);
    }
  };
  
  return (
    <ThemeProvider theme={theme}>
      {isDesktop ? (
        <ContainerWrapper>
          <Breadcrumbs aria-label="breadcrumb" style={{ margin: "20px auto 10px auto", maxWidth: "1200px" }}>
            <Link className={classes.Link} to="/"  >
              Home
            </Link>
            <Link className={classes.Link} to="/wishlist" >
              Wishlist
            </Link>
            <Typography style={{fontWeight: 700}} color="textPrimary" >Shopping Cart</Typography>
          </Breadcrumbs>
          <DesktopContainer>
            <Grid container spacing={2}>
              <Grid item xs={8}>
                {cartItems.length > 0 && (
                  <DesktopProductHeader>
                    <ProductColumnHeader
                      variant="h6"
                      style={{ width: "210px", textAlign: "start" }}
                    >
                      Products
                    </ProductColumnHeader>
                    <ProductColumnHeader variant="h6" style={{ width: "70px" }}>
                      Price
                    </ProductColumnHeader>
                    <ProductColumnHeader
                      variant="h6"
                      style={{ width: "100px" }}
                    >
                      Quantity
                    </ProductColumnHeader>
                    <ProductColumnHeader variant="h6" style={{ width: "70px" }}>
                      Total
                    </ProductColumnHeader>
                    <ProductColumnHeader variant="h6" style={{ width: "50px" }}>
                      Delete
                    </ProductColumnHeader>
                  </DesktopProductHeader>
                )}
                {currentItems.map((item) => (
                  <ProductDetailsContainer  onClick={() => {
                    navigate("/product");
                    dispatch(setProductId(item._id));
                  }} key={item._id}>
                    <ProductColumn       
           style={{ width: "220px" }}>
                      <ItemImage src={item.imageUrls[0]} alt={item.name} />
                      <NameTypography variant="body1">
                        {item.name}
                      </NameTypography>
                    </ProductColumn>
                    <ProductColumn        o style={{ width: "70px" }}>
                      <PriceTypography variant="body1">
                        ${item.currentPrice}
                      </PriceTypography>
                    </ProductColumn>
                    <ProductColumn        onClick={(e) => {
            e.stopPropagation();
          }} style={{ width: "100px" }}>
                      <ItemActions>
                        <QuantityButton
                          onClick={() =>
                            handleQuantityChange(item._id, -1, item.quantity)
                          }
                        >
                          <Remove />
                        </QuantityButton>
                        <Typography variant="body1">
                          {item.quantityCart}
                        </Typography>
                        <QuantityButton
                          onClick={() => {
                            handleQuantityChange(item._id, 1, item.quantity);
                          }}
                        >
                          <Add />
                        </QuantityButton>
                      </ItemActions>
                    </ProductColumn>
                    <ProductColumn>
                      <PriceTotalTypography variant="body1">
                        ${(item.currentPrice * item.quantityCart).toFixed(2)}
                      </PriceTotalTypography>
                    </ProductColumn>
                    <ProductColumn        onClick={(e) => {
            e.stopPropagation();
          }} style={{ width: "50px" }}>
                      <DeleteButton
                        onClick={() =>
                          handleDeleteItem(item._id, item.quantityCart)
                        }
                      >
                        <DeleteOutline />
                      </DeleteButton>
                    </ProductColumn>
                  </ProductDetailsContainer>
                ))}
                {cartItems.length > itemsPerPage && (
                  <CustomPagination
                    count={totalPages}
                    page={currentPage}
                    onChange={handlePageChange}
                    color="primary"
                  />
                )}
                {cartItems.length === 0 && (
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      color: "#46A358",
                      height: "50vh",
                      fontSize: "29px",
                      fontWeight: "600",
                    }}
                  >
                    Your cart is empty!
                  </Box>
                )}
              </Grid>
              <Grid item xs={4}>
                <DesktopCartTotals>
                  <Box>
                    <CartTotalsTitle variant="h6">Cart Totals</CartTotalsTitle>
                    <StyledDivider />
                    <CouponApplyTitle variant="subtitle1">
                      Coupon Apply
                    </CouponApplyTitle>
                  </Box>

                  <ApplyCouponContainer 
                  style={{
                    border: cartItems.length === 0 ? "1px solid rgb(165, 165, 165)"  : "1px solid #46A358",}}
                  >
                  <CouponTextField
                  variant="outlined"
                  placeholder="Enter coupon code here..."
                  value={coupon}
                  onChange={(e) => setCoupon(e.target.value)}
                  disabled={cartItems.length === 0}
                 style={{
                pointerEvents: cartItems.length === 0 ? "none" : "auto",}}/>
                  <ApplyButton
                 variant="contained"
                  style={{
                  backgroundColor: cartItems.length === 0 ? "rgb(165, 165, 165)" : "#46A358",
                 pointerEvents: cartItems.length === 0 ? "none" : "auto",}}
                onClick={handleApplyCoupon}>Apply</ApplyButton>
                  </ApplyCouponContainer>
                  <SummaryRow>
                    <Typography variant="body1">Subtotal</Typography>
                    <Typography variant="body1">
                      ${subtotal.toFixed(2)}
                    </Typography>
                  </SummaryRow>
                  <SummaryRow>
                  <Typography variant="body1">Coupon Discount</Typography>
                  <Typography variant="body1">(-) {((subtotal * discount) / 100).toFixed(2)}</Typography>
                  </SummaryRow>
                  <SummaryRow>
                    <Typography variant="body1">Shipping</Typography>
                    <Typography variant="body1">
                      ${shipping.toFixed(2)}
                    </Typography>
                  </SummaryRow>
                  <Divider />
                  <SummaryRow>
                    <TotalTypography variant="h6">Total</TotalTypography>
                    <TotalTypography variant="h6">
                      ${total.toFixed(2)}
                    </TotalTypography>
                  </SummaryRow>
                  <CheckoutButton
                    total={total}
                    onClick={proceedToCheckout}
                    fullWidth
                  >
                    Proceed to Checkout
                  </CheckoutButton>
                  <Link underline="hover" to="/">
                    <ContinueShoppingButton>
                      Continue Shopping
                    </ContinueShoppingButton>
                  </Link>
                </DesktopCartTotals>
              </Grid>
            </Grid>
            <ProductSlider />
          </DesktopContainer>
        </ContainerWrapper>
      ) : (
        <Container
          style={{ overflowY: "auto", maxHeight: "100vh", minWidth: "366px" }}
        >
          <BackButtonContainer>
            <IconButton
              onClick={() => {
                navigate(-1);
              }}
              className={classes.BackButton}
            >
              <ArrowBack style={{ color: "#000" }} />
            </IconButton>
            <CartTitle variant="h6">Cart</CartTitle>
          </BackButtonContainer>
          {cartItems.length === 0 && (
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                color: "#46A358",
                height: "50vh",
                fontSize: "29px",
                fontWeight: "600",
              }}
            >
              Your cart is empty!
            </Box>
          )}
          <PaperStyled>
            {cartItems.map((item) => (
              <Box onClick={() => {
                navigate("/product");
                dispatch(setProductId(item._id));
              }} key={item._id}>
                <ItemContent>
                  <ItemImage src={item.imageUrls[0]} alt={item.name} />
                  <ItemDetails>
                    <NameTypography variant="body1">{item.name}</NameTypography>
                    <Typography variant="body2">Size: {item.size}</Typography>
                    <PriceTypography variant="body1">
                      ${item.currentPrice}
                    </PriceTypography>
                  </ItemDetails>
                  <ItemActions   onClick={(e) => {
            e.stopPropagation();
          }}>
                    <QuantityButton
                      onClick={() =>
                        handleQuantityChange(item._id, -1, item.quantity)
                      }
                    >
                      <Remove />
                    </QuantityButton>
                    <Typography variant="body1">{item.quantityCart}</Typography>
                    <QuantityButton
                      onClick={() =>
                        handleQuantityChange(item._id, 1, item.quantity)
                      }
                    >
                      <Add />
                    </QuantityButton>
                  </ItemActions>
                  <DeleteButton
                    onClick={(e) =>
                     { e.stopPropagation();
                      handleDeleteItem(item._id, item.quantityCart)}
                    }
                  >
                    <DeleteOutline />
                  </DeleteButton>
                </ItemContent>
              </Box>
            ))}
            <FixedFooter>
              <InnerFooterContainer>
                <FooterContainer>
                  <SummaryContainer>
                    <ApplyCouponContainer>
                  <CouponTextField
                  placeholder="Enter coupon code here..."
                  value={coupon}
                  onChange={(e) => setCoupon(e.target.value)}
                  disabled={cartItems.length === 0}
                  style={{
                    color:
                      cartItems.length === 0
                        ? "rgb(165, 165, 165)"
                        : "#46A358",
                    borderRadius: cartItems.length === 0 ? "5px" : "4px",
                    pointerEvents: cartItems.length === 0 ? "none" : "auto",
                  }}
                />
                <ApplyButton
                  onClick={handleApplyCoupon}
                  style={{
                    backgroundColor:
                      cartItems.length === 0
                        ? "rgb(165, 165, 165)"
                        : "#46A358",
                    pointerEvents: cartItems.length === 0 ? "none" : "auto",
                  }}
                >
                  Apply
                </ApplyButton>
              </ApplyCouponContainer>
              <SummaryRow>
                <Typography variant="body1">Subtotal</Typography>
                <Typography variant="body1">
                  ${subtotal.toFixed(2)}
                </Typography>
              </SummaryRow>
              <SummaryRow>
                <Typography variant="body1">Coupon Discount</Typography>
                <Typography variant="body1">
                  (-) {((subtotal * discount) / 100).toFixed(2)}
                </Typography>
              </SummaryRow>
              <SummaryRow>
                <Typography variant="body1">Shipping</Typography>
                <Typography variant="body1">
                  ${shipping.toFixed(2)}
                </Typography>
              </SummaryRow>
              <SummaryRow>
                <TotalTypography variant="h6">Total</TotalTypography>
                <PriceTotalTypography variant="h6">
                  ${total.toFixed(2)}
                </PriceTotalTypography>
                    </SummaryRow>
                  </SummaryContainer>
                </FooterContainer>
                <CheckoutButton onClick={proceedToCheckout} fullWidth>
                  Proceed to Checkout
                </CheckoutButton>
              </InnerFooterContainer>
            </FixedFooter>
          </PaperStyled>
        </Container>
      )}
    </ThemeProvider>
  );
};

export default Cart;
