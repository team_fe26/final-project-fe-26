import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({

    container: {
        position: "relative",
        width: "100%",
        height: "100vh",
        overflow: "hidden",
    },
    image: {
        width: "100%",
        height: "100%",
        objectFit: "cover",
        position: "absolute",
        top: "0",
        left: "0",
        zIndex: "-1",
        opacity: "0.5",
    },
    overlay: {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        color: "white",
        textAlign: "center",
    },
    title404: {
        fontFamily: "'Monserrat', sans-serif",
        fontWeight: "900",
        color: "rgba(0, 0, 0, 0.5)",
        fontSize: "96px",
        margin: "0",
    },
    title: {
        fontFamily: "'Monserrat', sans-serif",
        color: "rgba(0, 0, 0, 0.5)",
        fontSize: "68px",
        margin: "0",
    },
    text: {
        fontFamily: "'Monserrat', sans-serif",
        color: "rgba(0, 0, 0, 0.5)",
        fontSize: "36px",
    },
    button: {
        margin: "0 auto",
        display: "block",
        padding: "0 20px 0 20px",
        marginTop: "35px",
        borderRadius: "3px",
        backgroundColor: "#46A358",
        fontFamily: "'Monserrat', sans-serif",
        color: "#FFFFFF",
        marginBottom: "48px",
        textTransform: "capitalize",
        fontWeight: "700",
        fontSize: "22px",
        height: "45px",
        border: "none",

        "&:hover": {
            backgroundColor: "#46A358",
        },
    }
});