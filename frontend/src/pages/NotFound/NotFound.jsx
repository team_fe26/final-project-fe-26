import { useNavigate } from 'react-router-dom';
import { Styles } from "./styles";
import { Button, Typography, Container, CardMedia, Box } from '@mui/material';
const NotFound = () => {
    const classes = Styles();
    const navigate = useNavigate();

    const handleGoHome = () => {
        navigate('/');
    };

    return (
        <Box className={classes.container}>
            <CardMedia component='img' src="/not-found.jpg" alt="Not Found" className={classes.image} />
            <Container className={classes.overlay}>
                <Typography variant='h1' className={classes.title404}>404</Typography>
                <Typography variant='h2' className={classes.title}>Page Not Found</Typography>
                <Typography variant="body1" className={classes.text}>Sorry, the page you are looking for does not exist.</Typography>
                <Button onClick={handleGoHome} className={classes.button}>Go to home page</Button>
            </Container>
        </Box>
    );


};

export default NotFound;