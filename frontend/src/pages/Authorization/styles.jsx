import { createUseStyles } from "react-jss";
export const Styles = createUseStyles({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    minWidth: "366px",
    maxWidth: '760px',
    paddingTop: '130px',
    paddingBottom: '40px',
  },
  title: {
    color: "rgba(61, 61, 61, 1)",
    fontFamily: '"Montserrat", sans-serif',
    fontSize: "20px",
    fontWeight: "500",
  },
  titleLogin: {
    marginBottom: '25px',
    marginTop: '65px',
  },
  titleRegister: {
    marginBottom: '15px',
    marginTop: '25px'
  },
  textBottom: {
    fontWeight: 400,
    fontSize: '15px',
    fontFamily: '"Montserrat", sans-serif',
    color: 'rgba(165, 165, 165, 1)',
    marginTop: '40px',
  },
  textBottomLink: (props) => ({
    fontSize: '15px',
    fontFamily: '"Montserrat", sans-serif',
    color: props.isForgotPassword ? (props.goToCode ? (props.isCodeTrue ? 'black' : '#EC6E6E') : `#B785B7`) : 'rgba(70, 163, 88, 1)',
    fontWeight: 700,
    cursor: 'pointer',
    textDecoration: 'none',
  }),

  BackButton: {
    width: "35px",
    height: "35px",
    background: "#f0f0f0",
    marginLeft: "28px",
    position: "absolute",
    top: "10px",
    left: "-10px"
  },
});
