import { Container, Typography, IconButton, } from "@mui/material";
import { Styles } from "./styles";
import LoginPart from "../../components/Authorization/Parts/Login";
import { useEffect } from 'react'
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { ArrowBack } from "@mui/icons-material";
import SuccessfulAuthorization from "../../components/Authorization/Parts/SuccessfulAuthorization";
import { setIsForgotPassword, setGoToCode, setIsCodeTrue } from "../../redux/authorization.slice/authorization.slice";
export default function LoginMobVersion() {
  const dispatch = useDispatch()
  const { loggedIn, isForgotPassword, goToCode, isCodeTrue } = useSelector((state) => state.authorization);

  const classes = Styles({ isForgotPassword, goToCode, isCodeTrue });
  const navigate = useNavigate()
  useEffect(() => {
    if (loggedIn) {
      const timer = setTimeout(() => {
        navigate('/');
      }, 5000);

      return () => clearTimeout(timer);
    }
  }, [loggedIn, navigate]);
  return (
    <Container className={classes.container}>
      <IconButton 
        onClick={() => {
          dispatch(setIsForgotPassword(false));
          dispatch(setGoToCode(false));
          dispatch(setIsCodeTrue(false));


          navigate("/");
        }}
        className={classes.BackButton}
      >
        <ArrowBack style={{ color: "#000" }} />
      </IconButton>
      
      {loggedIn ? (
        <SuccessfulAuthorization></SuccessfulAuthorization>
      ) : (
        <>
          {" "}
          {isForgotPassword === false ? (
            <div>
              <img src="/Logo-big.png" alt="Logo" />
              <Typography
                variant="h1"
                className={`${classes.title} ${classes.titleLogin}`}
              >
                Login
              </Typography>
            </div>
          ) : (<Typography
            variant="h1"
            className={`${classes.title} ${classes.titleLogin}`}
          >
            Reset Password
          </Typography>)}

          <LoginPart></LoginPart>
          <Typography className={classes.textBottom} variant="body1">
            Don&apos;t have an account?{" "}
            <Link to="/sign-up" onClick={() => {
              dispatch(setIsForgotPassword(false));
              dispatch(setGoToCode(false));
              dispatch(setIsCodeTrue(false));
            }} className={classes.textBottomLink}>
              Sign Up
            </Link>
          </Typography>
        </>
      )}
    </Container>
  );
}

