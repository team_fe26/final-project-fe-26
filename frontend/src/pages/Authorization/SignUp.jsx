import { Container, Typography, IconButton, } from "@mui/material";
import { Styles } from "./styles";
import { useEffect } from "react";
import SignUpPart from "../../components/Authorization/Parts/SignUp";
import { Link, useNavigate } from "react-router-dom";
import { ArrowBack } from "@mui/icons-material";
import SuccessfulAuthorization from "../../components/Authorization/Parts/SuccessfulAuthorization";
import { useSelector } from "react-redux";

export default function SignUpMobVersion() {
  const { loggedIn, isForgotPassword, goToCode, isCodeTrue } = useSelector(
    (state) => state.authorization
  );

  const classes = Styles({ isForgotPassword, goToCode, isCodeTrue });
  const navigate = useNavigate();
  useEffect(() => {
    if (loggedIn) {
      const timer = setTimeout(() => {
        navigate("/");
      }, 5000);

      return () => clearTimeout(timer);
    }
  }, [loggedIn, navigate]);
  return (
    <>
      <Container className={classes.container}>
      <IconButton 
        onClick={() => {
          


          navigate("/");
        }}
        className={classes.BackButton}
      >
        <ArrowBack style={{ color: "#000" }} />
      </IconButton>
        {loggedIn ? (
          <SuccessfulAuthorization></SuccessfulAuthorization>
        ) : (
          <>
            {" "}
            <img src="/Logo-big.png" />
            <Typography
              className={`${classes.title} ${classes.titleRegister}`}
              variant="h1"
            >
              Create an account
            </Typography>
            <SignUpPart></SignUpPart>
            <Typography className={classes.textBottom} variant="body1">
              Already have an account?{" "}
              <Link
                className={classes.textBottomLink}
                to="/login"
                variant="span"
              >
                Login
              </Link>
            </Typography>
          </>
        )}
      </Container>
    </>
  );
}
