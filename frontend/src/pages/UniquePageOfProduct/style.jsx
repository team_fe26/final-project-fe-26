import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  Wrapper: {
    position: "relative",
    padding: "0 25px",
    "@media (max-width: 768px)": {
      display: "block",
      maxWidth: "768px",
      marginTop: "0",
      padding: 0
    },
  },
  BreadcrumbsWrapper: {
    padding: 0,
  },
  Link: {
    textDecoration: "none",
    color: "inherit",
    "&:hover": {
      textDecoration: "underline",
    },
  },
  Container: {
    position: "relative",
    padding: 0,
    "@media (max-width: 768px)": {
      display: "block",

      maxWidth: "768px",
      marginTop: "0",
    },
    marginTop: "20px",
    maxWidth: "1200px",
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
  },
  Slider: {
    "@media (min-width: 768px)": {
      display: "none",
    },
  },
  ProductHeaderContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    maxWidth: "768px",
    paddingTop: "34px",
    paddingRight: "25px",
    paddingLeft: "25px",
    "@media (min-width: 768px)": {
      display: "flex",
      flexDirection: "column",
      gap: "20px",
      paddingTop: "0",
      alignItems: "flex-start",
    },
  },
  IconContainer: {
    paddingTop: "38px",
    display: "flex",
    position: "absolute",
    zIndex: 1000,
    top: -20,
    width: "100%",
    justifyContent: "space-between",
    "@media (min-width: 768px)": {
      display: "none",
    },
  },
  BackButton: {
    width: "35px",
    height: "35px",
    background: "#f0f0f0",
    marginLeft: "28px",
  },
  FavoriteButton: {
    width: "35px",
    height: "35px",
    background: "#f0f0f0",
    marginRight: "28px",
    fill: "#46a358",
  },
  ProductName: {
    fontWeight: "500",
    fontSize: "20px",
    color: "#3d3d3d",
    textAlign: 'start',
    "@media (min-width: 768px)": {
      fontWeight: "700",
      fontSize: "28px",
    },
  },
  Review: {
    border: "1px solid #46a358",
    borderRadius: "32px",
    display: "flex",
    alignItems: "center",
    padding: "10px",
    marginRight: 0,
    marginLeft: 'auto',
    "@media (min-width: 768px)": {
      display: "none",
    },
  },
  productRatings: {
    display: "none",
    "@media (min-width: 768px)": {
      display: "flex",
      alignItems: "center",
      gap: "11px",
    },
  },
  DesctopReviewText: {
    fontWeight: "400",
    fontSize: "15px",
    color: "#3d3d3d",
  },
  ReviewScore: {
    display: "flex",
    fontWeight: "500",
    fontSize: "14px",
    lineHeight: "114%",
    color: "#3d3d3d",
  },
  ReviewScoreSpan: {
    fontWeight: "400",
    fontSize: "14px",
    lineHeight: "114%",
    color: "#727272",
  },
  PoductDescription: {
    marginTop: "24px",
    fontWeight: "400",
    fontSize: "14px",
    lineHeight: "171%",
    paddingRight: "25px",
    paddingLeft: "25px",
    "@media (min-width: 768px)": {
      marginTop: "10px",
    },
  },
  ProductWrapper: {
    marginTop: "40px",
    borderRadius: "31px 31px 0 0",
    background: "#fff",
    "@media (min-width: 768px)": {
      marginTop: "0",
    },
  },
  ProductContainer: {
    display: "grid",
    padding: "0",
    "@media (max-width: 768px)": {
      paddingLeft: 0
    },
    "@media (min-width: 1100px)": {

      paddingLeft: "20px"
    },
  },
  ProductInfoContainer: {
    marginTop: "22px",
    display: "flex",
    flexDirection: "column",
    gap: "10px",
    paddingRight: "25px",
    paddingLeft: "25px",
  },
  ProductInfo: {
    display: "flex",
    fontWeight: "400",
    fontSize: "15px",
    lineHeight: "107%",
    color: "#A5A5A5",
    gap: "6px",
    
  },
  Text: {
    color: "#727272",
  },
  ShortDescription: {
    display: "none",
    "@media (min-width: 768px)": {
      display: "block",
      marginTop: "25px",
      fontWeight: "700",
      fontSize: "15px",
      lineHeight: "107%",
      color: "#3d3d3d",
      paddingLeft: "25px",
    },
  },
  ProductCartContainer: {
    marginTop: "29px",
    position: "fixed",
    bottom: "0%",
    zIndex: "10",
    width: "100%",
    "@media (min-width: 768px)": {
      paddingLeft: "25px",
      marginTop: "15px",
      gridArea: "4",
      position: "relative",
      height: "auto",
    },
  },
  Modal: {
    height: "95vh",
    margin: "auto",
    width: "50%"
  },
  ModalContent: {
    padding: "0"
  },
  ProductCart: {
    boxShadow: "0 0 20px 0 rgba(205, 205, 205, 0.3)",
    background: "#fff",
    borderRadius: "40px 40px 0 0",
    padding: "20px 25px 0 25px",

    "@media (min-width: 768px)": {
      boxShadow: "none",
      borderRadius: "0",
      padding: "0",
      display: "flex",

      gap: "8px",
      alignItems: "center",
      width: "100%",
    },
    "@media (min-width: 1100px)": {

      gap: "26px",
    },
  },
  ProductCount: {
    fontWeight: "400",
    fontSize: "15px",
    lineHeight: "107%",
  },
  Quantity: {
    "@media (min-width: 768px)": {
      display: "none",
    },
  },
  ProductCountContainer: {
    display: "flex",
    alignItems: "center",
    gap: "11px",
  },
  ProductButton: {
    color: "#3D3D3D",
    fontSize: "20px",
    width: "28px",
    cursor: 'pointer',
    height: "28px",
    borderRadius: "100%",
    outline: "none",
    border: "none",
    "@media (min-width: 768px)": {
      borderRadius: "31px",
      width: "38px",
      height: "38px",
      fontSize: "28px",
      background:
        "linear-gradient(180deg, #46a358 0%, rgba(70, 163, 88, 0.8) 100%)",
      color: "#fff",
    },
  },
  ProductPriceContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  ProductPrice: {
    fontWeight: "700",
    fontSize: "20px",
    lineHeight: "80%",
    textAlign: "right",
    color: "#46a358",
    "@media (min-width: 768px)": {
      display: "none",
    },
  },
  OrderButton: {
    
    background: "linear-gradient(137deg, #46a358 0%, rgba(70, 163, 88, 0.8) 100%)",
    border: "1px solid #46a358",
    borderRadius: "6px",
    width: "130px",
    height: "40px",
    fontWeight: "700",
    fontSize: "13px",
    lineHeight: "125%",
    cursor: 'pointer',
    color: "#fff",
    textTransform: "uppercase",

    "@media (max-width: 878px)": {
      width: "90px",
      boxShadow: "none",
    },
    "@media (max-width: 768px)": {
      fontSize: "16px",
      borderRadius: "40px",
      width: "196px",
      height: "60px",
      border: "none",
      boxShadow: "0 10px 20px 0 rgba(70, 163, 88, 0.3)",
    },
  },
  ProductOrderContainer: {
    marginTop: "20px",
    display: "flex",
    alignItems: "center",
    gap: "10px",
    paddingBottom: "36px",
    "@media (min-width: 768px)": {
      paddingBottom: "0",
      marginTop: "0",
    },
  },
  CartButtonIcon: {
    border: "none",
    borderRadius: "40px",
    width: "60px",
    height: "60px",
    background: "#f6f6f6",
  },
  CartButton: {
    border: "1px solid #46a358",
    borderRadius: "6px",
    width: "130px",
    height: "40px",
    background: "#fff",
    color: "#46A358",
    fontWeight: 700,
    textTransform: "uppercase",
    fontSize: "13px",
    cursor: 'pointer',
    "@media (max-width: 878px)": {
      width: "70px",
    },
  },
  ImageGalleryContainer: {
    "@media (max-width: 768px)": {
      display: "none",
    },
    "@media (max-width: 1100px)": {
      maxWidth: "400px"
    },
    display: "flex",
    maxWidth: "550px",
    height: "100%",
    gap: "40px",
  },
  MainImage: {
    width: "600px",
    height: "auto",
    marginBottom: "20px",
    position: "relative",
  },
  ImageGallery: {
    height: "450px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    gap: "10px",
  },
  ImageFromGallery: {
    height: "145px",
  },
  mainImg: {
    height: "570px",
  },
  SearchButton: {
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    position: "absolute",
    right: "30px",
    top: '20px'
  },
  DesctopProductPrice: {
    display: "none",
    "@media (min-width: 767px)": {
      display: "block",
      fontWeight: "700",
      fontSize: "20px",
      lineHeight: "80%",
      textAlign: "right",
      color: "#46a358",
    },
  },
  DesctopPrice: {
    display: "flex",
    justifyContent: "space-between",
    height: "16px",
    position: "relative",
    width: "100%",
    "@media (min-width: 767px)": {
      "&::after": {
        content: '""',
        border: "0.30px solid rgba(70, 163, 88, 0.5)",
        width: "100%",
        height: "0px",
        position: "absolute",
        bottom: "-10px",
        left: "0",
      },
    },
  },
  DesctopFavoriteButton: {
    display: "none",
    "@media (min-width: 767px)": {
      display: "block",
      border: "1px solid #46a358",
      borderRadius: "6px",
      width: "40px",
      height: "40px",
    },
  },
  DesctopSocial: {
    display: "none",
    "@media (min-width: 767px)": {
      display: "flex",
      marginTop: "10px",
      alignItems: "center",
      paddingLeft: "25px",
    },
  },
  divSlider: {
    position: 'relative',
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  cardSlider: {
    position: 'relative',
    zIndex: 2,
    height: '300px',
    width: '300px',
    objectFit: 'cover',
    margin: 'auto',
  },
  SocialShere: {
    fontWeight: 600,
    fontSize: "15px",
    color: "#3d3d3d",
  },
  ProductDescription: (section) => ({
    fontWeight: section === "description" ? "700" : "400",
    fontSize: "17px",
    color: section === "description" ? "#46a358" : "#3D3D3D",
    position: "relative",
    cursor: "pointer",
    "&::after": {
      content: '""',
      border: "2px solid rgba(70, 163, 88, 1)",
      width: "100%",
      height: "0px",
      position: "absolute",
      top: "25px",
      left: "0",
      display: section === "description" ? "block" : "none",
    },
  }),
  reviewTitle: (section) => ({
    fontWeight: section === "review" ? "700" : "400",
    fontSize: "17px",
    color: section === "review" ? "#46a358" : "#3D3D3D",
    position: "relative",
    cursor: "pointer",
    "&::after": {
      content: '""',
      border: "2px solid rgba(70, 163, 88, 1)",
      width: "100%",
      height: "0px",
      position: "absolute",
      top: "25px",
      left: "0",
      display: section === "review" ? "block" : "none",
    },
  }),
  containerSectionTitle: {
    position: "relative",
    marginTop: "25px",
    gap: "20px",
    paddingRight: "25px",
    "&::after": {
      content: '""',
      border: "0.30px solid rgba(70, 163, 88, 0.5)",
      width: "200%",
      height: "0px",
      position: "absolute",
      top: "26px",
      left: "0",

    },
    

    "@media (max-width: 768px)": {
      marginTop: "50px",
      // width: "100%",
      paddingLeft: "25px",
      "&::after": {
        width: "90%",
        marginLeft: "25px",

      },
    },
  },
  ProductFullDescription: {
    margin: "18px 15px 0 0",
    gridColumn: "span 2",
    "@media (max-width: 768px)": {
      margin: "18px 15px 200px 0",
    },
  },
  DescriptionText: {
    fontWeight: "400",
    fontSize: "14px",
    lineHeight: "171%",
    color: "#727272",
    paddingRight: "25px",

    "@media (max-width: 768px)": {
      paddingLeft: "25px",
    },
  },
  DescriptionTitle: {
    marginTop: "18px",
    fontWeight: "700",
    fontSize: "14px",
    paddingRight: "25px",
    lineHeight: "171%",
    color: "#3d3d3d",
    "@media (max-width: 768px)": {
      paddingLeft: "25px",
    }
  },
  MarginTop: {
    marginTop: "25px",
  },
  ProductSliderWrapper: {
    gridColumn: "span 2",
    padding: 0,
   
  },
});
