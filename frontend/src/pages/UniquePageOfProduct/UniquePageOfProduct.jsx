import {
  Container,
  Box,
  IconButton,
  Typography,
  Dialog,
  DialogContent,
  CardMedia,
  Stack,
  Breadcrumbs,
} from "@mui/material";
import { useState, useEffect } from "react";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import EmailIcon from "@mui/icons-material/Email";
import SearchIcon from "@mui/icons-material/Search";
import { ArrowBack, FavoriteBorder, ShoppingCart } from "@mui/icons-material";
import FavoriteIcon from "@mui/icons-material/Favorite";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Link, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { Styles } from "./style";
import CommentsList from "../../components/Comments/CommentsList";
import ProductSlider from "../../components/SliderReletedProduct/SliderReletedProsuct";
import { addItemToCart } from "../../redux/cart.slice/cart.slice";
import {
  addToWishlist,
  removeFromWishlist,
} from "../../redux/wishlist.slice/wishlist.slice";
import {
  setPaymentItems,
  setSubtotal,
  setTotal,
  setPaymentId,
} from "../../redux/payment.slice/payment.slice";

import { useDispatch } from "react-redux";
import { fetchComments } from "../../redux/comments.slice/comments.slice";
const UniquePageOfProduct = () => {
  const [section, setSection] = useState("description");
  const classes = Styles(section);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [product, setProduct] = useState(null);
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768);
  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768);
    };

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const { productId } = useSelector((state) => state.product);
  const { cartId } = useSelector((state) => state.cart);
  const { totalComments, comments } = useSelector(
    (state) => state.comments.comments
  );

  const wishlistId = useSelector((state) => state.wishlist.wishlistId);
  const isProductInWishlist = (productId) => {
    return wishlistId.includes(productId);
  };

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        const response = await fetch(
          "https://greenshop-server.onrender.com/api/product",
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({ _id: productId }),
          }
        );
        if (!response.ok) {
          const data = await response.json();
          console.error("Failed to fetch product data", data.error);
          return;
        }
        const data = await response.json();
        setProduct(data);
      } catch (error) {
        console.error("Fetch error:", error);
      }
    };

    fetchProduct();
    dispatch(fetchComments({ productId, page: 1 }));
  }, [productId, dispatch]);

  const CustomDot = ({ active }) => {
    return (
      <div
        style={{
          width: "10px",
          height: "10px",
          borderRadius: "50%",
          backgroundColor: active ? "green" : "#ccc",
          margin: "0 4px",
          marginBottom: "22px",
          marginTop: "12px",
          cursor: "pointer",
        }}
      />
    );
  };

  const settings = {
    dots: true,
    infinite: true,
    speed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    appendDots: (dots) => (
      <div
        style={{ display: "flex", justifyContent: "center", bottom: "-40px" }}
      >
        {dots.map((dot, index) => (
          <CustomDot
            key={index}
            {...dot.props}
            active={dot.props.className.includes("slick-active")}
          />
        ))}
      </div>
    ),
    responsive: [
      {
        breakpoint: 766,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
    ],
  };

  const [mainImage, setMainImage] = useState("");
  const [open, setOpen] = useState(false);

  const handleImageClick = (image) => {
    setMainImage(image);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const [quantity, setQuantity] = useState(1);

  if (!product) {
    return <div>Loading...</div>;
  }

  const handleIncreaseQuantity = () => {
    setQuantity((prevQuantity) => prevQuantity + 1);
  };

  const handleDecreaseQuantity = () => {
    if (quantity > 1) {
      setQuantity((prevQuantity) => prevQuantity - 1);
    }
  };

  const handleAddToCart = async (id, quantity) => {
    if (quantity <= 0) {
      console.log("Product is not in the stock");
      return;
    }
    dispatch(addItemToCart(id));
    try {
      const productResponse = await fetch(
        "https://greenshop-server.onrender.com/api/product-reject-one",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ _id: id }),
        }
      );

      if (!productResponse.ok) {
        const productData = await productResponse.json();
        console.error("Error:", productData.error);
        return;
      }
      const productData = await productResponse.json();
      console.log(productData.message);
    } catch (error) {
      console.error("Error with product-reject-one:", error);
    }
  };

  const handleAddToWishlist = async (id) => {
    if (isProductInWishlist(id)) {
      dispatch(removeFromWishlist(id));
      console.log("Product removed from the wishlist.");
    } else {
      dispatch(addToWishlist(id));
      console.log("Product added to the wishlist.");
    }
  };
  const subtotal = quantity * product.currentPrice;
  const total = subtotal + 16;

  const proceedToCheckout = () => {
    dispatch(setPaymentId([{ ...product._id, quantityCart: quantity }]));
    dispatch(setPaymentItems([{ ...product, quantityCart: quantity }]));
    dispatch(setSubtotal(subtotal));
    dispatch(setTotal(total));
    navigate("/checkout");
  };
  return (
    <Container className={classes.Wrapper}>
      <Container className={classes.BreadcrumbsWrapper}>
        {isMobile ? (
          <Box className={classes.IconContainer}>
            <IconButton
              onClick={() => {
                navigate(-1);
              }}
              className={classes.BackButton}
            >
              <ArrowBack style={{ color: "#000" }} />
            </IconButton>
            <IconButton
              onClick={() => handleAddToWishlist(product._id)}
              className={classes.FavoriteButton}
            >
              {isProductInWishlist(product._id) ? (
                <FavoriteIcon style={{ color: "#46A358" }} />
              ) : (
                <FavoriteBorder style={{ color: "#46a358" }} />
              )}
            </IconButton>
          </Box>
        ) : (
          <Breadcrumbs
            aria-label="breadcrumb"
            style={{ margin: "115px auto 0 auto", maxWidth: "1200px" }}
          >
            <Link className={classes.Link} to="/">
              Home
            </Link>
            <Typography style={{ fontWeight: 700 }} color="textPrimary">
              {product.name}
            </Typography>
          </Breadcrumbs>
        )}
      </Container>
      <Container className={classes.Container}>
        {/* Галерея для десктоп версии------------------------------------------------------------------- */}
        
        <Box className={classes.ImageGalleryContainer}>
          <Box className={classes.ImageGallery}>
            {product.imageUrls.map((image, index) => (
              <CardMedia
                component="img"
                className={classes.ImageFromGallery}
                key={index}
                src={image}
                alt={`Product ${index}`}
                onClick={() => handleImageClick(image)}
              />
            ))}
          </Box>
          <Box className={classes.MainImage}>
            <CardMedia
              component="img"
              src={mainImage || product.imageUrls[0]}
              className={classes.mainImg}
              alt="Main"
            />
            <IconButton className={classes.SearchButton} onClick={handleOpen}>
              <SearchIcon />
            </IconButton>
          </Box>
          <Dialog
            className={classes.Modal}
            open={open}
            onClose={handleClose}
            maxWidth="md"
            fullWidth
          >
            <DialogContent className={classes.ModalContent}>
              <CardMedia
                component="img"
                src={mainImage || product.imageUrls[0]}
                alt={product.name}
                style={{ width: "100%", height: "85vh" }}
              />
            </DialogContent>
          </Dialog>
        </Box>

        {/* Слайдер для мобильной версии------------------------------------------------------------------- */}

        <Slider className={classes.Slider} {...settings}>
          {product.imageUrls.map((image, index) => (
            <div
              key={index}
              style={{ position: "relative" }}
              className={classes.divSlider}
            >
              <div
                style={{
                  backgroundImage: `url(${image})`,
                  paddingTop: "50px",
                  paddingBottom: "50px",
                  overflow: `hidden`,
                }}
              >
                <CardMedia
                  component="img"
                  src={image}
                  alt={`Slide ${index}`}
                  className={classes.cardSlider}
                />
              </div>
            </div>
          ))}
        </Slider>

        <Box className={classes.ProductWrapper}>
          <Box className={classes.ProductContainer}>
            <Box className={classes.ProductHeaderContainer}>
              <Typography variant="h5" className={classes.ProductName}>
                {product.name}
              </Typography>
              <Box className={classes.DesctopPrice}>
                <Typography className={classes.DesctopProductPrice}>
                  ${product.currentPrice * quantity}
                </Typography>
              </Box>
            </Box>
            <Typography className={classes.ShortDescription}>
              Short Description:
            </Typography>
            <Typography
              variant="body1"
              className={`${classes.PoductDescription} ${classes.Text}`}
            >
              {product.shortDescription}
            </Typography>

            <Box className={classes.ProductInfoContainer}>
              <Typography className={classes.ProductInfo}>
                SKU:{" "}
                <span className={`${classes.ProductInfo} ${classes.Text}`}>
                  {product.itemNo}
                </span>
              </Typography>
              <Typography className={classes.ProductInfo}>
                Categories:{" "}
                <span className={`${classes.ProductInfo} ${classes.Text}`}>
                  {product.categories}
                </span>
              </Typography>
              <Typography className={classes.ProductInfo}>
                Tags:{" "}
                <span className={`${classes.ProductInfo} ${classes.Text}`}>
                  {product.categories}, {product.size}, Plants
                </span>
              </Typography>
            </Box>

            <Box className={classes.ProductCartContainer}>
              <Box className={classes.ProductCart}>
                <Box className={classes.ProductPriceContainer}>
                  <Box className={classes.ProductCountContainer}>
                    <Typography
                      className={`${classes.ProductCount} ${classes.Text} ${classes.Quantity}`}
                    >
                      Qty
                    </Typography>
                    <button
                      onClick={handleDecreaseQuantity}
                      className={classes.ProductButton}
                    >
                      -
                    </button>
                    <Typography
                      className={`${classes.ProductCount} ${classes.Text}`}
                    >
                      {quantity}
                    </Typography>
                    <button
                      onClick={handleIncreaseQuantity}
                      className={classes.ProductButton}
                    >
                      +
                    </button>
                  </Box>
                  <Typography className={classes.ProductPrice}>
                    ${product.currentPrice * quantity}
                  </Typography>
                </Box>
                <Box className={classes.ProductOrderContainer}>
                  <button
                    onClick={() => {
                      proceedToCheckout();
                    }}
                    className={classes.OrderButton}
                  >
                    Buy Now
                  </button>

                  {isMobile ? (
                    <IconButton
                      onClick={() =>
                        handleAddToCart(product._id, product.quantity)
                      }
                      className={classes.CartButtonIcon}
                    >
                      <ShoppingCart />
                    </IconButton>
                  ) : (
                    <button
                      onClick={() =>
                        handleAddToCart(product._id, product.quantity)
                      }
                      className={classes.CartButton}
                    >
                      Add to cart
                    </button>
                  )}

                  <IconButton
                    onClick={() => handleAddToWishlist(product._id)}
                    className={classes.DesctopFavoriteButton}
                  >
                    {isProductInWishlist(product._id) ? (
                      <FavoriteIcon style={{ color: "#46a358" }} />
                    ) : (
                      <FavoriteBorder style={{ color: "#46a358" }} />
                    )}
                  </IconButton>
                </Box>
              </Box>
            </Box>
            <Box className={classes.DesctopSocial}>
              <Typography className={classes.SocialShere}>
                Share this product:
              </Typography>
              <IconButton
                className={classes.SocialButton}
                onClick={() => {
                  window.open(
                    `https://www.facebook.com/sharer/sharer.php?u=${window.location.href}`,
                    "_blank"
                  );
                }}
              >
                <FacebookIcon />
              </IconButton>
              <IconButton
                className={classes.SocialButton}
                onClick={() => {
                  window.open(
                    `https://twitter.com/intent/tweet?url=${window.location.href}&text=Check%20out%20this%20product!`,
                    "_blank"
                  );
                }}
              >
                <TwitterIcon />
              </IconButton>
              <IconButton
                className={classes.SocialButton}
                onClick={() => {
                  window.open(
                    `https://www.linkedin.com/shareArticle?mini=true&url=${window.location.href}`,
                    "_blank"
                  );
                }}
              >
                <LinkedInIcon />
              </IconButton>
              <IconButton
                className={classes.SocialButton}
                onClick={() => {
                  window.location.href = `mailto:?subject=Check out this product!&body=I found this product interesting and wanted to share it with you: ${window.location.href}`;
                }}
              >
                <EmailIcon />
              </IconButton>
            </Box>
          </Box>
        </Box>
        <Stack direction="row" className={classes.containerSectionTitle}>
          <Typography
            onClick={() => {
              setSection("description");
            }}
            className={classes.ProductDescription}
          >
            Product Description
          </Typography>
          <Typography
            onClick={() => {
              setSection("review");
            }}
            className={classes.reviewTitle}
          >
            Reviews ({totalComments})
          </Typography>
        </Stack>
        {section === "description" ? (
          <Box className={classes.ProductFullDescription}>
            <Typography className={classes.DescriptionText}>
              {product.longDescription}
            </Typography>
            <Typography className={classes.DescriptionTitle}>
              Care Plant:
            </Typography>
            <Typography className={classes.DescriptionText}>
              {product.carePlant}
            </Typography>

            <Typography className={classes.DescriptionTitle}>
              Water Plant:
            </Typography>
            <Typography className={classes.DescriptionText}>
              {product.waterPlant}
            </Typography>

            <Typography className={classes.DescriptionTitle}>
              Feed Plant:
            </Typography>
            <Typography className={classes.DescriptionText}>
              {product.feedPlant}
            </Typography>
          </Box>
        ) : (
          <CommentsList productId={productId} comments={comments} />
        )}
        {window.innerWidth >= 768 && (
          <Container className={classes.ProductSliderWrapper}>
            <ProductSlider />
          </Container>
        )}
      </Container>
    </Container>
  );
};

export default UniquePageOfProduct;
