import {
  Container,
  Typography,
  Box,
  Button,
  CardMedia,
  MenuList,
  MenuItem,
  ListItemText,
  Radio,
  Checkbox,
} from "@mui/material";
import { Styles } from "./styles";
import { useSelector, useDispatch } from "react-redux";
import { ThemeProvider, createTheme} from "@mui/material/styles";
import EditIcon from '@mui/icons-material/Edit';
import {
  setAddress,
  setPaymentMethod,
  setIsPaymentSuccess
} from "../../../redux/payment.slice/payment.slice";
import { useState, useEffect } from "react";
import IconButton from "@mui/material/IconButton";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import { ArrowBack} from "@mui/icons-material";
import { useNavigate } from "react-router-dom";
import PayPalButton from "../../../components/Checkout/PaymentMethod.jsx/Paypal/Paypal";
import GooglePayButton from "../../../components/Checkout/PaymentMethod.jsx/GooglePay/GooglePay";
import WalletIcon from "@mui/icons-material/Wallet";
import PaymentItems from "../../../components/PaymentItems/PaymentItems";
import ModalMobMessage from "./ModalMobMessage";
export default function Payment() {
  const [addresses, setAddresses] = useState([]);
  const classes = Styles();
  const navigate = useNavigate();
  const {loggedIn} = useSelector((state)=> state.authorization)
  const { paymentMethod, optionalAddress, address, isPaymentSuccess } = useSelector((state) => state.payment);
  const [notMainAddresses, setNotMainAddresses] = useState([]);
  const { total } = useSelector((state) => state.payment);
  const [moreInfoIndex, setMoreInfoIndex] = useState(null);
  const [showAddresses, setIsShowAddresses] = useState(false);
  const [selectedAddress, setSelectedAddress] = useState(null);
  const theme = createTheme({
    palette: {
      primary: {
        main: "#46A358",
      },
      secondary: {
        main: "#ff5722",
      },
    },
    typography: {
      h5: {
        fontWeight: 700,
      },
    },
    breakpoints: {
      values: {
        xs: 0,
        sm: 600,
        md: 900,
        lg: 1200,
        xl: 1536,
        custom768: 768,
        custom400: 464,
      },
    },
  });
  
  const dispatch = useDispatch();
  useEffect(() => {
    if(loggedIn){
    const fetchFn = async () => {
      try {
        const token = JSON.parse(localStorage.getItem("token")) || "";

        const response = await fetch(
          "https://greenshop-server.onrender.com/api/user",
          {
            method: "POST",
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );

        if (!response.ok) {
          const data = await response.json();
          console.error("Error fetching data:", data.error);
          return;
        }

        const data = await response.json();
        console.log(data.message, data.customer);
        if (data.customer.addresses) {
          setAddresses(data.customer.addresses);
          const notMainAddressesFilter = data.customer.addresses.filter(
            (address) => !address.main
          );
          setNotMainAddresses(notMainAddressesFilter);
        }
      } catch (error) {
        console.error("Fetch error:", error);
      }
    };

    fetchFn();}
  }, [loggedIn]);

  const handleMoreInfoClick = (index) => {
    setMoreInfoIndex(index);
  };

  const handleAddressChange = (address) => {
    setSelectedAddress(address);
    dispatch(setAddress(address));
  };

  const renderAddresses = () => {
    return addresses.map((address, index) => {
      const isChecked =
        selectedAddress &&
        JSON.stringify(selectedAddress) === JSON.stringify(address);

      return (
        <MenuItem
          key={address.street}
          onClick={() => handleAddressChange(address)}
          className={isChecked ? classes.menuItemSelected : classes.menuItem}
        >
          <Radio
            checked={isChecked}
            value={address.street}
            color="success"
            name="radio-buttons"
          />
          <Box>
            <ListItemText>{address.town}</ListItemText>
            <Typography className={classes.infoAddress} variant="body1">
              {address.appartment} {address.street}
              {address.zip}, {address.country}, {address.state}
            </Typography>

            {moreInfoIndex === index ? (
              <Typography className={classes.infoAddress}>
                {address.firstName} {address.lastName}, {address.phoneNumber},{" "}
                {address.email}
              </Typography>
            ) : (
              <IconButton
                onClick={() => handleMoreInfoClick(index)}
                className={classes.iconButtonMore}
              >
                <MoreHorizIcon />
              </IconButton>
            )}
          </Box>
        </MenuItem>
      );
    });
  };

  const renderNotMainAddresses = () => {
    return notMainAddresses.map((address, index) => {
      const isChecked =
        selectedAddress &&
        JSON.stringify(selectedAddress) === JSON.stringify(address);

      return (
        <MenuItem
        onClick={() => handleAddressChange(address)}
          key={address.street}
          className={isChecked ? classes.menuItemSelected : classes.menuItem}
        >
          <Radio
            className={classes.radio}
            checked={isChecked}
            value={address.street}
            color="success"
            name="radio-buttons"
          />
          <Box>
            <ListItemText>{address.town}</ListItemText>
            <Typography className={classes.infoAddress} variant="body1">
              {address.appartment} {address.street}
              {address.zip}, {address.country}, {address.state}
            </Typography>

            {moreInfoIndex === index ? (
              <Typography className={classes.infoAddress}>
                {address.firstName} {address.lastName}, {address.phoneNumber},{" "}
                {address.email}
              </Typography>
            ) : (
              <IconButton
                onClick={() => handleMoreInfoClick(index)}
                className={classes.iconButtonMore}
              >
                <MoreHorizIcon />
              </IconButton>
            )}
          </Box>
        </MenuItem>
      );
    });
  };

  const mainAddress = addresses.find((address) => address.main);
  const isCheckedMain =
    selectedAddress &&
    JSON.stringify(selectedAddress) === JSON.stringify(mainAddress);
const isCheckedOptional = selectedAddress && JSON.stringify(selectedAddress) === JSON.stringify(optionalAddress)
const OPTIONALADDRESS = (()=>{
  return(<> {Object.keys(optionalAddress).length > 0  && (  <MenuItem
    key={optionalAddress.street}
    onClick={() => handleAddressChange(optionalAddress)}
    className={
      isCheckedOptional
        ? classes.menuItemSelected
        : classes.menuItem
    }
  >
    <Radio
      className={classes.radio}
      checked={isCheckedOptional}
      value={optionalAddress.street}
      color="success"
      name="radio-buttons"
    />
    <Box>
      <ListItemText>{optionalAddress.town}</ListItemText>
      <IconButton className={classes.editIcon}>
<EditIcon
onClick={() => {
navigate("/billing-address", { state: { from: "checkout" } });
}}
className={classes.iconButtonMore}
>
</EditIcon>
</IconButton>
      <Typography
        className={classes.infoAddress}
        variant="body1"
      >
        {optionalAddress.appartment} {optionalAddress.street},{" "}
        {optionalAddress.zip}, {optionalAddress.country},{" "}
        {optionalAddress.state}
      </Typography>

      {moreInfoIndex === optionalAddress.zip ? (
        <Typography className={classes.infoAddress}>
          {optionalAddress.firstName} {optionalAddress.lastName},{" "}
          {optionalAddress.phoneNumber}, {optionalAddress.email}
        </Typography>
      ) : (
        <IconButton
          onClick={() => handleMoreInfoClick(optionalAddress.zip)}
          className={classes.iconButtonMore}
        >
          <MoreHorizIcon />
        </IconButton>
      )}
    </Box>
  </MenuItem>)}</>)
})

if(isPaymentSuccess){
  return(<ModalMobMessage></ModalMobMessage>)
}
  return (
    <ThemeProvider theme={theme}>
    <Container className={classes.container}>
      <IconButton className={classes.btnBack} onClick={() => navigate(-1)}>
        <ArrowBack style={{ color: "#000" }} />
      </IconButton>
      <Typography className={classes.title} variant="h1">
        Checkout
      </Typography>
      <Typography className={classes.titleOfPart} variant="body1">
          Order
        </Typography>
      
        <Box className={classes.productWrapper}>
             <PaymentItems />
            </Box>
            <Typography className={classes.titleOfPart} variant="body1">
          Shipping to
        </Typography> 
      <Box className={classes.containerAddresses}>
       
        { Object.keys(optionalAddress).length === 0 ? (
    addresses.length <= 0 ? (
      <>
        <Button
          className={classes.btnBillingForm}
          onClick={() => {
            navigate("/billing-address", { state: { from: "checkout" } });
          }}
        >
          Go to billing form
        </Button>
        <Typography className={classes.message} variant="body1">
          It's required!
        </Typography>
      </>
    ) : (
      <Typography
        onClick={() => {
          navigate("/billing-address", { state: { from: "checkout" } });
        }}
        className={classes.addAddress}
      >
        Add Address
      </Typography>
    )
) : (  addresses.length <= 0 && (<MenuList className={classes.menuList}>
{OPTIONALADDRESS()}
</MenuList>))}

        {addresses.length > 0 && (
          <>
            {mainAddress ? (
              <Box className={classes.boxMenuList}>
                <MenuList className={classes.menuList}>
                  <MenuItem
                  onClick={() => handleAddressChange(mainAddress)}
                    key={mainAddress.street}
                    className={
                      isCheckedMain
                        ? classes.menuItemSelected
                        : classes.menuItem
                    }
                  >
                    <Radio
                      className={classes.radio}
                      checked={isCheckedMain}
                      value={mainAddress.street}
                      color="success"
                      name="radio-buttons"
                    />
                    <Box>
                      <ListItemText>{mainAddress.town}</ListItemText>
                      <Typography
                        className={classes.infoAddress}
                        variant="body1"
                      >
                        {mainAddress.appartment} {mainAddress.street},{" "}
                        {mainAddress.zip}, {mainAddress.country},{" "}
                        {mainAddress.state}
                      </Typography>

                      {moreInfoIndex === mainAddress.zip ? (
                        <Typography className={classes.infoAddress}>
                          {mainAddress.firstName} {mainAddress.lastName},{" "}
                          {mainAddress.phoneNumber}, {mainAddress.email}
                        </Typography>
                      ) : (
                        <IconButton
                          onClick={() => handleMoreInfoClick(mainAddress.zip)}
                          className={classes.iconButtonMore}
                        >
                          <MoreHorizIcon />
                        </IconButton>
                      )}
                    </Box>
                  </MenuItem>
                </MenuList>
                {notMainAddresses.length > 0 && (<><Box className={classes.boxCheckbox}>
                  <Checkbox
                    checked={showAddresses}
                    onClick={() => setIsShowAddresses(!showAddresses)}
                    color="success"
                  />
                  <Typography>
                    Show other addresses?
                  </Typography>
                </Box>
                {showAddresses && (
                  <MenuList className={classes.menuList}>
                    {renderNotMainAddresses()}
                    {Object.keys(optionalAddress).length > 0 && OPTIONALADDRESS()}
                  </MenuList>
                )}</>)}
              
              </Box>
            ) : (
              <MenuList className={classes.menuList}>
                {renderAddresses()}
                {Object.keys(optionalAddress).length > 0 && OPTIONALADDRESS()}
              </MenuList>
            )}
          </>
        )}
      </Box>
      <Box className={classes.containerPaymentMethod}>
        <Typography className={classes.titleOfPart} variant="body1">
          Payment Method
        </Typography>
        <Box>
          <MenuList className={classes.menuList}>
            <MenuItem
              key="GooglePay"
              className={
                paymentMethod === "GooglePay"
                  ? classes.menuItemMethodSelected
                  : classes.menuItemMethod
              }
            >
              <Box className={classes.method}>
                <CardMedia
                  className={classes.imgPayment}
                  component="img"
                  height="17"
                  src="/google-pay-primary-logo-logo-svgrepo-com.svg"
                  alt="GooglePay"
                  title="GooglePay"
                />
              </Box>
              <Radio
              color='success'
                checked={paymentMethod === "GooglePay"}
                
                onChange={(event) =>
                  dispatch(setPaymentMethod(event.target.value))
                }
                value="GooglePay"
                name="radio-buttons"
              />
            </MenuItem>

            <MenuItem
              key="PayPal"
              className={
                paymentMethod === "PayPal"
                  ? classes.menuItemMethodSelected
                  : classes.menuItemMethod
              }
            >
              <Box className={classes.method}>
                <CardMedia
                  component="img"
                  className={classes.imgPayment}
                  height="22"
                  src="/paypal-svgrepo-com.svg"
                  alt="PayPal"
                  title="PayPal"
                />
              </Box>
              <Radio
              color='success'
                checked={paymentMethod === "PayPal"}
                onChange={(event) =>
                  dispatch(setPaymentMethod(event.target.value))
                }
                value="PayPal"
                name="radio-buttons"
              />
            </MenuItem>

            <MenuItem
              key="Cash on delivery"
              className={
                paymentMethod === "Cash on delivery"
                  ? classes.menuItemMethodSelected
                  : classes.menuItemMethod
              }
            >
              <Box className={classes.method}>
                <WalletIcon color="success"></WalletIcon>
                <ListItemText className={classes.nameMethod}>
                  Cash on delivery
                </ListItemText>
              </Box>
              <Radio
              color='success'
                checked={paymentMethod === "Cash on delivery"}
                onChange={(event) =>
                  dispatch(setPaymentMethod(event.target.value))
                }
                value="Cash on delivery"
                name="radio-buttons"
              />
            </MenuItem>
          </MenuList>
        </Box>
      </Box>

      <Box className={classes.containerTotal}>
        <Typography className={classes.infoTotal} variant="body1">
          Total:
        </Typography>
        <Typography className={classes.infoTotalPrice} variant="body1">
          ${total.toFixed(2)}
        </Typography>
      </Box>
      {Object.keys(address).length > 0 ? (
  <>
      {paymentMethod === "PayPal" ? (
        <PayPalButton />
      ) : paymentMethod === "GooglePay" ? (
        <GooglePayButton />
      ) : paymentMethod === 'Cash on delivery' && (
        <Button onClick={(()=>{ dispatch(setIsPaymentSuccess(true))})} className={classes.btn}>Place Order</Button>
      )}
      </>
    ) : (<Typography className={classes.msgAddress}>You need to specify the address</Typography>)}
      </Container>
    </ThemeProvider>
  );
}
