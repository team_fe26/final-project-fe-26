import { createUseStyles } from "react-jss";
import { styled } from '@mui/material/styles';
import { Typography } from "@mui/material";
export const Styles = createUseStyles({
  container: {
    display: "flex",
    flexDirection: "column",
    
    justifyContent: "center",
    maxWidth: "766px",
    minWidth: "366px",
    position: "relative",
  },

  btnBack: {
    position: "absolute",
    width: "35px",
    height: "35px",
    backgroundColor: "rgba(243, 243, 243, 1)",
    border: "none",
    borderRadius: "50%",
    top: "30px",
    left: "5%",
    cursor: "pointer",
  },
  editIcon: {
    position: 'absolute',
    right: '0',
    top: '15px'
  },
  title: {
    textAlign: "center",
    fontSize: "20px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
    color: "#3D3D3D",
    marginBottom: "17px",
    marginTop: "33px",
  },

  containerAddresses: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
    width: "100%",
  },
  productWrapper: {
    margin: '15px auto 0 auto',
    minWidth: '366px',
    width: '100%'
    
  },
  menuList: {
    width: "100%",
    /* marginTop: "17px", */
    display: "flex",
    flexDirection: "column",
    gap: "20px",
  },

  addAddress: {
    width: "fitContent",
    fontSize: "14px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
    textDecoration: "none",
    color: "#46A358",
    marginRight: 0,
    marginLeft: 'auto',
    position: 'relative',
    bottom: '20px',
  },

  titleOfPart: {
    textAlign: "left",
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
    height: 'fit-content',
    color: "#3D3D3D",
    marginTop: "15px"
  },

  menuItem: {
    width: "100%",
    borderRadius: "14px",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    boxShadow: "0px 20px 20px 0px #C8C8C840",
    padding: "15px 60px 15px",
    flexWrap: 'wrap',
    gap: '10px',
  },
  boxMenuList: {
    width: '100%'
  },
  menuItemSelected: {
    width: "100%",
    borderRadius: "14px",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#F9F9F9",
    padding: "15px 60px 15px",
    flexWrap: 'wrap',
    gap: '10px',
  },

  infoAddress: {
    color: "#727272",
    fontSize: "14px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
    whiteSpace: 'wrap'
  },

  containerPaymentMethod: {
    marginTop: "10px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    width: "100%",
  },

  method: {
    display: "flex",
    flexDirection: "row",
    alignItems: 'center',
    gap: "12px",
  },

  menuItemMethod: {
    width: "100%",
    borderRadius: "14px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    boxShadow: "0px 20px 20px 0px #C8C8C840",
    padding: "15px 11px 15px",
  },

  menuItemMethodSelected: {
    width: "100%",
    borderRadius: "14px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#F9F9F9",
    padding: "15px 11px 15px",
  },
  msgAddress:{
    color: 'red',
    fontWeight: 700,
    textTransform: 'uppercase',
    textAlign: 'center'
  },
  containerTotal: {
    width: "100%",
    marginTop: "44px",

    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    gap: "10px",
  },

  infoTotal: {
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "700",
    color: "#3D3D3D",
    width: "fitContent",
  },

  infoTotalPrice: {
    fontSize: "18px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "700",
    color: "#46A358",
    width: "fitContent",
  },
  btn: {
    borderRadius: "40px",
    marginTop: "50px",
    backgroundImage:
      "linear-gradient(to right, rgba(70, 163, 88, 1), rgba(70, 163, 88, 0.8))",
    color: "#FFFFFF",
    width: "100%",
    marginBottom: "32px",
    textTransform: "capitalize",
    fontWeight: "700",
    fontSize: "15px",
    height: "60px",
  },
  boxCheckbox: {
    display: "flex",
    alignItems: "center",
    marginTop: "20px",
  },
  iconButtonMore: {
    position: "absolute",
    right: "20px",
    bottom: "5px",
  },
  imgPayment: {
    width: '60px',
    height: '40px',
  },
  nameMethod: {
    color: '#808080'
  },
  radio: {
    position: 'absolute',
    left: '10px'
  },
  btnBillingForm: {
    borderRadius: "40px",
    backgroundImage: "linear-gradient(to right, rgba(70, 163, 88, 1), rgba(70, 163, 88, 0.8))",
    color: "#FFFFFF",
    width: "60%",
    textTransform: "capitalize",
    fontWeight: "700",
    fontSize: "15px",
    marginRight: 'auto',
    marginLeft: 'auto',
    height: "60px",
    marginTop: '15px',
  },
  message: {
    color: "red",
    textAlign: "center",
    marginTop: '5px',
    width: '100%',
    fontSize: "14px",
  }, 
  successfulPayment: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    marginBottom: "80px",
    marginTop: "80px",
  },
  wrapCheckmark: {
    width: "100px",
    height: "100px",
    borderRadius: "50%",
    position: "relative",
    borderLeft: "8px solid rgba(70, 163, 88, 1)",
    borderBottom: "8px solid rgba(70, 163, 88, 1)",
    transform: "rotate(-20deg)",
    marginBottom: "60px",
    "&::after": {
      content: '""',
      display: "block",
      width: "50px",
      height: "20px",
      transform: "rotate(45deg)",
      position: "absolute",
      left: "10px",
      bottom: "30px",
      background: "rgba(70, 163, 88, 1)",
    },
    "&::before": {
      content: '""',
      display: "block",
      width: "100px",
      height: "20px",
      transform: "rotate(-45deg)",
      left: "26px",
      bottom: "49px",
      position: "absolute",
      background: "rgba(70, 163, 88, 1)",
    },
  },
  successfulMessage: {
    fontSize: "24px",
    fontWeight: "800",
    textAlign: "center",
    color: "rgba(165, 165, 165, 1)",
    fontFamily: '"Montserrat", sans-serif',
  },
  quantity: {
    marginRight: '30px',
  }
});
