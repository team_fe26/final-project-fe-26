import { Container, Typography, Box } from "@mui/material";
import { Styles } from "./styles";
export default function ModalMobMessage() {
  const classes = Styles();
  return (
    <>
      <Container className={classes.successfulPayment}>
        <Box className={classes.wrapCheckmark}></Box>
        <Typography className={classes.successfulMessage} variant="h1">
          You have successfully order!
        </Typography>
      </Container>
    </>
  );
}
