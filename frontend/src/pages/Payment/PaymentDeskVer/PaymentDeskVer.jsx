import {
  Button,
  Typography,
  Box,
  Radio,
  Container,
  CardMedia,
  MenuList,
  MenuItem,
  ListItemText,
  Checkbox,
  Breadcrumbs,
} from "@mui/material";
import {
  setPaymentMethod,
  setAddress,
  setEditOptionalAddress,
  setIsPaymentSuccess,
} from "../../../redux/payment.slice/payment.slice";
import { Styles } from "./styles";
import PaymentModal from "../../../components/PaymentModal/PaymentModal";
import { setIsShowModal } from "../../../redux/payment.slice/payment.slice";
import { useSelector, useDispatch } from "react-redux";
import PaymentItems from "../../../components/PaymentItems/PaymentItems";
import BillingAddress from "../../../components/Checkout/BillingAddress";
import EditIcon from "@mui/icons-material/Edit";
import PayPalButton from "../../../components/Checkout/PaymentMethod.jsx/Paypal/Paypal";
import WalletIcon from "@mui/icons-material/Wallet";
import GooglePayButton from "../../../components/Checkout/PaymentMethod.jsx/GooglePay/GooglePay";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import IconButton from "@mui/material/IconButton";

export default function PaymentDeskVer() {
  const classes = Styles();
  const dispatch = useDispatch();
  const {
    isShowModal,
    paymentMethod,
    address,
    optionalAddress,
    total,
    subtotal,
    editOptionalAddress,
  } = useSelector((state) => state.payment);
  const [moreInfoIndex, setMoreInfoIndex] = useState(null);
  const { loggedIn } = useSelector((state) => state.authorization);
  const {discount} = useSelector((state)=> state.coupon)
  const [addresses, setAddresses] = useState([]);
  const [showAddresses, setIsShowAddresses] = useState(false);
  const [notMainAddresses, setNotMainAddresses] = useState([]);
  const [selectedAddress, setSelectedAddress] = useState(null);
  useEffect(() => {
    if (loggedIn) {
      const fetchFn = async () => {
        try {
          const token = JSON.parse(localStorage.getItem("token")) || "";

          const response = await fetch(
            "https://greenshop-server.onrender.com/api/user",
            {
              method: "POST",
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          );

          if (!response.ok) {
            const data = await response.json();
            console.error("Error fetching data:", data.error);
            return;
          }

          const data = await response.json();
          console.log(data.message, data.customer);
          if (data.customer.addresses) {
            setAddresses(data.customer.addresses);
            const notMainAddressesFilter = data.customer.addresses.filter(
              (address) => !address.main
            );
            setNotMainAddresses(notMainAddressesFilter);
          }
        } catch (error) {
          console.error("Fetch error:", error);
        }
      };

      fetchFn();
    }
  }, [loggedIn]);

  const handleMoreInfoClick = (index) => {
    setMoreInfoIndex(index);
  };

  const handleAddressChange = (address) => {
    setSelectedAddress(address);
    dispatch(setAddress(address));
  };

  const renderNotMainAddresses = () => {
    if (notMainAddresses.length > 0) {
      return notMainAddresses.map((address, index) => {
        const isChecked =
          selectedAddress && address._id === selectedAddress._id;
        return (
          <MenuItem
            onClick={() => handleAddressChange(address)}
            className={
              isChecked ? classes.menuItemSelected : classes.menuItemAddress
            }
            key={address._id}
          >
            <Box className={classes.boxAddress}>
              <ListItemText className={classes.titleAddress}>
                {address.town}
              </ListItemText>
              <Typography className={classes.infoAddress} variant="body1">
                {address.appartment} {address.street}, {address.zip},{" "}
                {address.country}, {address.state}
              </Typography>
              {moreInfoIndex === index ? (
                <Typography className={classes.infoAddress}>
                  {address.firstName} {address.lastName}, {address.phoneNumber},{" "}
                  {address.email}
                </Typography>
              ) : (
                <IconButton
                  onClick={() => handleMoreInfoClick(index)}
                  className={classes.iconButtonMore}
                >
                  <MoreHorizIcon />
                </IconButton>
              )}
            </Box>
            <Radio
              color="success"
              checked={isChecked}
              className={classes.radioAddress}
            />
          </MenuItem>
        );
      });
    }
    return null;
  };

  const renderAddresses = () => {
    if (addresses.length > 0) {
      return addresses.map((address, index) => {
        const isChecked =
          selectedAddress && address._id === selectedAddress._id;
        return (
          <MenuItem
            onClick={() => handleAddressChange(address)}
            className={
              isChecked ? classes.menuItemSelected : classes.menuItemAddress
            }
            key={address._id}
          >
            <Box className={classes.boxAddress}>
              <ListItemText className={classes.titleAddress}>
                {address.town}
              </ListItemText>
              <Typography className={classes.infoAddress} variant="body1">
                {address.appartment} {address.street} {address.zip},{" "}
                {address.country}, {address.state}
              </Typography>
              {moreInfoIndex === index ? (
                <>
                  <Typography className={classes.infoAddress}>
                    {address.firstName} {address.lastName},{" "}
                    {address.phoneNumber}, {address.email}
                  </Typography>
                </>
              ) : (
                <IconButton
                  onClick={() => handleMoreInfoClick(index)}
                  className={classes.iconButtonMore}
                >
                  <MoreHorizIcon />
                </IconButton>
              )}
            </Box>
            <Radio
              color="success"
              className={classes.radioAddress}
              checked={isChecked}
            />
          </MenuItem>
        );
      });
    }
    return null;
  };

  const mainAddress = addresses.find((address) => address.main);
  const isCheckedMain =
    selectedAddress && mainAddress && mainAddress._id === selectedAddress._id;
  const isCheckedOptional =
    address &&
    JSON.stringify(address) === JSON.stringify(optionalAddress);
  const OPTIONALADDRESS = () => {
    return (
      <>
        {" "}
        {Object.keys(optionalAddress).length > 0 && (
          <MenuItem
            onClick={() => handleAddressChange(optionalAddress)}
            key={optionalAddress.street}
            className={
              isCheckedOptional
                ? classes.menuItemSelected
                : classes.menuItemAddress
            }
          >
            <Box className={classes.boxAddress}>
              <ListItemText className={classes.titleAddress}>
                {optionalAddress.town}
              </ListItemText>
              <IconButton
                onClick={() => {
                  dispatch(setEditOptionalAddress(true));
                }}
                className={classes.editIcon}
              >
                <EditIcon></EditIcon>
              </IconButton>
              <Typography className={classes.infoAddress} variant="body1">
                {optionalAddress.appartment} {optionalAddress.street},{" "}
                {optionalAddress.zip}, {optionalAddress.country},{" "}
                {optionalAddress.state}
              </Typography>

              {moreInfoIndex === optionalAddress.zip ? (
                <Typography className={classes.infoAddress}>
                  {optionalAddress.firstName} {optionalAddress.lastName},{" "}
                  {optionalAddress.phoneNumber}, {optionalAddress.email}
                </Typography>
              ) : (
                <IconButton
                  onClick={() => handleMoreInfoClick(optionalAddress.zip)}
                  className={classes.iconButtonMore}
                >
                  <MoreHorizIcon />
                </IconButton>
              )}
            </Box>
            <Radio
              className={classes.radio}
              checked={isCheckedOptional}
              value={optionalAddress.street}
              color="success"
              name="radio-buttons"
            />
          </MenuItem>
        )}
      </>
    );
  };
  return (
    <>
      {isShowModal && <PaymentModal />}
      <Container className={classes.Wrapper}>
        <Container className={classes.BreadcrumbsWrapper}>
          <Breadcrumbs
            aria-label="breadcrumb"
            style={{ margin: "115px auto 0 auto", maxWidth: "1200px" }}
          >
            <Link className={classes.Link} to="/">
              Home
            </Link>
            <Typography style={{fontWeight: 700}} color="textPrimary">Checkout</Typography>
          </Breadcrumbs>
        </Container>
        <Container className={classes.container}>
          <Container className={classes.billingAddress}>
            {mainAddress ? (
              <>
                <Typography className={classes.title} variant="h1">
                  Your Chosen Address
                </Typography>
                <MenuList>
                  <MenuItem
                    onClick={() => handleAddressChange(mainAddress)}
                    className={
                      isCheckedMain
                        ? classes.menuItemSelected
                        : classes.menuItemAddress
                    }
                    key={mainAddress._id}
                  >
                    <Box className={classes.boxAddress}>
                      <ListItemText className={classes.titleAddress}>
                        {mainAddress.town}
                      </ListItemText>
                      <Typography
                        className={classes.infoAddress}
                        variant="body1"
                      >
                        {mainAddress.appartment} {mainAddress.street},{" "}
                        {mainAddress.city} {mainAddress.zip},{" "}
                        {mainAddress.country}, {mainAddress.state}
                      </Typography>
                      {moreInfoIndex === mainAddress.zip ? (
                        <Typography className={classes.infoAddress}>
                          {mainAddress.firstName} {mainAddress.lastName},{" "}
                          {mainAddress.phoneNumber}, {mainAddress.email}
                        </Typography>
                      ) : (
                        <IconButton
                          onClick={() => handleMoreInfoClick(mainAddress.zip)}
                          className={classes.iconButtonMore}
                        >
                          <MoreHorizIcon />
                        </IconButton>
                      )}
                    </Box>
                    <Radio
                      color="success"
                      checked={isCheckedMain}
                      className={classes.radioAddress}
                    />
                  </MenuItem>
                </MenuList>
                <Box className={classes.boxCheckbox}>
                  <Checkbox
                    checked={showAddresses}
                    onClick={() => setIsShowAddresses(!showAddresses)}
                    color="success"
                  />
                  <Typography>
                    Show billing address or another your addresses?
                  </Typography>
                </Box>
                {showAddresses && (
                  <Box>
                    {editOptionalAddress ? (
                      <>
                        <Typography className={classes.title} variant="h1">
                          Billing Address
                        </Typography>
                        <BillingAddress />
                      </>
                    ) : (
                      Object.keys(optionalAddress).length > 0 && (
                        <>
                          <Typography className={classes.title} variant="h1">
                            Optional Address
                          </Typography>
                          <MenuList>{OPTIONALADDRESS()}</MenuList>
                        </>
                      )
                    )}
                    {notMainAddresses.length > 0 && (
                      <Box>
                        <Typography className={classes.title} variant="h1">
                          Your Another Addresses
                        </Typography>
                        <MenuList className={classes.menuListAddresses}>
                          {renderNotMainAddresses()}
                        </MenuList>
                      </Box>
                    )}
                  </Box>
                )}
              </>
            ) : (
              <>
                <Typography className={classes.title} variant="h1">
                  Billing Address
                </Typography>
                {editOptionalAddress && <BillingAddress />}
                {addresses.length > 0 ? (
                  <>
                    <Box className={classes.ship}>
                      <Checkbox
                        checked={showAddresses}
                        onClick={() => setIsShowAddresses(!showAddresses)}
                      />
                      <Typography className={classes.shipText} variant="body1">
                        Ship to a different address?
                      </Typography>
                    </Box>
                    {showAddresses && (
                      <MenuList className={classes.menuListAddresses}>
                        {renderAddresses()}
                        {Object.keys(optionalAddress).length > 0 &&
                          !editOptionalAddress &&
                          OPTIONALADDRESS()}
                      </MenuList>
                    )}
                  </>
                ) : (
                  <MenuList className={classes.menuListAddresses}>
                    {Object.keys(optionalAddress).length > 0 &&
                      !editOptionalAddress &&
                      OPTIONALADDRESS()}
                  </MenuList>
                )}
              </>
            )}
          </Container>

          <Container className={classes.containerOrder}>
            <Typography className={classes.title} variant="h1">
              Your Order
            </Typography>
            <PaymentItems />
            <Box className={classes.couponText}>
              Have a coupon code?{" "}
              <Link to="/cart" className={classes.couponLink}>
                Click here
              </Link>
            </Box>
            <MenuList className={classes.couponBlock}>
              <MenuItem className={classes.couponInfo}>
                <Typography className={classes.couponInfoText} variant="body1">
                  Subtotal
                </Typography>
                <Typography className={classes.couponInfoPrice} variant="body1">
                  ${subtotal.toFixed(2)}
                </Typography>
              </MenuItem>
              <MenuItem className={classes.couponInfo}>
                <Typography className={classes.couponInfoText} variant="body1">
                  Coupon Discount
                </Typography>
                <Typography className={classes.couponInfoText} variant="body1">
                (-) {((subtotal * discount) / 100).toFixed(2)}
                </Typography>
              </MenuItem>
              <MenuItem className={classes.couponInfo}>
                <Typography className={classes.couponInfoText} variant="body1">
                  Shipping
                </Typography>
                <Typography className={classes.couponInfoPrice} variant="body1">
                  $16.00
                </Typography>
              </MenuItem>
            </MenuList>
            <Box className={classes.couponTotalInfo}>
              <Box className={classes.couponTotal}>
                <Typography className={classes.totalText} variant="body1">
                  Total
                </Typography>
                <Typography className={classes.productPrice} variant="body1">
                  ${total.toFixed(2)}
                </Typography>
              </Box>
            </Box>
            <Container className={classes.containerPaymentMethod}>
              <Typography className={classes.titleOfPart} variant="body1">
                Payment Method
              </Typography>
              <MenuList className={classes.menuList}>
                <MenuItem className={classes.menuItemMethod}>
                  <Radio
                    checked={paymentMethod === "GooglePay"}
                    onChange={(event) =>
                      dispatch(setPaymentMethod(event.target.value))
                    }
                    value="GooglePay"
                    color="success"
                    name="radio-buttons"
                  />
                  <CardMedia
                    className={classes.googlePayPaypal}
                    component="img"
                    src="/google-pay-primary-logo-logo-svgrepo-com.svg"
                  />
                </MenuItem>
                <MenuItem className={classes.menuItemMethod}>
                  <Radio
                    checked={paymentMethod === "PayPal"}
                    onChange={(event) =>
                      dispatch(setPaymentMethod(event.target.value))
                    }
                    value="PayPal"
                    color="success"
                    name="radio-buttons"
                  />
                  <CardMedia
                    className={classes.googlePayPaypal}
                    component="img"
                    src="/paypal-svgrepo-com.svg"
                  />
                </MenuItem>
                <MenuItem className={classes.menuItemMethod}>
                  <Radio
                    checked={paymentMethod === "Cash on delivery"}
                    onChange={(event) =>
                      dispatch(setPaymentMethod(event.target.value))
                    }
                    value="Cash on delivery"
                    color="success"
                    name="radio-buttons"
                  />
                  <WalletIcon color="success" />
                  <Typography className={classes.nameMethod}>
                    Cash on delivery
                  </Typography>
                </MenuItem>
              </MenuList>
              {Object.keys(address).length > 0 ? (
                <>
                  {paymentMethod === "PayPal" ? (
                    <PayPalButton />
                  ) : paymentMethod === "GooglePay" ? (
                    <GooglePayButton />
                  ) : paymentMethod === "Cash on delivery" ? (
                    <Button
                      className={classes.btn}
                      onClick={() => {
                        dispatch(setIsShowModal(true));
                        dispatch(setIsPaymentSuccess(true));
                      }}
                    >
                      Place Order
                    </Button>
                  ) : null}
                </>
              ) : (
                <Typography className={classes.msgAddress}>
                  You need to specify the address
                </Typography>
              )}
            </Container>
          </Container>
        </Container>
      </Container>
    </>
  );
}
