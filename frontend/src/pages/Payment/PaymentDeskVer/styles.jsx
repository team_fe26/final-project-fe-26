import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  Wrapper: {
    padding: 0,
    maxWidth: "1200px",
    margin: "0 auto",
    "@media (max-width: 1200px)": {
    padding: "0 25px 0 25px"
    },
  },
  container: {
    display: "flex",
    flexDirection: "row",
    maxWidth: "1200px",
    gap: "8%",
    padding: "0",
    minWidth: '768px',
    marginTop: "20px",
   "@media (max-width: 820px)": {
    gap: "5%",
    },
  },
  BreadcrumbsWrapper: {
    padding: 0,
},
IconContainer: {
    paddingTop: "38px",
    display: "flex",
    position: "absolute",
    zIndex: 1000,
    top: -20,
    width: "100%",
    justifyContent: "space-between",
    "@media (min-width: 768px)": {
        display: "none",
    },
},
BackButton: {
    width: "35px",
    height: "35px",
    background: "#f0f0f0",
    marginLeft: "28px",
},
Link: {
    textDecoration: "none",
    color: "inherit",
    "&:hover": {
        textDecoration: "underline",
    },
},
  billingAddress: {
    minWidth: '450px',
    padding: 0
  },
  title: {
    fontSize: "17px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "700",
    color: "#3D3D3D",
    marginBottom: "17px",
    
  },
  ship: {
    marginTop: "32px",
    display: "flex",
    alignItems: "center",
  },

  shipText: {
    fontSize: "15px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
    color: "#3D3D3D",
  },

  subTitle: {
    marginTop: "22px",
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
    color: "#3D3D3D",
  },
  containerOrder: {
    padding: 0,
    minWidth: '330px'
  },
  orderInfo: {
    "&::after": {
      content: '""',
      display: "block",
      border: "0.3px solid #46A35880",
    },
  },
  boxAddress: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    gap: "10px",
    flexWrap: 'wrap',
    wordWrap: 'break-word',
    overflowWrap: 'break-word',
  },
  menuListAddresses: {
    display: "flex",
    gap: "20px",
    flexDirection: "column",
    position: "relative",
    flexWrap: 'wrap'
  },
  titleAddress: {
    color: "rgba(61, 61, 61, 1)",
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
  },
  infoAddress: {
    color: "#727272",
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
    wordWrap: "break-word",
    overflowWrap: "break-word",
    whiteSpace: "normal",
    wordBreak: "break-word",
  },
  info: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: "11px",
  },

  productDetails: {
    marginTop: "21px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },

  productItemInfo: {
    display: "flex",
    flexDirection: "column",
  },
  productItem: {
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
    color: "#3D3D3D",
  },

  productNumber: {
    fontSize: "14px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
    color: "#727272",
    whiteSpace: "nowrap",
  },

  productPrice: {
    fontSize: "18px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "700",
    color: "#46A358",
  },

  btn: {
    width: "100%",
    marginTop: "35px",
    borderRadius: "3px",
    backgroundColor: "rgba(70, 163, 88, 1)",
    color: "#FFFFFF",
    marginBottom: "210px",
    textTransform: "capitalize",
    fontWeight: "700",
    fontSize: "15px",
    height: "40px",
    "&:hover": {
      backgroundColor: "#3A8C47",
    },
  },

  couponText: {
    marginTop: "17px",
    marginBottom: "19px",
    fontSize: "14px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
    color: "#3D3D3D",
    textAlign: "right",
    "@media (max-width: 900px)": {
        width: "65%"
    },
  },

  couponInfo: {
    display: "flex",
    justifyContent: "space-between",
  },

  couponTotalInfo: {
    width: "72%",
    marginRight: "0",
    marginLeft: "auto",
    "@media (max-width: 900px)": {
      marginLeft: 0,
    },

    marginTop: "17px",
    "&::before": {
      content: '""',
      display: "block",
      border: "0.3px solid #46A35880",
    },
  },

  couponTotal: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: "16px",
  },

  infoText: {
    fontSize: "15px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
    color: "#3D3D3D",
  },

  couponInfoPrice: {
    fontSize: "18px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
    color: "#3D3D3D",
  },

  totalText: {
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "700",
    color: "#3D3D3D",
  },

  titleOfPart: {
    fontSize: "17px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "700",
    color: "#3D3D3D",
  },
  editIcon: {
    position: 'absolute',
    right: '10px',
    top: '0px'
  },
  menuItemMethod: {
    border: "1px solid #EAEAEA",
    backgroundColor: "rgba(251, 251, 251, 1)",
    borderRadius: "3px",
    marginBottom: "15px",
    display: 'flex',
    alignItems: 'center',
    gap: '12px'
  },

  couponBlock: {
    width: "75%",
    marginRight: "0",
    marginLeft: "auto",
    "@media (max-width: 900px)": {
      marginLeft: "0",
    },
  },

  menuList: {
    marginTop: "19px",
  },

  containerPaymentMethod: {
    width: "80%",
    marginRight: "0",
    marginLeft: "auto",
    marginTop: "47px",
    "@media (max-width: 900px)": {
      marginLeft: "0",
      padding: 0
    },
  },

  couponLink: {
    fontSize: "14px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
    color: "#46A358",
    cursor: 'pointer',
    textDecoration: "none",
  },

  shippingLink: {
    display: "block",
    textAlign: "end",
    fontSize: "12px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
    color: "#46A358",
    textDecoration: "none",
  },
  radioAddress:{
    marginRight: 0,
    marginLeft: 'auto'
  },
  boxCheckbox:{
    display: 'flex',
    alignItems: 'center',
    marginTop: '20px',
    marginBottom: '30px'
  },
 googlePayPaypal:{
  width: '60px',
  height: '30px',
 },
 nameMethod:{
  color: '#A0A0A0'
 },
 iconButtonMore: {
  position: "absolute",
  right: "13px",
  bottom: "0px",
},
menuItemAddress: {
  width: "100%",
  borderRadius: "14px",
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  cursor: 'default',
  backgroundColor: "#FFFFFF",
  boxShadow: "0px 20px 20px 0px #C8C8C840",
  padding: "20px 11px 20px",
},

menuItemSelected: {
  width: "100%",
  borderRadius: "14px",
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  backgroundColor: "#F9F9F9",
  padding: "20px 11px 20px",
},
msgAddress:{
  color: 'red',
  fontWeight: 700,
  textTransform: 'uppercase',
  textAlign: 'center'
}
});
