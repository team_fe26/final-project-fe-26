import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  field: {
    marginBottom: "15px",
    marginTop: "10px",
    color: "rgba(165, 165, 165, 1)",
    fontFamily: '"Montserrat", sans-serif',
    fontSize: "15px",
    fontWeight: 400,
    borderRadius: "10px",
    backgroundColor: "transparent",
    width: "92%",
    height: "50px",
    outline: "none",
    paddingLeft: "20px",
    border: "2px solid rgba(165, 165, 165, 1)",
    "&:hover": {
      border: "2px solid rgba(70, 163, 88, 1)",
    },
    "&:focus": {
      border: "2px solid rgba(70, 163, 88, 1)",
      backgroundColor: "rgba(255, 255, 255, 1)",
    },
  },

  errorMessage: {
    color: "red",
    position: "relative",
    top: -10,
    fontSize: "14px",
  },
  container: {
    position: "relative"
  }, 
  btnBack: {
    position: "absolute",
    width: "35px",
    height: "35px",
    backgroundColor: "rgba(243, 243, 243, 1)",
    border: "none",
    borderRadius: "50%",
    top: "-5px",
    left: "5%",
    cursor: "pointer",
  },

  title: {
    fontSize: "20px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
    color: "#3D3D3D",
    marginBottom: "17px",
    marginTop: "33px",
    textAlign: "center",
  },

  form: {
    marginTop: "30px",
  },

  label: {
    color: "#3D3D3D",
    fontSize: "15px",
    fontFamily: "'Montserrat', sans-serif",
    fontWeight: "400",
  },

  star: {
    color: "#F03800",
    fontSize: "22px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
  },

  btn: {
    borderRadius: "40px",
    backgroundImage:
      "linear-gradient(to right, rgba(70, 163, 88, 1), rgba(70, 163, 88, 0.8))",
    color: "#FFFFFF",
    width: "100%",
    marginTop: "30px",
    marginBottom: "32px",
    textTransform: "capitalize",
    fontWeight: "700",
    fontSize: "15px",
    height: "60px",
  },
});
