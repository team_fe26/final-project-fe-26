import { Button, Typography, Box, Container, Checkbox } from "@mui/material";
import { Formik, Form, Field, ErrorMessage } from "formik";
import IconButton from "@mui/material/IconButton";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import * as Yup from "yup";
import { Styles } from "./styles";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { useState } from "react";
import { setOptionalAddress } from "../../../redux/payment.slice/payment.slice";
const initialValues = {
  firstName: "",
  lastName: "",
  country: "",
  town: "",
  street: "",
  state: "",
  zip: "",
  email: "",
  appartment: "",
  phoneNumber: "",
  notes: "",
};
const checkEmailExists = async (email) => {
  try {
    console.log(email);
    
    const response = await fetch(
      `https://greenshop-server.onrender.com/api/check/email`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json", 
        },
        body: JSON.stringify({ email }), 
      }
    );
    if (response.ok) {
      const data = await response.json();
      return data.valid;
    }
    throw new Error("Failed to check email");
  } catch (error) {
    console.error("Error checking email:", error);
    return false;
  }
};

const validationSchema = Yup.object({
  firstName: Yup.string().required("First Name is required*"),
  lastName: Yup.string().required("Last Name is required*"),
  country: Yup.string().required("Country is required*"),
  town: Yup.string().required("Town is required*"),
  street: Yup.string().required("Street address is required*"),
  state: Yup.string(),
  zip: Yup.string()
  .matches(/^\d{5,8}$/, "Invalid zip code")
  .required("Zip is required*"),
  appartment: Yup.string(),
  email: Yup.string()
    .email("Invalid email*")
    .required("Email is required")
    .test("check-email", "Email not valid", async (value) => {
      if (!value) return true;
      const exists = await checkEmailExists(value);
      return exists;
    }),
  phoneNumber: Yup.string()
    .min(10, "Phone number should be 10 characters")
    .required("Phone number is required"),
  notes: Yup.string(),
});

export default function BillingAddressForm() {
  const dispatch = useDispatch();
  const classes = Styles();
  const navigate = useNavigate();
  const location = useLocation();
  const [mainAddress, setMainAddress] = useState(false);
  const referrer = location.state?.from;
  const handleRadioChange = () => {
    const mainAddressSetUp = !mainAddress;
    setMainAddress(mainAddressSetUp);
    console.log("Updated mainAddress:", mainAddressSetUp);
  };

  const onSubmit = async (values) => {
    if (referrer === "checkout") {
      dispatch(
        setOptionalAddress({
          firstName: values.firstName,
          lastName: values.lastName,
          country: values.country,
          town: values.town,
          street: values.street,
          ...(values.state && { state: values.state }),
          appartment: values.appartment,
          zip: values.zip,
          email: values.email,
          phoneNumber: values.phoneNumber,
          ...(values.notes && { notes: values.notes }),
        })
      );
    } else if (referrer === "user") {
      try {
        const token = JSON.parse(localStorage.getItem("token")) || "";
        const addressData = {
          firstName: values.firstName,
          lastName: values.lastName,
          phoneNumber: values.phoneNumber,
          email: values.email,
          country: values.country,
          street: values.street,
          zip: values.zip,
          town: values.town,
          main: mainAddress,
        };

        if (values.appartment) {
          addressData.appartment = values.appartment;
        }

        if (values.state) {
          addressData.state = values.state;
        }

        const response = await fetch(
          "https://greenshop-server.onrender.com/api/customer/address/add",
          {
            method: "POST",
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
            },
            body: JSON.stringify(addressData),
          }
        );
        if (!response.ok) {
          const data = await response.json();
          alert(`Error: ${data.error}`);
          return;
        }
        const data = await response.json();
        navigate("/user");
        console.log(data.message);
      } catch (error) {
        console.error("Fetch error:", error);
      }
    }
  };
  return (
    <>
      <Container className={classes.container}>
        <IconButton className={classes.btnBack} onClick={() => navigate(-1)}>
          <ArrowBackIcon style={{ color: "#000" }}  />
        </IconButton>
        <Typography  className={classes.title} variant="h1">
          Billing Address
        </Typography>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
        >
          <Form className={classes.form}>
            <div>
              <label className={classes.label} htmlFor="firstName">
                First Name <span className={classes.star}>*</span>
              </label>
              <Field className={classes.field} type="text" name="firstName">
                {({ field, form }) => (
                  <input
                    className={classes.field}
                    {...field}
                    placeholder="Enter your first name"
                    onChange={(e) => {
                      const name = e.target.value.replace(
                        /[^a-zA-Zа-яА-Я]/g,
                        ""
                      );
                      form.setFieldValue("firstName", name);
                    }}
                  />
                )}
              </Field>
              <ErrorMessage
                className={classes.errorMessage}
                name="firstName"
                component="div"
              />
            </div>
            <div>
              <label className={classes.label} htmlFor="lastName">
                Last Name <span className={classes.star}>*</span>
              </label>
              <Field className={classes.field} type="text" name="lastName">
                {({ field, form }) => (
                  <input
                    className={classes.field}
                    {...field}
                    placeholder="Enter your last name"
                    onChange={(e) => {
                      const name = e.target.value.replace(
                        /[^a-zA-Zа-яА-Я]/g,
                        ""
                      );
                      form.setFieldValue("lastName", name);
                    }}
                  />
                )}
              </Field>
              <ErrorMessage
                className={classes.errorMessage}
                name="lastName"
                component="div"
              />
            </div>
            <div>
              <label className={classes.label} htmlFor="country">
                Country <span className={classes.star}>*</span>
              </label>
              <Field className={classes.field} type="text" name="country">
                {({ field, form }) => (
                  <input
                    className={classes.field}
                    {...field}
                    placeholder="Enter your country"
                    onChange={(e) => {
                      const Country = e.target.value.replace(
                        /[^a-zA-Zа-яА-Я]/g,
                        ""
                      );
                      form.setFieldValue("country", Country);
                    }}
                  />
                )}
              </Field>
              <ErrorMessage
                className={classes.errorMessage}
                name="country"
                component="div"
              />
            </div>
            <div>
              <label className={classes.label} htmlFor="town">
                Town <span className={classes.star}>*</span>
              </label>
              <Field className={classes.field} type="text" name="town">
                {({ field, form }) => (
                  <input
                    className={classes.field}
                    {...field}
                    placeholder="Enter your town"
                    onChange={(e) => {
                      const Town = e.target.value.replace(
                        /[^a-zA-Zа-яА-Я]/g,
                        ""
                      );
                      form.setFieldValue("town", Town);
                    }}
                  />
                )}
              </Field>
              <ErrorMessage
                className={classes.errorMessage}
                name="town"
                component="div"
              />
            </div>
            <div>
              <label className={classes.label} htmlFor="street">
                Street <span className={classes.star}>*</span>
              </label>
              <Field
                className={classes.field}
                type="text"
                id="street"
                name="street"
                placeholder="Enter your street"
              />
              <ErrorMessage
                className={classes.errorMessage}
                name="street"
                component="div"
              />
            </div>
            <div>
              <label className={classes.label} htmlFor="state">
                State
              </label>
              <Field
                className={classes.field}
                type="text"
                id="state"
                name="state"
                placeholder="Enter your state"
              />
              <ErrorMessage
                className={classes.errorMessage}
                name="state"
                component="div"
              />
            </div>
            <div>
              <label className={classes.label} htmlFor="zip">
                Zip <span className={classes.star}>*</span>
              </label>
              <Field className={classes.field} type="text" name="zip">
                {({ field, form }) => (
                  <input
                    className={classes.field}
                    {...field}
                    placeholder="Enter your zip"
                    onChange={(e) => {
                      const Zip = e.target.value.replace(/[^\d]/g, "");
                      if (Zip.length <= 8) {
                        form.setFieldValue("zip", Zip);
                      }
                    }}
                  />
                )}
              </Field>
              <ErrorMessage
                className={classes.errorMessage}
                name="zip"
                component="div"
              />
            </div>
            <div>
              <label className={classes.label} htmlFor="appartment">
                Appartment
              </label>
              <Field
                className={classes.field}
                type="text"
                id="appartment"
                name="appartment"
                placeholder="Enter your appartment"
              />
              <ErrorMessage
                className={classes.errorMessage}
                name="appartment"
                component="div"
              />
            </div>
            <div>
              <label className={classes.label} htmlFor="email">
                Email <span className={classes.star}>*</span>
              </label>
              <Field
                className={classes.field}
                type="email"
                id="email"
                name="email"
                placeholder="Enter your email"
              />
              <ErrorMessage
                className={classes.errorMessage}
                name="email"
                component="div"
              />
            </div>

            <div>
              <label className={classes.label} htmlFor="phoneNumber">
                Phone Number <span className={classes.star}>*</span>
              </label>
              <Field className={classes.field} type="text" name="phoneNumber">
                {({ field, form }) => (
                  <input
                    className={classes.field}
                    {...field}
                    placeholder="(###)###-##-##"
                    onChange={(e) => {
                      const phoneNumber = e.target.value.replace(/[^\d]/g, "");
                      if (phoneNumber.length <= 10) {
                        form.setFieldValue("phoneNumber", phoneNumber);
                      }
                    }}
                  />
                )}
              </Field>
              <ErrorMessage
                className={classes.errorMessage}
                name="phoneNumber"
                component="div"
              />
            </div>
            {referrer === "checkout" ? (
  <div>
    <label className={classes.label} htmlFor="notes">
      Notes
    </label>
    <Field
      className={classes.field}
      type="text"
      id="notes"
      name="notes"
      placeholder="Enter notes"
    />
    <ErrorMessage
      className={classes.errorMessage}
      name="notes"
      component="div"
    />
  </div>
) : (
  <Box className={classes.shipRadio}>
                  <div>
                    <Checkbox
                      checked={mainAddress}
                      onChange={handleRadioChange}
                      color="success"
                      defaultChecked
                    >
                      Shipping Address
                    </Checkbox>
                    <Typography className={classes.p} variant="body1">
                      Shipping Address
                    </Typography>
                  </div>
                </Box>
)}
            <Button className={classes.btn} type="submit">
              Save
            </Button>
          </Form>
        </Formik>
      </Container>
    </>
  );
}
