import { createUseStyles } from "react-jss";
import { styled } from "@mui/system";

export const Styles = createUseStyles({
  container: {
    minWidth: "366px",
    maxWidth: "766px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    position: "relative",
  },
  title: {
    fontSize: "20px",
    color: "rgba(61, 61, 61, 1)",
    fontFamily: '"Montserrat", sans-serif',
    fontWeight: "500",
    marginTop: "30px",
  },
  tilteOfPart: {
    fontSize: "16px",
    color: "rgba(61, 61, 61, 1)",
    fontFamily: '"Montserrat", sans-serif',
    fontWeight: "500",
    marginTop: "38px",
    marginLeft: "3%",
    marginRight: "auto",
  },
  containerCategories: {
    marginTop: "20px",
    display: "flex",
    gap: "10px",
    flexWrap: "wrap",
    width: '100%',
    justifyContent: "space-around",
  },
  item:{
    borderRadius: "14px",
    width: "40%",
    height: "60px",
    fontSize: "16px",
    color: "rgba(61, 61, 61, 1)",
    fontFamily: '"Montserrat", sans-serif',
    display: "flex",
    justifyContent: "center",
    cursor: "pointer",
    alignItems: "center",
  },
  containerPrice: {
    marginTop: "20px",
    display: "flex",
    justifyContent: "space-between",
    gap: "50px",
  },
  textField: {
    "& .MuiOutlinedInput-root": {
      borderRadius: "14px",
      height: "40px",
      border: "1px",
      "&:hover fieldset": {
        borderColor: "rgba(70, 163, 88, 1)",
      },
      "&.Mui-focused fieldset": {
        borderColor: "rgba(70, 163, 88, 1)",
      },
    },
    "& .MuiInputBase-input": {
      textAlign: "center",
      color: "rgba(61, 61, 61, 1)",
      fontFamily: '"Montserrat", sans-serif',
      fontSize: "14px",
      fontWeight: 400,
    },
  },

  containerSlider: {
    marginTop: "30px",
    width: "80%",
  },
  slider: {
    color: "rgba(70, 163, 88, 1)",
  },
  containerSizes: {
    marginTop: "20px",
    display: "flex",
    gap: "10px",
    flexWrap: "wrap",
    width: '100%',
    justifyContent: "space-around",
  },
  isSale: (sale) => ({
    backgroundColor:
      sale == true ? "rgba(70, 163, 88, 0.7)" : "rgba(255, 255, 255, 1)",
    border:
      sale === true
        ? "3px solid rgba(70, 163, 88, 1)"
        : "3px solid rgba(114, 114, 114, 0.2)",
    color: sale === true ? "rgba(255, 255, 255, 1)" : "rgba(114, 114, 114, 1)",
  }),
  buttonFilter: {
    marginTop: "80px",
    backgroundImage:
      "linear-gradient(to right, rgba(70, 163, 88, 1), rgba(70, 163, 88, 0.8))",
    color: "rgba(255, 255, 255, 1)",
    fontSize: "15px",
    fontFamily: '"Montserrat", sans-serif',
    borderRadius: "40px",
    fontWeight: "700",
    textTransform: "capitalize",
    height: "60px",
    width: "90%",
  },
  btnBack:{
    position: 'absolute',
    top: '20px',
    left: '20px',
    backgroundColor: "rgba(243, 243, 243, 1)",
    width: "35px",
    height: "35px",
  }
});

export const BtnBack = styled("div")({
  position: "absolute",
  width: "35px",
  height: "35px",
  backgroundColor: "rgba(243, 243, 243, 1)",
  border: "none",
  borderRadius: "50%",
  top: "15px",
  left: "5%",
  cursor: "pointer",
  "&::before": {
    content: '""',
    width: "13px",
    position: "absolute",
    height: "2px",
    display: "block",
    backgroundColor: "rgba(114, 114, 114, 1)",
    transform: "rotate(30deg)",
    bottom: "14px",
    right: "12px",
    borderRadius: "15px",
  },
  "&::after": {
    content: '""',
    width: "13px",
    position: "absolute",
    height: "2px",
    display: "block",
    backgroundColor: "rgba(114, 114, 114, 1)",
    transform: "rotate(150deg)",
    top: "14px",
    right: "12px",
    borderRadius: "15px",
  },
});

export const BtnSale = styled("button")({
  borderRadius: "14px",
    width: "160px",
    height: "60px",
    fontSize: "18px",
    fontFamily: '"Montserrat", sans-serif',
    display: "flex",
    justifyContent: "center",
    cursor: "pointer",
    alignItems: "center",
    marginLeft: "6%",
    marginRight: "auto",
    marginTop: "20px",
    textTransform: "capitalize",
})
export const DiscountLabelDesktop = styled("button")({
  position: "absolute",
  width: "35px",
  height: "35px",
  backgroundColor: "rgba(243, 243, 243, 1)",
  border: "none",
  borderRadius: "50%",
  top: "15px",
  left: "5%",
  cursor: "pointer",
  "&::before": {
    content: '""',
    width: "13px",
    position: "absolute",
    height: "2px",
    display: "block",
    backgroundColor: "rgba(114, 114, 114, 1)",
    transform: "rotate(30deg)",
    bottom: "14px",
    right: "12px",
    borderRadius: "15px",
  },
  "&::after": {
    content: '""',
    width: "13px",
    position: "absolute",
    height: "2px",
    display: "block",
    backgroundColor: "rgba(114, 114, 114, 1)",
    transform: "rotate(150deg)",
    top: "14px",
    right: "12px",
    borderRadius: "15px",
  },
});