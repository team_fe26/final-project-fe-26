import {
  Container,
  Typography,
  Button,
  Box,
  Slider,
  MenuList,
  MenuItem,
  TextField,
} from "@mui/material";
import {
  setRangePrice,
  setCategory,
  setSize,
  setMaxPrice,
  setMinPrice,
  setSale,
  setFilterProducts,
  setEmptyButFilter,
} from "../../redux/filter.slice/filter.slice";
import { useDispatch, useSelector } from "react-redux";
import { BtnSale, Styles } from "./styles";
import { useEffect } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import IconButton from "@mui/material/IconButton";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { fetchSizes } from "../../redux/sizes.slice/sizes.slice";
import { fetchCategories } from "../../redux/categories.slice/categories.slice";
export default function Filters() {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { categories, sizes, sale, rangePrice } = useSelector(
    (state) => state.filters
  );
  const { allProducts, status } = useSelector((state) => state.products);
  const classes = Styles(sale, sizes);
  const {CATEGORIES}= useSelector((state)=> state.categories)
  const {SIZES} =useSelector((state)=>state.sizes)
useEffect(()=>{
  dispatch(fetchSizes())
  dispatch(fetchCategories())
}, [dispatch])

const renderMenuItems = (items, selectedItems, classes, onClick) => {
  return items.map((item) => (
    <MenuItem
      key={item.name}
      value={item.name}
      className={classes.item}
      style={{
        boxShadow:
          selectedItems.includes(item.name) === true
            ? "none"
            : "0 20px 20px rgba(200, 200, 200, 0.25)",
        backgroundColor:
          selectedItems.includes(item.name) === true
            ? "rgba(114, 114, 114, 0.4)"
            : "rgba(255, 255, 255, 1)",
      }}
      onClick={() => onClick(item)}
    >
      {item.name}
    </MenuItem>
  ));
};

  useEffect(() => {
    const params = Object.fromEntries([...searchParams]);
    if (Object.keys(params).length > 0) {
    if (params.category) {
      const categoriesArray = params.category.split(",");
      categoriesArray.forEach((category) => dispatch(setCategory(category)));
    }

    if (params.size) {
      const sizesArray = params.size.split(",");
      sizesArray.forEach((size) => dispatch(setSize(size)));
    }

    if (params.sale !== undefined) {
      dispatch(setSale(params.sale === "true"));
    }

    if (params.minPrice && params.maxPrice) {
      dispatch(setRangePrice([Number(params.minPrice), Number(params.maxPrice)]));
    }
    filterProducts(params);}
    else{
      dispatch(setFilterProducts([]));
    }

  }, [searchParams, dispatch]);

  const filterProducts = (params) => {
    if (status === "success") {
      const sizeFilter = params.size ? params.size.split(',') : sizes;
      const categoryFilter = params.category ? params.category.split(',') : categories;
      const minPrice = params.minPrice ? Number(params.minPrice) : rangePrice[0];
      const maxPrice = params.maxPrice ? Number(params.maxPrice) : rangePrice[1];
      const saleFilter = params.sale === "true" || sale;

      const filteredProducts = allProducts.filter((product) => {
        const sizeMatch = sizeFilter.length === 0 || sizeFilter.includes(product.size);
        const categoryMatch = categoryFilter.length === 0 || categoryFilter.includes(product.categories);
        const priceMatch = product.currentPrice >= minPrice && product.currentPrice <= maxPrice;

        if (sizeMatch && categoryMatch && priceMatch) {
          return saleFilter ? product.previousPrice > product.currentPrice : true;
        }
        return false;
      });

      dispatch(setFilterProducts(filteredProducts));
      dispatch(setEmptyButFilter(filteredProducts.length === 0));
    }
  };
  const applyFilters = () => {
    const params = {};
    if (categories.length > 0) {
      params.category = categories.join(",");
    }
    if (sizes.length > 0) {
      params.size = sizes.join(",");
    }
    if (sale) {
      params.sale = "true";
    } else {
      params.sale = "false";
    }
    params.minPrice = rangePrice[0];
    params.maxPrice = rangePrice[1];

    const queryString = new URLSearchParams(params).toString();
    navigate(`/?${queryString}`); 
    filterProducts(params);
  };
  const navigateToHome = ()=>{
    const queryString = searchParams.toString();
    navigate(`/?${queryString}`);
  }
  return (
    <Container className={classes.container}>
      <IconButton className={classes.btnBack} onClick={() => navigateToHome()}>
        <ArrowBackIcon style={{ color: "#000" }}/>
      </IconButton>
      <Typography className={classes.title} variant="h1">
        Filters
      </Typography>
      <Typography className={classes.tilteOfPart} variant="h1">
        Categories
      </Typography>
      <MenuList className={classes.containerCategories}>
      {renderMenuItems(CATEGORIES, categories, classes, (category) => dispatch(setCategory(category.name)))}
      </MenuList>
      <Typography className={classes.tilteOfPart} variant="h1">
        Price Range
      </Typography>
      <Container className={classes.containerPrice}>
        <TextField
          placeholder={`${rangePrice[0]}`}
          value={rangePrice[0]}
          className={classes.textField}
          onChange={(event) => {
            dispatch(setMinPrice(event.target.value));
          }}
        />
        <TextField
          placeholder={`${rangePrice[1]}`}
          value={rangePrice[1]}
          className={classes.textField}
          onChange={(event) => {
            dispatch(setMaxPrice(event.target.value));
          }}
        />
      </Container>
      <Box className={classes.containerSlider}>
        <Slider
          min={4.99}
          max={866.99}
          className={classes.slider}
          value={rangePrice}
          onChange={(event, newValue) => {
            dispatch(setRangePrice(newValue));
          }}
          valueLabelDisplay="auto"
        />
      </Box>
      <Typography className={classes.tilteOfPart} variant="h1">
        Sizes
      </Typography>
      <MenuList className={classes.containerSizes}>
      {renderMenuItems(SIZES, sizes, classes, (size) => dispatch(setSize(size.name)))}
      </MenuList>
      <Typography className={classes.tilteOfPart} variant="h1">
        Show only
      </Typography>
      <BtnSale
        onClick={() => {
          const reverseSale = !sale
          dispatch(setSale(reverseSale));
        }}
        className={`${classes.sale} ${classes.isSale}`}
      >
        Sale
      </BtnSale>
      <Button
        onClick={() => {
          applyFilters();
        }}
        className={classes.buttonFilter}
      >
        Filter
      </Button>
    </Container>
  );
}
