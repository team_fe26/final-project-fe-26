import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import {
  Grid,
  Typography,
  Snackbar,
  Breadcrumbs,
  Box,
  IconButton,
} from "@mui/material";
import {
  DeleteOutline,
  ShoppingCart,
  ArrowBack,
} from "@mui/icons-material";
import { Styles } from "./styles";
import { ThemeProvider, createTheme, styled } from "@mui/material/styles";
import { useSelector, useDispatch } from "react-redux";
import {
  removeFromWishlist,
  replaceWishlistItems,
} from "../../redux/wishlist.slice/wishlist.slice";
import { addItemToCart } from "../../redux/cart.slice/cart.slice";
import Pagination from "@mui/material/Pagination";
import {
  PaperStyled,
  ItemContent,
  ItemImage,
  NameTypography,
  ItemDetails,
  PriceTypography,
  DeleteButton,
  AddToCartButton,
  CenteredContainer,
  BackButtonContainer,
  WishlistTitle,
} from "./styles";

import { setProductId } from "../../redux/product.slice/product.slice";
import ProductSlider from "../../components/SliderReletedProduct/SliderReletedProsuct"
const theme = createTheme({
  palette: {
    primary: {
      main: "#46A358",
    },
    secondary: {
      main: "#ff5722",
    },
  },
  typography: {
    h5: {
      fontWeight: 700,
    },
  },
});

const Wishlist = () => {
  const classes = Styles();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [addedItemId, setAddedItemId] = useState(null);
  const { wishlistId, wishlistItems } = useSelector((state) => state.wishlist);
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768);
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 8;

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = wishlistItems.slice(indexOfFirstItem, indexOfLastItem);

  const handlePageChange = (event, value) => {
    setCurrentPage(value);
  };

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768);
    };

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const handleDeleteItem = async (itemId) => {
    dispatch(removeFromWishlist(itemId));
  };

  const CustomPagination = styled(Pagination)(({ theme }) => ({
    display: "flex",
    justifyContent: "flex-end",
    marginTop: theme.spacing(2),
    "& .MuiPaginationItem-root": {
      borderRadius: "8px",
    },
  }));

  const handleAddToCart = async (id, quantity) => {
    if (quantity <= 0) {
      console.log("Product is not in the stock");
      return;
    }
    dispatch(addItemToCart(id));
    try {
      const productResponse = await fetch(
        "https://greenshop-server.onrender.com/api/product-reject-one",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ _id: id }),
        }
      );

      if (!productResponse.ok) {
        const productData = await productResponse.json();
        console.error("Error:", productData.error);
        return;
      }
      const productData = await productResponse.json();
      console.log(productData.message);
    } catch (error) {
      console.error("Error with product-reject-one:", error);
    }
  };

  const handleAddToCartFromWishlist = (product) => {
    handleAddToCart(product._id, product.quantity);
    setAddedItemId(product._id);
    setSnackbarOpen(true);
  };

  const handleCloseSnackbar = () => {
    setSnackbarOpen(false);
  };

  useEffect(() => {
    if (wishlistId && wishlistId.length >= 0) {
      const fetchProducts = async () => {
        try {
          const productFetchPromises = wishlistId.map(async (itemId) => {
            const response = await fetch(
              "https://greenshop-server.onrender.com/api/product",
              {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({ _id: itemId }),
              }
            );

            if (!response.ok) {
              const data = await response.json();
              console.error("Failed to fetch product data", data.error);
              return null;
            }

            const data = await response.json();
            return data;
          });

          const products = await Promise.all(productFetchPromises);
          const validProducts = products.filter((product) => product !== null);
          dispatch(replaceWishlistItems(validProducts));
        } catch (error) {
          console.error("Fetch error:", error);
        }
      };

      fetchProducts();
    }
  }, [wishlistId, dispatch]);

  return (
    <ThemeProvider theme={theme}>
      <div style={{ margin: "20px 24px" }}>
        {isMobile ? (
          <BackButtonContainer>
            <IconButton
              onClick={() => {
                navigate(-1);
              }}
              className={classes.BackButton}
            >
              <ArrowBack style={{ color: "#000" }} />
            </IconButton>
            <WishlistTitle  variant="h6">Wishlist</WishlistTitle>
          </BackButtonContainer>
        ) : (
          <Breadcrumbs
            aria-label="breadcrumb"
            style={{ margin: "115px auto 0 auto", maxWidth: "1200px" }}
          >
            <Link className={classes.Link} to="/">
              Home
            </Link>
            <Link className={classes.Link} to="/cart">
              Shopping Cart
            </Link>
            <Typography style={{fontWeight: 700}} color="textPrimary">Wishlist</Typography>
          </Breadcrumbs>
        )}
      </div>
      <CenteredContainer>
        <Grid
          container
          spacing={2}
          justifyContent="center"
          style={{ minWidth: "366px", paddingRight: "8px" }}
        >
          {currentItems && currentItems.length > 0 ? (
            currentItems.map((item, index) => (
              
              <Grid
                onClick={() => {
                  navigate("/product");
                  dispatch(setProductId(item._id));
                }}
                item
                xs={12}
                md={6}
                key={item._id || index}
              >
                <PaperStyled>
                  <ItemContent>
                    <ItemImage src={item.imageUrls[0]} alt={item.name} />
                    <ItemDetails>
                      <NameTypography variant="body1">
                        {item.name}
                      </NameTypography>
                      <Typography variant="body2">Size: {item.size}</Typography>
                      <PriceTypography variant="h6" color="primary">
                        ${item.currentPrice}
                      </PriceTypography>
                    </ItemDetails>
                    <AddToCartButton
                      onClick={(e) => {
                        e.stopPropagation();
                        handleAddToCartFromWishlist(item);
                      }}
                    >
                      <ShoppingCart />
                    </AddToCartButton>
                    <DeleteButton
                      onClick={(e) => {
                        e.stopPropagation();
                        handleDeleteItem(item._id);
                      }}
                    >
                      <DeleteOutline />
                    </DeleteButton>
                  </ItemContent>
                </PaperStyled>
                
              </Grid>
              
            ))
          ) : (
            
            <Grid item xs={12}>
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  color: "#46A358",
                  height: "50vh",
                  fontSize: "29px",
                  fontWeight: "600",
                }}
              >
                Your wishlist is empty!
              </Box>
             
            </Grid>
            
          )}
        </Grid>
        {wishlistItems.length > itemsPerPage && (
          <CustomPagination
            count={Math.ceil(wishlistItems.length / itemsPerPage)}
            page={currentPage}
            onChange={handlePageChange}
            color="primary"
          />
        )}
        <Snackbar
          open={snackbarOpen}
          autoHideDuration={2000}
          onClose={handleCloseSnackbar}
          message="Item added to cart"
        />
         {window.innerWidth >= 768 && (
            <ProductSlider />
        )}
        
      </CenteredContainer>
    </ThemeProvider>
  );
};

export default Wishlist;
