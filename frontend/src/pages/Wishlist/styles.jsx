import { styled } from '@mui/material/styles';
import { Paper, IconButton, Typography, Container, Box } from '@mui/material';
import { createUseStyles } from "react-jss";

export const HeaderContainer = styled('div')({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  padding: '10px 0',
});

export const PaperStyled = styled(Paper)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  cursor: 'pointer',
  justifyContent: 'space-between',
  marginBottom: theme.spacing(2),
  paddingRight: theme.spacing(2),
  boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)',
  borderRadius: '14px',
  borderColor: '#D3D3D3',
  borderWidth: '1px',
  borderStyle: 'solid',
  background: '#FBFBFB',
  maxWidth: '100%', 
  '@media (max-width: 400px)': {
    maxWidth: '343px',
  },
}));

export const ItemContent = styled('div')({
  display: 'flex',
  alignItems: 'center',
  flexGrow: 1,
  '@media (max-width: 400px)': {
    maxWidthwidth: '343px',
  },
});

export const ItemImage = styled('img')(({ theme }) => ({
  width: '100px',
  height: '100px',
  minWidth: '100px',
  minHeight: '100px',
  objectFit: 'cover',
  borderRadius: '14px 0 0 14px',
 
}));

export const ItemDetails = styled('div')(({ theme }) => ({
  flexGrow: 1,
  marginLeft: theme.spacing(2),
  '@media (max-width: 768px)': {
    width: '120px',
  },
}));

export const PriceTypography = styled(Typography)(({ theme }) => ({
  fontWeight: 'bold',
  marginTop: theme.spacing(1),
}));

export const NameTypography = styled(Typography)(({ theme }) => ({
  fontWeight: 'bold',
  marginBottom: theme.spacing(0.5),
  maxWidth: '220px',
  whiteSpace: 'nowrap', 
  overflow: 'hidden', 
  textOverflow: 'ellipsis',
}));

export const DeleteButton = styled(IconButton)({
  color: '#46A358',
});

export const AddToCartButton = styled(IconButton)({
  color: '#46A358',
});

export const DesktopMediaQuery = styled('div')({
  '@media (min-width: 768px)': {
    padding: '20px',
  },
});

export const EmptyWishlistTypography = styled(Typography)({
  textAlign: 'center',
  color: '#46A358',
  marginTop: '20px',
});

export const CenteredContainer = styled(Container)({
  maxWidth: '1200px',
  minWidth: '366px',
  margin: '15px auto',
  padding: '0 16px',
});

export const BackButtonContainer = styled(Box)({
  display: 'flex',
  alignItems: 'center',
  margin: '20px 0',
  position: "relative"
});

export const CustomIconButton = styled(IconButton)({
  backgroundColor: '#f0f0f0',
  position: "absolute",
  top: "0",
});

export const WishlistTitle = styled(Typography)({
  flex: 1,
  textAlign: "center",
  marginTop: "5px",
});

export const Styles = createUseStyles({
  Link:{
    textDecoration: "none", 
    color:"inherit",
    "&:hover": {
      textDecoration: "underline",
    },
  },
  BackButton: {
    width: "35px",
    height: "35px",
    background: "#f0f0f0",
   
  },

})
