import Banners from "../../components/Home/Banners/Banners";
import HeaderMobVer from "../../components/Home/Header/HeaderMob";
import FooterMobVer from "../../components/Home/Footer/FooterMobVer";
import { Container, Box, Stack, Typography } from "@mui/material";
import ProductListDesktop from "../../components/ProductList/ProductListDesktop";
import ProductListMobile from "../../components/ProductList/ProductListMobile";
import Filters from "../../components/Home/Filters/Filters";
import Sorting from "../../components/Home/Sorting/Sorting";
import { Styles } from "./styles";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
export default function Home() {
  const { emptyButFilter } = useSelector((state) => state.filters);
  const classes = Styles();
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768);
  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);
  return (
    <>
      <Container className={classes.container}>
      {isMobile ? (
        <>
          <HeaderMobVer />
          <Banners />
          <Stack className={classes.columnListProducts} direction="column">
            <Sorting />
            {emptyButFilter === true ? (
              <Typography className={classes.appologizeMessage} variant="body1">
                Sorry, if you see this message, it means that no product matches the filters you selected, try expanding your product selection :&#40;
              </Typography>
            ) : (
              <ProductListMobile />
            )}
          </Stack>
          <FooterMobVer />
        </>
      ) : (
        <>
          <Banners />
          <Box className={classes.listProducts}>
            <Filters />
            <Stack className={classes.columnListProducts} direction="column">
              <Sorting className={classes.sorting} />
              {emptyButFilter === true ? (
                <Typography className={classes.appologizeMessage} variant="body1">
                  Sorry, if you see this message, it means that no product matches the filters you selected, try expanding your product selection :&#40;
                </Typography>
              ) : (
                <ProductListDesktop />
              )}
            </Stack>
          </Box>
        </>
      )}
    </Container>
    </>
  );
}
