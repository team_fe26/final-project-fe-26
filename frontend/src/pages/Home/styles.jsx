import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  container: {
    padding: 0,
    minWidth: '366px',
  },
  listProducts: {
    display: "flex",
    width: "100%",
    gap: "5%",
  },
  appologizeMessage: {
    color: "rgba(61, 61, 61, 0.8)",
    fontFamily: "'Monserrat', sans-serif",
    fontSize: "32px",
    fontWeight: 700,
    marginTop: "auto",
    marginBottom: "auto",
    backgroundColor: "rgba(70, 163, 88, 0.3)",
    borderRadius: "20px",
    padding: "20px",
    "@media (max-width: 768px)": {
      fontSize: "21px",
      marginTop: "20px",
      marginBottom: "120px",
    },
  }
});
