import React from "react";
import { Styles } from "./styles";
import { Typography, Box, Container, Paper, Stack, IconButton, Breadcrumbs,} from "@mui/material";
import { useState, useEffect } from "react";
import { ArrowBack} from "@mui/icons-material";
import { Link , useNavigate } from "react-router-dom";
const ContactUs = () => {
  const classes = Styles();
  const navigate = useNavigate();
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768);
  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768);
    };

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return (
    <Container className={classes.Wrapper}>
    
      <Container className={classes.BreadcrumbsWrapper}>
      {isMobile ? (<Box className={classes.IconContainer}>
        <IconButton
          onClick={() => {
            navigate("/");
          }}
          className={classes.BackButton}
        >
          <ArrowBack style={{ color: "#000" }} />
        </IconButton>
        
      </Box>) : (
        <Breadcrumbs aria-label="breadcrumb" style={{ margin: "115px auto 0 auto", maxWidth: "1200px"}}>
        <Link className={classes.Link} to="/">
          Home
        </Link>
        <Typography style={{fontWeight: 700}} color="textPrimary">Contact Us</Typography>
      </Breadcrumbs>
      )}
      </Container>
      <Paper className={classes.contactsLocations}>
      <Container className={classes.container}>
        <Typography variant="h2" className={classes.sectionTitle}>
          Contact Us
        </Typography>
        <Box className={classes.contactsItem}>
          <Box className={classes.contactsItemMap}>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1165492.0840951384!2d-74.78215907934427!3d40.458327601922775!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e82ac0b24fa76f%3A0xe9336668689fc8e6!2sWeldon%20E.%20Howitt%20Middle%20School!5e0!3m2!1suk!2spl!4v1720283782245!5m2!1suk!2spl"
              width="1500"
              height="1450"
              style={{ border: 0 }}
              allowFullScreen=""
              loading="lazy"
              referrerPolicy="no-referrer-when-downgrade"
              className={classes.lazyloaded}
            ></iframe>
          </Box>
          <Stack direction="column">
            <Typography variant="body1" className={classes.contactsItemCity}>
              USA, NEW YORK
            </Typography>
            <Typography variant="body1" className={classes.contactsItemEmail}>
              contact@greenshop.com
            </Typography>
            <Typography variant="body1" className={classes.contactsItemAddress}>
              70 West Buckingham Ave. Farmingdale, NY 11735
            </Typography>
            <Typography variant="body1" className={classes.contactsItemSchedule}>
              Mon-Fri: 10:00-17:00
              <br />
              Sat: 11:00-15:00
              <br />
              Sun: closed
            </Typography>
            <Typography variant="body1" className={classes.contactsItemPath}>
              Call us and order! <br /> +88 01911 717 490
            </Typography>
          </Stack>
        </Box>
      </Container>
    </Paper>
    </Container>
  );
};

export default ContactUs;
