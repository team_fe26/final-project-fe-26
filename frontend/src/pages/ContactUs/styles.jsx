import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  Wrapper: {
    position: "relative",
    padding: "0 25px",
    
    "@media (max-width: 768px)": {
      display: "block",
      maxWidth: "768px",
      marginTop: "0",
      padding: 0,
      
    },
  }, 
  BreadcrumbsWrapper:{
    padding: 0,
    "@media (max-width: 768px)": {
      position: "absolute",
      top: "20px"
      
    },
  },
  contactsLocations: {
    backgroundColor: "#46A3581A",
    padding: "20px",
    textAlign: "center",
    maxWidth: "1200px",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "20px",
    "@media (max-width: 766px)": {
      width: "95%",
      height: "100vh",
      overflow: "hidden",
      marginTop: "0",
      paddingTop: "100px",
      display: "flex",
      justifyContent: "center",
    },
  },
  container: {
    maxWidth: "1200px",
    margin: "0 auto",
  },
  sectionTitle: {
    fontFamily: "'Monserrat', sans-serif",
    color: "black",
    fontSize: "32px",
    fontWeight: "bold",
    marginBottom: "20px",
  },
  contactUsContainer: {
    fontFamily: "'Monserrat', sans-serif",
    padding: "20px",
    textAlign: "center",
  },
  contactsItem: {
    fontFamily: "'Monserrat', sans-serif",
    color: "black",
    display: "flex",
    flexDirection: "row-reverse",
    justifyContent: "space-around",
    gap: "80px",
    "@media (max-width: 766px)": {
      flexDirection: "column-reverse",
    },
  },
  contactsItemMap: {
    marginBottom: "20px",
  },
  lazyloaded: {
    width: "100%",
    height: "270px",
    border: "0",
  },

  contactsItemCity: {
    fontFamily: "'Monserrat', sans-serif",
    margin: "0",
    fontWeight: "bold",
    fontSize: "22px",
  },
  contactsItemEmail: {
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "bold",
    fontSize: "18px",
  },
  contactsItemAddress: {
    fontFamily: "'Monserrat', sans-serif",
    fontSize: "16px",
    margin: "10px 0",
  },
  contactsItemSchedule: {
    fontFamily: "'Monserrat', sans-serif",
    fontSize: "16px",
    margin: "10px 0",
  },
  contactsItemPath: {
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "bold",
    fontSize: "18px",
    margin: "10px 0",
  },
  contactsItemPathItem: {
    fontFamily: "'Monserrat', sans-serif",
    fontSize: "16px",
    margin: "5px 0",
  },
  BackButton: {
    width: "35px",
    height: "35px",
    background: "#f0f0f0",
    marginLeft: "28px",
  },
  Link:{
    textDecoration: "none", 
    color:"inherit",
    "&:hover": {
      textDecoration: "underline",
    },
  }
});
