import {
  CardMedia,
  Typography,
  Container,
  Button,
  Paper,
  Box,
  Stack,
  ListItemText,
  MenuItem,
  MenuList,
} from "@mui/material";
import { Styles } from "../styles";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { setLogged } from "../../../redux/authorization.slice/authorization.slice";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
 import { getStorage, ref, getDownloadURL } from "firebase/storage" 
 import { initializeApp } from "firebase/app"; 
 import { useDispatch } from "react-redux";
export default function Profile() {
  const dispatch = useDispatch()
  const navigate = useNavigate();
  const classes = Styles();
  const [img, setImg] = useState(null);
  const [data, setData] = useState(null);
  const [addresses, setAddresses] = useState(null);
  const [moreInfoIndex, setMoreInfoIndex] = useState(null);
  const [btnDeleteIndex, setBtnDeleteIndex] = useState(null);
  const firebaseConfig = {
    apiKey: process.env.REACT_APP_FIREBASE_APIKEY,
    authDomain: process.env.REACT_APP_FIREBASE_AUTHDOMAIN,
    projectId: process.env.REACT_APP_FIREBASE_PROJECTID,
    storageBucket: process.env.REACT_APP_FIREBASE_STORAGEBUCKET,
    messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDERID,
    appId: process.env.REACT_APP_FIREBASE_APPID,
    measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENTID,
  };

  const app = initializeApp(firebaseConfig);
  const storage = getStorage(app);
  useEffect(() => {
    const fetchFn = async () => {
      try {
        const token = JSON.parse(localStorage.getItem("token")) || "";

        const response = await fetch(
          "https://greenshop-server.onrender.com/api/user",
          {
            method: "POST",
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );

        if (!response.ok) {
          const errorData = await response.json();
          console.error("Error fetching data:", errorData.error);
          return;
        }

        const data = await response.json();
        console.log(data.message, data.customer);
        setData(data.customer);
        if (data.customer.addresses) {
          setAddresses(data.customer.addresses);
        }
        if (data.customer?.avatarUrl) {
          const avatarRef = ref(storage, data.customer.avatarUrl);
          try {
            const url = await getDownloadURL(avatarRef);
            setImg(url);
          } catch (error) {
            console.error("Error getting image URL:", error);
          }
        }
      } catch (error) {
        console.error("Fetch error:", error);
      }
    };

    fetchFn();
  }, []); 
  useEffect(()=>{
  },[addresses])
  const handleMoreInfoClick = (index) => {
    setMoreInfoIndex(index);
  };

  const handleDeleteClick = (index) => {
    setBtnDeleteIndex(index);
  };

  const handleDeleteAddress = async (address) => {
    try {
      const token = JSON.parse(localStorage.getItem("token")) || "";
      const response = await fetch(
        "https://greenshop-server.onrender.com/api/customer/address/remove",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({ address }),
        }
      );
      if (!response.ok) {
        const data = await response.json();
        console.error("Error:", data.error);
        return;
      }
      const data = await response.json();
      console.log(data.message);
      setAddresses((prevAddresses) =>
        prevAddresses.filter((addr) => addr !== address)
      );
    } catch (error) {
      console.error("Error fetch:", error);
    }
  };
  const selectMainAddress = async (zip, main) => {
    try {
      const token = JSON.parse(localStorage.getItem("token")) || "";
  
      if (!token) {
        console.error("No token found");
        return;
      }
  
      const response = await fetch(
        "https://greenshop-server.onrender.com/api/customer/address/main",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({ zip, main }), 
        }
      );
  
      if (!response.ok) {
        const data = await response.json();
        console.error("Error:", data.error);
        return;
      }
  
      const data = await response.json();
      console.log(data.message);
      setAddresses(data.customer.addresses);
    } catch (error) {
      console.error("Error fetch:", error);
    }
  };

  const renderAddresses = () => {
    if (addresses) {
      return addresses.map((Address, index) => (
        <MenuItem className={classes.menuItemAddress} key={Address._id}>
          <Box className={classes.boxAddress}>
            <ListItemText className={classes.titleAddress}>
              {Address.town}
            </ListItemText>
            <Typography className={classes.infoAddress} variant="body1">
              {Address.appartment} {Address.street}, {Address.city}{" "}
              {Address.zip}, {Address.country}, {Address.state}
            </Typography>
            {moreInfoIndex === index ? (
              <>
                <Typography className={classes.infoAddress}>
                  {Address.firstName} {Address.lastName},{Address.phoneNumber},{" "}
                  {Address.email}
                </Typography>
                {Address.main ? (
                <Typography className={classes.mainAddress}>Main address</Typography>
              ) : (
                <Button
                  onClick={() => {
                    selectMainAddress(Address.zip, true); 
                  }}
                  className={classes.btnMakeMainAddress}
                >
                  Select as main address
                </Button>
              )}
                {btnDeleteIndex === index ? (
                  <Stack className={classes.messageDeleteAddress}>
                    <Typography className={classes.titlemessageDeleteAddress}>
                      Are you sure you want to delete?
                    </Typography>
                    <Button
                      className={classes.btnYesNo}
                      onClick={() => handleDeleteAddress(Address)}
                    >
                      Yes
                    </Button>
                    <Button
                      className={classes.btnYesNo}
                      onClick={() => setBtnDeleteIndex(null)}
                    >
                      No
                    </Button>
                  </Stack>
                ) : (
                  <IconButton
                    onClick={() => handleDeleteClick(index)}
                    className={classes.iconButtonDelete}
                  >
                    <DeleteIcon />
                  </IconButton>
                )}
              </>
            ) : (
              <IconButton
                onClick={() => handleMoreInfoClick(index)}
                className={classes.iconButtonMore}
              >
                <MoreHorizIcon />
              </IconButton>
            )}
          </Box>
        </MenuItem>
      ));
    }
  };
  const deleteAcc = async () => {
    try {
      const token = JSON.parse(localStorage.getItem("token")) || "";
      const response = await fetch(
        "https://greenshop-server.onrender.com/api/delete-account",
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      if (!response.ok) {
        const data = await response.json();
        console.error("Error fetching data:", data.error);
        return;
      }

      const data = await response.json();
      navigate("/");
      dispatch(setLogged(false));
      console.log(data.message);
    } catch (error) {
      console.error("Fetch error:", error);
    }
  };
  return (
    <Container className={classes.containerProfile}>
      <Stack className={classes.containerProfileData}>
        <CardMedia
          component="img"
          src={img || "/user.png"}
          className={classes.personIcon}
        />
        <Paper className={classes.accountDetails}>
          <Typography className={classes.accountDetailsTypography}>
            <span className={classes.accountDetailsSpan}>First Name:</span>{" "}
            {data?.firstName || "Unknown"}
          </Typography>
          <Typography className={classes.accountDetailsTypography}>
            <span className={classes.accountDetailsSpan}>Last Name:</span>{" "}
            {data?.lastName || "Unknown"}
          </Typography>
          <Typography className={classes.accountDetailsTypography}>
            <span className={classes.accountDetailsSpan}>Username:</span>{" "}
            {data?.username || "Unknown"}
          </Typography>
          <Typography className={classes.accountDetailsTypography}>
            <span className={classes.accountDetailsSpan}>Email:</span>{" "}
            {data?.email || "Unknown"}
          </Typography>
          <Typography className={classes.accountDetailsTypography}>
            <span className={classes.accountDetailsSpan}>Phone Number:</span>{" "}
            {data?.phoneNumber || "Unknown"}
          </Typography>
          <Typography className={classes.accountDetailsTypography}>
            <span className={classes.accountDetailsSpan}>
              Date of registration:
            </span>{" "}
            {data?.date.substring(0, 10)}
          </Typography>
        </Paper>
      </Stack>
      <MenuList className={classes.menuListAddresses}>
        {renderAddresses()}
      </MenuList>
      <Button className={classes.btnDeleteAccount} onClick={deleteAcc}>
        Delete Account
      </Button>
    </Container>
  );
}
