import { Button } from "@mui/material";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Styles } from "../styles";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { useState } from "react";
export default function ChangePassword() {
  const classes = Styles();
  const [visibilityCur, setVisibilityCur] = useState(false);
  const [visibility, setVisibility] = useState(false);
  const [visibilityConfirm, setVisibilityConfirm] = useState(false);
  const initialValuesPassword = {
    currentPassword: "",
    newPassword: "",
    confirmNewPassword: "",
  };
  const onSubmitPassword = async (values) => {
    try {
      const token = JSON.parse(localStorage.getItem("token")) || "";
      const response = await fetch(
        "https://greenshop-server.onrender.com/api/customer/change-password",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            currentPassword: values.currentPassword,
            newPassword: values.newPassword,
          }),
        }
      );
      if (!response.ok) {
        const data = await response.json();
        alert(data.error);
        return;
      }
      const data = await response.json();
      console.log(data.message);
      alert("Password changed successfully");
    } catch (error) {
      console.error("Error fetch:", error);
    }
  };
  const validationSchemaPassword = Yup.object({
    currentPassword: Yup.string()
      .min(8, "Password should be min 8 characters*")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&_])[A-Za-z\d@$!%*?&_]{8,}$/,
        "Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character*"
      ),
    newPassword: Yup.string()
      .min(8, "Password should be min 8 characters*")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&_])[A-Za-z\d@$!%*?&_]{8,}$/,
        "Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character*"
      )
      .required("Password is required*"),
    confirmNewPassword: Yup.string()
      .oneOf([Yup.ref("newPassword"), null], "Passwords must match")
      .required("Confirm new password is required"),
  });
  return (
    <>
      <Formik
        initialValues={initialValuesPassword}
        validationSchema={validationSchemaPassword}
        onSubmit={onSubmitPassword}
      >
        <Form className={classes.formPassword}>
          <div>
            <div>
              <label className={classes.label} htmlFor="currentPassword">
                Current Password
              </label>
              <div className={classes.containerFielsPassword}>
                <Field
                  placeholder="Password"
                  type={visibilityCur ? "text" : "password"}
                  name="currentPassword"
                  variant="outlined"
                  className={classes.fieldPassword}
                />
                {visibilityCur ? (
                  <VisibilityIcon
                    onClick={() => {
                      setVisibilityCur(false);
                    }}
                    className={classes.eyeIcon}
                  />
                ) : (
                  <VisibilityOffIcon
                    onClick={() => {
                      setVisibilityCur(true);
                    }}
                    className={classes.eyeIcon}
                  />
                )}
              </div>
              <ErrorMessage
                className={classes.errorMessage}
                name="currentPassword"
                component="div"
              />
            </div>

            <div>
              <label className={classes.label} htmlFor="newPassword">
                New Password <span className={classes.star}>*</span>
              </label>
              <div className={classes.containerFielsPassword}>
                <Field
                  placeholder="New password"
                  type={visibility ? "text" : "password"}
                  name="newPassword"
                  variant="outlined"
                  className={classes.fieldPassword}
                />
                {visibility ? (
                  <VisibilityIcon
                    onClick={() => {
                      setVisibility(false);
                    }}
                    className={classes.eyeIcon}
                  />
                ) : (
                  <VisibilityOffIcon
                    onClick={() => {
                      setVisibility(true);
                    }}
                    className={classes.eyeIcon}
                  />
                )}
              </div>

              <ErrorMessage
                className={classes.errorMessage}
                name="newPassword"
                component="div"
              />
            </div>
            <div>
              <label className={classes.label} htmlFor="confirmNewPassword">
                Confirm New Password <span className={classes.star}>*</span>
              </label>
              <div className={classes.containerFielsPassword}>
                <Field
                  placeholder="Confirm new password"
                  type={visibilityConfirm ? "text" : "password"}
                  name="confirmNewPassword"
                  variant="outlined"
                  className={classes.fieldPassword}
                />
                {visibilityConfirm ? (
                  <VisibilityIcon
                    onClick={() => {
                      setVisibilityConfirm(false);
                    }}
                    className={classes.eyeIcon}
                  />
                ) : (
                  <VisibilityOffIcon
                    onClick={() => {
                      setVisibilityConfirm(true);
                    }}
                    className={classes.eyeIcon}
                  />
                )}
              </div>

              <ErrorMessage
                className={classes.errorMessage}
                name="confirmNewPassword"
                component="div"
              />
            </div>
          </div>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className={classes.btnChange}
          >
            Save Change
          </Button>
        </Form>
      </Formik>
    </>
  );
}
