import { Button, CardMedia, Box, Modal } from "@mui/material";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Styles } from "../styles";
import PortraitIcon from "@mui/icons-material/Portrait";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import SearchIcon from "@mui/icons-material/Search";
export default function ChangePersonalInformation() {
  const navigate = useNavigate();
  const [isHovered, setIsHovered] = useState(false);
  const [isShowModal, setIsShowModal] = useState(false);
  const initialValues = {
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    username: "",
    file: null,
  };
  const [imageUrl, setImageUrl] = useState(null);
  const classes = Styles(isHovered);
  useEffect(() => {
    if (imageUrl) {
      URL.revokeObjectURL(imageUrl);
    }
    if (initialValues.file) {
      const newImageUrl = URL.createObjectURL(initialValues.file);
      setImageUrl(newImageUrl);
      return () => URL.revokeObjectURL(newImageUrl);
    }
  }, [initialValues.file]);
  const onSubmit = async (values) => {
    try {
      const token = JSON.parse(localStorage.getItem("token")) || "";
      const formData = new FormData();
      if (values.firstName) {
        formData.append("firstName", values.firstName);
      }
      if (values.lastName) {
        formData.append("lastName", values.lastName);
      }
      if (values.username) {
        formData.append("username", values.username);
      }
      if (values.phoneNumber) {
        formData.append("phoneNumber", values.phoneNumber);
      }
      if (values.email) {
        formData.append("email", values.email);
      }
      if (values.file) {
        formData.append("file", values.file);
      }

      const response = await fetch(
        "https://greenshop-server.onrender.com/api/customer/settings",
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${token}`,
          },
          body: formData,
        }
      );

      if (!response.ok) {
        const data = await response.json();
        alert(`Error: ${data.error}`);  
      }
      const data = await response.json();
      console.log(data.message, data.avatar);
      navigate("/user");
    } catch (error) {
      console.error("Ошибка fetch:", error);
    }
  };
  const fileTypes = ["image/jpeg", "image/png", "image/gif", "image/svg+xml"];

  const fileSchema = Yup.mixed()
  .nullable() 
  .test(
    "fileType",
    "File must be (JPEG, PNG, GIF или SVG)",
    (value) => {
      if (!value) return true;
      return fileTypes.includes(value.type); 
    }
  );

  const validationSchema = Yup.object({
    firstName: Yup.string(),
    lastName: Yup.string(),
    email: Yup.string().email("Invalid email*"),
    phoneNumber: Yup.string().min(10, "Phone number should be 10 characters"),
    username: Yup.string(),
    file: fileSchema,
  });
  return (
    <>
      <Modal
        open={isShowModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        onClick={() => setIsShowModal(false)}
      >
        <Box  className={classes.modalImg}>
          <CardMedia
            component="img"
            image={imageUrl}
            alt="Uploaded image"
            className={classes.modalImg}
          />
        </Box>
      </Modal>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({ resetForm, setFieldValue }) => (
          <Form className={classes.form}>
            <div>
              <label className={classes.label} htmlFor="firstName">
                First Name
              </label>
              <Field className={classes.field} type="text" name="firstName">
                {({ field, form }) => (
                  <input
                    className={classes.field}
                    {...field}
                    placeholder="Enter your first name"
                    onChange={(e) => {
                      const name = e.target.value.replace(
                        /[^a-zA-Zа-яА-Я]/g,
                        ""
                      );
                      form.setFieldValue("firstName", name);
                    }}
                  />
                )}
              </Field>
              <ErrorMessage
                className={classes.errorMessage}
                name="firstName"
                component="div"
              />
            </div>

            <div>
              <label className={classes.label} htmlFor="lastName">
                Last Name
              </label>
              <Field className={classes.field} type="text" name="lastName">
                {({ field, form }) => (
                  <input
                    className={classes.field}
                    {...field}
                    placeholder="Enter your last name"
                    onChange={(e) => {
                      const name = e.target.value.replace(
                        /[^a-zA-Zа-яА-Я]/g,
                        ""
                      );
                      form.setFieldValue("lastName", name);
                    }}
                  />
                )}
              </Field>
              <ErrorMessage
                className={classes.errorMessage}
                name="lastName"
                component="div"
              />
            </div>

            <div>
              <label className={classes.label} htmlFor="email">
                Email
              </label>
              <Field
                className={classes.field}
                type="email"
                id="email"
                name="email"
                placeholder="Enter your email"
              />
              <ErrorMessage
                className={classes.errorMessage}
                name="email"
                component="div"
              />
            </div>

            <div>
              <label className={classes.label} htmlFor="phoneNumber">
                Phone Number
              </label>
              <Field className={classes.field} type="text" name="phoneNumber">
                {({ field, form }) => (
                  <input
                    className={classes.field}
                    {...field}
                    placeholder="(###)###-##-##"
                    onChange={(e) => {
                      const phoneNumber = e.target.value.replace(/[^\d]/g, "");
                      if (phoneNumber.length <= 10) {
                        form.setFieldValue("phoneNumber", phoneNumber);
                      }
                    }}
                  />
                )}
              </Field>
              <ErrorMessage
                className={classes.errorMessage}
                name="phoneNumber"
                component="div"
              />
            </div>

            <div>
              <label className={classes.label} htmlFor="username">
                Username
              </label>
              <Field className={classes.field} type="text" name="username">
                {({ field, form }) => (
                  <input
                    className={classes.field}
                    {...field}
                    placeholder="Enter your username"
                    onChange={(e) => {
                      const name = e.target.value.replace(
                        /[^a-zA-Zа-яА-Я]/g,
                        ""
                      );
                      form.setFieldValue("username", name);
                    }}
                  />
                )}
              </Field>
              <ErrorMessage
                className={classes.errorMessage}
                name="username"
                component="div"
              />
            </div>

            <div className={classes.containerFile}>
              <Field name="file" className={classes.field}>
                {({ form }) => (
                  <div className={classes.photoUser}>
                    <input
                      className={classes.fieldFile}
                      accept="image/*"
                      id="file"
                      type="file"
                      onChange={(event) => {
                        const file = event.currentTarget.files[0];
                        form.setFieldValue("file", file);
                        const newImageUrl = URL.createObjectURL(file);
                        setImageUrl(newImageUrl);
                      }}
                    />
                    <PortraitIcon className={classes.portraitIcon} />
                  </div>
                )}
              </Field>
              {imageUrl && (
                <Box
                  className={classes.containerUploadedImg}
                  onMouseEnter={() => setIsHovered(true)}
                  onMouseLeave={() => setIsHovered(false)}
                  onClick={() => {
                    if (isHovered) {
                      setIsShowModal(true);
                    }
                  }}
                >
                  <CardMedia
                   className={classes.uploadedImg}
                    src={imageUrl}
                    component="img"
                  />
                  {isHovered && window.innerWidth >= 768 && (
                    <SearchIcon className={classes.searchIcon} />
                  )}
                </Box>
              )}
              <ErrorMessage
                className={classes.errorMessage}
                name="file"
                component="div"
              />
              <Button type="submit" className={classes.btnChange}>
                Change
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
}
