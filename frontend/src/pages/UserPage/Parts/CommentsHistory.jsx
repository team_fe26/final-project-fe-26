import {
  CardMedia,
  Typography,
  MenuList,
  MenuItem,
  Box,
  Rating,
  Stack,
  ListItemText,
  Button,
  IconButton,
  Pagination,
} from "@mui/material";
import { Styles } from "../styles";
import { useEffect, useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
export default function CommentsHistory() {
  const [comments, setComments] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [commentsPerPage] = useState(10);
  const [btnDeleteIndex, setBtnDeleteIndex] = useState(null);
  const classes = Styles();
  const fetchComments = async (page = 1) => {
    try {
      const token = JSON.parse(localStorage.getItem("token")) || "";
      const response = await fetch(
        `https://greenshop-server.onrender.com/api/comments-user?page=${page}&limit=${commentsPerPage}`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        }
      );

      if (!response.ok) {
        const errorText = await response.text();
        console.error("Error fetching data:", errorText);
        setLoading(false);
        return;
      }

      const data = await response.json();
      console.log(data.message, data.comments);

      const commentsWithImages = await Promise.all(
        data.comments.map(async (comment) => {
          try {
            const response = await fetch(
              "https://greenshop-server.onrender.com/api/product",
              {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({ _id: comment.product }),
              }
            );

            if (!response.ok) {
              const data = await response.json();
              console.error("Failed to fetch product data:", data.error);
              return { ...comment, productImage: null };
            }

            const data = await response.json();
            console.log(data);

            return { ...comment, productImage: data.imageUrls[0] };
          } catch (error) {
            console.error("Error fetching product data:", error);
            return { ...comment, productImage: null };
          }
        })
      );

      setComments(commentsWithImages);
      setTotalPages(data.totalPages);
      setLoading(false);
    } catch (error) {
      console.error("Fetch error:", error);
      setLoading(false);
    }
  };
  useEffect(() => {
    fetchComments(currentPage);
  }, [currentPage]);
  const handleDeleteClick = (index) => {
    setBtnDeleteIndex(index);
  };

  const handleDeleteComment = async (comment) => {
    try {
      const token = JSON.parse(localStorage.getItem("token")) || "";
      const response = await fetch(
        "https://greenshop-server.onrender.com/api/comment/delete",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({ _id: comment._id }),
        }
      );
      if (!response.ok) {
        const data = await response.json();
        console.error("Error:", data.error);
        return;
      }
      const data = await response.json();
      console.log(data.message);
      setComments((prevComments) =>
        prevComments.filter((com) => com !== comment)
      );
    } catch (error) {
      console.error("Error fetch:", error);
    }
  };
  const renderComments = () => {
    if (comments.length > 0) {
      return comments.map((comment, index) => {
        console.log(comment.productImage);
        return (
          <MenuItem
            style={{
              flexDirection: btnDeleteIndex === index ? "column" : "row",
            }}
            className={classes.menuItemComment}
            key={comment._id}
          >
            <Box className={classes.boxComment}>
              <ListItemText className={classes.commentText}>
                {comment.date}
              </ListItemText>
              <Stack direction="row" className={classes.boxCardComment}>
                <CardMedia
                  className={classes.imgComment}
                  component="img"
                  image={comment.productImage}
                  alt="Product Image"
                />
                <Typography className={classes.commentText}>
                  {comment.content}
                </Typography>
              </Stack>
            </Box>
            <Rating
              className={classes.rating}
              value={comment.rating || 0}
              readOnly
            />
            {btnDeleteIndex === index ? (
              <Stack
                className={`${classes.messageDeleteAddress} ${classes.messageDeleteComment}`}
              >
                <Typography className={classes.titlemessageDeleteAddress}>
                  Are you sure you want to delete?
                </Typography>
                <Button
                  className={classes.btnYesNo}
                  onClick={() => handleDeleteComment(comment)}
                >
                  Yes
                </Button>
                <Button
                  className={classes.btnYesNo}
                  onClick={() => setBtnDeleteIndex(null)}
                >
                  No
                </Button>
              </Stack>
            ) : (
              <IconButton
                onClick={() => handleDeleteClick(index)}
                className={classes.iconButtonDelete}
              >
                <DeleteIcon />
              </IconButton>
            )}
          </MenuItem>
        );
      });
    }
    return <Typography>No comments available</Typography>;
  };

  return (
    <>
      {loading ? (
        <Typography>Loading...</Typography>
      ) : (
        <>
          <MenuList className={classes.menuListComments}>
            {renderComments()}
          </MenuList>
          <Box className={classes.pagination}>
            {comments.length >= 10 &&
            <Pagination
              count={totalPages}
              page={currentPage}
              onChange={(event, value) => setCurrentPage(value)}
                  shape="rounded"
      color="success"
            />}
          </Box>
        </>
      )}
    </>
  );
}
