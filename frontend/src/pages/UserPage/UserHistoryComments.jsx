import { Typography, Container, Box, IconButton } from "@mui/material";
import { Styles } from "./styles";
import { ArrowBack } from "@mui/icons-material";
import { useNavigate } from "react-router-dom";
import CommentsHistory from "./Parts/CommentsHistory";
export default function UserHistoryComments() {
  const navigate = useNavigate();
  const classes = Styles();
  return (
    <Container className={classes.container}>
      <Box className={classes.IconContainer}>
            <IconButton
              onClick={() => {
                navigate('/user');
              }}
              className={classes.BackButton}
            >
              <ArrowBack style={{ color: "#000" }} />
            </IconButton>
            
          </Box>
      <Typography className={classes.titleMob} variant="h1">
        History Comments
      </Typography>
      <CommentsHistory></CommentsHistory>
    </Container>
  );
}