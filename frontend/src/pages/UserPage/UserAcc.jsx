import { Typography, Container, Button, Menu, MenuItem, Fade, Box } from "@mui/material";
import { Styles } from "./styles";
import { useNavigate } from "react-router-dom";
import Profile from "./Parts/Profile";
import { useState } from "react";
import IconButton from "@mui/material/IconButton";
import { ArrowBack } from "@mui/icons-material";
import { useDispatch } from "react-redux";
import { setLogged } from "../../redux/authorization.slice/authorization.slice";

export default function UserAccount() {
  const dispatch = useDispatch()
  const navigate = useNavigate();
  const classes = Styles();
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  }
  const logOut = async () => {
    try {
      const token = JSON.parse(localStorage.getItem("token")) || "";
      const response = await fetch(
        "https://greenshop-server.onrender.com/api/logout",
        {
          method: "POST",
          credentials: "include",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      if (!response.ok) {
        const data = await response.json();
        console.error("Error logout:", data.error);
        return;
      }
      dispatch(setLogged(false));
      localStorage.removeItem("token");
    } catch (error) {
      console.error("Error fetch:", error);
    }
  };
  return (
    <Container className={classes.container}>
      <Box className={classes.IconContainer}>
        <IconButton
          onClick={() => {
            navigate('/');
          }}
          className={classes.BackButton}
        >
          <ArrowBack style={{ color: "#000" }} />
        </IconButton>

      </Box>
      <Typography className={classes.titleMob} variant="h1">
        Your Account
      </Typography>
      <Profile></Profile>
      <Button
        className={classes.btnNavigate}
        id="fade-button"
        aria-controls={open ? "fade-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        onClick={handleClick}
      >
        Settings
      </Button>
      <Menu
        id="fade-menu"
        MenuListProps={{
          "aria-labelledby": "fade-button",
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        <MenuItem
          onClick={() => {
            handleClose();
            navigate(`/user/change-password`)
          }}
        >
          Change Password
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleClose();
            navigate(`/user/personal-information`)
          }}
        >
          Change Personal Information
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleClose();
            navigate("/billing-address", { state: { from: "user" } });
          }}
        >
          Add Address
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleClose();
            navigate(`/user/comments`)
          }}
        >
          Comments History
        </MenuItem>
        <MenuItem
          onClick={() => {
            logOut()
          }}
        >
          Logout
        </MenuItem>
      </Menu>
     
    </Container>
  );
}
