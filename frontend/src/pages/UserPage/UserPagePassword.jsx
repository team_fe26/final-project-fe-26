import { Typography, Container, Box,  IconButton } from "@mui/material";
import { Styles } from "./styles";
import { ArrowBack } from "@mui/icons-material";
import { useNavigate } from "react-router-dom";
import ChangePassword from "./Parts/ChangePassword";
export default function UserPagePassword() {
  const navigate = useNavigate();
  const classes = Styles();
  return (
    <Container className={classes.container}>
      <Box className={classes.IconContainer}>
            <IconButton
              onClick={() => {
                navigate('/user');
              }}
              className={classes.BackButton}
            >
              <ArrowBack style={{ color: "#000" }} />
            </IconButton>
            
          </Box>
      <Typography className={classes.titleMob} variant="h1">
        Change Password
      </Typography>
      <ChangePassword></ChangePassword>
    </Container>
  );
}
