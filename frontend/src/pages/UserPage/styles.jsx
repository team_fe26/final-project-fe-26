import { createUseStyles } from "react-jss";
export const Styles = createUseStyles({
  billingContainer: {
    display: "flex",
    flexDirection: "row",
    marginLeft: "auto",
    marginRight: "auto",
    maxWidth: "1200px",
    padding: 0,
    gap: "3%",
  },

  title: {
    fontSize: "17px",
    fontFamily: "'Montserrat', sans-serif",
    fontWeight: "700",
    color: "#3D3D3D",
    marginBottom: "17px",
    marginTop: "40px",
  },

  field: {
    marginBottom: "15px",
    marginTop: "10px",
    color: "rgba(165, 165, 165, 1)",
    fontFamily: '"Montserrat", sans-serif',
    fontSize: "15px",
    fontWeight: 400,
    borderRadius: "3px",
    backgroundColor: "transparent",
    width: "92%",
    height: "50px",
    outline: "none",
    paddingLeft: "20px",
    border: "1px solid rgba(165, 165, 165, 1)",
    "&:hover": {
      border: "1px solid rgba(70, 163, 88, 1)",
    },
    "&:focus": {
      border: "1px solid rgba(70, 163, 88, 1)",
      backgroundColor: "rgba(255, 255, 255, 1)",
    },
    input: {
      border: "none",
    },
    "@media (max-width: 767px)": {
      marginBottom: "15px",
      borderRadius: "10px",
      width: "92%",
      paddingLeft: "20px",
      border: "2px solid rgba(165, 165, 165, 1)",
      "&:hover": {
        border: "2px solid rgba(70, 163, 88, 1)",
      },
      "&:focus": {
        border: "2px solid rgba(70, 163, 88, 1)",
        backgroundColor: "rgba(255, 255, 255, 1)",
      },
    },
  },
  fieldFile: {
    width: "40px",
    zIndex: 50,
    cursor: "pointer",
    opacity: 0,
    position: "absolute",
    "@media (max-width: 767px)": {},
  },
  errorMessage: {
    color: "red",
    position: "relative",
    top: -10,
    fontSize: "14px",
  },

  label: {
    color: "#3D3D3D",
    fontSize: "15px",
    fontFamily: "'Montserrat', sans-serif",
    fontWeight: "400",
  },

  star: {
    color: "#F03800",
    fontSize: "22px",
    fontFamily: "'Montserrat', sans-serif",
    fontWeight: "400",
  },

  form: {
    display: "grid",
    gridTemplateColumns: "repeat(2, 1fr)",
    gap: "28px",
    "@media (max-width: 767px)": {
      display: "flex",
      flexDirection: "column",
      width: "100%",
      marginTop: "40px",
    },
  },
  formPassword: {
    gap: "28px",
    marginTop: '40px',
    "@media (max-width: 767px)": {
      display: "flex",
      flexDirection: "column",
      width: "100%",
    },
  },

  passwordInput: {
    position: "relative",
    display: "flex",
    alignItems: "center",
  },
  containerFielsPassword: {
    position: "relative",
    width: "fit-content",
    "@media (max-width: 767px)": {
      width: "100%",
    },
  },
  fieldPassword: {
    width: "350px",
    marginTop: '10px',
    position: "relative",
    gridColumn: "span 2",
    display: "flex",
    height: "50px",
    flexDirection: "column",
    marginBottom: "30px",
    fontFamily: '"Montserrat", sans-serif',
    fontSize: "15px",
    fontWeight: 400,
    borderRadius: "3px",
    backgroundColor: "transparent",
    outline: "none",
    paddingLeft: "20px",
    border: "1px solid rgba(165, 165, 165, 1)",
    "&:hover": {
      border: "1px solid rgba(70, 163, 88, 1)",
    },
    "&:focus": {
      border: "1px solid rgba(70, 163, 88, 1)",
      backgroundColor: "rgba(255, 255, 255, 1)",
    },
    "@media (max-width: 767px)": {
      marginBottom: "15px",
      borderRadius: "10px",
      width: "92%",
      paddingLeft: "20px",
      border: "2px solid rgba(165, 165, 165, 1)",
      "&:hover": {
        border: "2px solid rgba(70, 163, 88, 1)",
      },
      "&:focus": {
        border: "2px solid rgba(70, 163, 88, 1)",
        backgroundColor: "rgba(255, 255, 255, 1)",
      },
    },
  },
  eyeIcon: {
    position: "absolute",
    right: "6%",
    cursor: "pointer",
    zIndex: 1,
    color: "#3D3D3D",
    top: "15px",
    opacity: "0.5",
  },

  btn: {
    marginTop: "50px",
    padding: "13px 23px",
    height: "40px",
    borderRadius: "3px",
    backgroundColor: "#46A358",
    color: "#FFFFFF",
    textTransform: "capitalize",
    fontWeight: "700",
    fontSize: "15px",
    border: "none",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#46A358",
      opacity: 0.8,
      transition: "opacity 0.3s ease",
    },
    "@media (max-width: 767px)": {
      height: "50px",
      borderRadius: "10px",
      width: "100%",
      backgroundImage:
        "linear-gradient(to right, rgba(70, 163, 88, 1), rgba(70, 163, 88, 0.8))",
      fontSize: "16px",
      marginTop: "30px",
    },
  },
  containerFile: {
    marginTop: "25px",
    display: "flex",
    alignItems: "center",
    gap: "20px",
    "@media (max-width: 767px)": {
      flexDirection: "column",
      alignItems: "flex-start",
      marginTop: "0",
    },
  },
  photoUser: {
    display: "flex",
    alignItems: "center",
    color: "#46A358",
    backgroundColor: "rgba(251, 251, 251, 1)",
    border: "1px solid rgba(234, 234, 234, 1)",
    borderRadius: "50%",
    width: "40px",
    height: "40px",
    marginTop: "5px",
    cursor: "pointer",
    "@media (max-width: 767px)": {
      marginLeft: "40px",
    },
  },
  portraitIcon: {
    position: "relative",
    left: "8px",
  },
  btnChange: {
    backgroundColor: "rgba(70, 163, 88, 1)",
    borderRadius: "3px",
    color: "rgba(255, 255, 255, 1)",
    fontWeight: "700",
    fontSize: "14px",
    width: '50%',
    height: "40px",
    padding: '20px',
    "&:hover": {
      backgroundColor: "#3A8C47" 
    },
    "@media (max-width: 767px)": {
      height: "50px",
      borderRadius: "10px",
      width: "100%",
      backgroundImage:
        "linear-gradient(to right, rgba(70, 163, 88, 1), rgba(70, 163, 88, 0.8))",
      fontSize: "16px",
      marginTop: "30px",
    },
  },
  btnRemove: {
    color: "rgba(61, 61, 61, 1)",
    fontSize: "rgba(61, 61, 61, 1)",
    height: "40px",
    width: "98px",
    "@media (max-width: 767px)": {
      height: "50px",
      borderRadius: "10px",
      width: "100%",
      fontSize: "16px",
    },
  },
  container: {
    minWidth: "366px",
    maxWidth: "766px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    position: "relative",
  },
  titleMob: {
    fontSize: "20px",
    color: "rgba(61, 61, 61, 1)",
    fontFamily: '"Montserrat", sans-serif',
    fontWeight: "500",
    marginTop: "30px",
    textAlign: 'center',
    lineHeight: '1.5'
  },

  textField: {
    "& .MuiOutlinedInput-root": {
      borderRadius: "14px",
      height: "40px",
      border: "1px",
      "&:hover fieldset": {
        borderColor: "rgba(70, 163, 88, 1)",
      },
      "&.Mui-focused fieldset": {
        borderColor: "rgba(70, 163, 88, 1)",
      },
    },
    "& .MuiInputBase-input": {
      textAlign: "center",
      color: "rgba(61, 61, 61, 1)",
      fontFamily: '"Montserrat", sans-serif',
      fontSize: "14px",
      fontWeight: 400,
    },
  },
  containerProfile: {
    minWidth: "366px",
    display: "flex",
    flexDirection: "column",
    padding: 0,
    marginTop: 40,
    gap: "40px",
    position: "relative",
    "@media (max-width: 1000px)": {
      marginTop: 0,
      gap: 60,
    },
    "@media (max-width: 768px)": {
      marginTop: "50px",
      gap: 40,
    },
  },
  containerProfileData: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    gap: "5%",
    justifyContent: "space-between",
    alignItems: "center",
    position: "relative",
    "@media (max-width: 1000px)": {
      flexDirection: "column",
      marginTop: 0,
      gap: 60,
    },
  },
  menuListAddresses: {
    display: "flex",
    gap: "20px",
    flexDirection: "column",
    position: "relative",
  },
  menuItemAddress: {
    backgroundColor: "rgba(249, 249, 249, 1)",
    borderRadius: "20px",
    padding: "20px",
    cursor: "default",
  },
  iconButtonMore: {
    position: "absolute",
    right: "20px",
    bottom: "5px",
  },
  boxAddress: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    gap: "10px",
  },
  titleAddress: {
    color: "rgba(61, 61, 61, 1)",
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
  },
  infoAddress: {
    color: "#727272",
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
    whiteSpace: 'wrap'
  },
  btnMakeMainAddress: {
    marginTop: "20px",
    color: "rgba(248, 248, 248, 1)",
    width: "230px",
    "&:hover": {
      backgroundColor: "rgba(70, 163, 88, 1)",
    },
    backgroundColor: "rgba(70, 163, 88, 1)",
    marginLeft: "0",
    marginRight: "auto",
  },
  messageDeleteAddress:{
    marginTop: '30px',
marginRight: '0',
marginLeft: 'auto',
    backgroundColor: "rgba(149, 149, 149, 1)",
    "&:hover": {
      backgroundColor: "rgba(109, 109, 109, 1)",
      opacity: 0.8,
      transition: "opacity 0.3s ease",
    },
display: 'flex',
flexWrap: 'wrap',
width: '330px',
gap: '10px',
borderRadius: '5px',
padding: '10px',
flexDirection: 'row',
alignItems: 'center',
justifyContent: 'center'
  },
  titlemessageDeleteAddress:{
textTransform: 'uppercase',
color: 'rgba(248, 248, 248, 1)',
width: '100%',
textAlign: 'center',
  },
  btnYesNo:{
    color: 'rgba(248, 248, 248, 1)',
  },
  iconButtonDelete: {
    position: "absolute",
    right: "20px",
    bottom: "10px",
  },
  personIcon: {
    minWidth: "310px",
    width: "310px",
    borderRadius: "50%",
    height: "310px",
  },
  accountDetailsTypography: {
    color: "#3D3D3D",
    fontSize: "18px",
    fontFamily: "'Montserrat', sans-serif",
    fontWeight: "400",
  },
  accountDetailsSpan: {
    fontWeight: "800",
  },
  accountDetails: {
    display: "flex",
    flexDirection: "column",
    gap: 8,
    padding: 40,
    width: "100%",
  },
  btnDeleteAccount: {
    padding: "13px 23px",
    height: "40px",
    borderRadius: "3px",
    backgroundColor: "red",
    color: "#FFFFFF",
    fontWeight: "700",
    fontSize: "15px",
    border: "none",
    cursor: "pointer",
    width: '200px',
    marginRight: 0,
    marginLeft: 'auto',
    "@media (max-width: 1000px)": {
      position: "relative",
      right: "0",
      marginTop: "15%",
    },
    "@media (max-width: 480px)": {
      marginTop: "35%",
      marginRight: 'auto',
      marginLeft: '0',
      },
    "&:hover": {
      backgroundColor: "#8e0922",
      opacity: 0.8,
      transition: "opacity 0.3s ease",
    },
  },
  btnNavigate: {
    position: "absolute",
    left: 17,
    bottom: 0,
    height: "40px",
    width: "200px",
    fontSize: "15px",
    fontWeight: "700",
    color: "rgba(248, 248, 248, 1)",
    backgroundColor: "rgba(70, 163, 88, 1)",
    "@media (max-width: 480px)": {
    bottom: '6%'
    },
    "&:hover": {
      backgroundColor: "rgba(70, 163, 88, 1)",
    },
  },
  menuListComments:{
gap: '20px',
display: 'flex',
width: '100%',
flexDirection: 'column',
"@media (max-width: 767px)": {
  marginTop: '30px',
  },
  },
  menuItemComment:{
    backgroundColor: "rgba(249, 249, 249, 1)",
    borderRadius: "20px",
    padding: "20px",
    width: '100%',
    cursor: "default",
    position: 'relative',
    
  },
  boxComment:{
display: 'flex',
width: '100%',
justifyContent: 'space-between',
flexDirection: 'column',
gap: '20px',
  },
  commentText:{
    color: "#727272",
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
    whiteSpace: 'pre-line'
  },
  imgComment:{
    width: '70px',
    height: '70px',
minWidth: '70px',
maxHeight: '70px',
borderRadius: '5px'
  },
  messageDeleteComment:{
  },
  boxCardComment:{
gap: '30px'
  },
  rating:{
position: 'absolute',
top: '20px',
right: '20px',
  },
  mainAddress:{
    backgroundColor: '#CCCCCC',
    marginTop: '10px',
    color: "#686868",
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
    padding: '8px',
    width: 'fit-content',
    borderRadius: '5px',
  },
 containerUploadedImg: (isHovered) => ({
    position: 'relative',
    cursor: isHovered ? 'pointer' : 'default',
    "&::before": {
      content: '""',
      display: isHovered ? 'block' : 'none', 
      width: "100%",
      position: "absolute",
      height: "100%",
      backgroundColor: "rgba(114, 114, 114, 0.5)",
    },
    "@media (max-width: 767px)": {
      position: 'absolute',
      left: '150px',
      bottom: '150px',
    },
  }),
  uploadedImg:{
height: "50px", width: "50px",
"@media (max-width: 767px)": {
  height: "90px", width: "90px",
  borderRadius: '10px',
},
  },
  searchIcon:{
    position: 'absolute',
    top: '15px',
    left: '15px',
    color: 'rgb(61, 61, 61)',
  },
  modalImg:{
    height: '100vh',
    width: 'auto',
    outline: 'none',
    marginTop: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  btnBack: {
    position: "absolute",
    width: "35px",
    height: "35px",
    backgroundColor: "rgba(243, 243, 243, 1)",
    border: "none",
    borderRadius: "50%",
    top: "30px",
    left: "5%",
    cursor: "pointer",
  },
  IconContainer: {
    paddingTop: "38px",
    display: "flex",
    position: "absolute",
    zIndex: 1000,
    top: -20,
    width: "100%",
    justifyContent: "space-between",
    "@media (min-width: 768px)": {
      display: "none",
    },
  },
  BackButton: {
    width: "35px",
    height: "35px",
    background: "#f0f0f0",
    marginLeft: "28px",
  },

});


