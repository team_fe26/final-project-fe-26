import { Container, Typography } from "@mui/material"
import { Styles } from "./styles";
import MyAccount from "../../../components/MyAccount/MyAccount";
import CommentsHistory from "../Parts/CommentsHistory";
import BreadCrumbsLink from "../../../components/BreadcrumbsLink/BeadcrumbsLink"
export default function UserPageHistoryComments() {
  const classes = Styles();
  return (
    <Container className={classes.Wrapper}>
      <BreadCrumbsLink />
      <Container className={classes.billingContainer}>
        <MyAccount />

        <Container>
          <Typography className={classes.title} variant="h1">
            History Comments
          </Typography>
          <Typography className={classes.description} variant="h2">
            You can see your history comments, delete comment.
          </Typography>
          <CommentsHistory></CommentsHistory>
        </Container>
      </Container>
    </Container>
  );
}