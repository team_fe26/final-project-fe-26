import {Typography, Container} from "@mui/material";
import { Styles } from "./styles";
import MyAccount from "../../../components/MyAccount/MyAccount";
import ChangePassword from '../Parts/ChangePassword'
import ChangePersonalInformation from '../Parts/ChangePersonalInformation'
import BreadCrumbsLink  from "../../../components/BreadcrumbsLink/BeadcrumbsLink"
export default function UserPageSettings() {
  const classes = Styles();
  return (
    <Container className={classes.Wrapper}>
      <BreadCrumbsLink />
    <Container className={classes.billingContainer}>
      <MyAccount/>
      <Container>
        <Typography className={classes.title} variant="h1">
          Personal Information
        </Typography>
        <ChangePersonalInformation></ChangePersonalInformation>
        <ChangePassword></ChangePassword>
      </Container>
    </Container>
    </Container>
  );
}
