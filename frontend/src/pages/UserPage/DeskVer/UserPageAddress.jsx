import { Typography, Container} from "@mui/material";
import { Styles } from "./styles";
import MyAccount from "../../../components/MyAccount/MyAccount";
import BillingAddress from "../../../components/Checkout/BillingAddress";
import BreadCrumbsLink  from "../../../components/BreadcrumbsLink/BeadcrumbsLink"

export default function UserPageAddress() {
  const classes = Styles();
  return (
    <Container className={classes.Wrapper}>
      <BreadCrumbsLink />
    <Container className={classes.billingContainer}>
    
      <MyAccount/>

      <Container>
        <Typography className={classes.title} variant="h1">
          Billing Address
        </Typography>
        <Typography className={classes.description} variant="h2">
          The following addresses will be used on the checkout page by default.
        </Typography>
        <BillingAddress></BillingAddress>
      </Container>
    </Container>
    </Container>
  );
}
