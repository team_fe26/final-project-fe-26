import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  Wrapper: {
    position: "relative",
    padding: "0 25px",
    "@media (max-width: 768px)": {
      display: "block",
      maxWidth: "768px",
      marginTop: "0",
      padding: 0
    },
  }, 
  billingContainer: {
    display: "flex",
    flexDirection: "row",
    padding: 0,
    gap: "8%",
    marginTop: "25px",
  },

  title: {
    fontSize: "17px",
    fontFamily: "'Montserrat', sans-serif",
    fontWeight: "700",
    color: "#3D3D3D",
    marginBottom: "9px",
  },
  description: {
    fontSize: "17px",
    fontFamily: "'Montserrat', sans-serif",
    color: "#3D3D3D",
    marginTop: "10px",
    marginBottom: "30px",
  },
  btn: {
    marginTop: "50px",
    padding: "13px 23px",
    height: "40px",
    borderRadius: "3px",
    backgroundColor: "#46A358",
    color: "#FFFFFF",
    textTransform: "capitalize",
    fontWeight: "700",
    fontSize: "15px",
    border: "none",
    cursor: "pointer",

    "&:hover": {
      backgroundColor: "#46A358",
      opacity: 0.8,
      transition: "opacity 0.3s ease",
    },
  },
  ship: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: "50px",
  },

});
