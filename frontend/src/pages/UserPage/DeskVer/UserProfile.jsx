import Profile from "../Parts/Profile";
import { Container, Typography } from "@mui/material";
import MyAccount from "../../../components/MyAccount/MyAccount";
import { Styles } from "./styles";

import BreadCrumbsLink  from "../../../components/BreadcrumbsLink/BeadcrumbsLink"
export default function UserProfile() {
  const classes = Styles();

  return (
    <Container className={classes.Wrapper}>

      <BreadCrumbsLink />

      <Container className={classes.billingContainer}>
      <MyAccount/>
      <Container  style={{padding: 0}}>
        <Typography className={classes.title} variant="h1">
          {" "}
          Your Account
        </Typography>
        <Profile></Profile>
      </Container>
    </Container>
    </Container>
  );
}
