import { Container, Typography, Box, Modal } from "@mui/material";
import { useEffect, useState } from "react";
import LoginPart from "../Parts/Login";
import SignUpPart from "../Parts/SignUp";
import { Styles } from "./styles";
import { useDispatch, useSelector } from "react-redux";
import SuccessfulAuthorization from "../Parts/SuccessfulAuthorization";
import { setIsShowModal, setIsForgotPassword, setGoToCode, setIsCodeTrue } from "../../../redux/authorization.slice/authorization.slice";
export default function ModalWindow() {
  const dispatch = useDispatch()
  const [stateAuthentication, setStateAuthentication] = useState("login");
  const { loggedIn, isShowModal, isForgotPassword, goToCode, isCodeTrue } = useSelector((state) => state.authorization)
  const classes = Styles({isForgotPassword, goToCode, isCodeTrue});
  useEffect(() => {
    if (loggedIn && isShowModal) {
      const autoCloseTimeoutId = setTimeout(() => {
        dispatch(setIsShowModal(false));
      }, 1000); 

      return () => {
        clearTimeout(autoCloseTimeoutId);
      };
    }
  }, [loggedIn, isShowModal, dispatch]);
  return (
    <>
      <Modal
    open={isShowModal}
  aria-labelledby="modal-modal-title"
  aria-describedby="modal-modal-description"
        onClick={() => {
          dispatch(setIsShowModal(false))
          dispatch(setIsForgotPassword(false))
          dispatch(setGoToCode(false))
          dispatch(setIsCodeTrue(false))
        }}
      >
        <Container
          onClick={(e) => {
            e.stopPropagation();
          }}
          className={classes.container}
        >
          {loggedIn ? (
            <SuccessfulAuthorization></SuccessfulAuthorization>
           ) : (
            <>
              <button
                onClick={() => {
                  dispatch(setIsShowModal(false));
                  dispatch(setIsForgotPassword(false))
                  dispatch(setGoToCode(false))
                  dispatch(setIsCodeTrue(false))
                }}
                className={classes.close}
              ></button>
              <Box className={classes.content}>
                <Typography variant="body1" className={classes.containerLink}>
                  <span
                    className={classes.navlink}
                    onClick={() => {
                      setStateAuthentication("login");
                    }}
                    style={{
                      color:
                        stateAuthentication === "login"
                          ? (isForgotPassword ? (goToCode ? (isCodeTrue ? 'black': '#EC6E6E') : `#B785B7`) :(`rgba(70, 163, 88, 1`))
                          : "rgba(61, 61, 61, 1)",
                    }}
                  >
                    Login
                  </span>
                  <span
                    className={classes.navlink}
                    onClick={() => {
                      setStateAuthentication("register");
                      dispatch(setIsForgotPassword(false))
                      dispatch(setGoToCode(false))
                    }}
                    style={{
                      color:
                        stateAuthentication === "register"
                          ? "rgba(70, 163, 88, 1)"
                          : "rgba(61, 61, 61, 1)",
                    }}
                  >
                    Register
                  </span>
                </Typography>
                <Typography variant="body1" className={classes.topComment}>
  {stateAuthentication === "login" ? (
    isForgotPassword ? (
      goToCode ? (
        isCodeTrue ? 'Create a new password and confirm it': 
        "Enter the valid code to reset your password"
      ) : (
        "Enter a valid email to receive the code"
      )
    ) : (
      "Enter your email and password to log in"
    )
  ) : (
    "Enter your email and password to register"
  )}
</Typography>
                {stateAuthentication === "login" ? (
                  <LoginPart></LoginPart>
                ) : (
                  <SignUpPart></SignUpPart>
                )}
              </Box>
            </>
          )} 
        </Container>
      </Modal>
    </>
  );
}
