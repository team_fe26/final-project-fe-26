import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
    container: (props) => ({
    width: '500px',
    backgroundColor:'rgba(234, 234, 234, 1)',
    display: 'flex',
    justifyContent: 'center',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    alignItems: 'center',
    '&::after':{
        content: '""',
        position: 'absolute',
        display: 'block', 
        backgroundColor: props.isForgotPassword ? (props.goToCode ? (props.isCodeTrue ? 'black': '#EC6E6E') : `#B785B7`): 'rgba(70, 163, 88, 1)',
        width: '100%',
        height: '10px',
        bottom: '0'
    }
    }),
close: (props) => ({
backgroundColor: 'transparent',
border: 'none',
width: '18px',
height: '18px',
position: 'absolute',
top: '20px',
right: '20px',
cursor: 'pointer',
"&::after":{
content: '""',
width: '1px',
height: '18px',
position: 'relative',
display: 'block',
bottom: '18px',
transform: 'rotate(135deg)',
backgroundColor: props.isForgotPassword ? (props.goToCode ? (props.isCodeTrue ? 'black': '#EC6E6E') : `#B785B7`): 'rgba(70, 163, 88, 1)',
},
"&::before":{
content: '""',
width: '1px',
position: 'relative',
height: '18px',
display: 'block',
transform: 'rotate(50deg)',
backgroundColor: props.isForgotPassword ? (props.goToCode ? (props.isCodeTrue ? 'black': '#EC6E6E') : `#B785B7`): 'rgba(70, 163, 88, 1)',
},

}),
content:{
    width: '300px',
    marginBottom: '60px',
    marginTop: '25px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
},
containerLink:{
wordSpacing: '20px',
position: 'relative',
display: 'flex',
gap: '30px',
marginBottom: '50px',
marginTop: '30px',
'&::after':{
content: '""',
position: 'absolute',
height: '15px',
width: '1px',
backgroundColor: 'rgba(61, 61, 61, 1)',
top: '24%',
left: '42%',
},
},
navlink:{
    fontSize: '20px',
    fontWeight: '500',
    cursor: 'pointer',
    fontFamily: '"Montserrat", sans-serif',
},
topComment:{
    fontSize: '13px',
    color: 'rgba(61, 61, 61, 1)',
    fontWeight: '400',
    fontFamily: '"Montserrat", sans-serif',
    marginBottom: '5px',
        },
})