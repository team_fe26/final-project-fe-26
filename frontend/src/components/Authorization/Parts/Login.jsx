import { Button, Typography, Box } from "@mui/material";
import { Styles } from "./styles";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import {
  setLogged,
  setUserId,
  setIsForgotPassword,
  setGoToCode,
  setIsCodeTrue,
  setTime,
} from "../../../redux/authorization.slice/authorization.slice";
import { useDispatch, useSelector } from "react-redux";
import { useState} from "react";
import { replaceWishlistFromServer } from "../../../redux/wishlist.slice/wishlist.slice";
import { replaceCartFromServer } from "../../../redux/cart.slice/cart.slice";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import VisibilityIcon from "@mui/icons-material/Visibility";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import IconButton from "@mui/material/IconButton";
import { useNavigate } from "react-router-dom";
export default function LoginPart() {
  const navigate = useNavigate();
  const [visibility, setVisibility] = useState(false);
  const [visibilityConfirm, setVisibilityConfirm] = useState(false);
  const dispatch = useDispatch();
  const [errorFromServer, setErrorFromServer] = useState("");
  const { isForgotPassword, goToCode, isCodeTrue, time } =
    useSelector((state) => state.authorization);
  const [code, setCode] = useState(0);
  const [email, setEmail] = useState("");
  const classes = Styles(time);
  const validationShema = Yup.object().shape({
    email: Yup.string().email("Invalid email*").required("Email is required*"),
    password: Yup.string()
      .min(8, "Password should be min 8 characters*")
      .required("Password is required*"),
  });
  const validationShemaForgotPassword = Yup.object().shape({
    email: Yup.string().email("Invalid email*").required("Email is required*"),
  });
  const validationShemaCode = Yup.object().shape({
    verificationCode: Yup.number()
      .required("Invalid code")
      .test(
        "Invalid code",
        "Invalid code",
        (val) => val && val.toString().length === 6
      ),
  });
  const validationShemaNewPassword = Yup.object().shape({
    password: Yup.string()
      .min(8, "Password should be min 8 characters*")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&_])[A-Za-z\d@$!%*?&_]{8,}$/,
        "Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character*"
      )
      .required("Password is required*"),
    confirmPassword: Yup.string()
      .required("Please confirm your password*")
      .test("passwords-match", "Passwords must match*", function (value) {
        return this.parent.password === value;
      }),
  });
  const initialValues = {
    email: "",
    password: "",
  };
  const initialValuesForgotPassword = {
    email: "",
  };
  const initialValuesNewPassword = {
    password: "",
    confirmPassword: "",
  };
  const initialValuesCode = {
    verificationCode: "",
  };
  const onSubmit = async (values, { setSubmitting }) => {
    try {
      const response = await fetch(
        "https://greenshop-server.onrender.com/api/login",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          credentials: "include",
          body: JSON.stringify({
            email: values.email,
            password: values.password,
          }),
        }
      );

      if (!response.ok) {
        const errorData = await response.json();
        if (response.status === 400) {
          setErrorFromServer(errorData.error);
          throw new Error(errorData.error);
        }
        throw new Error(`HTTP error! Status: ${response.status}`);
      }

      const data = await response.json();
      console.log(data);
      dispatch(setUserId(data.userId));
      dispatch(setLogged(true));
      if (data.wishlist?.products) {
        if (data.wishlist?.products) {
          if (data.wishlist.products.length > 0) { 
            dispatch(replaceWishlistFromServer(data.wishlist.products));
          }
        }
        
        if (data.cart?.products) {
          if (data.cart.products.length > 0) { 
            dispatch(replaceCartFromServer(data.cart.products));
          }
        }
      }
      localStorage.setItem("token", JSON.stringify(data.token));
      if (window.innerWidth <= 767) {
        navigate("/user");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    } finally {
      setSubmitting(false);
    }
  };
  const onSubmitForgotPassword = async (
    values,
    { setSubmitting, resetForm }
  ) => {
    try {
      const response = await fetch(
        "https://greenshop-server.onrender.com/api/forgot-password",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          credentials: "include",
          body: JSON.stringify({
            email: values.email,
          }),
        }
      );

      if (!response.ok) {
        const errorData = await response.json();
        if (response.status === 400) {
          setErrorFromServer(errorData.error);
          throw new Error(errorData.error);
        }
        throw new Error(`HTTP error! Status: ${response.status}`);
      }

      const data = await response.json();
      setCode(data.code);
      initialValuesCode.verificationCode = "";
      setEmail(values.email);
      dispatch(setGoToCode(true));
      resetForm();
      dispatch(setTime(60));
    } catch (error) {
      console.error("An error occurred:", error);
    } finally {
      setSubmitting(false);
    }
  };
  const onSubmitCode = (values, { resetForm }) => {
    console.log(values.verificationCode, code);
    if (values && Number(values.verificationCode) === code) {
      dispatch(setIsCodeTrue(true));
    } else {
      console.log("Incorrect code");
    }
    resetForm();
  };
  const onSubmitResetPassword = async (values, { setSubmitting }) => {
    try {
      const response = await fetch(
        "https://greenshop-server.onrender.com/api/reset-password",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          credentials: "include",
          body: JSON.stringify({
            email,
            password: values.password,
          }),
        }
      );

      if (!response.ok) {
        const errorData = await response.json();
        if (response.status === 400) {
          setErrorFromServer(errorData.error);
          throw new Error(errorData.error);
        }
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      dispatch(setIsForgotPassword(false));
      dispatch(setGoToCode(false));
      dispatch(setIsCodeTrue(false));
      setCode(0);
    } catch (error) {
      console.error("An error occurred:", error);
    } finally {
      setSubmitting(false);
    }
  };

  const handleGoogleLogin = () => {
    window.location.href =
      "https://greenshop-server.onrender.com/api/auth/google";
  };
  return (
    <>
      {isForgotPassword ? (
        goToCode ? (
          isCodeTrue ? (
            <Formik
              initialValues={initialValuesNewPassword}
              validationSchema={validationShemaNewPassword}
              onSubmit={onSubmitResetPassword}
            >
              <Form className={classes.form}>
                <Field name="password">
                  {({ field, form }) => (
                    <div className={classes.containerField}>
                      <input
                        className={classes.fieldPasswordReset}
                        placeholder="New Password"
                        autoComplete="off"
                        type={visibility ? "text" : "password"}
                        {...field}
                        onChange={(e) => {
                          const password = e.target.value;
                          if (password.length <= 18) {
                            form.setFieldValue("password", password);
                          }
                        }}
                      />
                      {visibility ? (
                        <VisibilityIcon
                          onClick={() => {
                            setVisibility(false);
                          }}
                          className={classes.eyeIcon}
                        />
                      ) : (
                        <VisibilityOffIcon
                          onClick={() => {
                            setVisibility(true);
                          }}
                          className={classes.eyeIcon}
                        />
                      )}
                    </div>
                  )}
                </Field>
                <ErrorMessage
                  className={classes.errorMessage}
                  name="password"
                  component="div"
                ></ErrorMessage>
                <Field name="confirmPassword">
                  {({ field, form }) => (
                    <div className={classes.containerField}>
                      <input
                        className={classes.fieldPasswordReset}
                        autoComplete="off"
                        placeholder="Confirm Password"
                        type={visibilityConfirm ? "text" : "password"}
                        {...field}
                        onChange={(e) => {
                          const password = e.target.value;
                          if (password.length <= 18) {
                            form.setFieldValue("confirmPassword", password);
                          }
                        }}
                      />
                      {visibilityConfirm ? (
                        <VisibilityIcon
                          onClick={() => {
                            setVisibilityConfirm(false);
                          }}
                          className={classes.eyeIcon}
                        />
                      ) : (
                        <VisibilityOffIcon
                          onClick={() => {
                            setVisibilityConfirm(true);
                          }}
                          className={classes.eyeIcon}
                        />
                      )}
                    </div>
                  )}
                </Field>
                <ErrorMessage
                  className={classes.errorMessage}
                  name="confirmPassword"
                  component="div"
                ></ErrorMessage>
                {errorFromServer && (
                  <Typography
                    className={classes.errorMessageMessageFromServer}
                    variant="body1"
                  >
                    {errorFromServer}
                  </Typography>
                )}
                <Button
                  type="submit"
                  className={classes.buttonResetPassword}
                  fullWidth
                >
                  Reset Password
                </Button>
              </Form>
            </Formik>
          ) : (
            <Formik
              onSubmit={onSubmitCode}
              initialValues={initialValuesCode}
              validationSchema={validationShemaCode}
            >
              <Form className={classes.form}>
                <IconButton
                  className={classes.buttonBackForgotPassword}
                  onClick={() => {
                    dispatch(setGoToCode(false));
                  }}
                >
                  <ArrowBackIcon></ArrowBackIcon>
                </IconButton>
                <input
                  type="text"
                  name="fakeEmail"
                  autoComplete="off"
                  style={{ display: "none" }}
                />
                <Field
                  className={classes.fieldSendCode}
                  placeholder="Enter code"
                  type="text"
                  name="verificationCode"
                  autoComplete="off"
                />
                <ErrorMessage
                  className={classes.errorMessage}
                  name="verificationCode"
                  component="div"
                />
                <Button
                  type="submit"
                  className={classes.buttonSendCode}
                  fullWidth
                >
                  Continue
                </Button>
              </Form>
            </Formik>
          )
        ) : (
          <Formik
            onSubmit={onSubmitForgotPassword}
            initialValues={initialValuesForgotPassword}
            validationSchema={validationShemaForgotPassword}
          >
            <Form className={classes.form}>
              <IconButton
                className={classes.buttonBackForgotPassword}
                onClick={() => {
                  dispatch(setIsForgotPassword(false));
                }}
              >
                <ArrowBackIcon></ArrowBackIcon>
              </IconButton>
              <Field
                className={classes.fieldForgotPassword}
                placeholder="Enter your email address"
                type="text"
                name="email"
              />
              <ErrorMessage
                className={classes.errorMessage}
                name="email"
                component="div"
              />
              {errorFromServer && (
                <Typography
                  className={classes.errorMessageMessageFromServer}
                  variant="body1"
                >
                  {errorFromServer}
                </Typography>
              )}
              {time > 0 && (
                <Typography className={classes.timer}>
                  You can resend the code after 00:{time}
                </Typography>
              )}
              <Button
                type={time > 0 ? "button" : "submit"}
                className={classes.buttonForgotPassword}
                fullWidth
              >
                Send Code
              </Button>
              {code !== 0 ? (
                <IconButton
                  className={classes.buttonBackSendCode}
                  onClick={() => {
                    dispatch(setGoToCode(true));
                    initialValuesCode.code = "";
                  }}
                >
                  <ArrowForwardIcon></ArrowForwardIcon>
                </IconButton>
              ) : null}
            </Form>
          </Formik>
        )
      ) : (
        <Formik
          onSubmit={onSubmit}
          initialValues={initialValues}
          validationSchema={validationShema}
        >
          <Form className={classes.form}>
            <Field
              className={classes.field}
              placeholder="Enter your email address"
              type="text"
              name="email"
            />
            <ErrorMessage
              className={classes.errorMessage}
              name="email"
              component="div"
            />
            <Field autoComplete="off" name="password">
              {({ field, form }) => (
                <div className={classes.containerField}>
                  <input
                    className={classes.fieldPassword}
                    placeholder="Password"
                    type={visibility ? "text" : "password"}
                    autoComplete="off"
                    {...field}
                    onChange={(e) => {
                      const password = e.target.value;
                      if (password.length <= 18) {
                        form.setFieldValue("password", password);
                      }
                    }}
                  />
                  {visibility ? (
                    <VisibilityIcon
                      onClick={() => {
                        setVisibility(false);
                      }}
                      className={classes.eyeIcon}
                    />
                  ) : (
                    <VisibilityOffIcon
                      onClick={() => {
                        setVisibility(true);
                      }}
                      className={classes.eyeIcon}
                    />
                  )}
                </div>
              )}
            </Field>
            <ErrorMessage
              name="password"
              className={classes.errorMessage}
              component="div"
            />
            <Typography
              onClick={() => {
                dispatch(setIsForgotPassword(true));
              }}
              className={classes.textForgotPassword}
              variant="body1"
            >
              Forgot Password?
            </Typography>
            {errorFromServer && (
              <Typography
                className={classes.errorMessageMessageFromServer}
               variant="body1"
              >
                {errorFromServer}
              </Typography>
            )}
            <Button
              type="submit"
              className={classes.buttonLoginCreate}
              fullWidth
            >
              Login
            </Button>
            <Box className={classes.orLoginRegisterBox}>
              <Typography className={classes.orLoginRegister} variant="body1">
                Or login with
              </Typography>
            </Box>
            <Button
              className={classes.buttonGoogle}
              onClick={handleGoogleLogin}
              fullWidth
              startIcon={
                <img
                  src={
                    window.innerWidth < 766 ? "/Google-icon-white.png" : "/Google-icon.png"
                  }
                  alt="Google"
                />
              }
            >
              Login with Google
            </Button>
          </Form>
        </Formik>
      )}
    </>
  );
}
