import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
  setLogged,
  setIsShowModal,
} from "../../../redux/authorization.slice/authorization.slice";
import { replaceWishlistFromServer } from "../../../redux/wishlist.slice/wishlist.slice";
import { replaceCartFromServer } from "../../../redux/cart.slice/cart.slice";

export default function TokenHandlerRegister() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    const queryParams = new URLSearchParams(window.location.search);
    const token = queryParams.get("token");

    if (token) {
      localStorage.setItem("token", JSON.stringify(token));
      dispatch(setLogged(true));
      dispatch(setIsShowModal(false));

      const fetchFn = async () => {
        try {
          const token = JSON.parse(localStorage.getItem("token"));

          const response = await fetch(
            "https://greenshop-server.onrender.com/api/confirm-email",
            {
              method: "GET",
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
              },
              credentials: "include",
            }
          );

          if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
          }

          const data = await response.json();
          if (data.customer.isConfirmed) {
            dispatch(setLogged(true));
            dispatch(setIsShowModal(false));

            if (data.wishlist?.products) {
              if (data.wishlist.products.length > 0) { 
                dispatch(replaceWishlistFromServer(data.wishlist.products));
              }
            }
            
            if (data.cart?.products) {
              if (data.cart.products.length > 0) { 
                dispatch(replaceCartFromServer(data.cart.products));
              }
            }
          }
          navigate("/");
          window.location.reload();
        } catch (error) {
          console.error("An error occurred:", error);
        }
      };

      fetchFn();
    }
  }, [navigate, dispatch]);

  return null;
}
