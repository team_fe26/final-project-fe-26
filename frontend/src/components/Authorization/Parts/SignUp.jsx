import { Button, Typography, Box } from "@mui/material";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Styles } from "./styles";
import {
  setLogged,
  setUserId,
} from "../../../redux/authorization.slice/authorization.slice";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import { setIsShowModalConfirm } from "../../../redux/authorization.slice/authorization.slice";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import VisibilityIcon from "@mui/icons-material/Visibility";
import ConfirmModal from "./ConfirmEmailModal";
export default function SignUpPart() {
  const [visibility, setVisibility] = useState(false);
  const [visibilityConfirm, setVisibilityConfirm] = useState(false);
  const dispatch = useDispatch();
  const [errorFromServer, setErrorFromServer] = useState("");
  const { isShowModalConfirm } = useSelector((state) => state.authorization);
  const validationShema = Yup.object().shape({
    username: Yup.string().required("Username is required*"),
    email: Yup.string().email("Invalid email*").required("Email is required*"),
    password: Yup.string()
      .min(8, "Password should be min 8 characters*")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&_])[A-Za-z\d@$!%*?&_]{8,}$/,
        "Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character*"
      )
      .required("Password is required*"),
    confirmPassword: Yup.string()
      .required("Please confirm your password*")
      .test("passwords-match", "Passwords must match*", function (value) {
        return this.parent.password === value;
      }),
  });
  const initialValues = {
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
  };
  const onSubmit = async (values, { setSubmitting }) => {
    try {
      const response = await fetch("https://greenshop-server.onrender.com/api/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        body: JSON.stringify({
          email: values.email,
          password: values.password,
          username: values.username,
        }),
      });

      if (!response.ok) {
        const errorData = await response.json();
        if (response.status === 400) {
          setErrorFromServer(errorData.error);
          throw new Error(errorData.error);
        }
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
dispatch(setIsShowModalConfirm(true))
    } catch (error) {
      console.error("An error occurred:", error);
    } finally {
      setSubmitting(false);
    }
  };
  const handleGoogleRegister = () => {
    window.location.href = "https://greenshop-server.onrender.com/api/auth/google";
  };
  const classes = Styles();
  return (
    <>
          {isShowModalConfirm ? <ConfirmModal></ConfirmModal> : null}
      <Formik
        onSubmit={onSubmit}
        initialValues={initialValues}
        validationSchema={validationShema}
      >
        <Form className={classes.form}>
          {" "}
          <Field
            className={classes.field}
            placeholder="Username"
            type="text"
            name="username"
          />
          <ErrorMessage
            className={classes.errorMessage}
            name="username"
            component="div"
          ></ErrorMessage>
          <Field
            className={classes.field}
            placeholder="Enter your email address"
            type="text"
            name="email"
          />
          <ErrorMessage
            className={classes.errorMessage}
            name="email"
            component="div"
          ></ErrorMessage>
          <Field name="password">
            {({ field, form }) => (
              <div className={classes.containerField}>
                <input
                  className={classes.fieldPassword}
                  placeholder="Password"
                  type={visibility ? 'text' : 'password'}
                  {...field}
                  onChange={(e) => {
                    const password = e.target.value;
                    if (password.length <= 18) {
                      form.setFieldValue("password", password);
                    }
                  }}
                />
                 {visibility ? (
                  <VisibilityIcon
                    onClick={() => {
                      setVisibility(false);
                    }}
                    className={classes.eyeIcon}
                  />
                ) : (
                  <VisibilityOffIcon
                    onClick={() => {
                      setVisibility(true);
                    }}
                    className={classes.eyeIcon}
                  />
                )}
              </div>
            )}
          </Field>
          <ErrorMessage
            className={classes.errorMessage}
            name="password"
            component="div"
          ></ErrorMessage>
          <Field name="confirmPassword">
            {({ field, form }) => (
              <div className={classes.containerField}>
                <input
                  className={classes.fieldPassword}
                  placeholder="Confirm Password"
                  type={visibilityConfirm ? 'text' : 'password'}
                  {...field}
                  onChange={(e) => {
                    const password = e.target.value;
                    if (password.length <= 18) {
                      form.setFieldValue("confirmPassword", password);
                    }
                  }}
                />
               {visibilityConfirm ? (
                  <VisibilityIcon
                    onClick={() => {
                      setVisibilityConfirm(false);
                    }}
                    className={classes.eyeIcon}
                  />
                ) : (
                  <VisibilityOffIcon
                    onClick={() => {
                      setVisibilityConfirm(true);
                    }}
                    className={classes.eyeIcon}
                  />
                )}
              </div>
            )}
          </Field>
          <ErrorMessage
            className={classes.errorMessage}
            name="confirmPassword"
            component="div"
          ></ErrorMessage>
          {errorFromServer ? (
            <Typography className={classes.errorMessageMessageFromServer}>
              {errorFromServer}
            </Typography>
          ) : null}
        <Button type="submit" className={classes.buttonLoginCreate} fullWidth>
  Register
</Button>
          <Box className={classes.orLoginRegisterBox}>
            <Typography className={classes.orLoginRegister} variant="body1">
              Or register with
            </Typography>
          </Box>
          <Button
            className={classes.buttonGoogle}
            onClick={handleGoogleRegister}
            fullWidth
            startIcon={
              <img
                src={
                  window.innerWidth < 766
                    ? "/Google-icon-white.png"
                    : "/Google-icon.png"
                }
                alt="Google"
              />
            }
          >
            Continue with Google
          </Button>
        </Form>
      </Formik>
    </>
  );
}
