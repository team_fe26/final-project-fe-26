import { Container, Typography, Box } from "@mui/material";
import { Styles } from "./styles";
export default function SuccessfulAuthorization() {
  const classes = Styles();
  return (
    <>
      <Container className={classes.successfulAuthorization}>
        <Box className={classes.wrapCheckmark}></Box>
        <Typography className={classes.successfulMessage} variant="h1">
          You have successfully logged in!
        </Typography>
      </Container>
    </>
  );
}
