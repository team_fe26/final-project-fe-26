import { Typography, Box, Modal } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { setIsShowModalConfirm } from "../../../redux/authorization.slice/authorization.slice";
import { Styles } from "./styles";
export default function ConfirmModal() {
  const classes = Styles();
  const dispatch = useDispatch();
  const { isShowModalConfirm } = useSelector((state) => state.authorization);
  return (
    <>
      <Modal
        open={isShowModalConfirm}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        onClick={() => {
          dispatch(setIsShowModalConfirm(false));
        }}
      >
        <Box className={classes.blockConfirmEmail}>
          {" "}
          <button
            onClick={() => {
              dispatch(setIsShowModalConfirm(false));
            }}
            className={classes.close}
          ></button>{" "}
          <Typography component="body1" className={classes.messageConfirmEmail}>
            Check your email to confirm your registration
          </Typography>
        </Box>
      </Modal>
    </>
  );
}
