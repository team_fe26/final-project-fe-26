import { Styles } from "./styles"
import clsx from 'clsx';
export default function Loader(){
    const classes = Styles({ color: 'rgba(70, 163, 88, 1)', size: 12, time: 1 });
  return (
    <main className={classes.main}>
        <h1 className={classes.title}>greenshop</h1>
      <div className={classes.dankAssLoader}>
        <div className={classes.row}>
          <div className={clsx(classes.arrow, 'up', 'outer-18')}></div>
          <div className={clsx(classes.arrow, 'down', 'outer-17')}></div>
          <div className={clsx(classes.arrow, 'up', 'outer-16')}></div>
          <div className={clsx(classes.arrow, 'down', 'outer-15')}></div>
          <div className={clsx(classes.arrow, 'up', 'outer-14')}></div>
        </div>
        <div className={classes.row}>
          <div className={clsx(classes.arrow, 'up', 'outer-1')}></div>
          <div className={clsx(classes.arrow, 'down', 'outer-2')}></div>
          <div className={clsx(classes.arrow, 'up', 'inner-6')}></div>
          <div className={clsx(classes.arrow, 'down', 'inner-5')}></div>
          <div className={clsx(classes.arrow, 'up', 'inner-4')}></div>
          <div className={clsx(classes.arrow, 'down', 'outer-13')}></div>
          <div className={clsx(classes.arrow, 'up', 'outer-12')}></div>
        </div>
        <div className={classes.row}>
          <div className={clsx(classes.arrow, 'down', 'outer-3')}></div>
          <div className={clsx(classes.arrow, 'up', 'outer-4')}></div>
          <div className={clsx(classes.arrow, 'down', 'inner-1')}></div>
          <div className={clsx(classes.arrow, 'up', 'inner-2')}></div>
          <div className={clsx(classes.arrow, 'down', 'inner-3')}></div>
          <div className={clsx(classes.arrow, 'up', 'outer-11')}></div>
          <div className={clsx(classes.arrow, 'down', 'outer-10')}></div>
        </div>
        <div className={classes.row}>
          <div className={clsx(classes.arrow, 'down', 'outer-5')}></div>
          <div className={clsx(classes.arrow, 'up', 'outer-6')}></div>
          <div className={clsx(classes.arrow, 'down', 'outer-7')}></div>
          <div className={clsx(classes.arrow, 'up', 'outer-8')}></div>
          <div className={clsx(classes.arrow, 'down', 'outer-9')}></div>
        </div>
      </div>
      <p className={classes.text}>Waiting, we prepare the best plants for you...</p>
    </main>
  );
}