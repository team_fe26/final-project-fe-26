import { createUseStyles } from 'react-jss';

export const Styles = createUseStyles({
  main: {
    width: '100vw',
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    background: '#fff',
    gap: '40px',
  },
  title: {
    textTransform: 'uppercase',
    color: 'rgba(70, 163, 88, 1)',
    fontSize: '72px',
    textShadow: `
      1px 1px 0 rgba(0, 0, 0, 0.1),
      2px 2px 0 rgba(0, 0, 0, 0.2),
      3px 3px 0 rgba(0, 0, 0, 0.3),
      4px 4px 0 rgba(0, 0, 0, 0.4)
    `,
    "@media (max-width: 767px)": {
        fontSize: '56px',
      },
  },
  text:{
fontSize: '21px',
fontWeight: '500',
textAlign: 'center',
"@media (max-width: 767px)": {

    fontSize: '18px',
  },
  },
  dankAssLoader: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  row: {
    display: 'flex',
  },
  arrow: {
    width: 0,
    height: 0,
    margin: ({ size }) => `0 ${-size / 2}px`,
    borderLeft: ({ size }) => `${size}px solid transparent`,
    borderRight: ({ size }) => `${size}px solid transparent`,
    borderBottom: ({ size, color }) => `${size * 1.8}px solid ${color}`,
    animation: ({ time }) => `$blink ${time}s infinite`,
    filter: ({ size, color }) => `drop-shadow(0 0 ${size * 1.5}px ${color})`,

    '&.down': {
      transform: 'rotate(180deg)',
    },

    ...Array.from({ length: 18 }).reduce((acc, _, i) => {
      acc[`&.outer-${i + 1}`] = {
        animationDelay: ({ time }) => `-${(time / 18) * (i + 1)}s`,
      };
      return acc;
    }, {}),
    ...Array.from({ length: 6 }).reduce((acc, _, i) => {
      acc[`&.inner-${i + 1}`] = {
        animationDelay: ({ time }) => `-${(time / 6) * (i + 1)}s`,
      };
      return acc;
    }, {}),
  },
  '@keyframes blink': {
    '0%': { opacity: 0.1 },
    '30%': { opacity: 1 },
    '100%': { opacity: 0.1 },
  },
});
