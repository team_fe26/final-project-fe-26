import {
    Container,
    Box,
    IconButton,
    Typography,
    Breadcrumbs,

} from "@mui/material";
import { ArrowBack } from "@mui/icons-material";
import { Link, useNavigate} from "react-router-dom";
import { Styles } from "./style";
import { useState, useEffect } from "react";




const BreadCrumbsLink = () => {
    const [isMobile, setIsMobile] = useState(window.innerWidth < 768);
    const classes = Styles();
    const navigate = useNavigate();

    useEffect(() => {
        const handleResize = () => setIsMobile(window.innerWidth < 768);
        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    }, []);

    return (
        <Container className={classes.BreadcrumbsWrapper}>
            {isMobile ? (
                <Box className={classes.IconContainer}>
                    <IconButton onClick={() => navigate("/")} className={classes.BackButton}>
                        <ArrowBack style={{ color: "#000" }} />
                    </IconButton>
                </Box>
            ) : (
                <Breadcrumbs aria-label="breadcrumb" style={{ margin: "115px auto 0 auto", maxWidth: "1200px" }}>
                    <Link className={classes.Link} to="/">Home</Link>
                    <Typography style={{fontWeight: 700}} color="textPrimary">User Page</Typography> 
                </Breadcrumbs>
            )}
        </Container>
    );
};

export default BreadCrumbsLink;