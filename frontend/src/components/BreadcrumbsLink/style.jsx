import { createUseStyles } from "react-jss";
export const Styles = createUseStyles({
    BreadcrumbsWrapper: {
        padding: 0,
    },
    IconContainer: {
        paddingTop: "38px",
        display: "flex",
        position: "absolute",
        zIndex: 1000,
        top: -20,
        width: "100%",
        justifyContent: "space-between",
        "@media (min-width: 768px)": {
            display: "none",
        },
    },
    BackButton: {
        width: "35px",
        height: "35px",
        background: "#f0f0f0",
        marginLeft: "28px",
    },
    Link: {
        textDecoration: "none",
        color: "inherit",
        "&:hover": {
            textDecoration: "underline",
        },
    },
})

