import { useSelector, useDispatch } from "react-redux";
import { useEffect, useRef } from "react";
import {
  setLogged,
  setUserId,
  setIntervalId,
  setTime,
  setIsShowModal,
} from "../../redux/authorization.slice/authorization.slice";
import { setFilterProducts } from "../../redux/filter.slice/filter.slice";
import { setProductId } from "../../redux/product.slice/product.slice";
import { setPaymentId } from "../../redux/payment.slice/payment.slice";
import {
  setPaymentItems,
  setTotal,
  setSubtotal,
  setPaymentMethod,
  setOptionalAddress,
  setAddress,
  setIsPaymentSuccess,
  setEditOptionalAddress,
} from "../../redux/payment.slice/payment.slice";
import {
  setDiscount,
  setCoupon,
  removeCoupon,
} from "../../redux/coupon.slice/coupon.slice";
import { replaceWishlistFromServer } from "../../redux/wishlist.slice/wishlist.slice";
import { replaceCartFromServer } from "../../redux/cart.slice/cart.slice";
import { useNavigate } from "react-router-dom";
export default function LocalStorage() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const intervalRef = useRef(null);
  const { loggedIn, userId, time, intervalId, isShowModal, code } = useSelector(
    (state) => state.authorization
  );
  const { filterProducts } = useSelector((state) => state.filters);
  const {
    paymentItems,
    total,
    subtotal,
    paymentMethod,
    paymentId,
    optionalAddress,
    address,
    isPaymentSuccess,
    editOptionalAddress,
  } = useSelector((state) => state.payment);
  const { coupon, discount } = useSelector((state) => state.coupon);
  const { productId } = useSelector((state) => state.product);
  const { wishlistId } = useSelector((state) => state.wishlist);
  const { cartId } = useSelector((state) => state.cart);
  useEffect(() => {
    const storedLoggedIn = JSON.parse(localStorage.getItem("loggedIn"));
    if (storedLoggedIn) {
      dispatch(setLogged(storedLoggedIn));
    }
    const saveFilterProducts = JSON.parse(
      localStorage.getItem("filterProducts")
    );
    if (saveFilterProducts) {
      dispatch(setFilterProducts(saveFilterProducts));
    }
    const saveUserId = JSON.parse(localStorage.getItem("userId"));
    if (saveUserId) {
      dispatch(setUserId(saveUserId));
    }
    const savedWishlist = JSON.parse(localStorage.getItem("wishlistId"));
    if (savedWishlist) {
      dispatch(replaceWishlistFromServer(savedWishlist));
    }
    const storedCart = JSON.parse(localStorage.getItem("cartId"));
    if (storedCart) {
      dispatch(replaceCartFromServer(storedCart));
    }
    const savedPaymentItems = JSON.parse(localStorage.getItem("paymentItems"));
    if (savedPaymentItems) {
      dispatch(setPaymentItems(savedPaymentItems));
    }
    const savedSubtotal = JSON.parse(localStorage.getItem("subtotal"));
    if (savedSubtotal) {
      dispatch(setSubtotal(savedSubtotal));
    }
    const savedTotal = JSON.parse(localStorage.getItem("total"));
    if (savedTotal) {
      dispatch(setTotal(savedTotal));
    }
    const savedProductId = JSON.parse(localStorage.getItem("productId"));
    if (savedProductId) {
      dispatch(setProductId(savedProductId));
    }
    const storedPaymentMethod = JSON.parse(
      localStorage.getItem("paymentMethod")
    );
    if (storedPaymentMethod) {
      dispatch(setPaymentMethod(storedPaymentMethod));
    }
    const storedPaymentId = JSON.parse(localStorage.getItem("paymentId"));
    if (storedPaymentId) {
      dispatch(setPaymentId(storedPaymentId));
    }
    const storedOptionalAddress = JSON.parse(
      localStorage.getItem("optionalAddress")
    );
    if (storedOptionalAddress) {
      dispatch(setOptionalAddress(storedOptionalAddress));
    }
    const storedAddress = JSON.parse(localStorage.getItem("address"));
    if (storedAddress) {
      dispatch(setAddress(storedAddress));
    }
    const storedEditOptionalAddress = JSON.parse(
      localStorage.getItem("editOptionalAddress")
    );
    if (storedEditOptionalAddress) {
      dispatch(setEditOptionalAddress(storedEditOptionalAddress));
    }
    const storedDiscount = JSON.parse(localStorage.getItem("discount"));
    if (storedDiscount) {
      dispatch(setDiscount(storedDiscount));
    }
    const storedCoupon = JSON.parse(localStorage.getItem("coupon"));
    if (storedCoupon) {
      dispatch(setCoupon(storedCoupon));
    }
    const storedTime = localStorage.getItem("time");
    if (storedTime) {
      try {
        const parsedTime = JSON.parse(storedTime);
        if (typeof parsedTime === "number") {
          dispatch(setTime(parsedTime));
        } else {
          console.error("Parsed time is not a number:", parsedTime);
          dispatch(setTime(0));
        }
      } catch (error) {
        console.error("Error parsing time from localStorage:", error);
        dispatch(setTime(0));
      }
    } else {
      dispatch(setTime(0));
    }

    const storedIntervalId = localStorage.getItem("intervalId");
    if (storedIntervalId) {
      try {
        const parsedIntervalId = JSON.parse(storedIntervalId);
        if (typeof parsedIntervalId === "number" || parsedIntervalId === null) {
          dispatch(setIntervalId(parsedIntervalId));
        } else {
          console.error(
            "Parsed intervalId is not a number or null:",
            parsedIntervalId
          );
          dispatch(setIntervalId(null));
        }
      } catch (error) {
        console.error("Error parsing intervalId from localStorage:", error);
        dispatch(setIntervalId(null));
      }
    }
    const savedIsShowModal = JSON.parse(localStorage.getItem("isShowModal"));
    if (savedIsShowModal) {
      dispatch(setIsShowModal(savedIsShowModal));
    }
  }, [dispatch]);
  useEffect(() => {
    if (loggedIn) {
      localStorage.setItem("loggedIn", JSON.stringify(loggedIn));
    } else {
      localStorage.removeItem("loggedIn");
    }
  }, [loggedIn]);
  useEffect(() => {
    if (filterProducts) {
      localStorage.setItem("filterProducts", JSON.stringify(filterProducts));
    } else {
      localStorage.removeItem("filterProducts");
    }
  }, [filterProducts]);
  useEffect(() => {
    if (userId && loggedIn) {
      localStorage.setItem("userId", JSON.stringify(userId));
    } else {
      localStorage.removeItem("userId");
    }
  }, [userId, loggedIn]);
  useEffect(() => {
    if (wishlistId) {
      localStorage.setItem("wishlistId", JSON.stringify(wishlistId));
    } else {
      localStorage.removeItem("wishlistId");
    }
  }, [wishlistId]);
  useEffect(() => {
    if (paymentItems) {
      localStorage.setItem("paymentItems", JSON.stringify(paymentItems));
    } else {
      localStorage.removeItem("paymentItems");
    }
  }, [paymentItems]);
  useEffect(() => {
    if (subtotal) {
      localStorage.setItem("subtotal", JSON.stringify(subtotal));
    } else {
      localStorage.removeItem("subtotal");
    }
  }, [subtotal]);
  useEffect(() => {
    if (total) {
      localStorage.setItem("total", JSON.stringify(total));
    } else {
      localStorage.removeItem("total");
    }
  }, [total]);
  useEffect(() => {
    if (productId) {
      localStorage.setItem("productId", JSON.stringify(productId));
    } else {
      localStorage.removeItem("productId");
    }
  }, [productId]);
  useEffect(() => {
    if (time) {
      localStorage.setItem("time", JSON.stringify(time));
    }
  }, [time]);
  useEffect(() => {
    if (intervalId !== null) {
      localStorage.setItem("intervalId", JSON.stringify(intervalId));
    } else {
      localStorage.removeItem("intervalId");
    }
  }, [intervalId]);
  useEffect(() => {
    if (isShowModal) {
      localStorage.setItem("isShowModal", JSON.stringify(isShowModal));
    } else {
      localStorage.removeItem("isShowModal");
    }
  }, [isShowModal]);
  useEffect(() => {
    if (cartId) {
      localStorage.setItem("cartId", JSON.stringify(cartId));
    } else {
      localStorage.removeItem("cartId");
    }
  }, [cartId]);
  useEffect(() => {
    if (paymentMethod) {
      localStorage.setItem("paymentMethod", JSON.stringify(paymentMethod));
    } else {
      localStorage.removeItem("paymentMethod");
    }
  }, [paymentMethod]);
  useEffect(() => {
    if (paymentId) {
      localStorage.setItem("paymentId", JSON.stringify(paymentId));
    } else {
      localStorage.removeItem("paymentId");
    }
  }, [paymentId]);
  useEffect(() => {
    if (optionalAddress) {
      localStorage.setItem("optionalAddress", JSON.stringify(optionalAddress));
    } else {
      localStorage.removeItem("optionalAddress");
    }
  }, [optionalAddress]);
  useEffect(() => {
    if (editOptionalAddress) {
      localStorage.setItem(
        "editOptionalAddress",
        JSON.stringify(editOptionalAddress)
      );
    } else {
      localStorage.removeItem("editOptionalAddress");
    }
  }, [editOptionalAddress]);
  useEffect(() => {
    if (address) {
      localStorage.setItem("address", JSON.stringify(address));
    } else {
      localStorage.removeItem("address");
    }
  }, [address]);
  useEffect(() => {
    if (coupon) {
      localStorage.setItem("coupon", JSON.stringify(coupon));
    } else {
      localStorage.removeItem("coupon");
    }
  }, [coupon]);
  useEffect(() => {
    if (discount) {
      localStorage.setItem("discount", JSON.stringify(discount));
    } else {
      localStorage.removeItem("discount");
    }
  }, [discount]);
  useEffect(() => {
    if (loggedIn) {
      const fetchFn = async () => {
        try {
          const token = JSON.parse(localStorage.getItem("token")) || "";

          const response = await fetch(
            "https://greenshop-server.onrender.com/api/cart/update",
            {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
              },
              body: JSON.stringify({ cart: cartId }),
            }
          );

          if (!response.ok) {
            return;
          }
        } catch (error) {
          console.error("Fetch error:", error);
        }
      };
      fetchFn();
    }
  }, [cartId, dispatch, loggedIn]);
  useEffect(() => {
    if (loggedIn) {
      const fetchFn = async () => {
        try {
          const token = JSON.parse(localStorage.getItem("token")) || "";

          const response = await fetch(
            "https://greenshop-server.onrender.com/api/wishlist/update",
            {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
              },
              body: JSON.stringify({ wishlist: wishlistId }),
            }
          );

          if (!response.ok) {
            const data = await response.json();
            console.error("Error updating wishlist:", data.error);
            return;
          }
        } catch (error) {
          console.error("Fetch error:", error);
        }
      };
      fetchFn();
    }
  }, [wishlistId, dispatch, loggedIn]);

  useEffect(() => {
    if (time > 0) {
      if (!intervalRef.current) {
        intervalRef.current = setInterval(() => {
          const updatedTime = time - 1;
          if (updatedTime <= 0) {
            clearInterval(intervalRef.current);
            intervalRef.current = null;
            dispatch(setIntervalId(null));
          }
          dispatch(setTime(updatedTime));
        }, 1000);
        dispatch(setIntervalId(intervalRef.current));
      }
    } else {
      if (intervalRef.current) {
        clearInterval(intervalRef.current);
        intervalRef.current = null;
        dispatch(setIntervalId(null));
      }
    }

    return () => {
      if (intervalRef.current) {
        clearInterval(intervalRef.current);
        intervalRef.current = null;
        dispatch(setIntervalId(null));
      }
    };
  }, [time, dispatch]);
  useEffect(() => {
    if (isPaymentSuccess) {
      const processOrder = async () => {
        console.log(coupon);

        try {
          const body = {
            total,
            address,
            products: paymentItems,
          };
          if (coupon) {
            body.coupon = coupon;
          }

          const response = await fetch(
            "https://greenshop-server.onrender.com/api/send/order",
            {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              credentials: "include",
              body: JSON.stringify(body),
            }
          );

          if (!response.ok) {
            const errorData = await response.json();
            console.error("Error processing order:", errorData);
            alert("Failed to process the order. Please try again.");
          } else {
            dispatch(setIsPaymentSuccess(false));
            if (coupon) {
              dispatch(removeCoupon());
            }
            if (window.innerWidth > 767) {
              alert("Order processed successfully!");
            } else {
              navigate("/");
              dispatch(replaceCartFromServer([]));
              dispatch(setPaymentItems([]));
            }
          }
        } catch (err) {
          console.error("Error:", err);
          alert("An error occurred. Please try again.");
        }
      };

      processOrder();
    }
  }, [
    isPaymentSuccess,
    total,
    address,
    paymentItems,
    coupon,
    dispatch,
    navigate,
  ]);
  return null;
}
