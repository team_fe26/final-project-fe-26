import {
  Button,
  Divider,
  Stack,
  Typography,
  Box,
  Container,
  Dialog,
  DialogContent,
  Paper,
} from "@mui/material";
import { Styles } from "./styles";
import { setIsShowModal, setPaymentItems } from "../../redux/payment.slice/payment.slice";
import { replaceCartFromServer } from "../../redux/cart.slice/cart.slice";
import { useSelector, useDispatch } from "react-redux";
import PaymentItems from "../PaymentItems/PaymentItems";
import { useNavigate } from "react-router-dom";
export default function PaymentModal() {
  const navigate = useNavigate()
  const { isShowModal, total, paymentMethod, address } = useSelector((state) => state.payment);
  const classes = Styles(isShowModal);
  const dispatch = useDispatch();
const formatDate = (date) =>{
  const options = {day: 'numeric', month: 'short', year: 'numeric'}
  return date.toLocaleDateString('en-US', options)
}
function generateRandomNumbers(count = 6, min = 0, max = 100) {
  const numbers = [];
  for (let i = 0; i < count; i++) {
    const randomNum = Math.floor(Math.random() * (max - min + 1)) + min;
    numbers.push(randomNum);
  }
  return numbers;
}
  return (
    <Dialog
    className={classes.modalShow}
    open={isShowModal}
    scroll="paper" 
    aria-labelledby="scroll-dialog-title"
    aria-describedby="scroll-dialog-description"
    onClose={() => {
      dispatch(setIsShowModal(false));
      navigate("/");
      dispatch(replaceCartFromServer([]))
      dispatch(setPaymentItems([]))
    }}
  >
    <DialogContent dividers className={classes.modalContent}>
  
        <Container
          className={classes.modalContainer}
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <Box className={classes.thankYouContainer}>
            <button
              className={classes.close}
              onClick={() => {
                dispatch(setIsShowModal(false));
                navigate("/");
                dispatch(replaceCartFromServer([]))
                dispatch(setPaymentItems([]))
              }}
            ></button>
            <Box className={classes.thankYouBlock}>
              <img src="/thank-you.png" alt="Thank you" />
              <Typography className={classes.thankYouText} variant="body1">
                Your order has been received
              </Typography>
            </Box>
          </Box>

          <Stack
            direction="row"
            divider={<Divider orientation="vertical" flexItem />}
            spacing={2}
            className={classes.stackContainer}
          >
            <Box>
              <Typography>Order Number</Typography>
              <span className={classes.stackInfo}>{generateRandomNumbers()}</span>
            </Box>
            <Box>
              <Typography>Date</Typography>
              <span className={classes.stackInfo}>{formatDate(new Date)}</span>
            </Box>
            <Box>
              <Typography>Total</Typography>
              <span className={classes.stackInfo}>{total.toFixed(2)}</span>
            </Box>
            <Box>
              <Typography>Payment Method</Typography>
              <span className={classes.stackInfo}>{paymentMethod}</span>
            </Box>
          </Stack>
          <Container>
            <Typography className={classes.title} variant="h1">
              Order Details
            </Typography>
            <PaymentItems></PaymentItems>
            <Typography className={classes.title} variant="h1">
              Shipping Address
            </Typography>
            <Paper className={classes.containerAddress}><Typography className={classes.infoAddress}>{address.town}</Typography><Box><Typography className={classes.infoAddress}> {address.appartment} {address.street}, {address.zip},{" "}
            {address.country}, {address.state}<br></br> {address.firstName} {address.lastName}, {address.phoneNumber},{" "}
            {address.email}</Typography></Box></Paper>
            <Container className={classes.PriceWrapper}>
            <Box className={classes.shipingInfo}>
              <Typography className={classes.couponInfoText} variant="body1">
                Shiping
              </Typography>
              <Typography className={classes.couponInfoPrice} variant="body1">
                $16.00
              </Typography>
            </Box>

            <Box className={classes.priceTotal}>
              <Typography className={classes.totalText} variant="body1">
                Total
              </Typography>
              <Typography className={classes.productPrice} variant="body1">
                ${total.toFixed(2)}
              </Typography>
            </Box>
            </Container>
           

            <Box className={classes.yourOrderInfo}>
              <Typography className={classes.text} variant="p">
                Your order is currently being processed. You will receive an
                order
                <br /> confirmation email shortly with the expected delivery
                date for your items.
              </Typography>
            </Box>

            <Button
              className={classes.btnModal}
              onClick={() => {
                dispatch(setIsShowModal(false));
                navigate("/");
                dispatch(replaceCartFromServer([]))
                dispatch(setPaymentItems([]))
              }}
            >
              Track your Order
            </Button>
          </Container>
        </Container>
      </DialogContent>
    </Dialog>
  );
}
