import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  modalShow: {
    overflowY: "scroll",
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalContent: {
    overflowY: 'auto',
    overflowX: 'hidden',
    maxHeight: '100vh',
    width: '100%',
    padding: 0,
    maxWidth: '600px', 
  },
  modalContainer: {
    width: "600px",
    padding: "0",
    backgroundColor: "#FFFFFF",
    position: 'relative',
    paddingBottom: '10px',
    overflowX: 'hidden', 
    boxSizing: 'border-box', 
    '&::after': {
      content: '""',
      position: 'absolute',
      display: 'block',
      backgroundColor: 'rgba(70, 163, 88, 1)',
      width: '100%',
      height: '10px',
      bottom: '0',
    },
  },
  thankYouContainer: {
    position: "relative",
    paddingTop: "29px",
    paddingBottom: "15px",
    background: "rgba(70, 163, 88, 0.06)",
  },

  thankYouBlock: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    gap: "16px",
    alignItems: "center",
    "& img": {
      height: "45px",
      width: "50px"
    },
  },

  close: {
    backgroundColor: "transparent",
    border: "none",
    width: "18px",
    height: "18px",
    position: "absolute",
    top: "20px",
    right: "20px",
    cursor: "pointer",
    "&::after": {
      content: '""',
      width: "1px",
      height: "18px",
      position: "relative",
      display: "block",
      bottom: "18px",
      transform: "rotate(135deg)",
      backgroundColor: "rgba(70, 163, 88, 1)",
    },
    "&::before": {
      content: '""',
      width: "1px",
      position: "relative",
      height: "18px",
      display: "block",
      transform: "rotate(50deg)",
      backgroundColor: "rgba(70, 163, 88, 1)",
    },
  },

  thankYouText: {
    color: "#727272",
    fontSize: "13px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
  },
containerAddress:{
marginTop: '10px',
marginBottom: '20px',
padding: '10px',
},
  stackContainer: {
    gap: "1%",
    fontFamily: "'Monserrat', sans-serif",
    borderBottom: "1px solid rgba(70, 163, 88, 0.2)",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "10px",
  },

  stackInfo: {
    fontWeight: "700",
  },

  title: {
    fontSize: "17px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "700",
    color: "#3D3D3D",
    marginBottom: "17px",
    marginTop: "20px",
    width: "80%",
    margin: "15px",
  },

  subTitle: {
    marginTop: "22px",
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
    color: "#3D3D3D",
  },

  subTitleQ: {
    marginTop: "22px",
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
    color: "#3D3D3D",
    marginLeft: "45%",
  },

  info: {
    width: "80%",
    margin: "0 auto",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: "11px",
    borderBottom: "1px solid rgba(70, 163, 88, 0.2)",
  },

  productDetails: {
    width: "80%",
    margin: "0 auto",
    marginTop: "1px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "rgba(251, 251, 251, 1)",
  },

  productItemInfo: {
    display: "flex",
    flexDirection: "column",
  },

  productItem: {
    fontSize: "16px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "500",
    color: "#3D3D3D",
  },

  productNumber: {
    fontSize: "14px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
    color: "#727272",
  },

  productPrice: {
    fontSize: "18px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "700",
    color: "#46A358",
  },

  shipingInfo: {
    color: "#727272",
    fontWeight: "700",
    fontFamily: "'Monserrat', sans-serif",
    marginRight: "12px",
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
  },
  couponInfoText:{
    fontWeight: 500,
    color: "#3D3D3D",
    fontSize: "15px"
  },

  priceTotal: {
    color: "#727272",
    fontWeight: "700",
    fontFamily: "'Monserrat', sans-serif",
    marginRight: "12px",
    marginTop: "10px",
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "center",
  },
  PriceWrapper:{
    padding: 0,
    display: "flex",
    flexDirection: "column"
  },
  totalText: {
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "700",
    fontSize: "16px",
    color: "black" 
  },

  yourOrderInfo: {
    width: "100%",
    marginTop: "20px",
    borderTop: "1px solid rgba(70, 163, 88, 0.2)",
    paddingTop: "18px",
    alignItems: "center",
    flexDirection: "column",
    display: "flex",
  },

  btnModal: {
    margin: "0 auto",
    display: "block",
    padding: "0 20px 0 20px",
    marginTop: "35px",
    borderRadius: "3px",
    backgroundColor: "#46A358",
    fontFamily: "'Monserrat', sans-serif",
    color: "#FFFFFF",
    marginBottom: "48px",
    textTransform: "capitalize",
    fontWeight: "700",
    fontSize: "18px",
    height: "45px",
    "&:hover": {
      backgroundColor: "#3A8C47",
    },
  },

  text: {
    fontWeight: "400",
    fontSize: "14px",
    color: "#727272",
    textAlign: "center"
  },
infoAddress:{
  lineHeight: 1.5,
  color: "#3D3D3D",
}
});