import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
    Wrapper: {
        marginTop: "20px",
        padding: 0,
        "@media (max-width: 900px)": {
            width: "80%"
        },
        "@media (max-width: 768px)": {
            display: "none"
        }
    },
    HeaderWrapper:{
        padding: 0,
        marginTop: "50px"
    },
    SliderTitle: {
        color: "#46A358",
        fontSize: "17px",
        fontWeight: 700,
        position: "relative",
        "&::after": {
            content: '""',
            border: "0.30px solid rgba(70, 163, 88, 0.5)",
            width: "100%",
            height: "0px",
            position: "absolute",
            top: "26px",
            left: "0"
        },
    },
    Slider: {
        marginTop: "56px",
        height: "450px",
        paddingTop: "10px",
       
    },

    SliderArrow: {
        color: '#46a358', 
        fontSize: '30px',
        zIndex: 2, 
        position: "absolute",
        cursor: "pointer"
    },
    SliderPrev:{
        top: "39%",
        transform: "translateY(-50%)",
        left: "-2%",
        "@media (max-width: 900px)": {
            left: "-5%",
        },
    },
    SliderNext:{
        top: "39%",
        transform: "translateY(-50%)",
        right: "-2%",
        "@media (max-width: 900px)": {
            right: "-5%",
        },
    },
    SliderProduct: {
        width: "200px",
        paddingTop: "10px",
        cursor: "pointer",
        height: "360px",
        "&:hover": {
            transform: "scale(1.05)",
            transition: "transform 0.3s ease-in-out"
        },
        "@media (max-width: 900px)": {
            width: "200px !important",
        }
        
    },
    SliderProductImg: {
        margin: "0 auto",
        height: "300px",
        width: "165px",
        objectFit: "cover",
        borderRadius: "8px",
        boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.1)",
        
    },
    SliderProductName: {
        fontSize: "15px",
        textAlign: "left",
        paddingLeft: "15px",
        marginTop: "12px",
        fontWeight: 700,
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
    },
    SliderProductPrice: {
        fontSize: "16px",
        fontWeight: 700,
        color: "#46A358",
        paddingLeft: "15px",
    },
    SliderLoading: {
        fontSize: "16px",
        fontWeight: 700,
        color: "#46A358",

    },
})