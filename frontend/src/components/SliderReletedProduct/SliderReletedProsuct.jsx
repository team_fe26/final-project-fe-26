import  {useEffect, useState } from "react";
import { Container, Typography, Box, CardMedia } from "@mui/material";
import Slider from "react-slick";
import { useNavigate } from "react-router-dom";
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import { setProductId } from "../../redux/product.slice/product.slice";
import { useDispatch } from "react-redux";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css"; 
import { Styles } from "./style";
import { fetchRandomProducts } from "../../redux/products.slice/products.slice";
import { useSelector } from "react-redux";
const ProductSlider = () => {
  const classes = Styles();
const {productsRandom} = useSelector((state)=> state.products)
const [isMobile, setIsMobile] = useState(window.innerWidth < 768);
useEffect(() => {
  const handleResize = () => {
    setIsMobile(window.innerWidth < 768);
  };

  window.addEventListener("resize", handleResize);
  return () => window.removeEventListener("resize", handleResize);
}, []);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchRandomProducts());
  }, [dispatch]);

  const navigate = useNavigate();
  const PreviousArrow = ({ onClick }) => (
    <div onClick={onClick} >
      <ArrowBackIosIcon className={`${classes.SliderArrow} ${classes.SliderPrev}`} />
    </div>
  );
  
  
  const NextArrow = ({ onClick }) => (
    <div onClick={onClick} >
      <ArrowForwardIosIcon className={`${classes.SliderArrow} ${classes.SliderNext}`}/>
    </div>
  );
  const CustomDot = ({onClick, active}) => {
    return (
      <div
      onClick={onClick}
        style={{
          width: "10px",
          height: "10px",
          borderRadius: "50%",
          backgroundColor: active ? "green" : "#ccc",
          margin: "0 4px",
          marginBottom: "22px",
          marginTop: "12px",
          cursor: "pointer",
        }}
      />
    );
  };
  const settings = {
    
    dots: true,
    infinite: true,
    speed: 1000,
    slidesToShow: 6,
    slidesToScroll: 6,
    prevArrow: <PreviousArrow  />,
    nextArrow: <NextArrow  />,
    appendDots: (dots) => (
      <div
        style={{ display: "flex", justifyContent: "center", bottom: "-40px" }}
      >
        {dots.map((dot, index) => (
          <CustomDot
            key={index}
            {...dot.props}
            active={dot.props.className.includes("slick-active")}
          />
        ))}
      </div>
    ),
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        },
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
    ],
  };

  return (
    <Container className={classes.Wrapper}>
    {!isMobile ? (<Container className={classes.HeaderWrapper} >
      <Typography className={classes.SliderTitle} variant="h6" >
      You may be interested in
      </Typography>
      <Container className={classes.Wrapper}>
      
      <Slider className={classes.Slider} {...settings}>
        {Array.isArray(productsRandom) && productsRandom.length > 0 ? (
          productsRandom.map((product) => (
            <Box className={classes.SliderProduct}
             key={product._id}>
            
              <CardMedia
                className={classes.SliderProductImg}
                component="img"
                src={product.imageUrls[0]}
                alt={product.name}
                onClick={() => {
                  navigate("/product");
                  dispatch(setProductId(product._id));
                }}
               
              />
              <Typography className={classes.SliderProductName} variant="body1" >
                {product.name}
              </Typography>
              <Typography className={classes.SliderProductPrice} variant="body2">
                ${product.currentPrice}
              </Typography>
            </Box>
          ))
        ) : (
          <Typography className={classes.SliderLoading} variant="body1" align="center">Loading...</Typography>
        )}
      </Slider>
      </Container>
    </Container>) : null}
    </Container>
  );
};

export default ProductSlider;
