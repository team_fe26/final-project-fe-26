import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { CardMedia, Typography, Paper, Box } from '@mui/material';
import { Styles } from "./styles";
import { useSelector } from 'react-redux';
export default function PaymentItems() {
    const classes = Styles();
    const {paymentItems} = useSelector((state)=> state.payment)
    const renderItems = () => {
        return paymentItems.map((product, index) => (
            <TableRow  key={index}>
                <TableCell align="left" className={classes.cellInfo}>
                    <CardMedia component="img" height="65px" image={product.imageUrls[0]} alt="plant" className={classes.imgDetails}/>
                    <Box>
                        <Typography className={classes.cellName}>{product.name}</Typography>
                        <Typography className={classes.productNumber}><span>SKU:</span>{product.itemNo}</Typography>
                    </Box>
                </TableCell>
                <TableCell align="right" className={classes.cellQty}>
                    <Typography>(x{product.quantityCart})</Typography>
                </TableCell>
                <TableCell align="right" className={classes.cellTotal}>
                    <Typography className={classes.price} >${product.currentPrice}</Typography>
                </TableCell>
            </TableRow>
        ));
    };

    return (
        <>
            <TableContainer className={classes.CheckoutProductInfo}  component={Paper}>
                <Table size="small" aria-label="a dense table" className={classes.table}>
                    <TableHead>
                        <TableRow >
                            <TableCell align="left" className={classes.tableItem}>Products</TableCell>
                            <TableCell align="right" className={classes.tableItem}>Qty</TableCell>
                            <TableCell align="right" className={classes.tableItem}>Subtotal</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {renderItems()}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    );
}
