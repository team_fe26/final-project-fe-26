import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
    table: {
        width: "100%",
    },
    tableItem: {
        fontSize: "17px",
        fontFamily: "'Monserrat', sans-serif",
        fontWeight: "700",
    },
    imgDetails: {
        width: "auto",
    },
    cellInfo: {
        display: "flex",
        gap: "10px",
        alignItems: "center",
        fontFamily: "'Monserrat', sans-serif",
        fontSize: "18px",
    },
    productNumber:{
        fontSize: "14px",
        fontWeight: 500,
        color: "#727272",
        
        "& span": {
            color: "#A5A5A5",
        }
    },
    cellName: {
        fontWeight: "700",
        overflow: "hidden",
        textOverflow: "ellipsis",
        whiteSpace: "nowrap",
        maxWidth: "185px",
    },
    cellQty: {
        fontFamily: "'Monserrat', sans-serif",
        fontSize: "14px",
        color: "#727272",

    },
    cellTotal: {
        fontSize: "18px",
        fontFamily: "'Monserrat', sans-serif",
        fontWeight: "700",
        color: "#46A358",
    },
    CheckoutProductInfo: {
        "@media (max-width: 900px)": {
            height: "105px",
            
        },
        "@media (max-width: 768px)": {
            margin: 0,
            width: "100%"
        },
        "@media (max-width: 400px)": {
            width: "94%"
        },
    },
    price: {
        fontWeight: "700"
    }

});