import { Formik, Form, Field, ErrorMessage } from "formik";
import { Button, Box, Typography, Checkbox } from "@mui/material";
import * as Yup from "yup";
import { Styles } from "./styles";
import { useLocation, useNavigate } from "react-router-dom";
import { useState } from "react";
import { setOptionalAddress, setAddress } from "../../redux/payment.slice/payment.slice";
import { setEditOptionalAddress} from "../../redux/payment.slice/payment.slice";
import { useDispatch, useSelector } from "react-redux";
export default function BillingAddress() {
  const dispatch = useDispatch();
  const location = useLocation();
  const navigate = useNavigate();
  const classes = Styles();
  const [mainAddress, setMainAddress] = useState(false);
  const {loggedIn} = useSelector((state)=>state.authorization)
  const initialValues = {
    firstName: "",
    lastName: "",
    country: "",
    town: "",
    street: "",
    state: "",
    zip: "",
    email: "",
    phoneNumber: "",
    notes: "",
    appartment: "",
  };
  const checkEmailExists = async (email) => {
    try {
      const response = await fetch(
        `https://greenshop-server.onrender.com/api/check/email`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ email }),
        }
      );
      if (response.ok) {
        const data = await response.json();
        return data.valid;
      }
      throw new Error("Failed to check email");
    } catch (error) {
      console.error("Error checking email:", error);
      return false;
    }
  };
  
  const onSubmit = async (values) => {
    if (location.pathname === "/checkout") {
      dispatch(
        setOptionalAddress({
          firstName: values.firstName,
          lastName: values.lastName,
          country: values.country,
          town: values.town,
          street: values.street,
          ...(values.state && { state: values.state }),
          appartment: values.appartment,
          zip: values.zip,
          email: values.email,
          phoneNumber: values.phoneNumber,
          ...(values.notes && { notes: values.notes }),
        })
      );
      dispatch(setEditOptionalAddress(false))
      if(!loggedIn){
dispatch(setAddress({
  firstName: values.firstName,
  lastName: values.lastName,
  country: values.country,
  town: values.town,
  street: values.street,
  ...(values.state && { state: values.state }),
  appartment: values.appartment,
  zip: values.zip,
  email: values.email,
  phoneNumber: values.phoneNumber,
  ...(values.notes && { notes: values.notes }),
}))
      }
    }
    if (location.pathname === "/user/address") {
      try {
        const token = JSON.parse(localStorage.getItem("token")) || "";
        const addressData = {
          firstName: values.firstName,
          lastName: values.lastName,
          phoneNumber: values.phoneNumber,
          email: values.email,
          country: values.country,
          street: values.street,
          zip: values.zip,
          town: values.town,
          main: mainAddress,
        };

        if (values.appartment) {
          addressData.appartment = values.appartment;
        }

        if (values.state) {
          addressData.state = values.state;
        }

        const response = await fetch(
          "https://greenshop-server.onrender.com/api/customer/address/add",
          {
            method: "POST",
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
            },
            body: JSON.stringify(addressData),
          }
        );
        if (!response.ok) {
          const data = await response.json();
          alert(`Error: ${data.error}`);
          return;
        }
        const data = await response.json();
        navigate("/user");
        console.log(data.message);
      } catch (error) {
        console.error("Fetch error:", error);
      }
    }
  };

  const validationSchema = Yup.object({
    firstName: Yup.string().required("First Name is required*"),
    lastName: Yup.string().required("Last Name is required*"),
    country: Yup.string().required("Country is required*"),
    town: Yup.string().required("Town is required*"),
    street: Yup.string().required("Street address is required*"),
    state: Yup.string(),
    zip: Yup.string()
    .matches(/^\d{5,8}$/, "Invalid zip code")
    .required("Zip is required*"),
      email: Yup.string()
      .email("Invalid email*")
      .required("Email is required")
      .test("check-email", "Email not valid", async (value) => {
        if (!value) return true;
        const exists = await checkEmailExists(value);
        return exists;
      }),
    phoneNumber: Yup.string()
      .matches(/^\d{10}$/, "Phone number should be 10 digits")
      .required("Phone number is required"),
    notes: Yup.string(),
    appartment: Yup.string(),
  });
  const handleRadioChange = () => {
    const mainAddressSetUp = !mainAddress;
    setMainAddress(mainAddressSetUp);
    console.log("Updated mainAddress:", mainAddressSetUp);
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        <Form className={classes.form}>
          <div>
            <label className={classes.label} htmlFor="firstName">
              First Name <span className={classes.star}>*</span>
            </label>
            <Field name="firstName">
              {({ field, form }) => (
                <input
                  {...field}
                  className={classes.field}
                  type="text"
                  id="firstName"
                  placeholder="Enter your first name"
                  onChange={(e) => {
                    const firstName = e.target.value.replace(
                      /[^a-zA-Zа-яА-Я]/g,
                      ""
                    );
                    form.setFieldValue("firstName", firstName);
                  }}
                />
              )}
            </Field>
            <ErrorMessage
              className={classes.errorMessage}
              name="firstName"
              component="div"
            />
          </div>

          <div>
            <label className={classes.label} htmlFor="lastName">
              Last Name <span className={classes.star}>*</span>
            </label>
            <Field name="lastName">
              {({ field, form }) => (
                <input
                  {...field}
                  className={classes.field}
                  type="text"
                  id="lastName"
                  placeholder="Enter your last name"
                  onChange={(e) => {
                    const lastName = e.target.value.replace(
                      /[^a-zA-Zа-яА-Я]/g,
                      ""
                    );
                    form.setFieldValue("lastName", lastName);
                  }}
                />
              )}
            </Field>
            <ErrorMessage
              className={classes.errorMessage}
              name="lastName"
              component="div"
            />
          </div>

          <div>
            <label className={classes.label} htmlFor="country">
              Country <span className={classes.star}>*</span>
            </label>
            <Field name="country">
              {({ field, form }) => (
                <input
                  {...field}
                  className={classes.field}
                  type="text"
                  id="country"
                  placeholder="Enter your country"
                  onChange={(e) => {
                    const country = e.target.value.replace(
                      /[^a-zA-Zа-яА-Я]/g,
                      ""
                    );
                    form.setFieldValue("country", country);
                  }}
                />
              )}
            </Field>
            <ErrorMessage
              className={classes.errorMessage}
              name="country"
              component="div"
            />
          </div>

          <div>
            <label className={classes.label} htmlFor="town">
              Town <span className={classes.star}>*</span>
            </label>
            <Field name="town">
              {({ field, form }) => (
                <input
                  {...field}
                  className={classes.field}
                  type="text"
                  id="town"
                  placeholder="Enter your town"
                  onChange={(e) => {
                    const town = e.target.value.replace(/[^a-zA-Zа-яА-Я]/g, "");
                    form.setFieldValue("town", town);
                  }}
                />
              )}
            </Field>
            <ErrorMessage
              className={classes.errorMessage}
              name="town"
              component="div"
            />
          </div>

          <div>
            <label className={classes.label} htmlFor="street">
              Street <span className={classes.star}>*</span>
            </label>
            <Field
              className={classes.field}
              type="text"
              id="street"
              name="street"
              placeholder="Enter your street"
            />
            <ErrorMessage
              className={classes.errorMessage}
              name="street"
              component="div"
            />
          </div>

          <div>
            <label className={classes.label} htmlFor="state">
              State
            </label>
            <Field
              className={`${classes.field} ${classes.withoutStar}`}
              type="text"
              id="state"
              name="state"
              placeholder="Enter your state"
            />
            <ErrorMessage
              className={classes.errorMessage}
              name="state"
              component="div"
            />
          </div>

          <div>
            <label className={classes.label} htmlFor="zip">
              Zip <span className={classes.star}>*</span>
            </label>
            <Field
              className={classes.field}
              type="text"
              id="zip"
              name="zip"
              placeholder="Enter your zip"
            />
            <ErrorMessage
              className={classes.errorMessage}
              name="zip"
              component="div"
            />
          </div>

          <div>
            <label className={classes.label} htmlFor="appartment">
              Appartment 
            </label>
            <Field
               className={`${classes.field} ${classes.withoutStar}`}
              type="text"
              id="appartment"
              name="appartment"
              placeholder="Enter your appartment"
            />
            <ErrorMessage
              className={classes.errorMessage}
              name="appartment"
              component="div"
            />
          </div>

          <div>
            <label className={classes.label} htmlFor="email">
              Email <span className={classes.star}>*</span>
            </label>
            <Field
              className={classes.field}
              type="email"
              id="email"
              name="email"
              placeholder="Enter your email"
            />
            <ErrorMessage
              className={classes.errorMessage}
              name="email"
              component="div"
            />
          </div>

          <div>
            <label className={classes.label} htmlFor="phoneNumber">
              Phone Number <span className={classes.star}>*</span>
            </label>
            <Field name="phoneNumber">
              {({ field, form }) => (
                <input
                  {...field}
                  className={classes.field}
                  type="text"
                  id="phoneNumber"
                  placeholder="(###)###-##-##"
                  onChange={(e) => {
                    const phoneNumber = e.target.value.replace(/[^\d]/g, "");
                    if (phoneNumber.length <= 10) {
                      form.setFieldValue("phoneNumber", phoneNumber);
                    }
                  }}
                />
              )}
            </Field>
            <ErrorMessage
              className={classes.errorMessage}
              name="phoneNumber"
              component="div"
            />
          </div>
          {location.pathname === "/checkout" && (
            <div>
              <label className={classes.label} htmlFor="notes">
                Order notes (optional)
              </label>
              <Field
                className={`${classes.field} ${classes.fieldNotes}`}
                type="text"
                id="notes"
                name="notes"
                placeholder="Enter any notes here"
              />
              <ErrorMessage
                className={classes.errorMessage}
                name="notes"
                component="div"
              />
            </div>
          )}
                    <Button
            type="submit"
            variant="contained"
            color="primary"
            className={classes.btn}
          >
            Save address
          </Button>
          {location.pathname === "/user/address" && (
            <Box className={classes.shipRadio}>
                <Checkbox
                  checked={mainAddress}
                  onChange={handleRadioChange}
                  color="success"
                  defaultChecked
                >
                  Shipping Address
                </Checkbox>
                <Typography className={classes.p} variant="p">
                  Shipping Address
                </Typography>
            </Box>
          )}

        </Form>
      </Formik>
    </>
  );
}
