import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { setIsShowModal, setIsPaymentSuccess } from '../../../../redux/payment.slice/payment.slice';
import { useDispatch } from 'react-redux';

const PayPalButton = () => {
  const dispatch = useDispatch();
  const { total } = useSelector((state) => state.payment);

  useEffect(() => {
    if (window.paypal) {
      window.paypal.Buttons({
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [{
              amount: {
                value: `${total}`
              }
            }]
          });
        },
        onApprove: async (data, actions) => {
          return actions.order.capture().then((details) => {
            const payerName = details.payer.name.given_name + " " + details.payer.name.surname;
            alert(`Payment successful! Processed by: ${payerName}. Amount: $${total}`);
            dispatch(setIsPaymentSuccess(true))
            dispatch(setIsShowModal(true));
          });
        },
        onError: (err) => {
          console.error('PayPal error:', err);
        }
      }).render('#paypal-button-container');
    }
  }, [total, dispatch]);

  return <div id="paypal-button-container"></div>;
};

export default PayPalButton;
