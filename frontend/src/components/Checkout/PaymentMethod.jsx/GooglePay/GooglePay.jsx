import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Styles } from "./styles";
import { setIsShowModal, setIsPaymentSuccess } from '../../../../redux/payment.slice/payment.slice';

export default function GooglePayButton() {
  const dispatch = useDispatch();
  const { total} = useSelector((state) => state.payment);
  const classes = Styles();

  useEffect(() => {
    const script = document.createElement("script");
    script.src = "https://pay.google.com/gp/p/js/pay.js";
    script.onload = () => {
      const paymentsClient = new google.payments.api.PaymentsClient({
        environment: "TEST",
      });

      const paymentDataRequest = {
        apiVersion: 2,
        apiVersionMinor: 0,
        allowedPaymentMethods: [
          {
            type: "CARD",
            parameters: {
              allowedAuthMethods: ["PAN_ONLY", "CRYPTOGRAM_3DS"],
              allowedCardNetworks: ["MASTERCARD", "VISA"],
            },
            tokenizationSpecification: {
              type: "PAYMENT_GATEWAY",
              parameters: {
                gateway: "example",
                gatewayMerchantId: "exampleGatewayMerchantId",
              },
            },
          },
        ],
        merchantInfo: {
          merchantId: process.env.GOOGLEPAY_MERCHANT_ID,
          merchantName: "Example Merchant",
        },
        transactionInfo: {
          totalPriceStatus: "FINAL",
          totalPriceLabel: "Total",
          totalPrice: `${total}`,
          currencyCode: "USD",
          countryCode: "US",
        },
      };

      const button = paymentsClient.createButton({
        onClick: () => {
          paymentsClient
            .loadPaymentData(paymentDataRequest)
            .then(() => {
              alert(`Payment successful! Amount: $${total}`);
              dispatch(setIsPaymentSuccess(true))
              dispatch(setIsShowModal(true));
            })
            .catch((err) => {
              console.error("Error:", err);
              alert("An error occurred during payment. Please try again.");
            });
        },
      });

      document.getElementById("google-pay-button-container").appendChild(button);
    };
    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, [total, dispatch]);

  return <div className={classes.container} id="google-pay-button-container"></div>;
}
