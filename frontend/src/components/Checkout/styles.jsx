import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  container: {
    display: "flex",
    flexDirection: "row",
    maxWidth: "1200px",
    gap: "8%",
    padding: "0",
    marginTop: "75px"
  },
  billingAddress: {
    padding: 0,
  },
  title: {
    fontSize: "17px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "700",
    color: "#3D3D3D",
    marginBottom: "17px",
    marginTop: "80px",
  },

  field: {
    marginBottom: "15px",
    marginTop: "10px",
    color: "rgba(165, 165, 165, 1)",
    fontFamily: '"Montserrat", sans-serif',
    fontSize: "15px",
    fontWeight: 400,
    borderRadius: "3px",
    backgroundColor: "transparent",
    width: "92%",
    height: "50px",
    outline: "none",
    paddingLeft: "20px",
    border: "1px solid rgba(165, 165, 165, 1)",
    "&:hover": {
      border: "1px solid rgba(70, 163, 88, 1)",
    },
    "&:focus": {
      border: "1px solid rgba(70, 163, 88, 1)",
      backgroundColor: "rgba(255, 255, 255, 1)",
    },
  },

  errorMessage: {
    color: "red",
    position: "relative",
    top: -10,
    fontSize: "14px",
  },
  fieldAppartment: {
    marginTop: "43px",
  },
  label: {
    color: "#3D3D3D",
    fontSize: "15px",
    fontFamily: "'Montserrat', sans-serif",
    fontWeight: "400",
  },

  star: {
    color: "#F03800",
    fontSize: "22px",
    fontFamily: "'Monserrat', sans-serif",
    fontWeight: "400",
  },

  form: {
    display: "grid",
    gridTemplateColumns: "repeat(2, 1fr)",
    gap: "22px",
    position: 'relative'
  },
  fieldNotes:{
    height: '120px'
  },
  btn:{
    marginTop: "30px",
    padding: "13px 23px",
    height: "40px",
    borderRadius: "3px",
    backgroundColor: "#46A358",
    color: "#FFFFFF",
    textTransform: "capitalize",
    fontWeight: "700",
    fontSize: "15px",
    border: "none",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#3A8C47",
    },
  },
  shipRadio:{
    marginTop: "30px",
marginLeft: 'auto',
marginRight: 0
  },
  withoutStar:{
    marginTop: '20px',
  }
});
