import { createUseStyles } from "react-jss";
import { Card, Typography, IconButton } from "@mui/material";
import { styled } from "@mui/system";

export const Styles = createUseStyles({
  container: {
    minWidth: "366px",
    maxWidth: "766px",
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
  },
  gridItem: {
    "@media (max-width: 390px)": {
      paddidngLeft: 0,
    },
  },
  containerGridMob: {
    justifyContent: "space-around",
    paddingLeft: "10%",
    paddingBottom: "70px"
  },
 
  priceContainer: {
    display: "flex",
  },
  CardWrapper: {
    position: "relative",
  },
  FavBtn: {
    position: "absolute",
    top: "10px",
    right: "10px",

  },
  desktopIcons: {
    display: "none",
    width: "250px",
    backgroundColor: "#FFFF",
    position: "relative",
    top: -40,
    gap: "10px",
  },
  cardMedia: {
    height: "250px",

    "&:hover $desktopIcons": {
      display: "flex",
      justifyContent: "center",
    },
  },

  modal: {
    position: "absolute",
    top: "30%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "400",
    height: 400,
    backgroundColor: "white",
  },

  modalImage: {
height: '85vh'
  },
  btnAddtoCart: {
position: 'absolute',
bottom: '25px',
  },
  pagination: {
    display: "flex",
    alignItems: "flex-end"
  },
  paginationMobile:{
    display: "flex",
    alignItems: "center",
    paddingBottom: "100px"
  }
});

export const PlantCard = styled(Card)(() => ({
  width: "168px",
  margin: "5px",
  height: "340px",
  boxShadow: "none",
  position: "relative",
  cursor: "pointer",
  backgroundColor: "rgba(250, 250, 250, 1)",
  "@media (max-width: 766px)": {
    borderRadius: "20px",
    height: "380px",
  },
  "@media (max-width: 390px)": {
    paddingLeft: "0",
  },
}));

export const PlantCardDesktop = styled(PlantCard)({
  width: "250px",
});

export const PlantName = styled(Typography)({
  color: "#3D3D3D",
  fontWeight: "400",
  fontSize: "15px",
  lineHeight: "16px",
  marginTop: (theme) => theme.spacing(0.5),
});

export const PlantPrice = styled(Typography)({
  fontWeight: "700",
  fontSize: "16px",
  lineHeight: "16px",
});

export const OldPlantPrice = styled(PlantPrice)({
  fontWeight: "400",
  color: "#A5A5A5",
  textDecoration: "line-through",
  marginTop: "5px",
});

export const OldPlantPriceDesktop = styled(PlantPrice)({
  fontWeight: "400",
  color: "#A5A5A5",
  textDecoration: "line-through",
  marginLeft: "18px",
});

export const DiscountLabel = styled("div")({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  position: "absolute",
  backgroundColor: "#46A358",
  width: "68px",
  height: "29px",
  top: 13,
  left: 0,
});

export const DiscountLabelDesktop = styled("div")({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  position: "relative",
  backgroundColor: "#46A358",
  width: "80px",
  height: "29px",
  top: -304,
  left: -16,
  transition: "background-color 0.3s ease",
});

export const DiscountPercentage = styled("p")({
  fontWeight: "500",
  fontSize: "13px",
  lineHeight: "16px",
  color: "#FFFFFF",
});

export const FavoriteIconButton = styled(IconButton)({
  position: "relative",
  backgroundColor: "#FFFFFF",
  top: 8,
  right: -120,
});
