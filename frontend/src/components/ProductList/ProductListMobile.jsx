import { useEffect, useState, useMemo } from "react";
import {
  Grid,
  CardContent,
  CardMedia,
  Button,
  Stack,
  Pagination,
} from "@mui/material";
import {
  Styles,
  PlantCard,
  FavoriteIconButton,
  PlantName,
  PlantPrice,
  OldPlantPrice,
  DiscountLabel,
  DiscountPercentage,
} from "./styles";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchProducts,
  setCurrentPage,
} from "../../redux/products.slice/products.slice";
import { addItemToCart } from "../../redux/cart.slice/cart.slice";
import { addToWishlist, removeFromWishlist } from "../../redux/wishlist.slice/wishlist.slice";
import { useNavigate } from "react-router-dom";
import { setProductId } from "../../redux/product.slice/product.slice";
import { setCurrentPage as setCommentsPage } from "../../redux/comments.slice/comments.slice";
import PopUpMessage from '../../pages/Cart/CouponModal/CouponModal';
import { useSearchParams } from "react-router-dom";
import { setFilterProducts } from "../../redux/filter.slice/filter.slice";
const truncateText = (text) => {
  if (text.length <= 20) {
    return text;
  }
  return text.substring(0, 17) + "...";
};

const theme = createTheme({
  palette: {
    primary: {
      main: "#46A358",
    },
    secondary: {
      main: "#ff5722",
    },
    icon: {
      main: "#3D3D3D",
    },
    wishlist: {
      main: "#52a346",
    },
  },
  typography: {
    h5: {
      fontWeight: 700,
    },
  },
});

const ProductListMobile = () => {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const dispatch = useDispatch();
  const itemsPerPage = 6;

  const currentPage = useSelector((state) => state.products.currentPage);
  const products = useSelector((state) => state.products.products.data);
  const total = useSelector((state) => state.products.products.total); 
  const { filterProducts, sortBy } = useSelector((state) => state.filters);
  const wishlistId = useSelector((state) => state.wishlist.wishlistId);
  const [sortedProducts, setSortedProducts] = useState([])
  const [pageValue, setPageValue] = useState(currentPage);
  const [openSnackbar, setOpenSnackbar] = useState(true);
  useEffect(() => {
    const PRODUCTS = Array.isArray(filterProducts) && filterProducts.length > 0 
      ? filterProducts 
      : Array.isArray(products) 
        ? products 
        : [];
    setSortedProducts(PRODUCTS);
  }, [products, filterProducts]);
  
  const sortedArray = useMemo(() => {
    if (!sortBy || !Array.isArray(sortedProducts)) return sortedProducts;
    const sorted = [...sortedProducts];
    sorted.sort((a, b) => {
      if (sortBy === "min to max") {
        return a.currentPrice - b.currentPrice;
      } else if (sortBy === "max to min") {
        return b.currentPrice - a.currentPrice;
      }
      return 0;
    });
    return sorted;
  }, [sortBy, sortedProducts]);

  const totalFilteredProducts = filterProducts.length > 0 ? sortedProducts.length : total ;
  const totalPages = Math.ceil(totalFilteredProducts / itemsPerPage);

  useEffect(() => {
    dispatch(fetchProducts({ page: currentPage, perPage: itemsPerPage }));
    dispatch(setCommentsPage(1));
  }, [dispatch, currentPage]);

  useEffect(() => {
    setPageValue(currentPage);
  }, [currentPage]);

  const isProductInWishlist = (productId) => {
    return wishlistId.includes(productId);
  };
  useEffect(() => {
    const params = Object.fromEntries([...searchParams]);
    if (Object.keys(params).length <= 0) {
      dispatch(setFilterProducts([]));
    }

  }, [searchParams, dispatch]);

  const handleAddToCart = async (id, quantity) => {

    if (quantity <= 0) {
      console.log("Product is not in the stock");
      return;
    }
    dispatch(addItemToCart(id));
    try {
      const productResponse = await fetch(
        "https://greenshop-server.onrender.com/api/product-reject-one",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ _id: id }),
        }
      );

      if (!productResponse.ok) {
        const productData = await productResponse.json();
        console.error("Error:", productData.error);
        return;
      }
      const productData = await productResponse.json();
      console.log(productData.message);
    } catch (error) {
      console.error("Error with product-reject-one:", error);
    }
  };

  const handleAddToWishlist = async (id) => {
    if (isProductInWishlist(id)) {
      dispatch(removeFromWishlist(id));
    } else {
      dispatch(addToWishlist(id));
    }
  };

  const handlePageChange = (event, value) => {
    dispatch(setCurrentPage(value));
    setPageValue(value);
  };

  const classes = Styles();
  const displayedProducts = useMemo(() => {
    if (!Array.isArray(sortedArray)) return [];
    return sortedArray.slice((currentPage - 1) * itemsPerPage, currentPage * itemsPerPage);
  }, [sortedArray, currentPage, itemsPerPage]);
  return (
    <ThemeProvider theme={theme}>
      <Grid
        className={`${classes.containerGridMob} ${classes.container}`}
        container
        spacing={4}
      >
{displayedProducts && displayedProducts.length > 0 && (
  displayedProducts.map((product, index) => (
    <Grid
      style={{ paddingLeft: 0 }}
      item
      key={index}
      className={`${index % 2 !== 0 ? classes.rightPlantItem : ""} ${
        classes.gridItem
      }`}
    >
      <PlantCard className={classes.CardWrapper}>
        <FavoriteIconButton
          className={classes.FavBtn}
          onClick={() => handleAddToWishlist(product._id)}
        >
          {isProductInWishlist(product._id) ? (
            <FavoriteIcon style={{ color: "#46a358" }} />
          ) : (
            <FavoriteBorderIcon style={{ color: "#46a358" }} />
          )}
        </FavoriteIconButton>
        <CardMedia
          onClick={() => {
            navigate(`/product`);
            dispatch(setProductId(product._id));
          }}
          component="img"
          height="200px"
          image={product.imageUrls[0]}
          alt={product.name}
        />
        <CardContent>
          <PlantName gutterBottom variant="h5" component="div">
            {truncateText(product.name)}
          </PlantName>
          <PlantPrice variant="body2" color="primary">
            ${product.currentPrice}
          </PlantPrice>
          {product.currentPrice !== product.previousPrice && (
            <>
              <OldPlantPrice>${product.previousPrice}</OldPlantPrice>
              <DiscountLabel>
                <DiscountPercentage>
                  {100 -
                    Math.round(
                      (product.currentPrice / product.previousPrice) * 100
                    )}
                  % OFF
                </DiscountPercentage>
              </DiscountLabel>
            </>
          )}
          <Button
            className={classes.btnAddtoCart}
            variant="contained"
            color="primary"
            onClick={() =>
              handleAddToCart(product._id, product.quantity)
            }
          >
            Add to Cart
          </Button>
        </CardContent>
      </PlantCard>
    </Grid>
  ))
)}
      </Grid>

      {totalPages > 1 && (
  <Stack spacing={2} className={classes.paginationMobile}>
    <Pagination
      count={totalPages}
      page={pageValue}
      onChange={handlePageChange}
      shape="rounded"
      color="primary"
      hidePrevButton={pageValue === 1}
      hideNextButton={pageValue === totalPages}
    />
  </Stack>
)}
     <PopUpMessage open={openSnackbar} onClose={() => setOpenSnackbar(false)} />
    </ThemeProvider>
  );
};

export default ProductListMobile;
