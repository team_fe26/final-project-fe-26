import React, { useMemo, useEffect, useState } from "react";
import {
  Grid,
  CardContent,
  CardMedia,
  Box,
  IconButton,
  Modal,
  Stack,
  Pagination,
} from "@mui/material";
import {
  Styles,
  PlantCardDesktop,
  PlantName,
  PlantPrice,
  OldPlantPriceDesktop,
  DiscountLabelDesktop,
  DiscountPercentage,
} from "./styles";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchProducts,
  setCurrentPage,
} from "../../redux/products.slice/products.slice";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import SearchIcon from "@mui/icons-material/Search";
import {
  addItemToCart,
} from "../../redux/cart.slice/cart.slice";
import { addToWishlist, removeFromWishlist } from "../../redux/wishlist.slice/wishlist.slice";
import { setProductId } from "../../redux/product.slice/product.slice";
import { setCurrentPage as setCommentsPage } from "../../redux/comments.slice/comments.slice";
import PopUpMessage from '../../pages/Cart/CouponModal/CouponModal';
import { useNavigate } from "react-router-dom";
const truncateText = (text) => {
  if (text.length <= 23) {
    return text;
  }
  return text.substring(0, 20) + "...";
};

const theme = createTheme({
  palette: {
    primary: {
      main: "#46A358",
    },
    secondary: {
      main: "#ff5722",
    },
    icon: {
      main: "#3D3D3D",
    },
    wishlist: {
      main: "#52a346",
    },
  },
  typography: {
    h5: {
      fontWeight: 700,
    },
  },
});

const ProductListDesktop = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const itemsPerPage = 9;

  const currentPage = useSelector((state) => state.products.currentPage);
  const allProducts = useSelector((state) => state.products.allProducts);
  const { filterProducts, sortBy } = useSelector((state) => state.filters);
  const wishlistId = useSelector((state) => state.wishlist.wishlistId);
  const [pageValue, setPageValue] = useState(currentPage);
  const [openSnackbar, setOpenSnackbar] = useState(true);
  const [open, setOpen] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);

  const SortedProducts = useMemo(() => {
    const PRODUCTS = 
      (Array.isArray(filterProducts) && filterProducts.length > 0) 
        ? filterProducts 
        : (Array.isArray(allProducts) ? allProducts : []);
  
    if (!sortBy) return PRODUCTS;
  
    return [...PRODUCTS].sort((a, b) => {
      if (sortBy === "min to max") return a.currentPrice - b.currentPrice;
      if (sortBy === "max to min") return b.currentPrice - a.currentPrice;
      return 0;
    });
  }, [allProducts, filterProducts, sortBy]);
  const totalFilteredProducts = SortedProducts.length; 
  const totalPages = Math.ceil(totalFilteredProducts / itemsPerPage);
  


  useEffect(() => {
    dispatch(fetchProducts({ page: currentPage, perPage: itemsPerPage }));
    dispatch(setCommentsPage(1));
    console.log(allProducts)
  }, [dispatch, currentPage]);
  
  useEffect(() => {
    setPageValue(currentPage);
  }, [currentPage]);
  
  const handleOpen = (product) => {
    setSelectedProduct(product);
    setOpen(true);
  };

  const handleClose = () => setOpen(false);
  const isProductInWishlist = (productId) => {
    return wishlistId.includes(productId);
  };

  const handleAddToCart = async (id, quantity) => {
    if (quantity <= 0) {
      console.log("Product is not in stock");
      return;
    }
    dispatch(addItemToCart(id));
    try {
      const productResponse = await fetch(
        "https://greenshop-server.onrender.com/api/product-reject-one",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ _id: id }),
        }
      );

      if (!productResponse.ok) {
        const productData = await productResponse.json();
        console.error("Error:", productData.error);
        return;
      }
      const productData = await productResponse.json();
      console.log(productData.message);
    } catch (error) {
      console.error("Error with product-reject-one:", error);
    }
  };

  const handleAddToWishlist = async (id) => {
    if (isProductInWishlist(id)) {
      dispatch(removeFromWishlist(id));
    } else {
      dispatch(addToWishlist(id));
    }
  };

  const handlePageChange = (event, value) => {
    setPageValue(value); // Локально обновляем номер страницы
    dispatch(setCurrentPage(value)); // Отправляем действие в Redux или другой стор
  };
  
  const classes = Styles();
  const displayedProducts = useMemo(() => {
    return SortedProducts.slice((pageValue - 1) * itemsPerPage, pageValue * itemsPerPage);
  }, [SortedProducts, pageValue, itemsPerPage]);
  
  return (
    <ThemeProvider theme={theme}>
<Grid container spacing={4} style={{ justifyContent: "center" }}>
{displayedProducts && displayedProducts.length > 0 && (
  displayedProducts.map((product, index) => (
          <Grid item key={product._id}>
      <PlantCardDesktop>
        <Box className={classes.cardMedia}>
          <CardMedia
            onClick={() => {
              navigate("/product");
              dispatch(setProductId(product._id));
            }}
            component="img"
            height="250"
            image={product.imageUrls[0]}
            alt={product.name}
          />

          <Box className={classes.desktopIcons}>
            <IconButton
              onClick={() => handleAddToCart(product._id, product.quantity)}
            >
              <ShoppingCartIcon style={{ color: "#46a358" }} />
            </IconButton>
            <IconButton
              onClick={() => handleAddToWishlist(product._id)}
            >
              {isProductInWishlist(product._id) ? (
                <FavoriteIcon style={{ color: "#46a358" }} />
              ) : (
                <FavoriteBorderIcon style={{ color: "#46a358" }} />
              )}
            </IconButton>
            <IconButton onClick={() => handleOpen(product)}>
              <SearchIcon style={{ color: "#46a358" }} />
            </IconButton>
          </Box>
        </Box>

        <CardContent>
          <PlantName gutterBottom variant="h5" component="div">
            {truncateText(product.name)}
          </PlantName>

          <Box className={classes.priceContainer}>
            <PlantPrice variant="body2" color="primary">
              ${product.currentPrice}
            </PlantPrice>
            {product.currentPrice !== product.previousPrice && (
              <OldPlantPriceDesktop>
                ${product.previousPrice}
              </OldPlantPriceDesktop>
            )}
          </Box>
          {product.currentPrice !== product.previousPrice && (
            <DiscountLabelDesktop>
              <DiscountPercentage>
                {100 - Math.round(
                  (product.currentPrice / product.previousPrice) * 100
                )}
                % OFF
              </DiscountPercentage>
            </DiscountLabelDesktop>
          )}
        </CardContent>
      </PlantCardDesktop>
    </Grid>
)))}
</Grid>
      {totalPages > 1 && (
        <Stack spacing={2} className={classes.pagination}>
          <Pagination
            count={totalPages}
            page={pageValue}
            onChange={handlePageChange}
            shape="rounded"
            color="primary"
            hidePrevButton={pageValue === 1}
            hideNextButton={pageValue === totalPages}
          />
        </Stack>
      )}

      {selectedProduct && (
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
          maxWidth="md"
          fullWidth
        >
          <Box className={classes.modal}>
            <CardMedia
              className={classes.modalImage}
              component="img"
              height="250"
              image={selectedProduct.imageUrls[0]}
              alt={selectedProduct.name}
            />
          </Box>
        </Modal>
      )}

      <PopUpMessage open={openSnackbar} onClose={() => setOpenSnackbar(false)} />
    </ThemeProvider>
  );
};

export default ProductListDesktop;
