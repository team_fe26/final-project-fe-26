import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  slider: {
    maxWidth: "1200px",
    minWidth: "366px",
    marginLeft: "auto",
    marginRight: "auto",
    backgroundColor: "rgba(245, 245, 245, 0.5)",
    position: "relative",
    marginTop: "115px",
    marginBottom: "50px",
    "@media (max-width: 766px)": {
      background:
        "linear-gradient(180deg, rgba(70, 163, 88, 0.2) 0%, rgba(70, 163, 88, 0.1) 100%)",
      borderRadius: "30px",
      marginTop: "85px",
    },
  },
  content: {
    display: "flex !important" ,
    justifyContent: "space-between",
    flexDirection: "column",
    alignItems: "flex-start",
    margin: "4%",
    position: "relative",
    height: "440px",
    width: "95% !important",

    "@media (max-width: 766px)": {
      margin: "3%",
      "&::before": {
        content: `""`,
        display: "block",
        position: "absolute",
        width: "550px",
        zIndex: "-1000",
        height: "400px",
        left: "-15%",
        bottom: "-40%",
        borderRadius: "50%",
        background:
          "linear-gradient(180deg, rgba(70, 163, 88, 0.2) 0%, rgba(70, 163, 88, 0.1) 100%)",
      },
    },
    "@media (max-width: 990px)": {
     
      height: "400px"
    },
  
    "@media (max-width: 600px)": {
      height: "235px"
    },
    "@media (max-width: 535px)": {
      height: "235px"
    },
    "@media (max-width: 400px)": {
     height: "330px"
    },
  },
  greetings: {
    color: "rgba(61, 61, 61, 1)",
    fontWeight: "500",
    fontSize: "14px",
    fontFamily: '"Montserrat", sans-serif',
    textTransform: "uppercase",

  },
  containerText: {
    width: "70%",
    marginLeft: "0",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    /* height: "80%", */
    padding: 0,
    justifyContent: "center",
    gap: "10px",
    "@media (max-width: 450px)": {
      width: "65%",
    },
    "@media (max-width: 400px)": {
      width: "60%",
    },
  },
  img: {
    height: "400px",
    width: "auto",
    borderRadius: "50%",
    border: "3px solid black",
    position: "absolute",
    top: "50%",
    transform: "translateY(-50%)",
    right: "3%",
    "@media (max-width: 990px)": {
      right: "2%",
      height: "350px",
    },
    "@media (max-width: 766px)": {
      height: "270px",
    },
    "@media (max-width: 600px)": {
      height: "230px",
    },
    "@media (max-width: 535px)": {

      height: "210px",
    },
    "@media (max-width: 400px)": {
     
    },
  },
  title: {
    fontSize: "55px",
    fontWeight: "900",
    color: "rgba(61, 61, 61, 1)",
    fontFamily: '"Montserrat", sans-serif',
    "@media (max-width: 990px)": {
      fontSize: "45px",
    },
    "@media (max-width: 766px)": {
      fontSize: "38px",
    },
    "@media (max-width: 600px)": {
      fontSize: "32px",
    },
    "@media (max-width: 470px)": {
      fontSize: "28px",
    },
  },
  namePlant: {
    fontSize: "55px",
    fontWeight: "900",
    color: "rgba(70, 163, 88, 1)",
    fontFamily: '"Montserrat", sans-serif',
    textTransform: "uppercase",
    "@media (max-width: 990px)": {
      fontSize: "45px",
    },
    "@media (max-width: 766px)": {
      fontSize: "38px",
    },
    "@media (max-width: 600px)": {
      fontSize: "32px",
    },
    "@media (max-width: 470px)": {
      fontSize: "28px",
    },
  },
  desBanner: {
    fontSize: "14px",
    color: "rgba(114, 114, 114, 1)",
    fontFamily: '"Montserrat", sans-serif',
    "@media (max-width: 766px)": {
      display: "none",
    },
  },
  btnShop: {
    backgroundColor: "rgba(70, 163, 88, 1)",
    color: "rgba(255, 255, 255, 1)",
    fontSize: "16px",
    fontWeight: "700",
    width: "140px",
    height: "40px",
    textTransform: "uppercase",
    /* marginTop: "20px", */
    borderRadius: "6px",
    "&:hover": {
      backgroundColor: "#3A8C47",
    },
    "@media (max-width: 766px)": {
      backgroundColor: "transparent",
      color: "rgba(70, 163, 88, 1)",
      "&::after": {
        content: `""`,
        display: "block",
        width: "16px",
        marginLeft: "10px",
        height: "3px",
        backgroundColor: "rgba(70, 163, 88, 1)",
      },
      "&::before": {
        content: `""`,
        display: "block",
        position: "absolute",
        right: "10px",
        transform: "rotate(50deg)",
        width: "8px",
        height: "8px",
        borderTop: "3px solid rgba(70, 163, 88, 1)",
        borderRight: "3px solid rgba(70, 163, 88, 1)",
      },
      "&:hover": {
        backgroundColor: "transparent",
      },
    },
  },
});
