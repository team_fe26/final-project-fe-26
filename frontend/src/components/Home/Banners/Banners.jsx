import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Box, Button, Typography, Container, CardMedia } from "@mui/material";
import { Styles } from "./styles";
import { useNavigate } from "react-router-dom";
import { setProductId } from "../../../redux/product.slice/product.slice";
import { useDispatch, useSelector } from "react-redux";
import { fetchSlides } from "../../../redux/slider.slice/slider.slice";
import { useEffect } from "react";
export default function Banners() {
  const navigate = useNavigate();
  const dispatch = useDispatch()
  const {slides, status} = useSelector((state)=> state.slides)
  useEffect(() => {
    dispatch(fetchSlides())
  }, [dispatch]);
  const renderSlides = () => {
    if (status === 'success') {
      return slides.map((slide, index) => (
        <Box key={index} className={classes.content}>
          <Container className={classes.containerText}>
            <Typography className={classes.greetings} variant="h3">
              welcome to greenshop
            </Typography>
            <Typography className={classes.title} variant="h1">
              {slide.title}
              <span  className={classes.namePlant}>
                {" "}
                {slide.name}
              </span>
            </Typography>
            <Typography className={classes.desBanner} variant="body1">
              {slide.description}
            </Typography>
          </Container>
          <Button
            onClick={() => {
              navigate(`/product`);
              dispatch(setProductId(slide.product));
            }}
            className={classes.btnShop}
          >
            shop now
          </Button>
          <CardMedia
            component="img"
            className={classes.img}
            src={slide.imageUrl}
          />
        </Box>
      ));
    } else {
      null
    }
  };
  const CustomDot = ({ onClick, active }) => {
    return (
      <div
        onClick={onClick}
        style={{
          width: "10px",
          height: "10px",
          borderRadius: "50%",
          backgroundColor: active ? "green" : "#ccc",
          margin: "0 4px",
          cursor: "pointer",
          bottom: "0"
        }}
      />
    );
  };
  const classes = Styles();
  const settings = {
    dots: true,
    infinite: true,
    speed: 2000,
    slidesToShow: 1,
    autoplay: true, 
    autoplaySpeed: 3000,
    slidesToScroll: 1,
    appendDots: (dots) => (
      <div
        style={{ display: "flex", justifyContent: "center", bottom: "10px" }}
      >
        {dots.map((dot, index) => (
          <CustomDot
            key={index}
            {...dot.props}
            active={dot.props.className.includes("slick-active")}
          />
        ))}
      </div>
    ),
    responsive: [
      {
        breakpoint: 766,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
    ],
  };
  return (
    <>
      {" "}
      <Slider className={classes.slider} {...settings}>
        {renderSlides()}
      </Slider>
    </>
  );
}
