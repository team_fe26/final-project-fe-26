import { Container, Typography, Box, Stack, Divider } from "@mui/material";
import { Styles } from "./styles";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import PhoneInTalkIcon from "@mui/icons-material/PhoneInTalk";
import { Link } from "react-router-dom";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import YouTubeIcon from "@mui/icons-material/YouTube";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addCategory } from "../../../redux/filter.slice/filter.slice";
export default function Footer() {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const classes = Styles();
  return (
    <>
      <Container className={classes.container}>
        <Stack
          direction="row"
          divider={<Divider orientation="vertical" flexItem />}
          spacing={2}
          className={classes.containerAboutPlants}
        >
          <Box
            className={classes.containerAbout}
          >

            <img className={`${classes.img} ${classes.firstImng}`} src="/garden_care_icon.png" alt="" />
            <Typography className={classes.aboutPlantsTitle} variant="h3">
              Garden Care
            </Typography>
            <Typography className={classes.aboutPlantsDes} variant="body1">
              We are an online plant shop offering a wide range of cheap and
              trendy plants.
            </Typography>
          </Box>
          <Box className={classes.containerAbout}>
            <img className={classes.img} src="/plant_renovation_icon.png" alt="" />
            <Typography className={classes.aboutPlantsTitle} variant="h3">
              Plant Renovation
            </Typography>
            <Typography className={classes.aboutPlantsDes} variant="body1">
              We are an online plant shop offering a wide range of cheap and
              trendy plants.
            </Typography>
          </Box>
          <Box className={classes.containerAbout}>
            {" "}
            <img className={classes.img} src="/watering_garden_icon.png" alt="" />
            <Typography className={classes.aboutPlantsTitle} variant="h3">
              Watering Garden
            </Typography>
            <Typography className={classes.aboutPlantsDes} variant="body1">
              We are an online plant shop offering a wide range of cheap and
              trendy plants.
            </Typography>
          </Box>
        </Stack>
        <Container className={classes.containerContact}>
          <Container onClick={()=>{navigate('/')}} className={classes.logo}>
            <img src="/Logo_small.png"></img>
            <Link to="/" variant="h3" className={classes.logoName}>
              greenshop
            </Link>
          </Container>
          <Box className={classes.contactPart}>
            <LocationOnIcon className={classes.iconContact}></LocationOnIcon>
            <Link to='/contact-us' className={classes.contactText} variant="body1">
              70 West Buckingham Ave. Farmingdale, NY 11735
            </Link>
          </Box>
          <Box className={classes.contactPart}>
            <MailOutlineIcon className={classes.iconContact}></MailOutlineIcon>
            <a href="mailto:greenshopfe26@gmail.com" className={classes.contactText}>
              greenshopfe26@gmail.com
            </a>
          </Box>
          <Box className={classes.contactPart}>
            <PhoneInTalkIcon className={classes.iconContact}></PhoneInTalkIcon>
            <Typography className={`${classes.contactText} ${classes.thirdSection}`} variant="body1">
              +88 01911 717 490
            </Typography>
          </Box>
        </Container>
        <Container className={classes.sectionLinks}>
          <Box className={classes.containerLinks}>
            <Typography className={classes.titleLinks}>My account</Typography>
            <Link className={classes.link} to="/user">My account</Link>
            <Link className={classes.link} to="#">
              Our stores
            </Link>
            <Link className={classes.link} to="/contact-us">Contact us</Link>
          </Box>{" "}

          <Box className={classes.containerLinks}>
            <Typography Typography className={classes.titleLinks}>Categories</Typography>
            <Typography    onClick={() => {
              dispatch(addCategory("House Plants"));
            }} className={classes.link}>House Plants</Typography>
            <Typography    onClick={() => {
              dispatch(addCategory("Potter Plants"));
            }} className={classes.link}>Potter Plants</Typography>
            <Typography    onClick={() => {
              dispatch(addCategory("Small Plants"));
            }} className={classes.link}>Small Plants</Typography>
            <Typography    onClick={() => {
              dispatch(addCategory("Big Plants"));
            }} className={classes.link}>Big Plants</Typography>
          </Box>
          <Stack direction="column" spacing={2}>
            <Box className={classes.containerSocialMedia}>
              <Typography className={classes.titleLinks} variant="body1">Social Media</Typography>
              <a href="https://www.facebook.com" target="_blank" rel="noopener noreferrer">
                <FacebookIcon className={classes.iconContact}></FacebookIcon>
              </a>
              <a href="https://www.instagram.com" target="_blank" rel="noopener noreferrer">
                <InstagramIcon className={classes.iconContact}></InstagramIcon>
              </a>
              <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer">
                <TwitterIcon className={classes.iconContact}></TwitterIcon>
              </a>
              <a href="https://www.linkedin.com" target="_blank" rel="noopener noreferrer">
                <LinkedInIcon className={classes.iconContact}></LinkedInIcon>
              </a>
              <a href="https://www.youtube.com" target="_blank" rel="noopener noreferrer">
                <YouTubeIcon className={classes.iconContact}></YouTubeIcon>
              </a>
            </Box>
            <Box>
              <Typography className={classes.titleLinks}>We accept</Typography>
              <Link></Link>
            </Box>
            <Box>
              <img src='/card.png'></img>
            </Box>

          </Stack>
        </Container>
      </Container>
    </>
  );
}
