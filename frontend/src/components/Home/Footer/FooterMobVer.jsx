import { useState } from 'react';
import { Container } from "@mui/material";
import { Styles } from "./styles";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import HomeIcon from "@mui/icons-material/Home";
import FavoriteIcon from "@mui/icons-material/Favorite";
import PersonIcon from "@mui/icons-material/Person";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { selectIsWishlistEmpty } from '../../../redux/wishlist.slice/wishlist.slice'
export default function FooterMobVer() {
  
  const { loggedIn, userData } = useSelector((state) => state.authorization);
  const navigate = useNavigate();
  const classes = Styles();
  const { cartId } = useSelector((state) => state.cart);
  const itemCount = cartId.reduce((acc, item) => acc + item.quantityCart, 0);
  const isWishlistEmpty = useSelector(selectIsWishlistEmpty); 

  const handleNavigation = (path) => {
  
    navigate(path);
  };
  return (
    <>
      <Container className={classes.containerMob}>
        <HomeIcon
          onClick={() => handleNavigation('/')}
          className={classes.mobIconHome}
        />
        <FavoriteIcon
          onClick={() => handleNavigation('/wishlist')}
          style={{ color: isWishlistEmpty ? 'rgba(217, 217, 217, 1)' : 'rgb(70, 163, 88)' }}
          className={classes.mobIcon}
        />
        <div className={classes.cartIconWrapper} style={{ position: 'relative', marginTop: '5px' }}>
          <ShoppingCartIcon
            onClick={() => handleNavigation('/cart')}
            
            className={classes.mobIcon}
          />
          {itemCount > 0 && (
            <span className={classes.cartItemCount}>{itemCount}</span>
          )}
        </div>
        <PersonIcon
          onClick={() => {
         loggedIn ? navigate("/user") : navigate('/login');
         
          }}
          
          className={classes.mobIcon}
        ></PersonIcon>
      </Container>
    </>
  );
}
