import { createUseStyles } from "react-jss";
export const Styles = createUseStyles({
  container: {
    backgroundColor: 'rgba(251, 251, 251, 1)',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
    marginTop: '120px',
    padding: 0,
    
  },

  containerAboutPlants: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '40px',
    marginBottom: '40px',
    paddingRight: '30px',
    paddingLeft: '30px',
  },
  aboutPlantsTitle: {
    fontWeight: '700',
    fontSize: '17px',
    fontFamily: '"Montserrat", sans-serif',
    color: 'rgba(61, 61, 61, 1)',
    position: 'relative',
    '&::after': {
      content: '""',
      display: 'block',
      height: '74px',
      width: '74px',
      borderRadius: '50%',
      backgroundColor: 'rgba(70, 163, 88, 0.13)',
      position: 'absolute',
      top: '-80px',
      left: '20px',
    }
  },
  containerAbout: {
    gap: '10px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    position: 'relative',
    '&:nth-child(1) $aboutPlantsTitle::after': {
      left: '0',
    },
  },
  aboutPlantsDes: {
    fontFamily: '"Montserrat", sans-serif',
    fontSize: '14px',
    color: 'rgba(114, 114, 114, 1)'
  },
  contactText: {
    color: 'rgba(61, 61, 61, 1)',
    fontFamily: '"Montserrat", sans-serif',
    fontSize: '14px',
    textDecoration: 'none',
  },
  contactPart: {
    display: 'flex',
    alignItems: 'center',
    gap: '10px',
    cursor: 'pointer',
  },
  containerContact: {
    backgroundColor: 'rgba(70, 163, 88, 0.1)',
    height: '90px',
    gap: '20px',
    display: 'flex',
    alignItems: 'center',
    paddingRight: '100px',
    justifyContent: 'space-between',
    '@media (max-width: 1080px)': {
      paddingRight: '20px',
    },
    '@media (max-width: 800px)': {
      height: '130px',
    },
  },
  thirdSection: {
    whiteSpace: 'nowrap',
  },
  logo: {
    display: "flex",
    cursor: "pointer",
    alignItems: "center",
    gap: "5px",
    width: 'fit-content',
    margin: '0',
    padding: '0',
  },
  logoName: {
    color: "rgba(70, 163, 88, 1)",
    fontWeight: "800",
    textTransform: "uppercase",
    fontFamily: '"Montserrat", sans-serif',
    fontSize: "18px",
    textDecoration: "none"
  },

  iconContact: {
    color: 'rgba(70, 163, 88, 1)',

  },

  sectionLinks: {
    display: 'flex',
    marginTop: '30px',
    marginBottom: '30px',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingRight: '10%',
    '@media (max-width: 900px)': {
      paddingRight: '0',
    },
  },
  containerLinks: {
    display: 'flex',
    flexDirection: 'column',
    gap: '10px',
  },
  titleLinks: {
    color: 'rgba(61, 61, 61, 1)',
    fontSize: '18px',
    fontWeight: '700',
    fontFamily: '"Montserrat", sans-serif',
    width: '100%'
  },
  link: {
    fontSize: '14px',
    fontFamily: '"Montserrat", sans-serif',
    color: 'rgba(61, 61, 61, 1)',
    textDecoration: 'none',
    cursor: 'pointer',
    "&:hover":{
textDecoration: 'underline'
    },
  },
  containerSocialMedia: {
    display: 'flex',
    gap: '8px',
    flexWrap: 'wrap'
  },
  firstImng: {
    height: "87px"
  },
  containerMob: {
    backgroundColor: 'rgba(255, 255, 255, 1)',
    display: 'flex',
    height: '80px',
    padding: '0',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
    position: 'fixed',
    bottom: '0',
    left: 0,
    borderTop: '2px solid rgba(165, 165, 165, 0.5)',
    borderTopLeftRadius: '20px',
    borderTopRightRadius: '20px',
    zIndex: 1000
  },
  mobIcon: {
    color: 'rgba(217, 217, 217, 1)',
    cursor: 'pointer'
  },
  mobIconHome: {
    color: 'rgba(70, 163, 88, 1)',
    cursor: 'pointer'
  },
  cartItemCount: {
    position: 'absolute',
    top: '-10px',
    right: '-10px',
    color: 'green',
    fontWeight: "700"

  },
})