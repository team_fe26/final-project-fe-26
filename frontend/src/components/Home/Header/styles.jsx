import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: 'space-between',
    padding: 0,
    zIndex: "1000",
    backgroundColor: "#FFF",
    position: "fixed",
    top: "0",
    left: "50%",
    transform: "translateX(-50%)",
    height: "95px",
    '&::after': {
      content: '""',
      position: 'absolute',
      bottom: '0px',
      display: "block",
      backgroundColor: "rgba(70, 163, 88, 0.5)",
      height: "0.3px",
      width: "100%",
    },
    '@media (max-width: 1200px)': {
      padding: "0 20px 0 20px",
    }
  },
  logo: {
    display: "flex",
    cursor: "pointer",
    alignItems: "center",
    gap: "5px",
    width: 'fit-content',
    margin: '0',
    padding: '0',
  },
  logoName: {
    color: "rgba(70, 163, 88, 1)",
    fontWeight: "800",
    textTransform: "uppercase",
    fontFamily: '"Montserrat", sans-serif',
    fontSize: "18px",
  },
  navbar: {
    width: 'fit-content',
    gap: '45px',
    '@media (max-width: 870px)': {
      gap: '30px'
    }
  },
  isGapNavbar: isSearch => ({
    display: isSearch === false ? 'flex' : 'block',
  }),
  isShowLinks: isSearch => ({
    width: isSearch === false ? '' : '0',
    opacity: isSearch === false ? '1' : '0',
    right: isSearch === false ? '' : '20px',
    transition: isSearch === false ? 'opacity 4s ease, transform 0.3s ease, width 0.2s ease, right 0.3s ease' : 'opacity 0.5s ease, transform 0.3s ease, width 0.2s ease, right 0.3s ease',

  }),
  link: {
    textDecoration: "none",
    color: "rgba(61, 61, 61, 1)",
    fontSize: "16px",
    fontWeight: "400",
    position: 'relative',
    transition: 'opacity 3s ease, transform 0.3s ease, width 0.2s ease, right 0.3s ease',
    "&:hover": {
      fontWeight: "700",
      "&::after": {
        content: '""',
        position: 'relative',
        bottom: '-35px',
        display: "block",
        backgroundColor: "rgba(70, 163, 88, 1)",
        height: "3px",
        width: "100%",
      },
    },
  },

  searchField: {
    marginBottom: "15px",
    color: "rgba(165, 165, 165, 1)",
    fontFamily: '"Montserrat", sans-serif',
    fontWeight: 400,
    height: "50px",
    outline: "none",
    transition: 'width 0.5s  ease-in-out',
    '& .MuiInputBase-root': {
      transition: 'width 0.5s ease-in-out',
      backgroundColor: "rgba(248, 248, 248, 1)",
      borderRadius: '15px',
      paddingLeft: "20px",
      height: "50px",
      '&.Mui-focused': {
        borderColor: 'rgba(165, 165, 165, 1)',
      },
      '& .MuiOutlinedInput-notchedOutline': {
        borderColor: 'rgba(248, 248, 248, 1)',
      },
      '&:hover .MuiOutlinedInput-notchedOutline': {
        borderColor: 'rgba(248, 248, 248, 1)',
      },
    },
  },
  isShowField: isSearch => ({
    width: isSearch === false ? '0' : '100%',
    opacity: isSearch === false ? '0' : '1',
    transition: 'width 0.5s ease-in-out, opacity 0.3s ease-in-out',
  }),
  containerField: isSearch => ({
    width: isSearch === false ? '0' : '100%',
    marginTop: '15px',
    position: 'relative',
    transition: 'width 0.5s ease-in-out',
  }),
  containerSearchResults: isSearch => ({
    display: isSearch === false ? 'none' : 'block',
    position: 'absolute',
    width: '100%',
    zIndex: 3000,
    borderRadius: '15px',
    backgroundColor: 'rgba(248, 248, 248, 1)',
    top: 34,
    transition: 'display 0.5s ease-in-out',
    '@media (max-width: 768px)': {
      top: 33,
    }
  }),
  menuItem: {
    fontSize: '15px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    '@media (max-width: 600px)': {
      fontSize: '14px',
      paddingTop: 0,
      paddingBottom: 0
    }
  },
  containerIcons: {
    display: 'flex',
    alignItems: 'center',
    gap: '25px',
    width: 'fit-content',
    margin: '0',
    padding: '0',
    position: 'relative'
  },
  cartItemCount: {
    height: "10px",
    width: "10px",
    position: 'absolute',
    top: '-10px',
    right: '-10px',
    color: 'green',
    fontWeight: "700",
    backgroundColor: "#fff",
    borderRadius: "100%"

  },

  iconSearchCart: {
    color: "rgba(61, 61, 61, 1)",
    cursor: 'pointer',
    position: 'relative',
    transition: 'opacity 0.3s ease, transform 0.3s ease, width 0.3s ease, left 0.3s ease',
  },
  isShowSearchIcon: isSearch => ({
    transform: isSearch === false ? '' : 'rotate(180deg)',
    opacity: isSearch === false ? '1' : '0',
    width: isSearch === false ? '' : '0',
    right: isSearch === false ? '' : '20px',
  }),
  buttonLog: {
    backgroundColor: "rgba(70, 163, 88, 1)",
    borderRadius: "6px",
    color: "rgba(255, 255, 255, 1)",
    width: '110px',
    "&:hover": {
      backgroundColor: "#3A8C47",
    },
  },
  containerMob: {
    position: "fixed",
    background: "#FFF",
    zIndex: "1000",
    top: "0",
    paddingTop: "10px",
    height: "75px",
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    gap: '5px',
    minWidth: '366px'
  },
  iconFilter: {
    color: "rgba(248, 248, 248, 1)",
  },
  containerIconFilter: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '45px',
    height: '45px',
    marginBottom: '13px',
    borderRadius: '20%',
    backgroundColor: 'rgba(70, 163, 88, 1)',
  },
});
