import FilterAltIcon from "@mui/icons-material/FilterAlt";
import {
  Container,
  TextField,
  InputAdornment,
  Box,
  MenuItem,
  MenuList,
  Stack,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import SearchIcon from "@mui/icons-material/Search";
import { useNavigate } from "react-router-dom";
import { Styles } from "./styles";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { fetchAllProducts } from "../../../redux/products.slice/products.slice";
import { setSearchField } from "../../../redux/filter.slice/filter.slice";
import { setProductId } from "../../../redux/product.slice/product.slice";
import { useSearchParams } from "react-router-dom";
export default function HeaderMobVer() {
  const { allProducts, status } = useSelector((state) => state.products);
  const [results, setResults] = useState([]);
  const { searchField } = useSelector((state) => state.filters);
  const [searchParams] = useSearchParams();
  const handleFilterClick = () => {
    const queryString = searchParams.toString();
    navigate(`/filters?${queryString}`);
  };
  const dispatch = useDispatch();
  const productsName = [];

  const wrapText = (text) => {
    if (!text) {
      return;
    }
    if (text.length <= 38) {
      return text;
    }
    return text.substring(0, 35) + "...";
  };

  useEffect(() => {
    dispatch(fetchAllProducts());
  }, [dispatch]);
  const navigate = useNavigate();
  const classes = Styles(results);
  useEffect(() => {
    if (status === "success") {
      allProducts.forEach((product) => {
        productsName.push(product.name);
      });
    }
  }, [status, allProducts, productsName]);
  const handleSearch = (event) => {
    dispatch(setSearchField(event.target.value));
    if (event.target.value === "") {
      setResults([]);
    } else {
      const filteredResults = productsName.filter((item) =>
        item.toLowerCase().includes(event.target.value.toLowerCase())
      );
      setResults(filteredResults);
    }
  };
  const navigateToUniquePageOfProduct = (name) => {
    if (status === "success") {
      const product = allProducts.find(
        (product) => product.name.toLowerCase() === name.toLowerCase()
      );
      console.log(product.name.toLowerCase() === name.toLowerCase());
      if (product) {
        navigate(`/product`);
        dispatch(setProductId(product._id));
      }
    }
  };
  const renderResults = () => {
    return results.slice(0, 10).map((result, index) => (
      <MenuItem
        key={index}
        className={classes.menuItem}
        onClick={() => {
          navigateToUniquePageOfProduct(result);
          dispatch(setSearchField(""));
        }}
      >
        {wrapText(result)}
      </MenuItem>
    ));
  };
  return (
    <>
      <Container className={classes.containerMob}>
        <Stack style={{ width: "100%", position: "relative" }}>
          <TextField
            value={searchField}
            type="text"
            onChange={handleSearch}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon className={`${classes.iconSearchCart}`} />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment
                  position="end"
                  onClick={() => {
                    setResults([]);
                    dispatch(setSearchField(""));
                  }}
                >
                  <CloseIcon className={`${classes.iconSearchCart}`} />
                </InputAdornment>
              ),
            }}
            className={classes.searchField}
            placeholder="Find your plants"
          ></TextField>{" "}
          {results.length > 0 ? (
            <MenuList className={classes.containerSearchResults}>
              {renderResults()}
            </MenuList>
          ) : null}
        </Stack>
        <Box
          onClick={() => {
            handleFilterClick()
          }}
          className={classes.containerIconFilter}
        >
          <FilterAltIcon className={classes.iconFilter}></FilterAltIcon>
        </Box>
      </Container>
    </>
  );
}
