import {
  Container,
  Typography,
  Button,
  InputAdornment,
  TextField,
  MenuList,
  MenuItem,
  Stack,
} from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import { useState } from "react";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import LogoutIcon from "@mui/icons-material/Logout";
import LoginIcon from "@mui/icons-material/Login";
import { Styles } from "./styles";
import CloseIcon from "@mui/icons-material/Close";
import {
  setLogged,
  setIsShowModal,
} from "../../../redux/authorization.slice/authorization.slice";
import { useSelector, useDispatch } from "react-redux";
import ModalWindow from "../../Authorization/ModalWindow/ModalWindow";
import { useEffect } from "react";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { setSearchField } from "../../../redux/filter.slice/filter.slice";
import { setProductId } from "../../../redux/product.slice/product.slice";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { selectIsWishlistEmpty } from "../../../redux/wishlist.slice/wishlist.slice";
export default function Header() {
  const [isSearch, setIsSearch] = useState(false);
  const { loggedIn, isShowModal } = useSelector((state) => state.authorization);
  const isWishlistEmpty = useSelector(selectIsWishlistEmpty);
  const { searchField } = useSelector((state) => state.filters);
  const { allProducts, status } = useSelector((state) => state.products);
  const [results, setResults] = useState([]);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const classes = Styles(isSearch, results);

  const { cartId } = useSelector((state) => state.cart);
  const itemCount = cartId.reduce((acc, item) => acc + item.quantityCart, 0);

  const logOut = async () => {
    try {
      const token = JSON.parse(localStorage.getItem("token")) || "";
      const response = await fetch(
        "https://greenshop-server.onrender.com/api/logout",
        {
          method: "POST",
          credentials: "include",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      if (!response.ok) {
        const data = await response.json();
        console.error("Error logout:", data.error);
        return;
      }
      dispatch(setLogged(false));
      localStorage.removeItem("token");
    } catch (error) {
      console.error("Error fetch:", error);
    }
  };
  const productsName = [];

  const wrapText = (text) => {
    if (!text) {
      return;
    }
    if (text.length <= 38) {
      return text;
    }
    return text.substring(0, 35) + "...";
  };

  useEffect(() => {
    if (status === "success") {
      allProducts.forEach((product) => {
        productsName.push(product.name);
      });
    }
  }, [status, allProducts, productsName]);
  const handleSearch = (event) => {
    dispatch(setSearchField(event.target.value));
    if (event.target.value === "") {
      setResults([]);
    } else {
      const filteredResults = productsName.filter((item) =>
        item.toLowerCase().includes(event.target.value.toLowerCase())
      );
      setResults(filteredResults);
    }
  };
  const navigateToUniquePageOfProduct = (name) => {
    if (status === "success") {
      const product = allProducts.find(
        (product) => product.name.toLowerCase() === name.toLowerCase()
      );
      console.log(product.name.toLowerCase() === name.toLowerCase());
      if (product) {
        navigate(`/product`);
        dispatch(setProductId(product._id));
      }
    }
  };
  const navigateUserId = () => {
    if (!loggedIn) {
      dispatch(setIsShowModal(true));
    } else {
      navigate(`/user`);
    }
  };
  const renderResults = () => {
    return results.slice(0, 10).map((result, index) => (
      <MenuItem
        key={index}
        className={classes.menuItem}
        onClick={() => {
          navigateToUniquePageOfProduct(result);
          setIsSearch(false);
        }}
      >
        {wrapText(result)}
      </MenuItem>
    ));
  };

  return (
    <>
      {isShowModal ? <ModalWindow></ModalWindow> : null}

      <Container className={classes.container}>
        <Container
          className={classes.logo}
          onClick={() => {
            navigate("/");
          }}
        >
          <img alt="logo" src="/Logo_small.png" />
          <Typography variant="h3" className={classes.logoName}>
            GREENSHOP
          </Typography>
        </Container>
        <Container className={`${classes.navbar} ${classes.isGapNavbar}`}>
          <Link to="/" className={`${classes.link} ${classes.isShowLinks}`}>
            Home
          </Link>
          <Link
            to="/plant-care"
            className={`${classes.link} ${classes.isShowLinks}`}
          >
            Plant Care
          </Link>
          <Link
            to="/contact-us"
            className={`${classes.link} ${classes.isShowLinks}`}
          >
            Contact Us
          </Link>{" "}
        </Container>
        <Stack
          className={classes.containerField}
        >
          <TextField
            value={searchField}
            type="text"
            onChange={handleSearch}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon className={`${classes.iconSearchCart}`} />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <CloseIcon
                    onClick={() => setIsSearch(false)}
                    className={classes.iconSearchCart}
                  />
                </InputAdornment>
              ),
            }}
            className={`${classes.searchField} ${classes.isShowField}`}
            placeholder="Find your plants"
          ></TextField>
          {results.length > 0 ? (
            <MenuList className={classes.containerSearchResults}>
              {renderResults()}
            </MenuList>
          ) : null}
        </Stack>
        <Container className={classes.containerIcons}>
          <SearchIcon
            onClick={() => {
              setIsSearch(true);
              dispatch(setSearchField(""));
              setResults([]);
            }}
            className={`${classes.iconSearchCart} ${classes.isShowSearchIcon}`}
          ></SearchIcon>
          <FavoriteIcon
            style={{ color: isWishlistEmpty ? "rgb(61, 61, 61)" : "#46a358" }}
            className={classes.iconSearchCart}
            onClick={() => navigate("/wishlist")}
          />
          <div
            className={classes.cartIconWrapper}
            style={{ position: "relative", marginTop: "5px" }}
          >
            <ShoppingCartIcon
              color="icon"
              className={classes.iconSearchCart}
              onClick={() => navigate("/cart")}
            />
            {itemCount > 0 && (
              <span className={classes.cartItemCount}>{itemCount}</span>
            )}
          </div>
          <AccountCircleIcon
            className={classes.iconSearchCart}
            onClick={navigateUserId}
          ></AccountCircleIcon>
          {loggedIn ? (
            <Button
              className={classes.buttonLog}
              startIcon={<LogoutIcon></LogoutIcon>}
              onClick={logOut}
            >
              Log out
            </Button>
          ) : (
            <Button
              className={classes.buttonLog}
              startIcon={<LoginIcon></LoginIcon>}
              onClick={() => {
                dispatch(setIsShowModal(true));
              }}
            >
              Login
            </Button>
          )}
        </Container>
      </Container>
    </>
  );
}
