import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  container: {
    width: "310px",
    backgroundColor: "rgba(251, 251, 251, 1)",
    paddingTop: "20px",
    paddingBottom: "20px",
    paddingLeft: "20px",
    height: 'fit-content'
  },
  titleOfPart: {
    color: "rgba(61, 61, 61, 1)",
    fontSize: "18px",
    fontWeight: "700",
  },
  menuList: {
    marginBottom: "30px",
  },
  filters: {
    marginBottom: "30px",
  },
  filtersMenuItem: {
    background: "rgba(114, 114, 114, 0.4)",
    color: "rgba(114, 114, 114, 1)",
    marginTop: "10px",
    marginRight: "20px",
    cursor: "default",
    marginLeft: "10px",
    fontWeight: 700,
    "&:hover": {
      background: "rgba(114, 114, 114, 0.4)",
    },
  },
  filtersMenuItemButton: {
    width: "15px",
    height: "15px",
    marginRight: "10px",
    marginLeft: "auto",
    border: "none",
    cursor: "pointer",
    position: "relative",
    bottom: "8px",
    backgroundColor: "transparent",
    "&::before": {
      content: '""',
      position: "absolute",
      display: "block",
      width: "2px",
      height: "18px",
      transform: "rotate(130deg)",
      backgroundColor: "rgba(114, 114, 114, 1)",
    },
    "&::after": {
      content: '""',
      display: "block",
      position: "absolute",
      width: "2px",
      height: "18px",
      transform: "rotate(50deg)",
      backgroundColor: "rgba(114, 114, 114, 1)",
    },
  },
  listItemText: {
    fontWeight: 700,
    fontSize: "15px",
    color: "rgba(114, 114, 114, 1)",
  },
  listItemSpan: {
    fontSize: "15px",
    color: "rgba(114, 114, 114, 1)",
  },
  menuItem: {
    "&:hover $listItemText": {
      color: "rgba(70, 163, 88, 1)",
    },
    "&:hover $listItemSpan": {
      color: "rgba(70, 163, 88, 1)",
    },
  },
  containerSlider: {
    width: "270px",
    marginTop: "20px",
    marginLeft: "0",
  },
  slider: {
    color: "rgba(70, 163, 88, 1)",
  },
  commentPrice: {
    fontSize: "15px",
    fontWeight: "400",
    color: "rgba(114, 114, 114, 1)",
  },
  commentPriceSpan: {
    fontSize: "15px",
    fontWeight: "700",
    marginLeft: "8px",
    color: "rgba(70, 163, 88, 1)",
  },
  buttonFilter: {
    background: "rgba(70, 163, 88, 1)",
    borderRadius: "6px",
    color: "rgba(255, 255, 255, 1)",
    fontWeight: "700",
    fontSize: "16px",
    width: "90px",
    height: "35px",
    border: "none",
    marginTop: "20px",
    marginBottom: "40px",
    cursor: "pointer",
    transition: "background-color 0.3s ease", 
    '&:hover': {
      backgroundColor: "#3A8C47" 
    }
  },
  controlLabel:{
    fontWeight: 700,
    fontSize: "15px",
    color: "rgba(114, 114, 114, 1)",
    paddingLeft: '15px',
    width: 'fit-content'
  },
  lastTitle:{
    color: "rgba(61, 61, 61, 1)",
    fontSize: "18px",
    fontWeight: "700",
    paddingTop: '20px',
  }
});
