import {
  Container,
  Box,
  MenuList,
  MenuItem,
  ListItemText,
  Typography,
  Slider,
  FormControlLabel,
  Switch,
  Stack,
} from "@mui/material";
import { Styles } from "./styles";
import { useSelector, useDispatch } from "react-redux";
import { useSearchParams } from "react-router-dom";
import { useEffect, useState } from "react";
import {
  setRangePrice,
  addCategory,
  deleteCategory,
  addSize,
  deleteSize,
  setSale,
  setFilterProducts,
  setEmptyButFilter,
} from "../../../redux/filter.slice/filter.slice";
import { fetchCategories } from "../../../redux/categories.slice/categories.slice";
import { fetchSizes } from "../../../redux/sizes.slice/sizes.slice";
export default function FiltersTable() {
  const [searchParams, setSearchParams] = useSearchParams();
  const dispatch = useDispatch();
  const { allProducts, status } = useSelector((state) => state.products);
  const { rangePrice, sizes, categories, sale } = useSelector(
    (state) => state.filters
  );
  const {CATEGORIES}= useSelector((state)=> state.categories)
  const {SIZES} =useSelector((state)=>state.sizes)
  const [categoryCounts, setCategoryCounts] = useState({});
  const [sizeCounts, setSizeCounts] = useState({});
useEffect(()=>{
  dispatch(fetchSizes())
  dispatch(fetchCategories())
}, [dispatch])

  const renderMenuItems = (items, counts, onClick) => {
    return items.map((item) => (
      <MenuItem
        key={item.name}
        value={item.name}
        onClick={() => onClick(item)}
        className={classes.menuItem}
      >
        <ListItemText className={classes.listItemText}>{item.name}</ListItemText>
        <span className={classes.listItemSpan}>({counts[item.name] || 0})</span>
      </MenuItem>
    ));
  };
  useEffect(() => {
    const params = Object.fromEntries([...searchParams]);

    if (Object.keys(params).length > 0) {
      if (params.category) {
        const categoriesArray = params.category.split(",");
        categoriesArray.forEach((category) => {
          dispatch(addCategory(category));
        });
      }

      if (params.size) {
        const sizesArray = params.size.split(",");
        sizesArray.forEach((size) => dispatch(addSize(size)));
      }

      if (params.sale !== undefined) {
        dispatch(setSale(params.sale === "true"));
      }

      if (params.minPrice && params.maxPrice) {
        dispatch(
          setRangePrice([Number(params.minPrice), Number(params.maxPrice)])
        );
      }

      filterProducts(params);
    } else {
      dispatch(setFilterProducts([]));
    }
  }, [searchParams, dispatch]);

  const filterProducts = (params) => {
    if (status === "success") {
      const noParams = Object.keys(params).length === 0;
      if (noParams) {
        dispatch(setFilterProducts([]));
        return;
      }
      const sizeFilter = params.size ? params.size.split(",") : sizes;
      const categoryFilter = params.category
        ? params.category.split(",")
        : categories;
      const minPrice = params.minPrice
        ? Number(params.minPrice)
        : rangePrice[0];
      const maxPrice = params.maxPrice
        ? Number(params.maxPrice)
        : rangePrice[1];
      const saleFilter = params.sale === "true" || sale;

      const filteredProducts = allProducts.filter((product) => {
        const sizeMatch =
          sizeFilter.length === 0 || sizeFilter.includes(product.size);
        const categoryMatch =
          categoryFilter.length === 0 ||
          categoryFilter.includes(product.categories);
        const priceMatch =
          product.currentPrice >= minPrice && product.currentPrice <= maxPrice;

        if (sizeMatch && categoryMatch && priceMatch) {
          return saleFilter
            ? product.previousPrice > product.currentPrice
            : true;
        }
        return false;
      });

      dispatch(setFilterProducts(filteredProducts));
      dispatch(setEmptyButFilter(filteredProducts.length === 0));
    }
  };

  const applyFilters = () => {
    const params = {};
    if (categories.length > 0) {
      params.category = categories.join(",");
    }
    if (sizes.length > 0) {
      params.size = sizes.join(",");
    }
    if (sale) {
      params.sale = "true";
    }
    params.minPrice = rangePrice[0];
    params.maxPrice = rangePrice[1];
    setSearchParams(params);
    filterProducts(params);
  };

  useEffect(() => {
    const countAndFilterProducts = () => {
      const counts = {};
      const sizesCount = {};

      const params = Object.fromEntries([...searchParams]);
      const sizeFilter = params.size ? params.size.split(",") : sizes;
      const categoryFilter = params.category
        ? params.category.split(",")
        : categories;
      const minPrice = params.minPrice ? Number(params.minPrice) : rangePrice[0];
      const maxPrice = params.maxPrice ? Number(params.maxPrice) : rangePrice[1];
      const saleFilter = params.sale === "true";
  
      if (status === "success") {
        allProducts.forEach((product) => {
          const sizeMatch =
            sizeFilter.length === 0 || sizeFilter.includes(product.size);
          const categoryMatch =
            categoryFilter.length === 0 ||
            categoryFilter.includes(product.categories);
          const priceMatch =
            product.currentPrice >= minPrice && product.currentPrice <= maxPrice;
          const hasDiscount = product.previousPrice > product.currentPrice;
          const saleMatch = !saleFilter || hasDiscount;
  
          if (sizeMatch && categoryMatch && priceMatch && saleMatch) {
            counts[product.categories] = (counts[product.categories] || 0) + 1;
            sizesCount[product.size] = (sizesCount[product.size] || 0) + 1;
          }
        });

        setCategoryCounts(counts);
        setSizeCounts(sizesCount);
      }
    };
  
    countAndFilterProducts();
  }, [allProducts, status, searchParams, categories, sizes, sale, rangePrice]);
  
  const classes = Styles();

  return (
    <>
      <Box className={classes.container}>
        {categories.length > 0 || sizes.length > 0 ? (
          <>
            <Typography className={classes.titleOfPart} variant="body1">
              Settings
            </Typography>
            <MenuList className={classes.filters}>
              {categories.length > 0
                ? categories.map((category) => (
                    <MenuItem
                      key={category}
                      className={classes.filtersMenuItem}
                      value={category}
                    >
                      {category}
                      <button
                        className={classes.filtersMenuItemButton}
                        onClick={() => dispatch(deleteCategory(category))}
                      ></button>
                    </MenuItem>
                  ))
                : null}
              {sizes.length > 0
                ? sizes.map((size) => (
                    <MenuItem
                      key={size}
                      className={classes.filtersMenuItem}
                      value={size}
                    >
                      {size}
                      <button
                        className={classes.filtersMenuItemButton}
                        onClick={() => dispatch(deleteSize(size))}
                      ></button>
                    </MenuItem>
                  ))
                : null}
            </MenuList>{" "}
          </>
        ) : null}
        <Typography className={classes.titleOfPart} variant="body1">
          Categories
        </Typography>
        <MenuList className={classes.menuList}>  {renderMenuItems(CATEGORIES, categoryCounts, (category) => dispatch(addCategory(category.name)))}</MenuList>
        <Typography className={classes.titleOfPart} variant="body1">
          Price Range
        </Typography>
        <Container className={classes.containerSlider}>
          {" "}
          <Slider
            min={4.99}
            max={866.99}
            className={classes.slider}
            value={rangePrice}
            onChange={(event, newValue) => {
              dispatch(setRangePrice(newValue));
            }}
            valueLabelDisplay="auto"
          />
          <Typography className={classes.commentPrice} variant="body1">
            Price:
            <span className={classes.commentPriceSpan}>
              ${rangePrice[0]}-${rangePrice[1]}
            </span>
          </Typography>
          <button onClick={applyFilters} className={classes.buttonFilter}>
            Filter
          </button>
        </Container>
        <Typography className={classes.titleOfPart} variant="body1">
          Size
        </Typography>
        <MenuList> {renderMenuItems(SIZES, sizeCounts, (size) => dispatch(addSize(size.name)))}</MenuList>
        <Stack direction="column" spacing={2}>
          <Typography className={classes.lastTitle} variant="body1">
            Show Only
          </Typography>
          <FormControlLabel
            className={classes.controlLabel}
            labelPlacement="start"
            label="Sale"
            control={
              <Switch
                color="success"
                checked={sale}
                onChange={(event) => {
                  dispatch(setSale(event.target.checked));
                }}
              />
            }
          />
        </Stack>
      </Box>
    </>
  );
}
