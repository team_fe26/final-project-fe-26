import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  text: {
    fontFamily: '"Montserrat", sans-serif',
    fontSize: "15px",
    color: "rgba(61, 61, 61, 1)",
  },
  formControl: {
    width: "120px",
    border: "none",
    position: "relative",
    padding: "0",
    top: "-40px",
  },
  inputLabel: {
    fontFamily: '"Montserrat", sans-serif',
    fontSize: "15px",
    color: "rgba(61, 61, 61, 1)",
    position: "absolute",
    top: "-5px",
  },
  button:{
    marginLeft: '25px',
    marginBottom: '15px',
width: '150px',
color: 'rgba(248, 248, 248, 1)',
backgroundColor: "rgba(70, 163, 88, 1)",
"&:hover": {
  backgroundColor: "#3A8C47",
},
'@media (min-width: 766px)':{
    marginLeft: 'auto', 
    marginRight: '20px'
},
  },
});
