import { Styles } from "./styles";
import { setSortBy } from "../../../redux/filter.slice/filter.slice";
import { useDispatch } from "react-redux";
import { MenuItem, Button, Menu, Fade } from "@mui/material";
import React from "react";
export default function Sorting() {
  const classes = Styles();
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <>
      <Button
        className={classes.button}
        id="fade-button"
        aria-controls={open ? "fade-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        onClick={handleClick}
      >
        Sort By Price
      </Button>
      <Menu
        id="fade-menu"
        MenuListProps={{
          "aria-labelledby": "fade-button",
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        <MenuItem
          onClick={() => {
            handleClose();
            dispatch(setSortBy("min to max"));
          }}
          value={"min to max"}
        >
          min to max
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleClose();
            dispatch(setSortBy("max to min"));
          }}
          value={"max to min"}
        >
          max to min
        </MenuItem>
      </Menu>
    </>
  );
}
