import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchComments, addComment, setCurrentPage } from '../../redux/comments.slice/comments.slice';
import { List, ListItem, ListItemText, CircularProgress, Typography, TextField, Button, Rating, Box, Stack, Pagination } from '@mui/material';
import { ThemeProvider, createTheme } from "@mui/material/styles";
import {
  Styles
} from "./styles";

const CommentsList = ({ productId, comments}) => {
  const classes = Styles();
  const dispatch = useDispatch();
  const { totalPages } = useSelector((state) => state.comments.comments);
  const {currentPage, status, error} = useSelector((state) => state.comments);
  const isAuthenticated = useSelector((state) => state.authorization.loggedIn);

  const [newComment, setNewComment] = useState('');
  const [newRating, setNewRating] = useState(0);
  const [commentError, setNewCommentError] = useState('')

  const handleAddComment = async () => {

    setNewCommentError('')

    if (newComment.trim().length === 0) {
      setNewCommentError("Error: Comment content cannot be empty");
      return;
    }

    if (newComment.length > 500) {
      setNewCommentError("Error: Comment content cannot have more than 500 symbols");
      return;
    }

    try {
      if(totalPages === 0) {
        await dispatch(addComment({ productId: productId, content: newComment, rating: newRating }));
        setNewComment('');
        setNewRating(0);
        await dispatch(fetchComments({productId: productId, page: 1}));
        await dispatch(setCurrentPage(1));
      } else {
        await dispatch(addComment({ productId: productId, content: newComment, rating: newRating }));
        setNewComment('');
        setNewRating(0);
        await dispatch(fetchComments({productId: productId, page: totalPages}));
        await dispatch(setCurrentPage(totalPages));
      }
      
    } catch (error) {
      console.error("Failed to add comment", error);
    }
  };

  if (status === 'loading') {
    return (
      <CircularProgress className={classes.loader}/>
    )
  }

  if (status === 'failed') {
    return <Typography color="error">Error: {error}</Typography>;
  }

  const handlePageChange = (event, value) => {
    dispatch(setCurrentPage(value));
    dispatch(fetchComments({productId, page: value}))
  };


  const productComments = Array.isArray(comments) ?  comments.filter(comment => comment.product._id === productId) : [];

  const theme = createTheme({
    palette: {
      primary: {
        main: "#46A358",
      },
      secondary: {
        main: "#ff5722",
      }
  }})

  return (
    <ThemeProvider theme={theme}>
    
      <Box className={classes.commentsContainer}>
        <List  >
          {productComments.length ? (
            productComments.map((comment) => (
              <ListItem className={classes.CommentsList} key={comment._id}>
              <ListItemText
                primary={
                  <>
                    <Typography variant="body1">{comment.content}</Typography>
                    <Rating value={comment.rating || 0} readOnly />
                  </>
                }
                secondary={`By ${comment.customer && comment.customer.username ? comment.customer.username : 'Deleted Account'}`}
              />
            </ListItem>
            ))
          ) : (
            <Typography>No comments yet.</Typography>
          )}

          {totalPages > 1 && (
            <Stack spacing={2} className={classes.pagination}>
              <Pagination
                count={totalPages}
                page={currentPage}
                onChange={handlePageChange}
                shape="rounded"
                color="primary"
                hidePrevButton={currentPage === 1}
                hideNextButton={currentPage === totalPages}
              />
          </Stack>
          )}
        </List>


        {isAuthenticated && (
          <Box className={classes.enterComment}>
            <TextField
              label="Add a comment"
              variant="outlined"
              fullWidth
              multiline
              rows={4}
              value={newComment.content}
              onChange={
                (e) => {
                  setNewCommentError('');
                  setNewComment(e.target.value);
                }
              }
              error={Boolean(commentError)}
              helperText={commentError}
            />
            <Box className={classes.enterRating}>
              <Rating
                name="new-comment-rating"
                value={newRating || 0}
                onChange={(event, newValue) => setNewRating(newValue)}
              />
            </Box>
            <Button
              variant="contained"
              color="primary"
              onClick={handleAddComment}
              className={classes.submitCommentButton}
            >
              Add Comment
            </Button>
          </Box>
        )}

      </Box>

    </ThemeProvider>
  );

};

export default CommentsList;