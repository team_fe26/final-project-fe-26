import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
    enterComment: {
        marginTop: '20px',
        '@media (max-width: 768px)': {
            paddingBottom: "25px"
        },
    },
    enterRating: {
        marginTop: '10px'
    },
    submitCommentButton: {
        marginTop: '10px'
    },
    CommentsList: {
        padding: 0
    },

    commentsContainer: {
        margin: "18px 15px 0 0",
        gridColumn: "span 2",
        "@media (max-width: 768px)": {
            margin: "18px 0 200px 25px",
        },
    },
    loader: {
        margin: "18px 15px 0 0",
        gridColumn: "span 2",
        "@media (max-width: 768px)": {
            margin: "18px 0 200px 25px",
        },
    }
})