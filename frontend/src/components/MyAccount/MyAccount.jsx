import { Styles } from "./styles";
import {
  Container,
  MenuList,
  Box,
  MenuItem,
  Typography,
  Button,
} from "@mui/material";
import { Navigate, NavLink, useNavigate } from "react-router-dom";
import ShoppingCartCheckoutIcon from "@mui/icons-material/ShoppingCartCheckout";
import PlaceIcon from "@mui/icons-material/Place";
import PermIdentityIcon from "@mui/icons-material/PermIdentity";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import SettingsIcon from "@mui/icons-material/Settings";
import { useLocation } from "react-router-dom";
const MyAccount = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const classes = Styles();
  return (
    <Container className={classes.container}>
      <Typography variant="body1" className={classes.sectionTitle}>
        My Account
      </Typography>
      <MenuList className={classes.myAccountList}>
        <MenuItem
          onClick={() => {
            navigate("/user");
          }}
          className={`${classes.myAccountItem} ${
            location.pathname === "/user" ? classes.activeLink : ""
          }`}
        >
          <NavLink to={`/user`} className={classes.myAccountLink}>
            <div className={classes.verticalLine} />
            <PermIdentityIcon className={classes.myAccountIcon} />
            Account Details
          </NavLink>
        </MenuItem>
        <MenuItem
          onClick={() => {
            navigate("/user/settings");
          }}
          className={`${classes.myAccountItem} ${
            location.pathname === "/user/settings" ? classes.activeLink : ""
          }`}
        >
          <NavLink to={`/user/settings`} className={classes.myAccountLink}>
            <div className={classes.verticalLine} />
            <SettingsIcon className={classes.myAccountIcon} />
            Settings
          </NavLink>
        </MenuItem>
        <MenuItem
          onClick={() => {
            navigate("/user/address");
          }}
          className={`${classes.myAccountItem} ${
            location.pathname === "/user/address" ? classes.activeLink : ""
          }`}
        >
          <NavLink to={`/user/address`} className={classes.myAccountLink}>
            <div className={classes.verticalLine} />
            <PlaceIcon className={classes.myAccountIcon} />
            Address
          </NavLink>
        </MenuItem>
        <MenuItem
          onClick={() => {
            navigate("/user/comments");
          }}
          className={`${classes.myAccountItem} ${
            location.pathname === "/user/comments" ? classes.activeLink : ""
          }`}
        >
          <NavLink href="/user/comments" className={classes.myAccountLink}>
            <div className={classes.verticalLine} />
            <AccessTimeIcon className={classes.myAccountIcon} />
            Comments History
          </NavLink>
        </MenuItem>
      </MenuList>
    </Container>
  );
};

export default MyAccount;
