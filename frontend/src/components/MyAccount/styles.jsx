import { createUseStyles } from "react-jss";

export const Styles = createUseStyles({
  container: {
    width: "400px",
    minWidth: "274px",
    textAlign: "center",
    backgroundColor: "#FBFBFB",
    padding: 0,
    paddingTop: "5px",
    height: "fit-content",
  },
  sectionTitle: {
    fontSize: "22px",
    fontWeight: "bold",
    marginBottom: "20px",
    color: "#727272",
    fontFamily: "'Monserrat', sans-serif",
    textAlign: "left",
    marginLeft: 20,
  },

  myAccountList: {
    fontFamily: "'Monserrat', sans-serif",
    color: "#727272",
    listStyleType: "none",
    padding: "0",
    margin: "0 auto",
    minWidth: "274px",
    width: "274px",
    textAlign: "left",
    lineHeight: "45px",
    fontSize: "15px",
    fontWeight: "400",
  },

  green: {
    borderTop: "1px solid rgba(70, 163, 88, 0.2)",
  },
  myAccountItem: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    padding: "0",
    fontSize: "18px",
    color: "#727272",
  },
  activeLink: {
    "& $myAccountLink, & $li.myAccountItem": {
      color: "#46A358",
      fontWeight: "700",
    },
    "& $myAccountIcon *": {
      fill: "#46A358",
    },
    "& $verticalLine": {
      backgroundColor: "#46A358",
    },
  },
  verticalLine: {
    padding: "10px 0",
    width: "5px",
    height: "40px",
    backgroundColor: "transparent",
    marginRight: "10px",
  },
  myAccountIcon: {
    marginRight: "10px",
    width: "24px",
    height: "24px",
    transition: "color 0.3s ease",
  },

  myAccountLink: {
    display: "flex",
    alignItems: "center",
    textDecoration: "none",
    color: "inherit",
    transition: "color 0.3s ease",
  },
  logout: {
    color: "#46A358",
  },
});
