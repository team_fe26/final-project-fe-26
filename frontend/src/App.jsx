import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home/Home";
import UniquePageOfProduct from "./pages/UniquePageOfProduct/UniquePageOfProduct";
import Cart from "./pages/Cart/Cart";
import Wishlist from "./pages/Wishlist/Wishlist";
import UserPageSettings from "./pages/UserPage/DeskVer/UserPageSettings";
import SignUpMobVersion from "./pages/Authorization/SignUp";
import LoginMobVersion from "./pages/Authorization/Login";
import Filters from "./pages/Filters/FiltersMobVersion";
import Payment from "./pages/Payment/payment/Payment";
import BillingAddressForm from "./pages/Payment/billing/BillingAddress";
import PaymentDeskVer from "./pages/Payment/PaymentDeskVer/PaymentDeskVer";
import Header from "./components/Home/Header/Header";
import Footer from "./components/Home/Footer/Footer";
import UserPageAddress from "./pages/UserPage/DeskVer/UserPageAddress";
import UserPagePersonalInformation from "./pages/UserPage/UserPagePersonalInformatiom";
import UserPageHistoryComments from "./pages/UserPage/DeskVer/UserPageHistoryComments";
import ContactUs from "./pages/ContactUs/ContactUs";
import NotFound from "./pages/NotFound/NotFound";
import UserHistoryComments from "./pages/UserPage/UserHistoryComments";
import PlantCare from "./pages/PantCare/PlantCare";
import { useSelector, useDispatch } from "react-redux";
import TokenHandler from "./components/Authorization/Parts/HandlerToken";
import TokenHandlerRegister from "./components/Authorization/Parts/HandlerTokenRegester";
import { useEffect, useState, useLayoutEffect } from "react";
import Loader from "./components/Loader/Loader";
import { setIsShowModal } from "./redux/authorization.slice/authorization.slice";
import UserPagePassword from "./pages/UserPage/UserPagePassword";
import UserProfile from "./pages/UserPage/DeskVer/UserProfile";
import UserAccount from "./pages/UserPage/UserAcc";
import { useLocation, useNavigate } from "react-router-dom";
import { fetchAllProducts } from "./redux/products.slice/products.slice";
import LocalStorage from "./components/localStorage/localStorage";
function App() {
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { allProducts } = useSelector((state) => state.products);
  const { loggedIn } = useSelector((state) => state.authorization);
  const { paymentItems } = useSelector((state) => state.payment);
  const existingPaths = [
    "/",
    "/wishlist",
    "/cart",
    "/checkout",
    "/contact-us",
    "/billing-address",
    "/filters",
    "/login",
    "/sign-up",
    "/user/settings",
    "/user/address",
    "/user",
    "/product",
    "/user/change-password",
    "/user/personal-information",
    "/user/comments",
    "/plant-care",
  ];
  const isPathValid = (path) => {
    return existingPaths.some((existingPaths) => {
      const regexPath = new RegExp(
        "^" + existingPaths.replace(/:\w+/g, "\\w+") + "$"
      );
      return regexPath.test(path);
    });
  };
  const isNotFoundPage = !isPathValid(location.pathname);
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  useLayoutEffect(() => {
    const protectedPaths = [
      "/user",
      "/user/address",
      "/user/settings",
      "/user/change-password",
      "/user/personal-information",
      "/user/comments",
    ];

    if (protectedPaths.includes(location.pathname)) {
      if (loggedIn === false) {
        if (isMobile) {
          navigate("/login");
        } else {
          navigate("/");
          dispatch(setIsShowModal(true));
        }
      }
    }
  }, [location, loggedIn, isMobile, navigate, dispatch]);
  useEffect(() => {
    dispatch(fetchAllProducts());
  }, [dispatch]);

  useEffect(() => {}, [allProducts]);
  if (allProducts.length <= 0) {
    return (
      <>
        <Loader></Loader>
      </>
    );
  }

  return (
    <>
      {!isMobile && !isNotFoundPage ? <Header /> : null}
      <LocalStorage></LocalStorage>
      <Routes>
        <Route path="/" element={<Home />} />
        {isMobile && (
          <>
            <Route path="/login" element={<LoginMobVersion />} />
            <Route path="/sign-up" element={<SignUpMobVersion />} />
            <Route path="/filters" element={<Filters />} />
            <Route path="/billing-address" element={<BillingAddressForm />} />
            <Route path="/user/comments" element={<UserHistoryComments />} />
            {paymentItems.length > 0 ? (
              <Route path="/checkout" element={<Payment />} />
            ) : null}
            {loggedIn ? (
              <>
                <Route
                  path="/user"
                  element={<UserAccount></UserAccount>}
                ></Route>
                <Route
                  path="/user/change-password"
                  element={<UserPagePassword></UserPagePassword>}
                ></Route>
                <Route
                  path="/user/personal-information"
                  element={
                    <UserPagePersonalInformation></UserPagePersonalInformation>
                  }
                ></Route>
              </>
            ) : null}
          </>
        )}
        <Route path="/product" element={<UniquePageOfProduct />} />
        <Route path="/token-handler" element={<TokenHandler />} />
        <Route
          path="/token-handler-registration"
          element={<TokenHandlerRegister />}
        />
        {paymentItems.length > 0 ? (
          <Route path="/checkout" element={<PaymentDeskVer />} />
        ) : null}
        <Route path="/cart" element={<Cart />} />
        <Route path="/wishlist" element={<Wishlist />} />
        <Route path="/user/settings" element={<UserPageSettings />} />
        <Route path="/plant-care" element={<PlantCare />} />
        {loggedIn ? (
          <>
            <Route path="/user" element={<UserProfile></UserProfile>}></Route>
            <Route path="/" element={<Home />} />
            <Route
              path="/user/address"
              element={<UserPageAddress></UserPageAddress>}
            ></Route>
            <Route
              path="/user/comments"
              element={<UserPageHistoryComments></UserPageHistoryComments>}
            ></Route>
          </>
        ) : null}
        <Route path="/contact-us" element={<ContactUs />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
      {!isMobile && !isNotFoundPage ? <Footer /> : null}
    </>
  );
}

export default App;
