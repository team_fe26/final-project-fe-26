import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  cartId: [],
  cartItems: [],
};

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addItemToCart: (state, action) => {
      const existingItem = state.cartId.find((item) => item.product === action.payload);
      if (existingItem) {
        existingItem.quantityCart += 1;
      } else {
        state.cartId.push({ 
          product: action.payload, 
          quantityCart: 1 
        });
      }
    },
    removeItemFromCart: (state, action) => {
      state.cartId = state.cartId.filter((item) => item.product !== action.payload);
    },
    updateItemQuantity: (state, action) => {
      const { productId, quantityCart } = action.payload;
      const item = state.cartId.find((item) => item.product === productId);
      if (item) {
        item.quantityCart = quantityCart;
      }
    },
    replaceCartItems: (state, action) =>{
      state.cartItems = action.payload
    },
    replaceCartFromServer: (state, action) =>{
      state.cartId = action.payload
    },
  },
});

export const { addItemToCart, removeItemFromCart, updateItemQuantity, replaceCartFromServer, replaceCartItems, setCoupon } = cartSlice.actions;
export default cartSlice.reducer;