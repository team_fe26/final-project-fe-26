import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  loggedIn: '',
  isShowModal: false,
  userId: '',
  isForgotPassword: false,
  goToCode: false,
  isCodeTrue: false,
  time: 0,
  intervalId: null,
  isShowModalConfirm: false,
};

const authorizationSlice = createSlice({
  name: "authorization",
  initialState,
  reducers: {
    setLogged: (state, action) => {
        state.loggedIn = action.payload;

    },
    setIsShowModal: (state, action) =>{
      state.isShowModal = action.payload
    },
    setUserId: (state, action) =>{
      state.userId = action.payload
    },
    setIsForgotPassword: (state, action) =>{
      state.isForgotPassword = action.payload
    },
    setGoToCode: (state, action) =>{
      state.goToCode = action.payload
    },
    setIsCodeTrue: (state, action) => {
      state.isCodeTrue = action.payload
    },
    setTime: (state, action) => {
      if (typeof action.payload === 'number') {
        state.time = action.payload;
      } 
    },
setIntervalId: (state, action) => {
  if (typeof action.payload === 'number' || action.payload === null) {
    state.intervalId = action.payload; 
  } 
},
setIsShowModalConfirm: (state, action) => {
  state.isShowModalConfirm = action.payload
}
},
});

export const {setLogged, setIsShowModal, setUserId, setIsForgotPassword,setGoToCode,setIsCodeTrue, setTime, setIntervalId, setIsShowModalConfirm} = authorizationSlice.actions

export default authorizationSlice.reducer
