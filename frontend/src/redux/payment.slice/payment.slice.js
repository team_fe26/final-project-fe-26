import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  address: {},
  paymentMethod: "",
  isShowModal: false,
  paymentItems: [],
  paymentId: [],
  subtotal: 0,
  total: 0,
  mainAddress: {},
  optionalAddress: {},
  editOptionalAddress: true,
  isPaymentSuccess: false
};
const paymentSlice = createSlice({
  name: "payment",
  initialState,
  reducers: {
    setAddress: (state, action) => {
      state.address = action.payload;
    },

    setIsShowModal: (state, action) => {
      state.isShowModal = action.payload;
    },
    setPaymentMethod: (state, action) => {
      state.paymentMethod = action.payload;
    },
    setPaymentItems: (state, action) => {
      state.paymentItems = action.payload;
    },
    setPaymentId: (state, action) => {
      state.paymentId = action.payload;
    },
    setSubtotal: (state, action) => {
      state.subtotal = action.payload;
    },
    setTotal: (state, action) => {
      state.total = action.payload;
    },
    setMainAddress: (state, action) => {
      state.mainAddress = action.payload;
    },
    setOptionalAddress: (state, action) => {
      state.optionalAddress = action.payload;
    },
    setEditOptionalAddress: (state, action) =>{
      state.editOptionalAddress = action.payload
    },
    setIsPaymentSuccess: (state, action) =>{
      state.isPaymentSuccess = action.payload
    }
  },
});

export const {
  setAddress,
  setPaymentMethod,
  setIsShowModal,
  setPaymentItems,
  setSubtotal,
  setTotal,
  setPaymentId,
  setMainAddress,
  setOptionalAddress,
  setEditOptionalAddress,
  setIsPaymentSuccess
} = paymentSlice.actions;
export default paymentSlice.reducer;
