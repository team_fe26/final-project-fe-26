import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
SIZES: []
};

export const fetchSizes = createAsyncThunk(
  'sizes/fetchSizes',
  async () => {
    const data = await fetch(`https://greenshop-server.onrender.com/api/sizes`);
    const responseData = await data.json();
    return responseData;
  }
);
const sizesSlice = createSlice({
  name: 'sizes',
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchSizes.pending, (state) => {
        state.status = 'pending';
      })
      .addCase(fetchSizes.rejected, (state) => {
        state.status = 'error';
      })
      .addCase(fetchSizes.fulfilled, (state, action) => {
        state.status = 'success';
        state.SIZES = action.payload;
      })
  }
});

export default sizesSlice.reducer;