import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  sizes: [],
  categories: [],
  rangePrice: [4.99, 866.99],
  sale: false,
  sortBy: "",
  filterProducts: [],
  searchField: "",
  emptyButFilter: false,
};

const filterSlice = createSlice({
  name: "filters",
  initialState,
  reducers: {
    setSize: (state, action) => {
      const newArray = [...state.sizes];
      const index = newArray.indexOf(action.payload);

      if (index !== -1) {
        newArray.splice(index, 1);
      } else {
        newArray.push(action.payload);
      }

      state.sizes = newArray;
    },
    addSize: (state, action) => {
      const newArray = [...state.sizes];
      const index = newArray.indexOf(action.payload);
      if (index !== -1) {
        return;
      } else {
        newArray.push(action.payload);
      }
      state.sizes = newArray;
    },
    deleteSize: (state, action) => {
      const newArray = [...state.sizes];
      const index = newArray.indexOf(action.payload);
      newArray.splice(index, 1);
      state.sizes = newArray;
    },
    setCategory: (state, action) => {
      const newArray = [...state.categories];
      const index = newArray.indexOf(action.payload);
      if (index !== -1) {
        newArray.splice(index, 1);
      } else {
        newArray.push(action.payload);
      }
      state.categories = newArray;
    },
    addCategory: (state, action) => {
      const newArray = [...state.categories];
      const index = newArray.indexOf(action.payload);
      if (index !== -1) {
        return;
      } else {
        newArray.push(action.payload);
      }
      state.categories = newArray;
    },
    deleteCategory: (state, action) => {
      const newArray = [...state.categories];
      const index = newArray.indexOf(action.payload);
      newArray.splice(index, 1);
      state.categories = newArray;
    },
    setRangePrice: (state, action) => {
      state.rangePrice = action.payload;
    },
    setMinPrice: (state, action) => {
      state.rangePrice[0] = Number(action.payload);
    },
    setMaxPrice: (state, action) => {
      state.rangePrice[1] = Number(action.payload);
    },
    setSale: (state, action) => {
state.sale = action.payload
    },
    setSortBy: (state, action) => {
      state.sortBy = action.payload;
    },
    setFilterProducts: (state, action) => {
      state.filterProducts = action.payload;
    },
    setSearchField: (state, action) => {
      state.searchField = action.payload;
    },
    setEmptyButFilter: (state, action) => {
      state.emptyButFilter = action.payload;
    },
  },
});

export const {
  setCategory,
  addCategory,
  deleteCategory,
  setSize,
  addSize,
  deleteSize,
  setRangePrice,
  setMinPrice,
  setMaxPrice,
  setSale,
  setSortBy,
  setFilterProducts,
  setSearchField,
  setEmptyButFilter,
} = filterSlice.actions;

export default filterSlice.reducer;
