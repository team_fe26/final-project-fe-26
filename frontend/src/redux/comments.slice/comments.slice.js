import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
  comments: [],
  status: "idle",
  error: null,
  currentPage: 1
};

export const fetchComments = createAsyncThunk(
  "comments/fetchComments",
  async ({productId, page }) => {
    const response = await fetch(
     `https://greenshop-server.onrender.com/api/comments/product/${productId}/page/${page}`
    );
    if (!response.ok) throw new Error("Failed to fetch comments");
    return response.json();
  }
);

export const addComment = createAsyncThunk(
  "comments/addComment",
  async ({ productId, content, rating }) => {
    const initialToken = localStorage.getItem("token");
    const token = initialToken.substring(1, initialToken.length - 1);
    const response = await fetch(
      `https://greenshop-server.onrender.com/api/comment/add`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({ productId, content, rating }),
      }
    );

    if (!response.ok) throw new Error("Failed to add comment");
    return response.json();
  }
);

const commentsSlice = createSlice({
  name: "comments",
  initialState,
  reducers: {
    setCurrentPage: (state, action) => {
      state.currentPage = action.payload;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchComments.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchComments.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.comments = action.payload;
      })
      .addCase(fetchComments.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      })
      .addCase(addComment.pending, (state) => {
        state.status = "loading";
      })
      .addCase(addComment.fulfilled, (state, action) => {
        state.status = "succeeded";
        const { newComment, customer } = action.payload;
        if(Array.isArray(state.comments)){
          state.comments.push({ ...newComment, customer });
        } else {
          state.comments = [action.payload.newComment];
        }
      })
      .addCase(addComment.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });
  },
});
export const { setCurrentPage } = commentsSlice.actions;

export default commentsSlice.reducer;
