import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  coupon: null,
  discount: 0,
};

const couponSlice = createSlice({
  name: 'coupon',
  initialState,
  reducers: {
    applyCoupon(state, action) {
      state.coupon = action.payload.code;
      state.discount = action.payload.discount;
    },
    setCoupon(state, action){
      state.coupon = action.payload
    },
    setDiscount(state, action){
      state.discount = action.payload
    },
    removeCoupon(state) {
      state.coupon = null;
      state.discount = 0;
    },
  },
});

export const { applyCoupon, removeCoupon, setDiscount, setCoupon } = couponSlice.actions;
export default couponSlice.reducer;