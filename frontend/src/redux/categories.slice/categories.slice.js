import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
CATEGORIES: []
};

export const fetchCategories = createAsyncThunk(
  'categories/fetchCategories',
  async () => {
    const data = await fetch(`https://greenshop-server.onrender.com/api/categories`);
    const responseData = await data.json();
    return responseData;
  }
);
const categoriesSlice = createSlice({
  name: 'categories',
  initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchCategories.pending, (state) => {
        state.status = 'pending';
      })
      .addCase(fetchCategories.rejected, (state) => {
        state.status = 'error';
      })
      .addCase(fetchCategories.fulfilled, (state, action) => {
        state.status = 'success';
        state.CATEGORIES = action.payload;
      })
  }
});

export default categoriesSlice.reducer;