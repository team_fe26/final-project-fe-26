import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  wishlistId: [],
  wishlistItems: [],
  status: null,
  error: null,
  isAuthenticated: false, 
};

const wishlistSlice = createSlice({
  name: 'wishlist',
  initialState,
  reducers: {
    addToWishlist: (state, action) => {
      if (!state.wishlistId.some(id => id === action.payload)) {
        state.wishlistId.push(action.payload);
      }
    },
    removeFromWishlist: (state, action) => {
      state.wishlistId = state.wishlistId.filter(id => id !== action.payload);
    },
    addToWishlistLocal: (state, action) => {
      state.wishlistId = action.payload;
    },
    removeFromWishlistLocal: (state, action) => {
      state.wishlistId = state.wishlistId.filter(id => id !== action.payload);
    },
    replaceWishlistFromServer: (state, action) =>{
      state.wishlistId = action.payload;
    },
    replaceWishlistItems: (state, action)=>{
      state.wishlistItems = action.payload
    }
  },
});

export const { addToWishlist, removeFromWishlist, addToWishlistLocal, removeFromWishlistLocal, replaceWishlistFromServer, replaceWishlistItems } = wishlistSlice.actions;
export const selectIsWishlistEmpty = (state) => state.wishlist.wishlistId.length === 0;
export default wishlistSlice.reducer;
