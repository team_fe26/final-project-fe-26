import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
  products: [],
  total: 0,
  currentPage: 1,
  perPage: 9,
  productsRandom: [],
  status: null,
  allProducts: []
};

export const fetchProducts = createAsyncThunk(
  'products/fetchProducts',
  async ({ page, perPage}) => {
    const data = await fetch(`https://greenshop-server.onrender.com/api/products?startPage=${page}&perPage=${perPage}`);
    const responseData = await data.json();
    return responseData;
  }
);
export const fetchAllProducts = createAsyncThunk(
  'allProducts/fetchAllProducts',
  async () => {
    const data = await fetch(`https://greenshop-server.onrender.com/api/products-all`);
    const responseData = await data.json();
    return responseData;
  }
);
export const fetchRandomProducts= createAsyncThunk(
  'randomProducts/fetchRandomProducts',
  async () => {
    const data = await fetch(`https://greenshop-server.onrender.com/api/products-random`);
    const responseData = await data.json();
    return responseData;
  }
);
const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    setCurrentPage: (state, action) => {
      state.currentPage = action.payload;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchProducts.pending, (state) => {
        state.status = 'pending';
      })
      .addCase(fetchProducts.rejected, (state) => {
        state.status = 'error';
      })
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.status = 'success';
        state.products = action.payload;
      })
      .addCase(fetchAllProducts.pending, (state) => {
        state.status = 'pending';
      })
      .addCase(fetchAllProducts.rejected, (state) => {
        state.status = 'error';
      })
      .addCase(fetchAllProducts.fulfilled, (state, action) => {
        state.status = 'success';
        state.allProducts = action.payload;
      })
      .addCase(fetchRandomProducts.pending, (state) => {
        state.status = 'pending';
      })
      .addCase(fetchRandomProducts.rejected, (state) => {
        state.status = 'error';
      })
      .addCase(fetchRandomProducts.fulfilled, (state, action) => {
        state.status = 'success';
        state.productsRandom = action.payload;
      });
  }
});

export const { setCurrentPage } = productsSlice.actions;

export default productsSlice.reducer;