import { configureStore } from '@reduxjs/toolkit';
import productsSlice from './products.slice/products.slice';
import authorizationSlice from './authorization.slice/authorization.slice';
import filterSlice from './filter.slice/filter.slice';
import paymentSlice from './payment.slice/payment.slice';
import cartSlice from './cart.slice/cart.slice';
import wishlistSlice from './wishlist.slice/wishlist.slice';
import commentsSlice from './comments.slice/comments.slice';
import productSlice from './product.slice/product.slice';
import couponSlice from './coupon.slice/coupon.slice';
import sliderSlice from './slider.slice/slider.slice';
import sizesSlice from './sizes.slice/sizes.slice';
import categoriesSlice from './categories.slice/categories.slice';
export const store = configureStore({
  reducer: {
    products: productsSlice,
    authorization: authorizationSlice,
    filters: filterSlice,
    payment: paymentSlice,
    cart: cartSlice,
    wishlist: wishlistSlice, 
    comments: commentsSlice,
    product: productSlice,
    coupon: couponSlice,
    slides: sliderSlice,
    sizes: sizesSlice,
    categories: categoriesSlice
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
});