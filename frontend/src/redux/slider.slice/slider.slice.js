import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
slides: [],
status: null
};

export const fetchSlides = createAsyncThunk(
  'slider/fetchSliders',
  async () => {
    const data = await fetch(`https://greenshop-server.onrender.com/api/slides`);
    const responseData = await data.json();
    
    return responseData;
  }
);
const slidesSlice = createSlice({
  name: 'slides',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchSlides.pending, (state) => {
        state.status = 'pending';
      })
      .addCase(fetchSlides.rejected, (state) => {
        state.status = 'error';
      })
      .addCase(fetchSlides.fulfilled, (state, action) => {
        state.status = 'success';
        state.slides = action.payload;
      })
  }
});

export default slidesSlice.reducer;