<h1 align="center" ="color: #46A358">Final Project GREENSHOP FE-26</h1>

<h2 style="color: #46A358">Test and Deploy</h2>
<ul>
    <li>Deploy backend on the platform <a href="https://onrender.com" target="_blank">onrender</a></li>
    <li>Deploy frontend on the platform <a href="https://netlify.com" target="_blank">netlify</a></li>
</ul>

<h2 style="color: #46A358">Name</h2>

<img src="https://gitlab.com/team_fe26/final-project-fe-26/raw/main/frontend/public/greenshop.png" alt="greenshop">


<h2 style="color: #46A358">Description</h2>
<p>Our project is a web store where you can purchase rare plants. This is a non-commercial project with a fake payment system. We’ve designed it to be user-friendly and highly focused on the customer experience. The site allows you to easily browse favorite items, add them to your cart, and complete your order. We have integrated popular payment methods such as PayPal and Google Pay.</p>
<p>Registration and login are straightforward, with the option to sign in using Google. Users can also leave comments and discuss products with others. Each product page features detailed descriptions of the plants and care instructions. Additionally, the user profile page allows you to view your purchase and comment history and add multiple delivery addresses.</p>

<h2 style="color: #46A358">Installation</h2>
<p>Main dependencies include:</p>
<ul>
    <li><strong>React and React DOM:</strong> react, react-dom</li>
    <li><strong>Routing:</strong> react-router-dom</li>
    <li><strong>Styling:</strong> @mui/material, @mui/icons-material, styled-components</li>
    <li><strong>State Management:</strong> @reduxjs/toolkit, react-redux</li>
    <li><strong>Form Handling and Validation:</strong> formik, yup</li>
    <li><strong>Testing:</strong> jest, @testing-library/react</li>
</ul>

<h2 style="color: #46A358">Authors and Acknowledgment</h2>
<p>All work is done by our team</p>
<p>Photos of plants and descriptions are taken from the store <a href="https://www.happyhouseplants.co.uk/" target="_blank">Happy House Plants</a></p>
<p>Loader is taken from CodePen <a href="https://codepen.io/jpanter/pen/PWWQXK" target="_blank">CodePen</a></p>

<h2 style="color: #46A358">Participants</h2>
<ul>
    <li><strong>Viktoriia Dmytriak:</strong>
        <ul>
            <li> Backend</li> 
            <li>DataBase</li> 
            <li>Authorization</li>
            <li>Filters</li> 
            <li>Header</li> 
            <li>Banner</li>
            <li>Footer</li>
            <li> User Page</li> 
            <li>Checkout</li> 
            <li>Deployment</li>
            <li>Wishlist</li>
        </ul>
    </li>
    <li><strong>Oleg Snigurets:</strong>
        <ul>
            <li>List of Products</li> 
            <li>Comments</li> 
        </ul> 
    </li>
    <li><strong>Katheryna Butkovskaya:</strong>
        <ul>
            <li> Coupon</li> 
            <li>Cart</li> 
            <li>Wishlist</li>
        </ul>
    </li>
    <li><strong>Alisa Bielenko:</strong> 
        <ul>
            <li>Contact Us</li> 
            <li>Modal Windows 
                <ul>
                    <li>Checkout</li> 
                    <li>User Page</li> 
                </ul>
            </li> 
            <li>Page Not Found</li>
        </ul>
    </li>
    <li><strong>Anna Kyriak:</strong>
        <ul>
            <li>Checkout</li> 
            <li>Testing</li> 
        </ul>
    </li>
    <li><strong>Alexey Bolotin:</strong> 
        <ul>
            <li>Fixing Styles of all components</li> 
            <li>Slider "You may be interested in"</li> 
            <li>Page of Product</li> 
            <li>Page "Plant Care"</li> 
        </ul>
    </li>
</ul>
